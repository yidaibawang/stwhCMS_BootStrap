﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_questionslist:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_questionslist : BaseModel
	{
		public stwh_questionslist()
		{}
		#region Model
		private int _stwh_qulid;
		private int _stwh_quid;
		private int _stwh_qulorder=1;
		private string _stwh_qulmiaoshu;
		private DateTime _stwh_quladdtime= DateTime.Now;
        private string _stwh_qutitle;
        private int _stwh_uiid;

        public int stwh_uiid
        {
            get { return _stwh_uiid; }
            set { _stwh_uiid = value; }
        }

        public string stwh_qutitle
        {
            get { return _stwh_qutitle; }
            set { _stwh_qutitle = value; }
        }

		/// <summary>
		/// 
		/// </summary>
		public int stwh_qulid
		{
			set{ _stwh_qulid=value;}
			get{return _stwh_qulid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_quid
		{
			set{ _stwh_quid=value;}
			get{return _stwh_quid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_qulorder
		{
			set{ _stwh_qulorder=value;}
			get{return _stwh_qulorder;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_qulmiaoshu
		{
			set{ _stwh_qulmiaoshu=value;}
			get{return _stwh_qulmiaoshu;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_quladdtime
		{
			set{ _stwh_quladdtime=value;}
			get{return _stwh_quladdtime;}
		}
		#endregion Model
	}
}


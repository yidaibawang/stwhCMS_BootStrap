﻿using System;
namespace stwh_Model
{
	/// <summary>
    /// Jquery中serializeArray方法序列化表格元素，返回 JSON 数据结构数据对应的实体类
	/// </summary>
	[Serializable]
    public partial class stwh_FormModel : BaseModel
	{
        public stwh_FormModel()
		{}
        private string _name;
        private string _value;

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
	}
}


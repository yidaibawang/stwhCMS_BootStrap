﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_fucntion:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_function : BaseModel
	{
        public stwh_function()
		{}
		#region Model
		private int _stwh_fid;
		private int _stwh_menuid;
        private int _stwh_rid;
        private int _stwh_fishow = 0;

        public int stwh_fishow
        {
            get { return _stwh_fishow; }
            set { _stwh_fishow = value; }
        }

        public int stwh_rid
        {
            get { return _stwh_rid; }
            set { _stwh_rid = value; }
        }
		private string _stwh_fname;
		private string _stwh_fico;
		private string _stwh_furl;
		private string _stwh_fremark;
        private int _stwh_forder;
        private string _stwh_idname;
        private string _stwh_fpath;

        public string stwh_fpath
        {
            get { return _stwh_fpath; }
            set { _stwh_fpath = value; }
        }

        public string stwh_idname
        {
            get { return _stwh_idname; }
            set { _stwh_idname = value; }
        }

        public int stwh_forder
        {
            get { return _stwh_forder; }
            set { _stwh_forder = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int stwh_fid
		{
			set{ _stwh_fid=value;}
			get{return _stwh_fid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_menuid
		{
			set{ _stwh_menuid=value;}
			get{return _stwh_menuid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_fname
		{
			set{ _stwh_fname=value;}
			get{return _stwh_fname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_fico
		{
			set{ _stwh_fico=value;}
			get{return _stwh_fico;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_furl
		{
			set{ _stwh_furl=value;}
			get{return _stwh_furl;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_fremark
		{
			set{ _stwh_fremark=value;}
			get{return _stwh_fremark;}
		}
		#endregion Model
	}
}


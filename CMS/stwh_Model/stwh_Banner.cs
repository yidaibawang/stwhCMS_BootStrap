﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_Banner:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_Banner : BaseModel
	{
		public stwh_Banner()
		{}
		#region Model
		private int _stwh_baid;
		private string _stwh_bafl;
		private string _stwh_baurl;
		private string _stwh_bams="上海舒同文化有限公司";
		private string _stwh_balink="#";
		private string _stwh_batarget="_self";
		private DateTime _stwh_baaddtime;
        private int _stwh_baorder;

        /// <summary>
        /// 序号
        /// </summary>
        public int stwh_baorder
        {
            get { return _stwh_baorder; }
            set { _stwh_baorder = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int stwh_baid
		{
			set{ _stwh_baid=value;}
			get{return _stwh_baid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_bafl
		{
			set{ _stwh_bafl=value;}
			get{return _stwh_bafl;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_baurl
		{
			set{ _stwh_baurl=value;}
			get{return _stwh_baurl;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_bams
		{
			set{ _stwh_bams=value;}
			get{return _stwh_bams;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_balink
		{
			set{ _stwh_balink=value;}
			get{return _stwh_balink;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_batarget
		{
			set{ _stwh_batarget=value;}
			get{return _stwh_batarget;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_baaddtime
		{
			set{ _stwh_baaddtime=value;}
			get{return _stwh_baaddtime;}
		}
		#endregion Model
	}
}


﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_artype:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_artype : BaseModel
	{
		public stwh_artype()
		{}
		#region Model
		private int _stwh_artid;
		private string _stwh_artname;
		private string _stwh_artdescription;
		private string _stwh_artimg;
		private int _stwh_artparentid;
        private int _stwh_artorder = 0;

        /// <summary>
        /// 
        /// </summary>
        public int stwh_artorder
        {
            get { return _stwh_artorder; }
            set { _stwh_artorder = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int stwh_artid
		{
			set{ _stwh_artid=value;}
			get{return _stwh_artid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_artname
		{
			set{ _stwh_artname=value;}
			get{return _stwh_artname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_artdescription
		{
			set{ _stwh_artdescription=value;}
			get{return _stwh_artdescription;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_artimg
		{
			set{ _stwh_artimg=value;}
			get{return _stwh_artimg;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_artparentid
		{
			set{ _stwh_artparentid=value;}
			get{return _stwh_artparentid;}
		}
		#endregion Model

	}
}


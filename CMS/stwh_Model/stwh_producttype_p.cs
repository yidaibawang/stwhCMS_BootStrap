﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_producttype_p:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_producttype_p : BaseModel
	{
		public stwh_producttype_p()
		{}
		#region Model
		private int _stwh_ptid;
		private string _stwh_ptptitle;
		private string _stwh_ptpname;
		private string _stwh_ptptype;
		private int _stwh_ptporder=0;
		/// <summary>
		/// 
		/// </summary>
		public int stwh_ptid
		{
			set{ _stwh_ptid=value;}
			get{return _stwh_ptid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_ptptitle
		{
			set{ _stwh_ptptitle=value;}
			get{return _stwh_ptptitle;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_ptpname
		{
			set{ _stwh_ptpname=value;}
			get{return _stwh_ptpname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_ptptype
		{
			set{ _stwh_ptptype=value;}
			get{return _stwh_ptptype;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_ptporder
		{
			set{ _stwh_ptporder=value;}
			get{return _stwh_ptporder;}
		}
		#endregion Model
        #region Model
        private int _stwh_ptpvid;
        private int _stwh_pid;
        private int _stwh_ptpid;
        private string _stwh_ptpvvalue;
        /// <summary>
        /// 
        /// </summary>
        public int stwh_ptpvid
        {
            set { _stwh_ptpvid = value; }
            get { return _stwh_ptpvid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_pid
        {
            set { _stwh_pid = value; }
            get { return _stwh_pid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_ptpid
        {
            set { _stwh_ptpid = value; }
            get { return _stwh_ptpid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_ptpvvalue
        {
            set { _stwh_ptpvvalue = value; }
            get { return _stwh_ptpvvalue; }
        }
        #endregion Model
        #region Model
        private string _stwh_ptname;
        private string _stwh_ptdescription;
        private string _stwh_ptdetails;
        private string _stwh_ptimg;
        private int _stwh_ptshowmenu = 0;
        private int _stwh_ptparentid = 0;
        private int _stwh_ptorder = 0;

        /// <summary>
        /// 
        /// </summary>
        public int stwh_ptorder
        {
            get { return _stwh_ptorder; }
            set { _stwh_ptorder = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_ptname
        {
            set { _stwh_ptname = value; }
            get { return _stwh_ptname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_ptdescription
        {
            set { _stwh_ptdescription = value; }
            get { return _stwh_ptdescription; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_ptdetails
        {
            set { _stwh_ptdetails = value; }
            get { return _stwh_ptdetails; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_ptimg
        {
            set { _stwh_ptimg = value; }
            get { return _stwh_ptimg; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_ptshowmenu
        {
            set { _stwh_ptshowmenu = value; }
            get { return _stwh_ptshowmenu; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_ptparentid
        {
            set { _stwh_ptparentid = value; }
            get { return _stwh_ptparentid; }
        }
        #endregion Model
	}
}


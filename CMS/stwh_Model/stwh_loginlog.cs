﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_loginlog:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_loginlog : BaseModel
	{
		public stwh_loginlog()
		{}
		#region Model
		private int _stwh_loid;
        private string _stwh_loname;

        public string stwh_loname
        {
            get { return _stwh_loname; }
            set { _stwh_loname = value; }
        }
		private string _stwh_lotime;
		private string _stwh_loplace;
		private string _stwh_loip;
		private string _stwh_lobrowse;
		private string _stwh_lodevice;
		private string _stwh_loremark;
		/// <summary>
		/// 
		/// </summary>
		public int stwh_loid
		{
			set{ _stwh_loid=value;}
			get{return _stwh_loid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_lotime
		{
			set{ _stwh_lotime=value;}
			get{return _stwh_lotime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_loplace
		{
			set{ _stwh_loplace=value;}
			get{return _stwh_loplace;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_loip
		{
			set{ _stwh_loip=value;}
			get{return _stwh_loip;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_lobrowse
		{
			set{ _stwh_lobrowse=value;}
			get{return _stwh_lobrowse;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_lodevice
		{
			set{ _stwh_lodevice=value;}
			get{return _stwh_lodevice;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_loremark
		{
			set{ _stwh_loremark=value;}
			get{return _stwh_loremark;}
		}
		#endregion Model
	}
}


﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_product:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_product : BaseModel
	{
		public stwh_product()
		{}
		#region Model
		private int _stwh_pid;
		private int _stwh_ptid;
		private int _stwh_pbid;
		private int _stwh_porder=1;
		private string _stwh_ptitlesimple;
		private string _stwh_ptitle="内容标题";
		private string _stwh_pimage="/stwhup/image/1.jpg";
		private string _stwh_pimagelist;
		private string _stwh_pcountry="中国";
		private string _stwh_pcompany="未知";
		private double _stwh_pprice;
		private DateTime _stwh_pcreation= DateTime.Now;
		private string _stwh_pdescription;
		private string _stwh_pcontent;
		private string _stwh_pbiaoqian;
		private int _stwh_pissh=0;
		private int _stwh_piszhiding=0;
		private int _stwh_ptuijian=0;
		private int _stwh_ptoutiao=0;
		private int _stwh_pgundong=0;
		private string _stwh_pseotitle;
		private string _stwh_psetokeywords;
		private string _stwh_psetodescription;
		private DateTime _stwh_paddtime= DateTime.Now;
        private int _stwh_psumcount = 999;

        public int stwh_psumcount
        {
            get { return _stwh_psumcount; }
            set { _stwh_psumcount = value; }
        }
		/// <summary>
		/// 
		/// </summary>
		public int stwh_pid
		{
			set{ _stwh_pid=value;}
			get{return _stwh_pid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_ptid
		{
			set{ _stwh_ptid=value;}
			get{return _stwh_ptid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_pbid
		{
			set{ _stwh_pbid=value;}
			get{return _stwh_pbid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_porder
		{
			set{ _stwh_porder=value;}
			get{return _stwh_porder;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_ptitlesimple
		{
			set{ _stwh_ptitlesimple=value;}
			get{return _stwh_ptitlesimple;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_ptitle
		{
			set{ _stwh_ptitle=value;}
			get{return _stwh_ptitle;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_pimage
		{
			set{ _stwh_pimage=value;}
			get{return _stwh_pimage;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_pimagelist
		{
			set{ _stwh_pimagelist=value;}
			get{return _stwh_pimagelist;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_pcountry
		{
			set{ _stwh_pcountry=value;}
			get{return _stwh_pcountry;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_pcompany
		{
			set{ _stwh_pcompany=value;}
			get{return _stwh_pcompany;}
		}
		/// <summary>
		/// 
		/// </summary>
        public double stwh_pprice
		{
			set{ _stwh_pprice=value;}
			get{return _stwh_pprice;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_pcreation
		{
			set{ _stwh_pcreation=value;}
			get{return _stwh_pcreation;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_pdescription
		{
			set{ _stwh_pdescription=value;}
			get{return _stwh_pdescription;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_pcontent
		{
			set{ _stwh_pcontent=value;}
			get{return _stwh_pcontent;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_pbiaoqian
		{
			set{ _stwh_pbiaoqian=value;}
			get{return _stwh_pbiaoqian;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_pissh
		{
			set{ _stwh_pissh=value;}
			get{return _stwh_pissh;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_piszhiding
		{
			set{ _stwh_piszhiding=value;}
			get{return _stwh_piszhiding;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_ptuijian
		{
			set{ _stwh_ptuijian=value;}
			get{return _stwh_ptuijian;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_ptoutiao
		{
			set{ _stwh_ptoutiao=value;}
			get{return _stwh_ptoutiao;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_pgundong
		{
			set{ _stwh_pgundong=value;}
			get{return _stwh_pgundong;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_pseotitle
		{
			set{ _stwh_pseotitle=value;}
			get{return _stwh_pseotitle;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_psetokeywords
		{
			set{ _stwh_psetokeywords=value;}
			get{return _stwh_psetokeywords;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_psetodescription
		{
			set{ _stwh_psetodescription=value;}
			get{return _stwh_psetodescription;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_paddtime
		{
			set{ _stwh_paddtime=value;}
			get{return _stwh_paddtime;}
		}
		#endregion Model
        #region Model
        private string _stwh_pbname;
        private string _stwh_pbimage;
        private string _stwh_pburl;
        private string _stwh_pbdescription;
        private int _stwh_pborder;
        /// <summary>
        /// 
        /// </summary>
        public string stwh_pbname
        {
            set { _stwh_pbname = value; }
            get { return _stwh_pbname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_pbimage
        {
            set { _stwh_pbimage = value; }
            get { return _stwh_pbimage; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_pburl
        {
            set { _stwh_pburl = value; }
            get { return _stwh_pburl; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_pbdescription
        {
            set { _stwh_pbdescription = value; }
            get { return _stwh_pbdescription; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_pborder
        {
            set { _stwh_pborder = value; }
            get { return _stwh_pborder; }
        }
        #endregion Model
        #region Model
        private string _stwh_ptname;
        private string _stwh_ptdescription;
        private string _stwh_ptdetails;
        private string _stwh_ptimg;
        private int _stwh_ptshowmenu = 0;
        private int _stwh_ptparentid = 0;
        private int _stwh_ptorder = 0;

        /// <summary>
        /// 
        /// </summary>
        public int stwh_ptorder
        {
            get { return _stwh_ptorder; }
            set { _stwh_ptorder = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_ptname
        {
            set { _stwh_ptname = value; }
            get { return _stwh_ptname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_ptdescription
        {
            set { _stwh_ptdescription = value; }
            get { return _stwh_ptdescription; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_ptdetails
        {
            set { _stwh_ptdetails = value; }
            get { return _stwh_ptdetails; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_ptimg
        {
            set { _stwh_ptimg = value; }
            get { return _stwh_ptimg; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_ptshowmenu
        {
            set { _stwh_ptshowmenu = value; }
            get { return _stwh_ptshowmenu; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_ptparentid
        {
            set { _stwh_ptparentid = value; }
            get { return _stwh_ptparentid; }
        }
        #endregion Model
	}
}


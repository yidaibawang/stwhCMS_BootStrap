﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_bidding:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_bidding : BaseModel
	{
		public stwh_bidding()
		{}
		#region Model
		private int _stwh_bdid;
		private string _stwh_bdimg;
		private string _stwh_bdnumber;
		private string _stwh_bdtitle;
		private string _stwh_bdjianjie;
		private string _stwh_bdcontent;
		private string _stwh_bdcompany;
		private string _stwh_bdlxr;
		private string _stwh_bdtel;
		private string _stwh_bdaddress;
		private string _stwh_bdyoubian;
		private DateTime _stwh_bdendtime;
		private int _stwh_bdstatus=1;
		private string _stwh_bdpath="";
		private int _stwh_bdorder;
		private DateTime _stwh_bdaddtime;
		/// <summary>
		/// 
		/// </summary>
		public int stwh_bdid
		{
			set{ _stwh_bdid=value;}
			get{return _stwh_bdid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_bdimg
		{
			set{ _stwh_bdimg=value;}
			get{return _stwh_bdimg;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_bdnumber
		{
			set{ _stwh_bdnumber=value;}
			get{return _stwh_bdnumber;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_bdtitle
		{
			set{ _stwh_bdtitle=value;}
			get{return _stwh_bdtitle;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_bdjianjie
		{
			set{ _stwh_bdjianjie=value;}
			get{return _stwh_bdjianjie;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_bdcontent
		{
			set{ _stwh_bdcontent=value;}
			get{return _stwh_bdcontent;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_bdcompany
		{
			set{ _stwh_bdcompany=value;}
			get{return _stwh_bdcompany;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_bdlxr
		{
			set{ _stwh_bdlxr=value;}
			get{return _stwh_bdlxr;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_bdtel
		{
			set{ _stwh_bdtel=value;}
			get{return _stwh_bdtel;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_bdaddress
		{
			set{ _stwh_bdaddress=value;}
			get{return _stwh_bdaddress;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_bdyoubian
		{
			set{ _stwh_bdyoubian=value;}
			get{return _stwh_bdyoubian;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_bdendtime
		{
			set{ _stwh_bdendtime=value;}
			get{return _stwh_bdendtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_bdstatus
		{
			set{ _stwh_bdstatus=value;}
			get{return _stwh_bdstatus;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_bdpath
		{
			set{ _stwh_bdpath=value;}
			get{return _stwh_bdpath;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_bdorder
		{
			set{ _stwh_bdorder=value;}
			get{return _stwh_bdorder;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_bdaddtime
		{
			set{ _stwh_bdaddtime=value;}
			get{return _stwh_bdaddtime;}
		}
		#endregion Model
	}
}


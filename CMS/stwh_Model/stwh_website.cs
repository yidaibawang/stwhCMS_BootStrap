﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stwh_Model
{
    [Serializable]
    public class stwh_website : BaseModel
    {
        public stwh_website()
        { }

        #region 属性
        private string _webchecktext = "上海舒同传播有限公司abcdefghijklmnopqrstuvwxyzABCDEFGHIJ文化KLMNOPQRSTUVWXYZ0123456789";
        private int _webfilesize = 1024;
        private string _webfiletype = "png,gif,jpg,bmp,ico";
        private string _webuploaddire = "/stwhUp/";
        private string _webzcname = "上海舒同文化传播有限公司";
        private string _webcopyright = "Copyright 2015 ofall.cn All Rights Reserved.";
        private string _webcopyright_m = "Copyright 2015 ofall.cn All Rights Reserved.";
        private string _webcopyright_s = "Copyright 2015 ofall.cn All Rights Reserved.";
        private int _webbtnsizeH = 30;
        private int _webbtnsizeW = 30;
        private string _webbtncolor = "60636D";
        private int _webbtnborder = 1;
        private int _webbtnradius = 5;
        private int _webbtnbottom = 150;
        private int _webbtnright = 100;
        private string _webbtnbackground = "333640";
        private string _websytk = "";
        private string _webystk = "";
        private string _webico = "/Upload/image/favicon.ico";
        private string _webico_m = "/Upload/image/favicon.ico";
        private string _webico_s = "/Upload/image/favicon.ico";
        private string _seokey = "cms,cms系统,asp,asp系统,系统,网站建设,维护,网站UI设计,手机app";
        private string _seokey_m = "cms,cms系统,asp,asp系统,系统,网站建设,维护,网站UI设计,手机app";
        private string _seokey_s = "cms,cms系统,asp,asp系统,系统,网站建设,维护,网站UI设计,手机app";
        private string _seodescription = "上海舒同文化传播有限公司是CMS行业最流行的网站建设解决方案提供商之一。专业提供电子商务网站建设、在线考试系统网站建设、企业网站建设、教育及行业门户网站建设解决方案等于一体的高新技术企业。";
        private string _seodescription_m = "上海舒同文化传播有限公司是CMS行业最流行的网站建设解决方案提供商之一。专业提供电子商务网站建设、在线考试系统网站建设、企业网站建设、教育及行业门户网站建设解决方案等于一体的高新技术企业。";
        private string _seodescription_s = "上海舒同文化传播有限公司是CMS行业最流行的网站建设解决方案提供商之一。专业提供电子商务网站建设、在线考试系统网站建设、企业网站建设、教育及行业门户网站建设解决方案等于一体的高新技术企业。";
        private int _emailport = 25;
        private string _emailsmtp = "smtp.live.com";
        private string _emailsend = "a474197200@hotmail.com";
        private string _emailname = "上海舒同文化传播有限公司";
        private string _emailuser = "a474197200@hotmail.com";
        private string _emailpwd = "DE364517950990C0E0CF8B44912806F714DEA4972B7F6FD3";
        private string _emailoldpwd = "DE364517950990C0E0CF8B44912806F714DEA4972B7F6FD3";
        private string _webname = "上海舒同文化传播有限公司";
        private string _webname_m = "上海舒同文化传播有限公司";
        private string _webname_s = "上海舒同文化传播有限公司";
        private string _weblogo = "";
        private string _weblogo_m = "";
        private string _weblogo_s = "";
        private string _webtemplate = "default";
        private string _webtemplate_m = "default";
        private string _webtemplate_s = "default";
        private string _WebKey;
        private string _WebKeyTime;
        private int _webmaintain = 0;
        private int _webmaintain_m = 1;
        private int _webmaintain_s = 1;
        private string _webversion = "v2.6.5";
        private int _weblanguage = 0;
        private int _webcheck = 0;
        private int _weblogs = 0;
        private int _webthumbnail = 614400;
        private int _webid = 0;

        /// <summary>
        /// 商城网站描述
        /// </summary>
        public string Seodescription_s
        {
            get { return _seodescription_s; }
            set { _seodescription_s = value; }
        }

        /// <summary>
        /// 移动网站描述
        /// </summary>
        public string Seodescription_m
        {
            get { return _seodescription_m; }
            set { _seodescription_m = value; }
        }

        /// <summary>
        /// 商城网站关键词
        /// </summary>
        public string Seokey_s
        {
            get { return _seokey_s; }
            set { _seokey_s = value; }
        }

        /// <summary>
        /// 移动网站关键词
        /// </summary>
        public string Seokey_m
        {
            get { return _seokey_m; }
            set { _seokey_m = value; }
        }

        /// <summary>
        /// 商城网站版权
        /// </summary>
        public string Webcopyright_s
        {
            get { return _webcopyright_s; }
            set { _webcopyright_s = value; }
        }

        /// <summary>
        /// 移动网站版权
        /// </summary>
        public string Webcopyright_m
        {
            get { return _webcopyright_m; }
            set { _webcopyright_m = value; }
        }

        /// <summary>
        /// 商城网站名称
        /// </summary>
        public string Webname_s
        {
            get { return _webname_s; }
            set { _webname_s = value; }
        }

        /// <summary>
        /// 移动网站名称
        /// </summary>
        public string Webname_m
        {
            get { return _webname_m; }
            set { _webname_m = value; }
        }

        /// <summary>
        /// 商城网站logo
        /// </summary>
        public string Weblogo_s
        {
            get { return _weblogo_s; }
            set { _weblogo_s = value; }
        }

        /// <summary>
        /// 移动网站logo
        /// </summary>
        public string Weblogo_m
        {
            get { return _weblogo_m; }
            set { _weblogo_m = value; }
        }

        /// <summary>
        /// 商城网站ico
        /// </summary>
        public string Webico_s
        {
            get { return _webico_s; }
            set { _webico_s = value; }
        }

        /// <summary>
        /// 移动网站ico
        /// </summary>
        public string Webico_m
        {
            get { return _webico_m; }
            set { _webico_m = value; }
        }

        /// <summary>
        /// 网站ID
        /// </summary>
        public int Webid
        {
            get { return _webid; }
            set { _webid = value; }
        }

        /// <summary>
        /// 商城网站模板
        /// </summary>
        public string Webtemplate_s
        {
            get { return _webtemplate_s; }
            set { _webtemplate_s = value; }
        }

        /// <summary>
        /// 程序版权信息
        /// </summary>
        public string Webversion
        {
            get { return _webversion; }
            set { _webversion = value; }
        }

        /// <summary>
        /// 是否开启商城网站维护（默认0关闭，1开启）
        /// </summary>
        public int Webmaintain_s
        {
            get { return _webmaintain_s; }
            set { _webmaintain_s = value; }
        }

        /// <summary>
        /// 手机网站是否在维护（0正常，1维护）
        /// </summary>
        public int Webmaintain_m
        {
            get { return _webmaintain_m; }
            set { _webmaintain_m = value; }
        }

        /// <summary>
        /// 网站是否在维护（0正常，1维护）
        /// </summary>
        public int Webmaintain
        {
            get { return _webmaintain; }
            set { _webmaintain = value; }
        }

        /// <summary>
        /// 系统key有效时间（不可用）
        /// </summary>
        public string WebKeyTime
        {
            get { return _WebKeyTime; }
            set { _WebKeyTime = value; }
        }

        /// <summary>
        /// 系统key（不可用）
        /// </summary>
        public string WebKey
        {
            get { return _WebKey; }
            set { _WebKey = value; }
        }

        /// <summary>
        /// 网站模板
        /// </summary>
        public string Webtemplate
        {
            get { return _webtemplate; }
            set { _webtemplate = value; }
        }

        /// <summary>
        /// 手机网站模板
        /// </summary>
        public string Webtemplate_m
        {
            get { return _webtemplate_m; }
            set { _webtemplate_m = value; }
        }

        /// <summary>
        /// 网站logo
        /// </summary>
        public string Weblogo
        {
            get { return _weblogo; }
            set { _weblogo = value; }
        }
        /// <summary>
        /// 是否开启操作日志记录（0开启，1关闭）（不可用）
        /// </summary>
        public int Weblogs
        {
            get { return _weblogs; }
            set { _weblogs = value; }
        }

        /// <summary>
        /// 验证码字符串数据源（生成验证码时，通过随机获取该数据源中的字符进行生成）（不可用）
        /// </summary>
        public string Webchecktext
        {
            get { return _webchecktext; }
            set { _webchecktext = value; }
        }
        /// <summary>
        /// 允许上传缩略图文件大小限制（单位字节，默认600KB）
        /// </summary>
        public int Webthumbnail
        {
            get { return _webthumbnail; }
            set { _webthumbnail = value; }
        }

        /// <summary>
        /// 邮箱旧密码（不可用）
        /// </summary>
        public string Emailoldpwd
        {
            get { return _emailoldpwd; }
            set { _emailoldpwd = value; }
        }
        /// <summary>
        /// smtp登录密码
        /// </summary>
        public string Emailpwd
        {
            get { return _emailpwd; }
            set { _emailpwd = value; }
        }
        /// <summary>
        /// smtp登录用户
        /// </summary>
        public string Emailuser
        {
            get { return _emailuser; }
            set { _emailuser = value; }
        }
        /// <summary>
        /// 显示名称
        /// </summary>
        public string Emailname
        {
            get { return _emailname; }
            set { _emailname = value; }
        }
        /// <summary>
        /// 发件人邮箱
        /// </summary>
        public string Emailsend
        {
            get { return _emailsend; }
            set { _emailsend = value; }
        }
        /// <summary>
        /// smtp端口
        /// </summary>
        public int Emailport
        {
            get { return _emailport; }
            set { _emailport = value; }
        }
        /// <summary>
        /// smtp服务器
        /// </summary>
        public string Emailsmtp
        {
            get { return _emailsmtp; }
            set { _emailsmtp = value; }
        }
        /// <summary>
        /// 网站全局seo描述信息
        /// </summary>
        public string Seodescription
        {
            get { return _seodescription; }
            set { _seodescription = value; }
        }
        /// <summary>
        /// 网站全局seo关键词
        /// </summary>
        public string Seokey
        {
            get { return _seokey; }
            set { _seokey = value; }
        }
        /// <summary>
        /// 网站图标
        /// </summary>
        public string Webico
        {
            get { return _webico; }
            set { _webico = value; }
        }
        /// <summary>
        /// 网站隐私条款
        /// </summary>
        public string Webystk
        {
            get { return _webystk; }
            set { _webystk = value; }
        }
        /// <summary>
        /// 网站使用条款
        /// </summary>
        public string Websytk
        {
            get { return _websytk; }
            set { _websytk = value; }
        }
        /// <summary>
        /// 返回按钮背景颜色（不可用）
        /// </summary>
        public string Webbtnbackground
        {
            get { return _webbtnbackground; }
            set { _webbtnbackground = value; }
        }
        /// <summary>
        /// 返回按钮距离浏览器右边位置（不可用）
        /// </summary>
        public int Webbtnright
        {
            get { return _webbtnright; }
            set { _webbtnright = value; }
        }
        /// <summary>
        /// 返回按钮距离浏览器底部位置（不可用）
        /// </summary>
        public int Webbtnbottom
        {
            get { return _webbtnbottom; }
            set { _webbtnbottom = value; }
        }
        /// <summary>
        /// 返回按钮圆角大小（不可用）
        /// </summary>
        public int Webbtnradius
        {
            get { return _webbtnradius; }
            set { _webbtnradius = value; }
        }
        /// <summary>
        /// 返回按钮边框大小（不可用）
        /// </summary>
        public int Webbtnborder
        {
            get { return _webbtnborder; }
            set { _webbtnborder = value; }
        }
        /// <summary>
        /// 返回按钮字体颜色（不可用）
        /// </summary>
        public string Webbtncolor
        {
            get { return _webbtncolor; }
            set { _webbtncolor = value; }
        }
        /// <summary>
        /// 返回按钮宽度（不可用）
        /// </summary>
        public int WebbtnsizeW
        {
            get { return _webbtnsizeW; }
            set { _webbtnsizeW = value; }
        }
        /// <summary>
        /// 返回按钮高度（不可用）
        /// </summary>
        public int WebbtnsizeH
        {
            get { return _webbtnsizeH; }
            set { _webbtnsizeH = value; }
        }
        /// <summary>
        /// 网站版权信息
        /// </summary>
        public string Webcopyright
        {
            get { return _webcopyright; }
            set { _webcopyright = value; }
        }
        /// <summary>
        /// 站长名称（不可用）
        /// </summary>
        public string Webzcname
        {
            get { return _webzcname; }
            set { _webzcname = value; }
        }
        /// <summary>
        /// 文件上传目录，默认（/stwhUp/）（不可用）
        /// </summary>
        public string WebUploadDire
        {
            get { return _webuploaddire; }
            set { _webuploaddire = value; }
        }
        /// <summary>
        /// 允许上传文件类型
        /// </summary>
        public string Webfiletype
        {
            get { return _webfiletype; }
            set { _webfiletype = value; }
        }
        /// <summary>
        /// 允许上传文件大小限制（单位KB，默认1KB）
        /// </summary>
        public int Webfilesize
        {
            get { return _webfilesize; }
            set { _webfilesize = value; }
        }
        /// <summary>
        /// 是否启用验证（0启用，1未启用）（不可用）
        /// </summary>
        public int Webcheck
        {
            get { return _webcheck; }
            set { _webcheck = value; }
        }
        /// <summary>
        /// 语言（0中文，1英文，等）（不可用）
        /// </summary>
        public int Weblanguage
        {
            get { return _weblanguage; }
            set { _weblanguage = value; }
        }
        /// <summary>
        /// 网站名称
        /// </summary>
        public string Webname
        {
            get { return _webname; }
            set { _webname = value; }
        }
        #endregion
    }
}

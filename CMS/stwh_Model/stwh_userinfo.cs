﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_userinfo:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_userinfo : BaseModel
	{
		public stwh_userinfo()
		{}
		#region Model
		private int _stwh_uiid;
		private int _stwh_rid;
		private string _stwh_uiname;
		private string _stwh_uilname;
		private string _stwh_uipwd;
		private int _stwh_uistatus=0;
		private DateTime _stwh_uictime= DateTime.Now;
		private string _stwh_uiportrait="/Upload/image/noimg.jpg";
		private string _stwh_uidescription="暂无描述";
		/// <summary>
		/// 
		/// </summary>
		public int stwh_uiid
		{
			set{ _stwh_uiid=value;}
			get{return _stwh_uiid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_rid
		{
			set{ _stwh_rid=value;}
			get{return _stwh_rid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_uiname
		{
			set{ _stwh_uiname=value;}
			get{return _stwh_uiname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_uilname
		{
			set{ _stwh_uilname=value;}
			get{return _stwh_uilname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_uipwd
		{
			set{ _stwh_uipwd=value;}
			get{return _stwh_uipwd;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_uistatus
		{
			set{ _stwh_uistatus=value;}
			get{return _stwh_uistatus;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_uictime
		{
			set{ _stwh_uictime=value;}
			get{return _stwh_uictime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_uiportrait
		{
			set{ _stwh_uiportrait=value;}
			get{return _stwh_uiportrait;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_uidescription
		{
			set{ _stwh_uidescription=value;}
			get{return _stwh_uidescription;}
		}
		#endregion Model

        #region Model
        private string _stwh_rname;
        private string _stwh_rdescription = "暂无描述";
        private DateTime _stwh_rctime = DateTime.Now;
        private int _stwh_rstate = 0;
        private int _stwh_rdelstate = 0;
        /// <summary>
        /// 
        /// </summary>
        public string stwh_rname
        {
            set { _stwh_rname = value; }
            get { return _stwh_rname; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_rdescription
        {
            set { _stwh_rdescription = value; }
            get { return _stwh_rdescription; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime stwh_rctime
        {
            set { _stwh_rctime = value; }
            get { return _stwh_rctime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_rstate
        {
            set { _stwh_rstate = value; }
            get { return _stwh_rstate; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_rdelstate
        {
            set { _stwh_rdelstate = value; }
            get { return _stwh_rdelstate; }
        }
        #endregion Model
	}
}


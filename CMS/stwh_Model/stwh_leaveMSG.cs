﻿using System;
namespace stwh_Model
{
	/// <summary>
	/// stwh_leaveMSG:实体类(属性说明自动提取数据库字段的描述信息)
	/// </summary>
	[Serializable]
    public partial class stwh_leaveMSG : BaseModel
	{
		public stwh_leaveMSG()
		{}
		#region Model
		private int _stwh_lmid;
		private string _stwh_lmnc;
		private string _stwh_lmname;
		private int _stwh_lmage;
		private int _stwh_lmsex;
		private string _stwh_lmsj;
		private string _stwh_lmemail;
		private string _stwh_lmsubject;
		private string _stwh_lmlynr;
		private DateTime _stwh_lmaddtime;
		private int _stwh_lmissh=1;
		/// <summary>
		/// 
		/// </summary>
		public int stwh_lmid
		{
			set{ _stwh_lmid=value;}
			get{return _stwh_lmid;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_lmnc
		{
			set{ _stwh_lmnc=value;}
			get{return _stwh_lmnc;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_lmname
		{
			set{ _stwh_lmname=value;}
			get{return _stwh_lmname;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_lmage
		{
			set{ _stwh_lmage=value;}
			get{return _stwh_lmage;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_lmsex
		{
			set{ _stwh_lmsex=value;}
			get{return _stwh_lmsex;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_lmsj
		{
			set{ _stwh_lmsj=value;}
			get{return _stwh_lmsj;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_lmemail
		{
			set{ _stwh_lmemail=value;}
			get{return _stwh_lmemail;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_lmsubject
		{
			set{ _stwh_lmsubject=value;}
			get{return _stwh_lmsubject;}
		}
		/// <summary>
		/// 
		/// </summary>
		public string stwh_lmlynr
		{
			set{ _stwh_lmlynr=value;}
			get{return _stwh_lmlynr;}
		}
		/// <summary>
		/// 
		/// </summary>
		public DateTime stwh_lmaddtime
		{
			set{ _stwh_lmaddtime=value;}
			get{return _stwh_lmaddtime;}
		}
		/// <summary>
		/// 
		/// </summary>
		public int stwh_lmissh
		{
			set{ _stwh_lmissh=value;}
			get{return _stwh_lmissh;}
		}
		#endregion Model

	}
}


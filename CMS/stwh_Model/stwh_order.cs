﻿using System;
namespace stwh_Model
{
    /// <summary>
    /// stwh_order:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class stwh_order : BaseModel
    {
        public stwh_order()
        { }
        #region Model
        private int _stwh_orid;
        private int _stwh_buid;
        private string _stwh_orddid;
        private DateTime _stwh_ortime;
        private int _stwh_orstatus = 0;
        private int _stwh_orpaystyle = 0;
        private string _stwh_oryjstyle;
        private string _stwh_oryjdd;
        private string _stwh_orremark;
        private string _stwh_bumobile;
        private string _stwh_oraddress;
        private string _pidAndpcount;
        private string _stwh_oruser;
        private string _stwh_ortel;
        private string _stwh_ortel_sub;
        private int _stwh_orcity;

        public int stwh_orcity
        {
            get { return _stwh_orcity; }
            set { _stwh_orcity = value; }
        }

        /// <summary>
        /// 非数据库字段，联系电话（隐藏中间字符）
        /// </summary>
        public string stwh_ortel_sub
        {
            get { return _stwh_ortel_sub; }
            set { _stwh_ortel_sub = value; }
        }
        private string _stwh_oryoubian;

        public string stwh_oruser
        {
            get { return _stwh_oruser; }
            set { _stwh_oruser = value; }
        }

        public string stwh_ortel
        {
            get { return _stwh_ortel; }
            set { _stwh_ortel = value; }
        }

        public string stwh_oryoubian
        {
            get { return _stwh_oryoubian; }
            set { _stwh_oryoubian = value; }
        }

        /// <summary>
        /// 非数据库字段，商品id集合和商品购买数量（格式：1_2-2_1-3_1）
        /// </summary>
        public string pidAndpcount
        {
            get { return _pidAndpcount; }
            set { _pidAndpcount = value; }
        }

        public string stwh_oraddress
        {
            get { return _stwh_oraddress; }
            set { _stwh_oraddress = value; }
        }

        public string stwh_bumobile
        {
            get { return _stwh_bumobile; }
            set { _stwh_bumobile = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_orid
        {
            set { _stwh_orid = value; }
            get { return _stwh_orid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_buid
        {
            set { _stwh_buid = value; }
            get { return _stwh_buid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_orddid
        {
            set { _stwh_orddid = value.Trim(); }
            get { return _stwh_orddid; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime stwh_ortime
        {
            set { _stwh_ortime = value; }
            get { return _stwh_ortime; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_orstatus
        {
            set { _stwh_orstatus = value; }
            get { return _stwh_orstatus; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int stwh_orpaystyle
        {
            set { _stwh_orpaystyle = value; }
            get { return _stwh_orpaystyle; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_oryjstyle
        {
            set { _stwh_oryjstyle = value; }
            get { return _stwh_oryjstyle; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_oryjdd
        {
            set { _stwh_oryjdd = value; }
            get { return _stwh_oryjdd; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string stwh_orremark
        {
            set { _stwh_orremark = value; }
            get { return _stwh_orremark; }
        }
        #endregion Model
    }
}


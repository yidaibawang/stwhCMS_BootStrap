﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_producttype
    /// </summary>
    public partial class stwh_producttypeDAL : BaseDAL, Istwh_producttypeDAL
    {
        public stwh_producttypeDAL()
        { }
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_producttype");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_producttype", "stwh_ptid", FieldColumn, FieldOrder, "stwh_ptid,stwh_ptname,stwh_ptdescription,stwh_ptdetails,stwh_ptimg,stwh_ptshowmenu,stwh_ptparentid,stwh_ptorder", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_producttype where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_producttype", "stwh_ptid", FieldColumn, FieldOrder, "stwh_ptid,stwh_ptname,stwh_ptdescription,stwh_ptdetails,stwh_ptimg,stwh_ptshowmenu,stwh_ptparentid,stwh_ptorder", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_ptid", "stwh_producttype");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_ptid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_producttype");
            strSql.Append(" where stwh_ptid=@stwh_ptid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ptid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_ptid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_producttype jbmodel = model as stwh_producttype;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("if (select count(1) from stwh_producttype where stwh_ptname = '" + jbmodel.stwh_ptname + "') = 0 begin ");
            strSql.Append("insert into stwh_producttype(");
            strSql.Append("stwh_ptname,stwh_ptdescription,stwh_ptdetails,stwh_ptimg,stwh_ptshowmenu,stwh_ptparentid,stwh_ptorder)");
            strSql.Append(" values (");
            strSql.Append("@stwh_ptname,@stwh_ptdescription,@stwh_ptdetails,@stwh_ptimg,@stwh_ptshowmenu,@stwh_ptparentid,@stwh_ptorder)");
            strSql.Append(";select @@IDENTITY");
            strSql.Append(" end");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ptname", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_ptdescription", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_ptdetails", SqlDbType.NText),
					new SqlParameter("@stwh_ptimg", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_ptshowmenu", SqlDbType.Int,4),
					new SqlParameter("@stwh_ptparentid", SqlDbType.Int,4),
                    new SqlParameter("@stwh_ptorder", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_ptname;
            parameters[1].Value = jbmodel.stwh_ptdescription;
            parameters[2].Value = jbmodel.stwh_ptdetails;
            parameters[3].Value = jbmodel.stwh_ptimg;
            parameters[4].Value = jbmodel.stwh_ptshowmenu;
            parameters[5].Value = jbmodel.stwh_ptparentid;
            parameters[6].Value = jbmodel.stwh_ptorder;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null) return 0;
            else return Convert.ToInt32(obj);
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_producttype jbmodel = model as stwh_producttype;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("if (select count(1) from stwh_producttype where stwh_ptname = '" + jbmodel.stwh_ptname + "' and stwh_ptid <> " + jbmodel.stwh_ptid + ") = 0 begin ");
            strSql.Append("update stwh_producttype set ");
            strSql.Append("stwh_ptname=@stwh_ptname,");
            strSql.Append("stwh_ptdescription=@stwh_ptdescription,");
            strSql.Append("stwh_ptdetails=@stwh_ptdetails,");
            strSql.Append("stwh_ptimg=@stwh_ptimg,");
            strSql.Append("stwh_ptshowmenu=@stwh_ptshowmenu,");
            strSql.Append("stwh_ptparentid=@stwh_ptparentid,");
            strSql.Append("stwh_ptorder=@stwh_ptorder");
            strSql.Append(" where stwh_ptid=@stwh_ptid");
            strSql.Append(" end");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ptname", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_ptdescription", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_ptdetails", SqlDbType.NText),
					new SqlParameter("@stwh_ptimg", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_ptshowmenu", SqlDbType.Int,4),
					new SqlParameter("@stwh_ptparentid", SqlDbType.Int,4),
                    new SqlParameter("@stwh_ptorder", SqlDbType.Int,4),
					new SqlParameter("@stwh_ptid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_ptname;
            parameters[1].Value = jbmodel.stwh_ptdescription;
            parameters[2].Value = jbmodel.stwh_ptdetails;
            parameters[3].Value = jbmodel.stwh_ptimg;
            parameters[4].Value = jbmodel.stwh_ptshowmenu;
            parameters[5].Value = jbmodel.stwh_ptparentid;
            parameters[6].Value = jbmodel.stwh_ptorder;
            parameters[7].Value = jbmodel.stwh_ptid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0) return true;
            else return false;
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_ptid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_producttype ");
            strSql.Append(" where stwh_ptid=@stwh_ptid or stwh_ptparentid = " + stwh_ptid);
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ptid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_ptid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0) return true;
            else return false;
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_ptidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_producttype ");
            strSql.Append(" where stwh_ptid in (" + stwh_ptidlist + ") or stwh_ptparentid in (" + stwh_ptidlist + ")");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0) return true;
            else return false;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_ptid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_ptid,stwh_ptname,stwh_ptdescription,stwh_ptdetails,stwh_ptimg,stwh_ptshowmenu,stwh_ptparentid,stwh_ptorder from stwh_producttype ");
            strSql.Append(" where stwh_ptid=@stwh_ptid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ptid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_ptid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0) return DataRowToModel(ds.Tables[0].Rows[0]);
            else return null;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_producttype jbmodel = new stwh_producttype();
            if (row != null)
            {
                if (row["stwh_ptid"] != null)
                {
                    jbmodel.stwh_ptid = int.Parse(row["stwh_ptid"].ToString());
                }
                if (row["stwh_ptname"] != null)
                {
                    jbmodel.stwh_ptname = row["stwh_ptname"].ToString();
                }
                if (row["stwh_ptdescription"] != null)
                {
                    jbmodel.stwh_ptdescription = row["stwh_ptdescription"].ToString();
                }
                if (row["stwh_ptdetails"] != null)
                {
                    jbmodel.stwh_ptdetails = row["stwh_ptdetails"].ToString();
                }
                if (row["stwh_ptimg"] != null)
                {
                    jbmodel.stwh_ptimg = row["stwh_ptimg"].ToString();
                }
                if (row["stwh_ptshowmenu"] != null)
                {
                    jbmodel.stwh_ptshowmenu = int.Parse(row["stwh_ptshowmenu"].ToString());
                }
                if (row["stwh_ptparentid"] != null)
                {
                    jbmodel.stwh_ptparentid = int.Parse(row["stwh_ptparentid"].ToString());
                }
                if (row["stwh_ptorder"] != null)
                {
                    jbmodel.stwh_ptorder = int.Parse(row["stwh_ptorder"].ToString());
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_ptid,stwh_ptname,stwh_ptdescription,stwh_ptdetails,stwh_ptimg,stwh_ptshowmenu,stwh_ptparentid,stwh_ptorder ");
            strSql.Append(" FROM stwh_producttype ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_ptid,stwh_ptname,stwh_ptdescription,stwh_ptdetails,stwh_ptimg,stwh_ptshowmenu,stwh_ptparentid,stwh_ptorder ");
            strSql.Append(" FROM stwh_producttype ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_producttype ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_ptid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_producttype T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}


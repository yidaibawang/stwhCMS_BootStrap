﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_loginlog
    /// </summary>
    public partial class stwh_loginlogDAL : BaseDAL, Istwh_loginlogDAL
    {
        public stwh_loginlogDAL()
        { }
        #region Istwh_loginlogDAL接口实现方法
        /// <summary>
        /// 删除全部数据
        /// </summary>
        public bool DeleteListALL()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_loginlog ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_loginlog");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_loginlog", "stwh_loid", FieldColumn, FieldOrder, "stwh_loid,stwh_loname,stwh_lotime,stwh_loplace,stwh_loip,stwh_lobrowse,stwh_lodevice,stwh_loremark", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_loginlog where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_loginlog", "stwh_loid", FieldColumn, FieldOrder, "stwh_loid,stwh_loname,stwh_lotime,stwh_loplace,stwh_loip,stwh_lobrowse,stwh_lodevice,stwh_loremark", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_loid", "stwh_loginlog");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_loid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_loginlog");
            strSql.Append(" where stwh_loid=@stwh_loid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_loid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_loid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_loginlog jbmodel = model as stwh_loginlog;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_loginlog(");
            strSql.Append("stwh_loname,stwh_lotime,stwh_loplace,stwh_loip,stwh_lobrowse,stwh_lodevice,stwh_loremark)");
            strSql.Append(" values (");
            strSql.Append("@stwh_loname,@stwh_lotime,@stwh_loplace,@stwh_loip,@stwh_lobrowse,@stwh_lodevice,@stwh_loremark)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_loname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_lotime", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_loplace", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_loip", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_lobrowse", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_lodevice", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_loremark", SqlDbType.NVarChar,300)};
            parameters[0].Value = jbmodel.stwh_loname;
            parameters[1].Value = jbmodel.stwh_lotime;
            parameters[2].Value = jbmodel.stwh_loplace;
            parameters[3].Value = jbmodel.stwh_loip;
            parameters[4].Value = jbmodel.stwh_lobrowse;
            parameters[5].Value = jbmodel.stwh_lodevice;
            parameters[6].Value = jbmodel.stwh_loremark;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_loginlog jbmodel = model as stwh_loginlog;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_loginlog set ");
            strSql.Append("stwh_loname=@stwh_loname,");
            strSql.Append("stwh_lotime=@stwh_lotime,");
            strSql.Append("stwh_loplace=@stwh_loplace,");
            strSql.Append("stwh_loip=@stwh_loip,");
            strSql.Append("stwh_lobrowse=@stwh_lobrowse,");
            strSql.Append("stwh_lodevice=@stwh_lodevice,");
            strSql.Append("stwh_loremark=@stwh_loremark");
            strSql.Append(" where stwh_loid=@stwh_loid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_loname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_lotime", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_loplace", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_loip", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_lobrowse", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_lodevice", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_loremark", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_loid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_loname;
            parameters[1].Value = jbmodel.stwh_lotime;
            parameters[2].Value = jbmodel.stwh_loplace;
            parameters[3].Value = jbmodel.stwh_loip;
            parameters[4].Value = jbmodel.stwh_lobrowse;
            parameters[5].Value = jbmodel.stwh_lodevice;
            parameters[6].Value = jbmodel.stwh_loremark;
            parameters[7].Value = jbmodel.stwh_loid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_loid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_loginlog ");
            strSql.Append(" where stwh_loid=@stwh_loid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_loid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_loid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_loidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_loginlog ");
            strSql.Append(" where stwh_loid in (" + stwh_loidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_loid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_loid,stwh_loname,stwh_lotime,stwh_loplace,stwh_loip,stwh_lobrowse,stwh_lodevice,stwh_loremark from stwh_loginlog ");
            strSql.Append(" where stwh_loid=@stwh_loid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_loid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_loid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_loginlog jbmodel = new stwh_loginlog();
            if (row != null)
            {
                if (row["stwh_loid"] != null)
                {
                    jbmodel.stwh_loid = int.Parse(row["stwh_loid"].ToString());
                }
                if (row["stwh_loname"] != null)
                {
                    jbmodel.stwh_loname = row["stwh_loname"].ToString();
                }
                if (row["stwh_lotime"] != null)
                {
                    jbmodel.stwh_lotime = row["stwh_lotime"].ToString();
                }
                if (row["stwh_loplace"] != null)
                {
                    jbmodel.stwh_loplace = row["stwh_loplace"].ToString();
                }
                if (row["stwh_loip"] != null)
                {
                    jbmodel.stwh_loip = row["stwh_loip"].ToString();
                }
                if (row["stwh_lobrowse"] != null)
                {
                    jbmodel.stwh_lobrowse = row["stwh_lobrowse"].ToString();
                }
                if (row["stwh_lodevice"] != null)
                {
                    jbmodel.stwh_lodevice = row["stwh_lodevice"].ToString();
                }
                if (row["stwh_loremark"] != null)
                {
                    jbmodel.stwh_loremark = row["stwh_loremark"].ToString();
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_loid,stwh_loname,stwh_lotime,stwh_loplace,stwh_loip,stwh_lobrowse,stwh_lodevice,stwh_loremark ");
            strSql.Append(" FROM stwh_loginlog ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_loid,stwh_loname,stwh_lotime,stwh_loplace,stwh_loip,stwh_lobrowse,stwh_lodevice,stwh_loremark ");
            strSql.Append(" FROM stwh_loginlog ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_loginlog ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_loid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_loginlog T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}


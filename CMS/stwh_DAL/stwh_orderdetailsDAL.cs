﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_orderdetails
    /// </summary>
    public partial class stwh_orderdetailsDAL : BaseDAL, Istwh_orderdetailsDAL
    {
        public stwh_orderdetailsDAL()
        { }
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_orderdetails");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_orderdetails", "stwh_orsid", FieldColumn, FieldOrder, "stwh_orsid,stwh_orddid,stwh_pid,stwh_orspname,stwh_orsprice,stwh_orscount,stwh_orspimage", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_orderdetails where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_orderdetails", "stwh_orsid", FieldColumn, FieldOrder, "stwh_orsid,stwh_orddid,stwh_pid,stwh_orspname,stwh_orsprice,stwh_orscount,stwh_orspimage", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_orsid", "stwh_orderdetails");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_orsid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_orderdetails");
            strSql.Append(" where stwh_orsid=@stwh_orsid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_orsid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_orsid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_orderdetails jbmodel = model as stwh_orderdetails;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_orderdetails(");
            strSql.Append("stwh_orddid,stwh_pid,stwh_orspname,stwh_orsprice,stwh_orscount,stwh_orspimage)");
            strSql.Append(" values (");
            strSql.Append("@stwh_orddid,@stwh_pid,@stwh_orspname,@stwh_orsprice,@stwh_orscount,@stwh_orspimage)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_orddid", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_pid", SqlDbType.Int,4),
					new SqlParameter("@stwh_orspname", SqlDbType.NVarChar,200),
					new SqlParameter("@stwh_orsprice", SqlDbType.Decimal,18),
					new SqlParameter("@stwh_orscount", SqlDbType.Int,4),
                    new SqlParameter("@stwh_orspimage", SqlDbType.NVarChar,500)};
            parameters[0].Value = jbmodel.stwh_orddid;
            parameters[1].Value = jbmodel.stwh_pid;
            parameters[2].Value = jbmodel.stwh_orspname;
            parameters[3].Value = jbmodel.stwh_orsprice;
            parameters[4].Value = jbmodel.stwh_orscount;
            parameters[5].Value = jbmodel.stwh_orspimage;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_orderdetails jbmodel = model as stwh_orderdetails;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_orderdetails set ");
            strSql.Append("stwh_orddid=@stwh_orddid,");
            strSql.Append("stwh_pid=@stwh_pid,");
            strSql.Append("stwh_orspname=@stwh_orspname,");
            strSql.Append("stwh_orsprice=@stwh_orsprice,");
            strSql.Append("stwh_orscount=@stwh_orscount,");
            strSql.Append("stwh_orspimage=@stwh_orspimage");
            strSql.Append(" where stwh_orsid=@stwh_orsid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_orddid", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_pid", SqlDbType.Int,4),
					new SqlParameter("@stwh_orspname", SqlDbType.NVarChar,200),
					new SqlParameter("@stwh_orsprice", SqlDbType.Decimal,18),
					new SqlParameter("@stwh_orscount", SqlDbType.Int,4),
                    new SqlParameter("@stwh_orspimage", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_orsid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_orddid;
            parameters[1].Value = jbmodel.stwh_pid;
            parameters[2].Value = jbmodel.stwh_orspname;
            parameters[3].Value = jbmodel.stwh_orsprice;
            parameters[4].Value = jbmodel.stwh_orscount;
            parameters[5].Value = jbmodel.stwh_orspimage;
            parameters[6].Value = jbmodel.stwh_orsid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_orsid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_orderdetails ");
            strSql.Append(" where stwh_orsid=@stwh_orsid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_orsid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_orsid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_orsidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_orderdetails ");
            strSql.Append(" where stwh_orsid in (" + stwh_orsidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_orsid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_orsid,stwh_orddid,stwh_pid,stwh_orspname,stwh_orsprice,stwh_orscount,stwh_orspimage from stwh_orderdetails ");
            strSql.Append(" where stwh_orsid=@stwh_orsid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_orsid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_orsid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_orderdetails jbmodel = new stwh_orderdetails();
            if (row != null)
            {
                if (row["stwh_orsid"] != null)
                {
                    jbmodel.stwh_orsid = int.Parse(row["stwh_orsid"].ToString());
                }
                if (row["stwh_orddid"] != null)
                {
                    jbmodel.stwh_orddid = row["stwh_orddid"].ToString();
                }
                if (row["stwh_pid"] != null)
                {
                    jbmodel.stwh_pid = int.Parse(row["stwh_pid"].ToString());
                }
                if (row["stwh_orspname"] != null)
                {
                    jbmodel.stwh_orspname = row["stwh_orspname"].ToString();
                }
                if (row["stwh_orspimage"] != null)
                {
                    jbmodel.stwh_orspimage = row["stwh_orspimage"].ToString();
                }
                if (row["stwh_orsprice"] != null)
                {
                    jbmodel.stwh_orsprice = decimal.Parse(row["stwh_orsprice"].ToString());
                }
                if (row["stwh_orscount"] != null)
                {
                    jbmodel.stwh_orscount = int.Parse(row["stwh_orscount"].ToString());
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_orsid,stwh_orddid,stwh_pid,stwh_orspname,stwh_orsprice,stwh_orscount,stwh_orspimage ");
            strSql.Append(" FROM stwh_orderdetails ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_orsid,stwh_orddid,stwh_pid,stwh_orspname,stwh_orsprice,stwh_orscount,stwh_orspimage ");
            strSql.Append(" FROM stwh_orderdetails ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_orderdetails ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_orsid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_orderdetails T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}


﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_Banner
    /// </summary>
    public partial class stwh_BannerDAL : BaseDAL, Istwh_BannerDAL
    {
        public stwh_BannerDAL()
        { }
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_Banner");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_Banner", "stwh_baid", FieldColumn, FieldOrder, "stwh_baid,stwh_baurl,stwh_bams,stwh_balink,stwh_batarget,stwh_baaddtime,stwh_baorder", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_Banner where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_Banner", "stwh_baid", FieldColumn, FieldOrder, "stwh_baid,stwh_baurl,stwh_bams,stwh_balink,stwh_batarget,stwh_baaddtime,stwh_baorder", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_baid", "stwh_Banner");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_baid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_Banner");
            strSql.Append(" where stwh_baid=@stwh_baid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_baid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_baid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_Banner jbmodel = model as stwh_Banner;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_Banner(");
            strSql.Append("stwh_baurl,stwh_bams,stwh_balink,stwh_batarget,stwh_baaddtime,stwh_baorder)");
            strSql.Append(" values (");
            strSql.Append("@stwh_baurl,@stwh_bams,@stwh_balink,@stwh_batarget,@stwh_baaddtime,@stwh_baorder)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_baurl", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_bams", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_balink", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_batarget", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_baaddtime", SqlDbType.DateTime),
                    new SqlParameter("@stwh_baorder", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_baurl;
            parameters[1].Value = jbmodel.stwh_bams;
            parameters[2].Value = jbmodel.stwh_balink;
            parameters[3].Value = jbmodel.stwh_batarget;
            parameters[4].Value = jbmodel.stwh_baaddtime;
            parameters[5].Value = jbmodel.stwh_baorder;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_Banner jbmodel = model as stwh_Banner;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_Banner set ");
            strSql.Append("stwh_baurl=@stwh_baurl,");
            strSql.Append("stwh_bams=@stwh_bams,");
            strSql.Append("stwh_balink=@stwh_balink,");
            strSql.Append("stwh_batarget=@stwh_batarget,");
            strSql.Append("stwh_baaddtime=@stwh_baaddtime,");
            strSql.Append("stwh_baorder=@stwh_baorder");
            strSql.Append(" where stwh_baid=@stwh_baid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_baurl", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_bams", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_balink", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_batarget", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_baaddtime", SqlDbType.DateTime),
                    new SqlParameter("@stwh_baorder", SqlDbType.Int,4),
					new SqlParameter("@stwh_baid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_baurl;
            parameters[1].Value = jbmodel.stwh_bams;
            parameters[2].Value = jbmodel.stwh_balink;
            parameters[3].Value = jbmodel.stwh_batarget;
            parameters[4].Value = jbmodel.stwh_baaddtime;
            parameters[5].Value = jbmodel.stwh_baorder;
            parameters[6].Value = jbmodel.stwh_baid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_baid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_Banner ");
            strSql.Append(" where stwh_baid=@stwh_baid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_baid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_baid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_baidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_Banner ");
            strSql.Append(" where stwh_baid in (" + stwh_baidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_baid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_baid,stwh_baurl,stwh_bams,stwh_balink,stwh_batarget,stwh_baaddtime,stwh_baorder from stwh_Banner ");
            strSql.Append(" where stwh_baid=@stwh_baid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_baid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_baid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_Banner model = new stwh_Banner();
            if (row != null)
            {
                if (row["stwh_baid"] != null)
                {
                    model.stwh_baid = int.Parse(row["stwh_baid"].ToString());
                }
                if (row["stwh_baurl"] != null)
                {
                    model.stwh_baurl = row["stwh_baurl"].ToString();
                }
                if (row["stwh_bams"] != null)
                {
                    model.stwh_bams = row["stwh_bams"].ToString();
                }
                if (row["stwh_balink"] != null)
                {
                    model.stwh_balink = row["stwh_balink"].ToString();
                }
                if (row["stwh_batarget"] != null)
                {
                    model.stwh_batarget = row["stwh_batarget"].ToString();
                }
                if (row["stwh_baaddtime"] != null)
                {
                    model.stwh_baaddtime = DateTime.Parse(row["stwh_baaddtime"].ToString());
                }
                if (row["stwh_baorder"] != null)
                {
                    model.stwh_baorder = int.Parse(row["stwh_baorder"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_baid,stwh_baurl,stwh_bams,stwh_balink,stwh_batarget,stwh_baaddtime,stwh_baorder ");
            strSql.Append(" FROM stwh_Banner ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_baid,stwh_baurl,stwh_bams,stwh_balink,stwh_batarget,stwh_baaddtime,stwh_baorder ");
            strSql.Append(" FROM stwh_Banner ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_Banner ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_baid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_Banner T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}


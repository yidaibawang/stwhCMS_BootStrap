﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_cjtel
    /// </summary>
    public partial class stwh_cjtelDAL : BaseDAL, Istwh_cjtelDAL
    {
        public stwh_cjtelDAL()
        { }
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_cjtel");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_cjtel", "stwh_cjid", FieldColumn, FieldOrder, "stwh_cjid,stwh_cjname,stwh_cjmobile,stwh_cjaddtime", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_cjtel where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_cjtel", "stwh_cjid", FieldColumn, FieldOrder, "stwh_cjid,stwh_cjname,stwh_cjmobile,stwh_cjaddtime", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_cjid", "stwh_cjtel");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_cjid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_cjtel");
            strSql.Append(" where stwh_cjid=@stwh_cjid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_cjid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_cjid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_cjtel jbmodel = model as stwh_cjtel;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("if (select count(1) from stwh_cjtel where stwh_cjmobile = '" + jbmodel.stwh_cjmobile + "') = 0 begin ");
            strSql.Append("insert into stwh_cjtel(");
            strSql.Append("stwh_cjname,stwh_cjmobile,stwh_cjaddtime)");
            strSql.Append(" values (");
            strSql.Append("@stwh_cjname,@stwh_cjmobile,@stwh_cjaddtime)");
            strSql.Append(";select @@IDENTITY");
            strSql.Append(" end");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_cjname", SqlDbType.NVarChar,800),
					new SqlParameter("@stwh_cjmobile", SqlDbType.NVarChar,800),
					new SqlParameter("@stwh_cjaddtime", SqlDbType.DateTime)};
            parameters[0].Value = jbmodel.stwh_cjname;
            parameters[1].Value = jbmodel.stwh_cjmobile;
            parameters[2].Value = jbmodel.stwh_cjaddtime;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_cjtel jbmodel = model as stwh_cjtel;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("if (select count(1) from stwh_cjtel where stwh_cjmobile = '" + jbmodel.stwh_cjmobile + "' and stwh_cjid <> " + jbmodel.stwh_cjid + ") = 0 begin ");
            strSql.Append("update stwh_cjtel set ");
            strSql.Append("stwh_cjname=@stwh_cjname,");
            strSql.Append("stwh_cjmobile=@stwh_cjmobile,");
            strSql.Append("stwh_cjaddtime=@stwh_cjaddtime");
            strSql.Append(" where stwh_cjid=@stwh_cjid");
            strSql.Append(" end");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_cjname", SqlDbType.NVarChar,800),
					new SqlParameter("@stwh_cjmobile", SqlDbType.NVarChar,800),
					new SqlParameter("@stwh_cjaddtime", SqlDbType.DateTime),
					new SqlParameter("@stwh_cjid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_cjname;
            parameters[1].Value = jbmodel.stwh_cjmobile;
            parameters[2].Value = jbmodel.stwh_cjaddtime;
            parameters[3].Value = jbmodel.stwh_cjid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_cjid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_cjtel ");
            strSql.Append(" where stwh_cjid=@stwh_cjid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_cjid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_cjid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_cjidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_cjtel ");
            strSql.Append(" where stwh_cjid in (" + stwh_cjidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_cjid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_cjid,stwh_cjname,stwh_cjmobile,stwh_cjaddtime from stwh_cjtel ");
            strSql.Append(" where stwh_cjid=@stwh_cjid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_cjid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_cjid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_cjtel jbmodel = new stwh_cjtel();
            if (row != null)
            {
                if (row["stwh_cjid"] != null)
                {
                    jbmodel.stwh_cjid = int.Parse(row["stwh_cjid"].ToString());
                }
                if (row["stwh_cjname"] != null)
                {
                    jbmodel.stwh_cjname = row["stwh_cjname"].ToString();
                }
                if (row["stwh_cjmobile"] != null)
                {
                    jbmodel.stwh_cjmobile = row["stwh_cjmobile"].ToString();
                }
                if (row["stwh_cjaddtime"] != null)
                {
                    jbmodel.stwh_cjaddtime = DateTime.Parse(row["stwh_cjaddtime"].ToString());
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_cjid,stwh_cjname,stwh_cjmobile,stwh_cjaddtime ");
            strSql.Append(" FROM stwh_cjtel ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_cjid,stwh_cjname,stwh_cjmobile,stwh_cjaddtime ");
            strSql.Append(" FROM stwh_cjtel ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_cjtel ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_cjid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_cjtel T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}


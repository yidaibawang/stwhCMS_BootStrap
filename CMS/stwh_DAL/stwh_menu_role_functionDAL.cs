﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    public partial class stwh_menu_role_functionDAL : BaseDAL, Istwh_menu_role_functionDAL
    {
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_menu_role_function model = new stwh_menu_role_function();
            if (row != null)
            {
                if (row["stwh_fid"] != null)
                {
                    model.stwh_fid = int.Parse(row["stwh_fid"].ToString());
                }
                if (row["stwh_rid"] != null)
                {
                    model.stwh_rid = int.Parse(row["stwh_rid"].ToString());
                }
            }
            return model;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_fid,stwh_rid ");
            strSql.Append(" FROM stwh_menu_role_function ");
            if (strWhere.Trim() != "") strSql.Append(" where " + strWhere);
            return DbHelperSQL.Query(strSql.ToString());
        }

        public int GetMaxId()
        {
            throw new NotImplementedException();
        }

        public bool Exists(int id)
        {
            throw new NotImplementedException();
        }

        public int Add(BaseModel model)
        {
            throw new NotImplementedException();
        }

        public bool Update(BaseModel model)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public bool DeleteList(string idlist)
        {
            throw new NotImplementedException();
        }

        public BaseModel GetModel(int id)
        {
            throw new NotImplementedException();
        }

        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            throw new NotImplementedException();
        }

        public int GetRecordCount(string strWhere)
        {
            throw new NotImplementedException();
        }

        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            throw new NotImplementedException();
        }

        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            throw new NotImplementedException();
        }

        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using stwh_DBUtility;//Please add references

namespace stwh_DAL
{
    /// <summary>
    /// DAL基础类
    /// </summary>
    public partial class BaseDAL
    {
        public BaseDAL()
        {
            DbHelperSQL.connectionString = PubConstant.ConnectionString;
        }
    }
}

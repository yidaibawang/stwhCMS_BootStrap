﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_productbrand
    /// </summary>
    public partial class stwh_productbrandDAL : BaseDAL, Istwh_productbrandDAL
    {
        public stwh_productbrandDAL()
        { }
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_productbrand");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_productbrand", "stwh_pbid", FieldColumn, FieldOrder, "stwh_pbid,stwh_pbname,stwh_pbimage,stwh_pburl,stwh_pbdescription,stwh_pborder", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_productbrand where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_productbrand", "stwh_pbid", FieldColumn, FieldOrder, "stwh_pbid,stwh_pbname,stwh_pbimage,stwh_pburl,stwh_pbdescription,stwh_pborder", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_pbid", "stwh_productbrand");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_pbid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_productbrand");
            strSql.Append(" where stwh_pbid=@stwh_pbid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_pbid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_pbid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }


        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_productbrand jbmodel = model as stwh_productbrand;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_productbrand(");
            strSql.Append("stwh_pbname,stwh_pbimage,stwh_pburl,stwh_pbdescription,stwh_pborder)");
            strSql.Append(" values (");
            strSql.Append("@stwh_pbname,@stwh_pbimage,@stwh_pburl,@stwh_pbdescription,@stwh_pborder)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_pbname", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pbimage", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pburl", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pbdescription", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pborder", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_pbname;
            parameters[1].Value = jbmodel.stwh_pbimage;
            parameters[2].Value = jbmodel.stwh_pburl;
            parameters[3].Value = jbmodel.stwh_pbdescription;
            parameters[4].Value = jbmodel.stwh_pborder;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }
        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_productbrand jbmodel = model as stwh_productbrand;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_productbrand set ");
            strSql.Append("stwh_pbname=@stwh_pbname,");
            strSql.Append("stwh_pbimage=@stwh_pbimage,");
            strSql.Append("stwh_pburl=@stwh_pburl,");
            strSql.Append("stwh_pbdescription=@stwh_pbdescription,");
            strSql.Append("stwh_pborder=@stwh_pborder");
            strSql.Append(" where stwh_pbid=@stwh_pbid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_pbname", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pbimage", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pburl", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pbdescription", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_pborder", SqlDbType.Int,4),
					new SqlParameter("@stwh_pbid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_pbname;
            parameters[1].Value = jbmodel.stwh_pbimage;
            parameters[2].Value = jbmodel.stwh_pburl;
            parameters[3].Value = jbmodel.stwh_pbdescription;
            parameters[4].Value = jbmodel.stwh_pborder;
            parameters[5].Value = jbmodel.stwh_pbid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_pbid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_productbrand ");
            strSql.Append(" where stwh_pbid=@stwh_pbid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_pbid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_pbid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_pbidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_productbrand ");
            strSql.Append(" where stwh_pbid in (" + stwh_pbidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_pbid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_pbid,stwh_pbname,stwh_pbimage,stwh_pburl,stwh_pbdescription,stwh_pborder from stwh_productbrand ");
            strSql.Append(" where stwh_pbid=@stwh_pbid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_pbid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_pbid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_productbrand jbmodel = new stwh_productbrand();
            if (row != null)
            {
                if (row["stwh_pbid"] != null)
                {
                    jbmodel.stwh_pbid = int.Parse(row["stwh_pbid"].ToString());
                }
                if (row["stwh_pbname"] != null)
                {
                    jbmodel.stwh_pbname = row["stwh_pbname"].ToString();
                }
                if (row["stwh_pbimage"] != null)
                {
                    jbmodel.stwh_pbimage = row["stwh_pbimage"].ToString();
                }
                if (row["stwh_pburl"] != null)
                {
                    jbmodel.stwh_pburl = row["stwh_pburl"].ToString();
                }
                if (row["stwh_pbdescription"] != null)
                {
                    jbmodel.stwh_pbdescription = row["stwh_pbdescription"].ToString();
                }
                if (row["stwh_pborder"] != null)
                {
                    jbmodel.stwh_pborder = int.Parse(row["stwh_pborder"].ToString());
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_pbid,stwh_pbname,stwh_pbimage,stwh_pburl,stwh_pbdescription,stwh_pborder ");
            strSql.Append(" FROM stwh_productbrand ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_pbid,stwh_pbname,stwh_pbimage,stwh_pburl,stwh_pbdescription,stwh_pborder ");
            strSql.Append(" FROM stwh_productbrand ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_productbrand ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_pbid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_productbrand T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}


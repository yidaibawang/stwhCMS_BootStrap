﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_config
    /// </summary>
    public partial class stwh_configDAL : BaseDAL, Istwh_configDAL
    {
        public stwh_configDAL()
        { }
        #region Istwh_configDAL接口实现方法
        /// <summary>
        /// 增加一条数据
        /// </summary>
        public bool Add(stwh_config model)
        {
            bool result = false;
            try
            {
                SqlParameter[] parameters = {
                    new SqlParameter("@stwh_cfid", SqlDbType.Int,4),
					new SqlParameter("@stwh_cfweihu", SqlDbType.Int,4),
                    new SqlParameter("@stwh_cfweihu_m", SqlDbType.Int,4),
                    new SqlParameter("@stwh_cfweihu_s", SqlDbType.Int,4),
					new SqlParameter("@stwh_cfico", SqlDbType.NVarChar,1000),
                    new SqlParameter("@stwh_cfico_m", SqlDbType.NVarChar,1000),
                    new SqlParameter("@stwh_cfico_s", SqlDbType.NVarChar,1000),
					new SqlParameter("@stwh_cflogo", SqlDbType.NVarChar,1000),
                    new SqlParameter("@stwh_cflogo_m", SqlDbType.NVarChar,1000),
                    new SqlParameter("@stwh_cflogo_s", SqlDbType.NVarChar,1000),
					new SqlParameter("@stwh_cfwebname", SqlDbType.NVarChar,1000),
                    new SqlParameter("@stwh_cfwebname_m", SqlDbType.NVarChar,1000),
                    new SqlParameter("@stwh_cfwebname_s", SqlDbType.NVarChar,1000),
					new SqlParameter("@stwh_cfuploadsize", SqlDbType.Int,4),
					new SqlParameter("@stwh_cfthumbnail", SqlDbType.Int,4),
					new SqlParameter("@stwh_cfuploadtype", SqlDbType.NVarChar,1000),
					new SqlParameter("@stwh_cfcopyright", SqlDbType.NVarChar,1000),
                    new SqlParameter("@stwh_cfcopyright_m", SqlDbType.NVarChar,1000),
                    new SqlParameter("@stwh_cfcopyright_s", SqlDbType.NVarChar,1000),
					new SqlParameter("@stwh_cfkeywords", SqlDbType.NVarChar,1000),
                    new SqlParameter("@stwh_cfkeywords_m", SqlDbType.NVarChar,1000),
                    new SqlParameter("@stwh_cfkeywords_s", SqlDbType.NVarChar,1000),
					new SqlParameter("@stwh_cfdescription", SqlDbType.NVarChar,1000),
                    new SqlParameter("@stwh_cfdescription_m", SqlDbType.NVarChar,1000),
                    new SqlParameter("@stwh_cfdescription_s", SqlDbType.NVarChar,1000),
					new SqlParameter("@stwh_cfwebtemplate", SqlDbType.NVarChar,1000),
					new SqlParameter("@stwh_cfmwebtemplate", SqlDbType.NVarChar,1000),
                    new SqlParameter("@stwh_cfshoptemplate", SqlDbType.NVarChar,1000),
					new SqlParameter("@stwh_cfsytk", SqlDbType.NText),
					new SqlParameter("@stwh_cfystk", SqlDbType.NText),
					new SqlParameter("@stwh_cfsmtp", SqlDbType.NVarChar,1000),
                    new SqlParameter("@stwh_cfport", SqlDbType.Int,4),
					new SqlParameter("@stwh_cfsendname", SqlDbType.NVarChar,1000),
					new SqlParameter("@stwh_cfname", SqlDbType.NVarChar,1000),
					new SqlParameter("@stwh_cfuser", SqlDbType.NVarChar,1000),
					new SqlParameter("@stwh_cfpwd", SqlDbType.NVarChar,1000),
                    new SqlParameter("@stwh_cfversion", SqlDbType.NVarChar,1000),
                    new SqlParameter("@stwh_cfkey", SqlDbType.NVarChar,1000),
                    new SqlParameter("@stwh_cfkeytime", SqlDbType.NVarChar,1000),
                    new SqlParameter("@result",SqlDbType.Int,4)};
                parameters[0].Value = model.stwh_cfid;
                parameters[1].Value = model.stwh_cfweihu;
                parameters[2].Value = model.stwh_cfweihu_m;
                parameters[3].Value = model.stwh_cfweihu_s;
                parameters[4].Value = model.stwh_cfico;
                parameters[5].Value = model.stwh_cfico_m;
                parameters[6].Value = model.stwh_cfico_s;
                parameters[7].Value = model.stwh_cflogo;
                parameters[8].Value = model.stwh_cflogo_m;
                parameters[9].Value = model.stwh_cflogo_s;
                parameters[10].Value = model.stwh_cfwebname;
                parameters[11].Value = model.stwh_cfwebname_m;
                parameters[12].Value = model.stwh_cfwebname_s;
                parameters[13].Value = model.stwh_cfuploadsize;
                parameters[14].Value = model.stwh_cfthumbnail;
                parameters[15].Value = model.stwh_cfuploadtype;
                parameters[16].Value = model.stwh_cfcopyright;
                parameters[17].Value = model.stwh_cfcopyright_m;
                parameters[18].Value = model.stwh_cfcopyright_s;
                parameters[19].Value = model.stwh_cfkeywords;
                parameters[20].Value = model.stwh_cfkeywords_m;
                parameters[21].Value = model.stwh_cfkeywords_s;
                parameters[22].Value = model.stwh_cfdescription;
                parameters[23].Value = model.stwh_cfdescription_m;
                parameters[24].Value = model.stwh_cfdescription_s;
                parameters[25].Value = model.stwh_cfwebtemplate;
                parameters[26].Value = model.stwh_cfmwebtemplate;
                parameters[27].Value = model.stwh_cfshoptemplate;
                parameters[28].Value = model.stwh_cfsytk;
                parameters[29].Value = model.stwh_cfystk;
                parameters[30].Value = model.stwh_cfsmtp;
                parameters[31].Value = model.stwh_cfport;
                parameters[32].Value = model.stwh_cfsendname;
                parameters[33].Value = model.stwh_cfname;
                parameters[34].Value = model.stwh_cfuser;
                parameters[35].Value = model.stwh_cfpwd;
                parameters[36].Value = model.stwh_cfversion;
                parameters[37].Value = model.stwh_cfkey;
                parameters[38].Value = model.stwh_cfkeytime;
                parameters[39].Direction = ParameterDirection.Output;
                using (DbHelperSQL.RunProcedure("ProcAddConfig", parameters))
                {
                    result = int.Parse(parameters[24].Value.ToString()) == 1 ? false : true;
                }
            }
            catch (Exception)
            {

            }
            return result;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public stwh_config GetModel()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_cfid,stwh_cfweihu,stwh_cfweihu_m,stwh_cfico,stwh_cfico_m,stwh_cfico_s,stwh_cflogo,stwh_cflogo_m,stwh_cflogo_s,stwh_cfwebname,stwh_cfwebname_m,stwh_cfwebname_s,stwh_cfuploadsize,stwh_cfthumbnail,stwh_cfuploadtype,stwh_cfcopyright,stwh_cfcopyright_m,stwh_cfcopyright_s,stwh_cfkeywords,stwh_cfkeywords_m,stwh_cfkeywords_s,stwh_cfdescription,stwh_cfdescription_m,stwh_cfdescription_s,stwh_cfwebtemplate,stwh_cfmwebtemplate,stwh_cfsytk,stwh_cfystk,stwh_cfaddtime,stwh_cfsmtp,stwh_cfport,stwh_cfsendname,stwh_cfname,stwh_cfuser,stwh_cfpwd,stwh_cfversion,stwh_cfshoptemplate,stwh_cfweihu_s,stwh_cfkey,stwh_cfkeytime from stwh_config ");

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), null);
            if (ds.Tables[0].Rows.Count > 0) return DataRowToModel(ds.Tables[0].Rows[0]) as stwh_config;
            else return null;
        }
        #endregion
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_cfidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_config ");
            strSql.Append(" where stwh_cfid in (" + stwh_cfidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_config model = new stwh_config();
            if (row != null)
            {
                if (row["stwh_cfid"] != null)
                {
                    model.stwh_cfid = int.Parse(row["stwh_cfid"].ToString());
                }
                if (row["stwh_cfweihu"] != null)
                {
                    model.stwh_cfweihu = int.Parse(row["stwh_cfweihu"].ToString());
                }
                if (row["stwh_cfweihu_m"] != null)
                {
                    model.stwh_cfweihu_m = int.Parse(row["stwh_cfweihu_m"].ToString());
                }
                if (row["stwh_cfweihu_s"] != null)
                {
                    model.stwh_cfweihu_s = int.Parse(row["stwh_cfweihu_s"].ToString());
                }
                if (row["stwh_cfico"] != null)
                {
                    model.stwh_cfico = row["stwh_cfico"].ToString();
                }
                if (row["stwh_cfico_m"] != null)
                {
                    model.stwh_cfico_m = row["stwh_cfico_m"].ToString();
                }
                if (row["stwh_cfico_s"] != null)
                {
                    model.stwh_cfico_s = row["stwh_cfico_s"].ToString();
                }
                if (row["stwh_cflogo"] != null)
                {
                    model.stwh_cflogo = row["stwh_cflogo"].ToString();
                }
                if (row["stwh_cflogo_m"] != null)
                {
                    model.stwh_cflogo_m = row["stwh_cflogo_m"].ToString();
                }
                if (row["stwh_cflogo_s"] != null)
                {
                    model.stwh_cflogo_s = row["stwh_cflogo_s"].ToString();
                }
                if (row["stwh_cfwebname"] != null)
                {
                    model.stwh_cfwebname = row["stwh_cfwebname"].ToString();
                }
                if (row["stwh_cfwebname_m"] != null)
                {
                    model.stwh_cfwebname_m = row["stwh_cfwebname_m"].ToString();
                }
                if (row["stwh_cfwebname_s"] != null)
                {
                    model.stwh_cfwebname_s = row["stwh_cfwebname_s"].ToString();
                }
                if (row["stwh_cfuploadsize"] != null)
                {
                    model.stwh_cfuploadsize = int.Parse(row["stwh_cfuploadsize"].ToString());
                }
                if (row["stwh_cfthumbnail"] != null)
                {
                    model.stwh_cfthumbnail = int.Parse(row["stwh_cfthumbnail"].ToString());
                }
                if (row["stwh_cfuploadtype"] != null)
                {
                    model.stwh_cfuploadtype = row["stwh_cfuploadtype"].ToString();
                }
                if (row["stwh_cfcopyright"] != null)
                {
                    model.stwh_cfcopyright = row["stwh_cfcopyright"].ToString();
                }
                if (row["stwh_cfcopyright_m"] != null)
                {
                    model.stwh_cfcopyright_m = row["stwh_cfcopyright_m"].ToString();
                }
                if (row["stwh_cfcopyright_s"] != null)
                {
                    model.stwh_cfcopyright_s = row["stwh_cfcopyright_s"].ToString();
                }
                if (row["stwh_cfkeywords"] != null)
                {
                    model.stwh_cfkeywords = row["stwh_cfkeywords"].ToString();
                }
                if (row["stwh_cfkeywords_m"] != null)
                {
                    model.stwh_cfkeywords_m = row["stwh_cfkeywords_m"].ToString();
                }
                if (row["stwh_cfkeywords_s"] != null)
                {
                    model.stwh_cfkeywords_s = row["stwh_cfkeywords_s"].ToString();
                }
                if (row["stwh_cfdescription"] != null)
                {
                    model.stwh_cfdescription = row["stwh_cfdescription"].ToString();
                }
                if (row["stwh_cfdescription_m"] != null)
                {
                    model.stwh_cfdescription_m = row["stwh_cfdescription_m"].ToString();
                }
                if (row["stwh_cfdescription_s"] != null)
                {
                    model.stwh_cfdescription_s = row["stwh_cfdescription_s"].ToString();
                }
                if (row["stwh_cfwebtemplate"] != null)
                {
                    model.stwh_cfwebtemplate = row["stwh_cfwebtemplate"].ToString();
                }
                if (row["stwh_cfshoptemplate"] != null)
                {
                    model.stwh_cfshoptemplate = row["stwh_cfshoptemplate"].ToString();
                }
                if (row["stwh_cfport"] != null)
                {
                    model.stwh_cfport = int.Parse(row["stwh_cfport"].ToString());
                }
                if (row["stwh_cfsmtp"] != null)
                {
                    model.stwh_cfsmtp = row["stwh_cfsmtp"].ToString();
                }
                if (row["stwh_cfsendname"] != null)
                {
                    model.stwh_cfsendname = row["stwh_cfsendname"].ToString();
                }
                if (row["stwh_cfname"] != null)
                {
                    model.stwh_cfname = row["stwh_cfname"].ToString();
                }
                if (row["stwh_cfuser"] != null)
                {
                    model.stwh_cfuser = row["stwh_cfuser"].ToString();
                }
                if (row["stwh_cfpwd"] != null)
                {
                    model.stwh_cfpwd = row["stwh_cfpwd"].ToString();
                }
                if (row["stwh_cfversion"] != null)
                {
                    model.stwh_cfversion = row["stwh_cfversion"].ToString();
                }
                if (row["stwh_cfmwebtemplate"] != null)
                {
                    model.stwh_cfmwebtemplate = row["stwh_cfmwebtemplate"].ToString();
                }
                if (row["stwh_cfsytk"] != null)
                {
                    model.stwh_cfsytk = row["stwh_cfsytk"].ToString();
                }
                if (row["stwh_cfystk"] != null)
                {
                    model.stwh_cfystk = row["stwh_cfystk"].ToString();
                }
                if (row["stwh_cfkey"] != null)
                {
                    model.stwh_cfkey = row["stwh_cfkey"].ToString();
                }
                if (row["stwh_cfkeytime"] != null)
                {
                    model.stwh_cfkeytime = row["stwh_cfkeytime"].ToString();
                }
                if (row["stwh_cfaddtime"] != null)
                {
                    model.stwh_cfaddtime = DateTime.Parse(row["stwh_cfaddtime"].ToString());
                }
            }
            return model;
        }

        public int GetMaxId()
        {
            throw new NotImplementedException();
        }

        public bool Exists(int id)
        {
            throw new NotImplementedException();
        }

        public int Add(BaseModel model)
        {
            throw new NotImplementedException();
        }

        public bool Update(BaseModel model)
        {
            throw new NotImplementedException();
        }

        public bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public BaseModel GetModel(int id)
        {
            throw new NotImplementedException();
        }

        public DataSet GetList(string where)
        {
            throw new NotImplementedException();
        }

        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            throw new NotImplementedException();
        }

        public int GetRecordCount(string strWhere)
        {
            throw new NotImplementedException();
        }

        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            throw new NotImplementedException();
        }

        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            throw new NotImplementedException();
        }

        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            throw new NotImplementedException();
        }
        #endregion  BasicMethod
    }
}


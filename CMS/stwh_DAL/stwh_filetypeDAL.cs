﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_filetype
    /// </summary>
    public partial class stwh_filetypeDAL : BaseDAL, Istwh_filetypeDAL
    {
        public stwh_filetypeDAL()
        { }
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_filetype");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_filetype", "stwh_ftid", FieldColumn, FieldOrder, "stwh_ftid,stwh_ftname,stwh_ftdescription,stwh_ftimg,stwh_ftorder", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_filetype where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_filetype", "stwh_ftid", FieldColumn, FieldOrder, "stwh_ftid,stwh_ftname,stwh_ftdescription,stwh_ftimg,stwh_ftorder", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_ftid", "stwh_filetype");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_ftid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_filetype");
            strSql.Append(" where stwh_ftid=@stwh_ftid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ftid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_ftid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_filetype jbmodel = model as stwh_filetype;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_filetype(");
            strSql.Append("stwh_ftname,stwh_ftdescription,stwh_ftorder,stwh_ftimg)");
            strSql.Append(" values (");
            strSql.Append("@stwh_ftname,@stwh_ftdescription,@stwh_ftorder,@stwh_ftimg)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ftname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_ftdescription", SqlDbType.NVarChar,300),
                    new SqlParameter("@stwh_ftorder", SqlDbType.Int,4),
					new SqlParameter("@stwh_ftimg", SqlDbType.NVarChar,300)};
            parameters[0].Value = jbmodel.stwh_ftname;
            parameters[1].Value = jbmodel.stwh_ftdescription;
            parameters[2].Value = jbmodel.stwh_ftorder;
            parameters[3].Value = jbmodel.stwh_ftimg;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_filetype jbmodel = model as stwh_filetype;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_filetype set ");
            strSql.Append("stwh_ftname=@stwh_ftname,");
            strSql.Append("stwh_ftdescription=@stwh_ftdescription,");
            strSql.Append("stwh_ftimg=@stwh_ftimg,");
            strSql.Append("stwh_ftorder=@stwh_ftorder");
            strSql.Append(" where stwh_ftid=@stwh_ftid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ftname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_ftdescription", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_ftimg", SqlDbType.NVarChar,300),
                    new SqlParameter("@stwh_ftorder", SqlDbType.Int,4),
					new SqlParameter("@stwh_ftid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_ftname;
            parameters[1].Value = jbmodel.stwh_ftdescription;
            parameters[2].Value = jbmodel.stwh_ftimg;
            parameters[3].Value = jbmodel.stwh_ftorder;
            parameters[4].Value = jbmodel.stwh_ftid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_ftid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_filetype ");
            strSql.Append(" where stwh_ftid=@stwh_ftid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ftid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_ftid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_ftidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_filetype ");
            strSql.Append(" where stwh_ftid in (" + stwh_ftidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_ftid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_ftid,stwh_ftname,stwh_ftdescription,stwh_ftimg,stwh_ftorder from stwh_filetype ");
            strSql.Append(" where stwh_ftid=@stwh_ftid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_ftid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_ftid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_filetype jbmodel = new stwh_filetype();
            if (row != null)
            {
                if (row["stwh_ftid"] != null)
                {
                    jbmodel.stwh_ftid = int.Parse(row["stwh_ftid"].ToString());
                }
                if (row["stwh_ftname"] != null)
                {
                    jbmodel.stwh_ftname = row["stwh_ftname"].ToString();
                }
                if (row["stwh_ftdescription"] != null)
                {
                    jbmodel.stwh_ftdescription = row["stwh_ftdescription"].ToString();
                }
                if (row["stwh_ftimg"] != null)
                {
                    jbmodel.stwh_ftimg = row["stwh_ftimg"].ToString();
                }
                if (row["stwh_ftorder"] != null)
                {
                    jbmodel.stwh_ftorder = int.Parse(row["stwh_ftorder"].ToString());
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_ftid,stwh_ftname,stwh_ftdescription,stwh_ftimg,stwh_ftorder ");
            strSql.Append(" FROM stwh_filetype ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_ftid,stwh_ftname,stwh_ftdescription,stwh_ftimg,stwh_ftorder ");
            strSql.Append(" FROM stwh_filetype ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_filetype ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_ftid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_filetype T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}


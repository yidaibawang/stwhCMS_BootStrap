﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_department
    /// </summary>
    public partial class stwh_departmentDAL : BaseDAL, Istwh_departmentDAL
    {
        public stwh_departmentDAL()
        { }
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_department");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_department", "stwh_dtid", FieldColumn, FieldOrder, "stwh_dtid,stwh_dtname,stwh_dtdescription,stwh_dtimg,stwh_dtorder,stwh_dtparentid", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_department where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_department", "stwh_dtid", FieldColumn, FieldOrder, "stwh_dtid,stwh_dtname,stwh_dtdescription,stwh_dtimg,stwh_dtorder,stwh_dtparentid", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_dtid", "stwh_department");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_dtid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_department");
            strSql.Append(" where stwh_dtid=@stwh_dtid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_dtid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_dtid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_department jbmodel = model as stwh_department;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_department(");
            strSql.Append("stwh_dtname,stwh_dtdescription,stwh_dtimg,stwh_dtorder,stwh_dtparentid)");
            strSql.Append(" values (");
            strSql.Append("@stwh_dtname,@stwh_dtdescription,@stwh_dtimg,@stwh_dtorder,@stwh_dtparentid)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_dtname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_dtdescription", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_dtimg", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_dtorder", SqlDbType.Int,4),
					new SqlParameter("@stwh_dtparentid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_dtname;
            parameters[1].Value = jbmodel.stwh_dtdescription;
            parameters[2].Value = jbmodel.stwh_dtimg;
            parameters[3].Value = jbmodel.stwh_dtorder;
            parameters[4].Value = jbmodel.stwh_dtparentid;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_department jbmodel = model as stwh_department;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_department set ");
            strSql.Append("stwh_dtname=@stwh_dtname,");
            strSql.Append("stwh_dtdescription=@stwh_dtdescription,");
            strSql.Append("stwh_dtimg=@stwh_dtimg,");
            strSql.Append("stwh_dtorder=@stwh_dtorder,");
            strSql.Append("stwh_dtparentid=@stwh_dtparentid");
            strSql.Append(" where stwh_dtid=@stwh_dtid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_dtname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_dtdescription", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_dtimg", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_dtorder", SqlDbType.Int,4),
					new SqlParameter("@stwh_dtparentid", SqlDbType.Int,4),
					new SqlParameter("@stwh_dtid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_dtname;
            parameters[1].Value = jbmodel.stwh_dtdescription;
            parameters[2].Value = jbmodel.stwh_dtimg;
            parameters[3].Value = jbmodel.stwh_dtorder;
            parameters[4].Value = jbmodel.stwh_dtparentid;
            parameters[5].Value = jbmodel.stwh_dtid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_dtid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_department ");
            strSql.Append(" where stwh_dtid=@stwh_dtid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_dtid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_dtid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_dtidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_department ");
            strSql.Append(" where stwh_dtid in (" + stwh_dtidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_dtid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_dtid,stwh_dtname,stwh_dtdescription,stwh_dtimg,stwh_dtorder,stwh_dtparentid from stwh_department ");
            strSql.Append(" where stwh_dtid=@stwh_dtid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_dtid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_dtid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_department jbmodel = new stwh_department();
            if (row != null)
            {
                if (row["stwh_dtid"] != null)
                {
                    jbmodel.stwh_dtid = int.Parse(row["stwh_dtid"].ToString());
                }
                if (row["stwh_dtname"] != null)
                {
                    jbmodel.stwh_dtname = row["stwh_dtname"].ToString();
                }
                if (row["stwh_dtdescription"] != null)
                {
                    jbmodel.stwh_dtdescription = row["stwh_dtdescription"].ToString();
                }
                if (row["stwh_dtimg"] != null)
                {
                    jbmodel.stwh_dtimg = row["stwh_dtimg"].ToString();
                }
                if (row["stwh_dtorder"] != null)
                {
                    jbmodel.stwh_dtorder = int.Parse(row["stwh_dtorder"].ToString());
                }
                if (row["stwh_dtparentid"] != null)
                {
                    jbmodel.stwh_dtparentid = int.Parse(row["stwh_dtparentid"].ToString());
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_dtid,stwh_dtname,stwh_dtdescription,stwh_dtimg,stwh_dtorder,stwh_dtparentid ");
            strSql.Append(" FROM stwh_department ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_dtid,stwh_dtname,stwh_dtdescription,stwh_dtimg,stwh_dtorder,stwh_dtparentid ");
            strSql.Append(" FROM stwh_department ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_department ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_dtid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_department T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}


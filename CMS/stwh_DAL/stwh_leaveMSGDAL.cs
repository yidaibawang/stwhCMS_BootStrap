﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_leaveMSG
    /// </summary>
    public partial class stwh_leaveMSGDAL : BaseDAL, Istwh_leaveMSGDAL
    {
        public stwh_leaveMSGDAL()
        { }
        #region Istwh_leaveMSGDAL接口实现方法
        /// <summary>
        /// 删除全部数据
        /// </summary>
        public bool DeleteListALL()
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_leaveMSG ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_leaveMSG");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_leaveMSG", "stwh_lmid", FieldColumn, FieldOrder, "stwh_lmid,stwh_lmnc,stwh_lmname,stwh_lmage,stwh_lmsex,stwh_lmsj,stwh_lmemail,stwh_lmsubject,stwh_lmlynr,stwh_lmaddtime,stwh_lmissh", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_leaveMSG where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_leaveMSG", "stwh_lmid", FieldColumn, FieldOrder, "stwh_lmid,stwh_lmnc,stwh_lmname,stwh_lmage,stwh_lmsex,stwh_lmsj,stwh_lmemail,stwh_lmsubject,stwh_lmlynr,stwh_lmaddtime,stwh_lmissh", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_lmid", "stwh_leaveMSG");
        }

        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(int stwh_lmid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_leaveMSG");
            strSql.Append(" where stwh_lmid=@stwh_lmid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_lmid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_lmid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_leaveMSG jbmodel = model as stwh_leaveMSG;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into stwh_leaveMSG(");
            strSql.Append("stwh_lmnc,stwh_lmname,stwh_lmage,stwh_lmsex,stwh_lmsj,stwh_lmemail,stwh_lmsubject,stwh_lmlynr,stwh_lmaddtime,stwh_lmissh)");
            strSql.Append(" values (");
            strSql.Append("@stwh_lmnc,@stwh_lmname,@stwh_lmage,@stwh_lmsex,@stwh_lmsj,@stwh_lmemail,@stwh_lmsubject,@stwh_lmlynr,@stwh_lmaddtime,@stwh_lmissh)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_lmnc", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_lmname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_lmage", SqlDbType.Int,4),
					new SqlParameter("@stwh_lmsex", SqlDbType.Int,4),
					new SqlParameter("@stwh_lmsj", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_lmemail", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_lmsubject", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_lmlynr", SqlDbType.NText),
					new SqlParameter("@stwh_lmaddtime", SqlDbType.DateTime),
					new SqlParameter("@stwh_lmissh", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_lmnc;
            parameters[1].Value = jbmodel.stwh_lmname;
            parameters[2].Value = jbmodel.stwh_lmage;
            parameters[3].Value = jbmodel.stwh_lmsex;
            parameters[4].Value = jbmodel.stwh_lmsj;
            parameters[5].Value = jbmodel.stwh_lmemail;
            parameters[6].Value = jbmodel.stwh_lmsubject;
            parameters[7].Value = jbmodel.stwh_lmlynr;
            parameters[8].Value = jbmodel.stwh_lmaddtime;
            parameters[9].Value = jbmodel.stwh_lmissh;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_leaveMSG jbmodel = model as stwh_leaveMSG;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_leaveMSG set ");
            strSql.Append("stwh_lmnc=@stwh_lmnc,");
            strSql.Append("stwh_lmname=@stwh_lmname,");
            strSql.Append("stwh_lmage=@stwh_lmage,");
            strSql.Append("stwh_lmsex=@stwh_lmsex,");
            strSql.Append("stwh_lmsj=@stwh_lmsj,");
            strSql.Append("stwh_lmemail=@stwh_lmemail,");
            strSql.Append("stwh_lmsubject=@stwh_lmsubject,");
            strSql.Append("stwh_lmlynr=@stwh_lmlynr,");
            strSql.Append("stwh_lmaddtime=@stwh_lmaddtime,");
            strSql.Append("stwh_lmissh=@stwh_lmissh");
            strSql.Append(" where stwh_lmid=@stwh_lmid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_lmnc", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_lmname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_lmage", SqlDbType.Int,4),
					new SqlParameter("@stwh_lmsex", SqlDbType.Int,4),
					new SqlParameter("@stwh_lmsj", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_lmemail", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_lmsubject", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_lmlynr", SqlDbType.NText),
					new SqlParameter("@stwh_lmaddtime", SqlDbType.DateTime),
					new SqlParameter("@stwh_lmissh", SqlDbType.Int,4),
					new SqlParameter("@stwh_lmid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_lmnc;
            parameters[1].Value = jbmodel.stwh_lmname;
            parameters[2].Value = jbmodel.stwh_lmage;
            parameters[3].Value = jbmodel.stwh_lmsex;
            parameters[4].Value = jbmodel.stwh_lmsj;
            parameters[5].Value = jbmodel.stwh_lmemail;
            parameters[6].Value = jbmodel.stwh_lmsubject;
            parameters[7].Value = jbmodel.stwh_lmlynr;
            parameters[8].Value = jbmodel.stwh_lmaddtime;
            parameters[9].Value = jbmodel.stwh_lmissh;
            parameters[10].Value = jbmodel.stwh_lmid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_lmid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_leaveMSG ");
            strSql.Append(" where stwh_lmid=@stwh_lmid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_lmid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_lmid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_lmidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_leaveMSG ");
            strSql.Append(" where stwh_lmid in (" + stwh_lmidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_lmid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_lmid,stwh_lmnc,stwh_lmname,stwh_lmage,stwh_lmsex,stwh_lmsj,stwh_lmemail,stwh_lmsubject,stwh_lmlynr,stwh_lmaddtime,stwh_lmissh from stwh_leaveMSG ");
            strSql.Append(" where stwh_lmid=@stwh_lmid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_lmid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_lmid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return DataRowToModel(ds.Tables[0].Rows[0]);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_leaveMSG jbmodel = new stwh_leaveMSG();
            if (row != null)
            {
                if (row["stwh_lmid"] != null)
                {
                    jbmodel.stwh_lmid = int.Parse(row["stwh_lmid"].ToString());
                }
                if (row["stwh_lmnc"] != null)
                {
                    jbmodel.stwh_lmnc = row["stwh_lmnc"].ToString();
                }
                if (row["stwh_lmname"] != null)
                {
                    jbmodel.stwh_lmname = row["stwh_lmname"].ToString();
                }
                if (row["stwh_lmage"] != null)
                {
                    jbmodel.stwh_lmage = int.Parse(row["stwh_lmage"].ToString());
                }
                if (row["stwh_lmsex"] != null)
                {
                    jbmodel.stwh_lmsex = int.Parse(row["stwh_lmsex"].ToString());
                }
                if (row["stwh_lmsj"] != null)
                {
                    jbmodel.stwh_lmsj = row["stwh_lmsj"].ToString();
                }
                if (row["stwh_lmemail"] != null)
                {
                    jbmodel.stwh_lmemail = row["stwh_lmemail"].ToString();
                }
                if (row["stwh_lmsubject"] != null)
                {
                    jbmodel.stwh_lmsubject = row["stwh_lmsubject"].ToString();
                }
                if (row["stwh_lmlynr"] != null)
                {
                    jbmodel.stwh_lmlynr = row["stwh_lmlynr"].ToString();
                }
                if (row["stwh_lmaddtime"] != null)
                {
                    jbmodel.stwh_lmaddtime = DateTime.Parse(row["stwh_lmaddtime"].ToString());
                }
                if (row["stwh_lmissh"] != null)
                {
                    jbmodel.stwh_lmissh = int.Parse(row["stwh_lmissh"].ToString());
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_lmid,stwh_lmnc,stwh_lmname,stwh_lmage,stwh_lmsex,stwh_lmsj,stwh_lmemail,stwh_lmsubject,stwh_lmlynr,stwh_lmaddtime,stwh_lmissh ");
            strSql.Append(" FROM stwh_leaveMSG ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_lmid,stwh_lmnc,stwh_lmname,stwh_lmage,stwh_lmsex,stwh_lmsj,stwh_lmemail,stwh_lmsubject,stwh_lmlynr,stwh_lmaddtime,stwh_lmissh ");
            strSql.Append(" FROM stwh_leaveMSG ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_leaveMSG ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_lmid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_leaveMSG T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}


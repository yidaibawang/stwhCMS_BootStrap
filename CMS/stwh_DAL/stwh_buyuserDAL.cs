﻿using System;
using System.Data;
using System.Text;
using System.Data.SqlClient;
using stwh_DBUtility;
using stwh_IDAL;
using stwh_Model;

namespace stwh_DAL
{
    /// <summary>
    /// 数据访问类:stwh_buyuser
    /// </summary>
    public partial class stwh_buyuserDAL : BaseDAL, Istwh_buyuserDAL
    {
        public stwh_buyuserDAL()
        { }
        #region Istwh_buyuserDAL接口实现方法
        /// <summary>
        /// 是否存在该记录
        /// </summary>
        public bool Exists(string stwh_bumobile, int stwh_buid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) from stwh_buyuser");
            strSql.Append(" where stwh_bumobile=@stwh_bumobile and stwh_buid=@stwh_buid ");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_bumobile", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_buid", SqlDbType.Int,4)			};
            parameters[0].Value = stwh_bumobile;
            parameters[1].Value = stwh_buid;

            return DbHelperSQL.Exists(strSql.ToString(), parameters);
        }

        /// <summary>
        /// 批量修改会员状态
        /// </summary>
        /// <param name="stwh_buidlist">会员id</param>
        /// <param name="stwh_bustatus">状态（0,1）</param>
        /// <returns></returns>
        public bool Update(string stwh_buidlist, int stwh_bustatus)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_buyuser set stwh_bustatus = " + stwh_bustatus);
            strSql.Append(" where stwh_buid in (" + stwh_buidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0) return true;
            else return false;
        }

        /// <summary>
        /// 修改会员登录密码
        /// </summary>
        /// <param name="stwh_bupwd">新密码</param>
        /// <param name="stwh_buid">会员ID</param>
        /// <returns></returns>
        public bool UpdatePwd(string stwh_bupwd, int stwh_buid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_buyuser set stwh_bupwd = '" + stwh_bupwd + "'");
            strSql.Append(" where stwh_buid =" + stwh_buid);
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0) return true;
            else return false;
        }

        /// <summary>
        /// 修改会员登录密码(用户会员找回密码)
        /// </summary>
        /// <param name="stwh_bumobile">会员手机</param>
        /// <param name="stwh_bupwd">新密码</param>
        /// <returns></returns>
        public bool UpdatePwd(string stwh_bumobile, string stwh_bupwd)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_buyuser set stwh_bupwd = '" + stwh_bupwd + "'");
            strSql.Append(" where stwh_bumobile ='" + stwh_bumobile + "'");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0) return true;
            else return false;
        }

        /// <summary>
        /// 修改会员手机
        /// </summary>
        /// <param name="oldphone">旧手机号码</param>
        /// <param name="newphone">新手机号码</param>
        /// <returns></returns>
        public bool UpdatePhone(string oldphone, string newphone)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("if (select count(1) from stwh_buyuser where stwh_bumobile = '" + newphone + "') = 0 begin ");
            strSql.Append("update stwh_buyuser set stwh_bumobile = '" + newphone + "'");
            strSql.Append(" where stwh_bumobile ='" + oldphone + "'");
            strSql.Append(" end");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0) return true;
            else return false;
        }

        /// <summary>
        /// 更新会员基本信息
        /// </summary>
        /// <param name="model">更新对象</param>
        /// <returns></returns>
        public bool UpdateInfo(stwh_buyuser model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update stwh_buyuser set ");
            strSql.Append("stwh_buimg=@stwh_buimg,");
            strSql.Append("stwh_bunicheng=@stwh_bunicheng,");
            strSql.Append("stwh_buname=@stwh_buname,");
            strSql.Append("stwh_busex=@stwh_busex,");
            strSql.Append("stwh_buemail=@stwh_buemail,");
            strSql.Append("stwh_bushengri=@stwh_bushengri");
            strSql.Append(" where stwh_buid=@stwh_buid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_buimg", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bunicheng", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_buname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_busex", SqlDbType.Int,4),
					new SqlParameter("@stwh_buemail", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_bushengri", SqlDbType.DateTime),
					new SqlParameter("@stwh_buid", SqlDbType.Int,4)};
            parameters[0].Value = model.stwh_buimg;
            parameters[1].Value = model.stwh_bunicheng;
            parameters[2].Value = model.stwh_buname;
            parameters[3].Value = model.stwh_busex;
            parameters[4].Value = model.stwh_buemail;
            parameters[5].Value = model.stwh_bushengri;
            parameters[6].Value = model.stwh_buid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0) return true;
            else return false;
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(string stwh_bumobile, int stwh_buid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_buyuser ");
            strSql.Append(" where stwh_bumobile=@stwh_bumobile and stwh_buid=@stwh_buid ");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_bumobile", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_buid", SqlDbType.Int,4)			};
            parameters[0].Value = stwh_bumobile;
            parameters[1].Value = stwh_buid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 根据手机号码和密码获取对象（启用）
        /// </summary>
        /// <param name="stwh_bumobile">手机号码</param>
        /// <param name="stwh_bupwd">登录密码</param>
        /// <returns></returns>
        public stwh_buyuser GetModel(string stwh_bumobile, string stwh_bupwd)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_buid,stwh_buimg,stwh_bunicheng,stwh_buname,stwh_busex,stwh_buemail,stwh_bumobile,stwh_bushengri,stwh_bupwd,stwh_buopenid,stwh_buqqid,stwh_buwbo,stwh_buaddtime,stwh_bustatus from stwh_buyuser ");
            strSql.Append(" where stwh_bumobile=@stwh_bumobile and stwh_bupwd = @stwh_bupwd and stwh_bustatus = 0");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_bumobile", SqlDbType.NVarChar,100),
                    new SqlParameter("@stwh_bupwd", SqlDbType.NVarChar,100)
			};
            parameters[0].Value = stwh_bumobile;
            parameters[1].Value = stwh_bupwd;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0) return DataRowToModel(ds.Tables[0].Rows[0]) as stwh_buyuser;
            else return null;
        }
        #endregion
        #region  IBaseDAL接口实现方法
        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_buyuser");
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_buyuser", "stwh_buid", FieldColumn, FieldOrder, "stwh_buid,stwh_buimg,stwh_bunicheng,stwh_buname,stwh_busex,stwh_buemail,stwh_bumobile,stwh_bushengri,stwh_bupwd,stwh_buopenid,stwh_buqqid,stwh_buaddtime,stwh_buwbo,stwh_bustatus", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 分页获取文本消息
        /// </summary>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（带条件）</param>
        /// <returns></returns>
        public DataSet GetListByPage(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag)
        {
            object obj = DbHelperSQL.GetSingle("select count(1) from stwh_buyuser where " + If);
            d_peopleCount = obj != null ? int.Parse(obj.ToString()) : 0;
            return DbHelperSQL.PageData("stwh_buyuser", "stwh_buid", FieldColumn, FieldOrder, "stwh_buid,stwh_buimg,stwh_bunicheng,stwh_buname,stwh_busex,stwh_buemail,stwh_bumobile,stwh_bushengri,stwh_bupwd,stwh_buopenid,stwh_buqqid,stwh_buaddtime,stwh_buwbo,stwh_bustatus", If, pageSize, pageNumber, ref selectCount);
        }

        /// <summary>
        /// 得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return DbHelperSQL.GetMaxID("stwh_buid", "stwh_buyuser");
        }

        public bool Exists(int id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 增加一条数据
        /// </summary>
        public int Add(BaseModel model)
        {
            stwh_buyuser jbmodel = model as stwh_buyuser;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("if (select count(1) from stwh_buyuser where stwh_bumobile = '" + jbmodel.stwh_bumobile + "') = 0 begin ");
            strSql.Append("insert into stwh_buyuser(");
            strSql.Append("stwh_buimg,stwh_bunicheng,stwh_buname,stwh_busex,stwh_buemail,stwh_bumobile,stwh_bushengri,stwh_bupwd,stwh_buopenid,stwh_buqqid,stwh_buwbo,stwh_bustatus,stwh_buaddtime)");
            strSql.Append(" values (");
            strSql.Append("@stwh_buimg,@stwh_bunicheng,@stwh_buname,@stwh_busex,@stwh_buemail,@stwh_bumobile,@stwh_bushengri,@stwh_bupwd,@stwh_buopenid,@stwh_buqqid,@stwh_buwbo,@stwh_bustatus,@stwh_buaddtime)");
            strSql.Append(";select @@IDENTITY");
            strSql.Append(" end");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_buimg", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bunicheng", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_buname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_busex", SqlDbType.Int,4),
					new SqlParameter("@stwh_buemail", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_bumobile", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_bushengri", SqlDbType.DateTime),
					new SqlParameter("@stwh_bupwd", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_buopenid", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_buqqid", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_buwbo", SqlDbType.NVarChar,100),
                    new SqlParameter("@stwh_bustatus", SqlDbType.Int,4),
					new SqlParameter("@stwh_buaddtime", SqlDbType.DateTime)};
            parameters[0].Value = jbmodel.stwh_buimg;
            parameters[1].Value = jbmodel.stwh_bunicheng;
            parameters[2].Value = jbmodel.stwh_buname;
            parameters[3].Value = jbmodel.stwh_busex;
            parameters[4].Value = jbmodel.stwh_buemail;
            parameters[5].Value = jbmodel.stwh_bumobile;
            parameters[6].Value = jbmodel.stwh_bushengri;
            parameters[7].Value = jbmodel.stwh_bupwd;
            parameters[8].Value = jbmodel.stwh_buopenid;
            parameters[9].Value = jbmodel.stwh_buqqid;
            parameters[10].Value = jbmodel.stwh_buwbo;
            parameters[11].Value = jbmodel.stwh_bustatus;
            parameters[12].Value = jbmodel.stwh_buaddtime;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null) return 0;
            else return Convert.ToInt32(obj);
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(BaseModel model)
        {
            stwh_buyuser jbmodel = model as stwh_buyuser;
            StringBuilder strSql = new StringBuilder();
            strSql.Append("if (select count(1) from stwh_buyuser where stwh_bumobile = '" + jbmodel.stwh_bumobile + "' and stwh_buid <> " + jbmodel.stwh_buid + ") = 0 begin ");
            strSql.Append("update stwh_buyuser set ");
            strSql.Append("stwh_buimg=@stwh_buimg,");
            strSql.Append("stwh_bunicheng=@stwh_bunicheng,");
            strSql.Append("stwh_buname=@stwh_buname,");
            strSql.Append("stwh_busex=@stwh_busex,");
            strSql.Append("stwh_buemail=@stwh_buemail,");
            strSql.Append("stwh_bumobile=@stwh_bumobile,");
            strSql.Append("stwh_bushengri=@stwh_bushengri,");
            strSql.Append("stwh_bupwd=@stwh_bupwd,");
            strSql.Append("stwh_buopenid=@stwh_buopenid,");
            strSql.Append("stwh_buqqid=@stwh_buqqid,");
            strSql.Append("stwh_buwbo=@stwh_buwbo,");
            strSql.Append("stwh_buaddtime=@stwh_buaddtime,");
            strSql.Append("stwh_bustatus=@stwh_bustatus");
            strSql.Append(" where stwh_buid=@stwh_buid");
            strSql.Append(" end");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_buimg", SqlDbType.NVarChar,500),
					new SqlParameter("@stwh_bunicheng", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_buname", SqlDbType.NVarChar,300),
					new SqlParameter("@stwh_busex", SqlDbType.Int,4),
					new SqlParameter("@stwh_buemail", SqlDbType.NVarChar,100),
                    new SqlParameter("@stwh_bumobile", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_bushengri", SqlDbType.DateTime),
					new SqlParameter("@stwh_bupwd", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_buopenid", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_buqqid", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_buwbo", SqlDbType.NVarChar,100),
					new SqlParameter("@stwh_buaddtime", SqlDbType.DateTime),
                    new SqlParameter("@stwh_bustatus", SqlDbType.Int,4),
					new SqlParameter("@stwh_buid", SqlDbType.Int,4)};
            parameters[0].Value = jbmodel.stwh_buimg;
            parameters[1].Value = jbmodel.stwh_bunicheng;
            parameters[2].Value = jbmodel.stwh_buname;
            parameters[3].Value = jbmodel.stwh_busex;
            parameters[4].Value = jbmodel.stwh_buemail;
            parameters[5].Value = jbmodel.stwh_bumobile;
            parameters[6].Value = jbmodel.stwh_bushengri;
            parameters[7].Value = jbmodel.stwh_bupwd;
            parameters[8].Value = jbmodel.stwh_buopenid;
            parameters[9].Value = jbmodel.stwh_buqqid;
            parameters[10].Value = jbmodel.stwh_buwbo;
            parameters[11].Value = jbmodel.stwh_buaddtime;
            parameters[12].Value = jbmodel.stwh_bustatus;
            parameters[13].Value = jbmodel.stwh_buid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0) return true;
            else return false;
        }

        /// <summary>
        /// 删除一条数据
        /// </summary>
        public bool Delete(int stwh_buid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_buyuser ");
            strSql.Append(" where stwh_buid=@stwh_buid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_buid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_buid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 批量删除数据
        /// </summary>
        public bool DeleteList(string stwh_buidlist)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("delete from stwh_buyuser ");
            strSql.Append(" where stwh_buid in (" + stwh_buidlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel GetModel(int stwh_buid)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select  top 1 stwh_buid,stwh_buimg,stwh_bunicheng,stwh_buname,stwh_busex,stwh_buemail,stwh_bumobile,stwh_bushengri,stwh_bupwd,stwh_buopenid,stwh_buqqid,stwh_buwbo,stwh_buaddtime,stwh_bustatus from stwh_buyuser ");
            strSql.Append(" where stwh_buid=@stwh_buid");
            SqlParameter[] parameters = {
					new SqlParameter("@stwh_buid", SqlDbType.Int,4)
			};
            parameters[0].Value = stwh_buid;

            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0) return DataRowToModel(ds.Tables[0].Rows[0]);
            else return null;
        }

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        public BaseModel DataRowToModel(DataRow row)
        {
            stwh_buyuser jbmodel = new stwh_buyuser();
            if (row != null)
            {
                if (row["stwh_buid"] != null)
                {
                    jbmodel.stwh_buid = int.Parse(row["stwh_buid"].ToString());
                }
                if (row["stwh_buimg"] != null)
                {
                    jbmodel.stwh_buimg = row["stwh_buimg"].ToString();
                }
                if (row["stwh_bunicheng"] != null)
                {
                    jbmodel.stwh_bunicheng = row["stwh_bunicheng"].ToString();
                }
                if (row["stwh_buname"] != null)
                {
                    jbmodel.stwh_buname = row["stwh_buname"].ToString();
                }
                if (row["stwh_busex"] != null)
                {
                    jbmodel.stwh_busex = int.Parse(row["stwh_busex"].ToString());
                }
                if (row["stwh_buemail"] != null)
                {
                    jbmodel.stwh_buemail = row["stwh_buemail"].ToString();
                }
                if (row["stwh_bumobile"] != null)
                {
                    jbmodel.stwh_bumobile = row["stwh_bumobile"].ToString();
                }
                if (row["stwh_bushengri"] != null)
                {
                    jbmodel.stwh_bushengri = DateTime.Parse(row["stwh_bushengri"].ToString());
                }
                if (row["stwh_bupwd"] != null)
                {
                    jbmodel.stwh_bupwd = row["stwh_bupwd"].ToString();
                }
                if (row["stwh_buopenid"] != null)
                {
                    jbmodel.stwh_buopenid = row["stwh_buopenid"].ToString();
                }
                if (row["stwh_buqqid"] != null)
                {
                    jbmodel.stwh_buqqid = row["stwh_buqqid"].ToString();
                }
                if (row["stwh_buwbo"] != null)
                {
                    jbmodel.stwh_buwbo = row["stwh_buwbo"].ToString();
                }
                if (row["stwh_bustatus"] != null)
                {
                    jbmodel.stwh_bustatus = int.Parse(row["stwh_bustatus"].ToString());
                }
                if (row["stwh_buaddtime"] != null)
                {
                    jbmodel.stwh_buaddtime = DateTime.Parse(row["stwh_buaddtime"].ToString());
                }
            }
            return jbmodel;
        }

        /// <summary>
        /// 获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select stwh_buid,stwh_buimg,stwh_bunicheng,stwh_buname,stwh_busex,stwh_buemail,stwh_bumobile,stwh_bushengri,stwh_bupwd,stwh_buopenid,stwh_buqqid,stwh_buwbo,stwh_buaddtime,stwh_bustatus ");
            strSql.Append(" FROM stwh_buyuser ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" stwh_buid,stwh_buimg,stwh_bunicheng,stwh_buname,stwh_busex,stwh_buemail,stwh_bumobile,stwh_bushengri,stwh_bupwd,stwh_buopenid,stwh_buqqid,stwh_buwbo,stwh_buaddtime,stwh_bustatus ");
            strSql.Append(" FROM stwh_buyuser ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获取记录总数
        /// </summary>
        public int GetRecordCount(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select count(1) FROM stwh_buyuser ");
            if (strWhere.Trim() != "") strSql.Append(" where " + strWhere);
            object obj = DbHelperSQL.GetSingle(strSql.ToString());
            if (obj == null) return 0;
            else return Convert.ToInt32(obj);
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT * FROM ( ");
            strSql.Append(" SELECT ROW_NUMBER() OVER (");
            if (!string.IsNullOrEmpty(orderby.Trim()))
            {
                strSql.Append("order by T." + orderby);
            }
            else
            {
                strSql.Append("order by T.stwh_buid desc");
            }
            strSql.Append(")AS Row, T.*  from stwh_buyuser T ");
            if (!string.IsNullOrEmpty(strWhere.Trim()))
            {
                strSql.Append(" WHERE " + strWhere);
            }
            strSql.Append(" ) TT");
            strSql.AppendFormat(" WHERE TT.Row between {0} and {1}", startIndex, endIndex);
            return DbHelperSQL.Query(strSql.ToString());
        }
        #endregion  BasicMethod
    }
}


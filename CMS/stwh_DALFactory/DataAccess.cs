﻿using System;
using System.Reflection;
using System.Configuration;
using stwh_Common;
using stwh_IDAL;

namespace stwh_DALFactory
{
    /// <summary>
    /// 数据访问工具
    /// </summary>
    public sealed class DataAccess
    {
        private static readonly string AssemblyPath = ConfigurationManager.AppSettings["DAL"];
        public DataAccess()
        { }

        #region private method
        /// <summary>
        /// 加载程序集（不使用Memcached缓存）
        /// </summary>
        /// <param name="AssemblyPath">程序集名称（路径）</param>
        /// <param name="classNamespace">类名称</param>
        /// <returns></returns>
        private static object CreateObjectNoCache(string AssemblyPath, string classNamespace)
        {
            try
            {
                object objType = Assembly.Load(AssemblyPath).CreateInstance(classNamespace);
                return objType;
            }
            catch(Exception)
            {
                return null;
            }

        }

        /// <summary>
        /// 加载程序集（使用Memcached缓存）
        /// </summary>
        /// <param name="AssemblyPath">程序集名称（路径）</param>
        /// <param name="classNamespace">类名称</param>
        /// <returns></returns>
        private static object CreateObject(string AssemblyPath, string classNamespace)
        {
            object objType = DataCache.GetMCache(classNamespace);
            if (objType == null)
            {
                try
                {
                    objType = Assembly.Load(AssemblyPath).CreateInstance(classNamespace);
                    DataCache.AddMCache(classNamespace, objType);// 写入缓存
                }
                catch (Exception)
                {
                }
            }
            return objType;
        }
        #endregion

        #region 创建各自接口类
        ///// <summary>
        ///// 创建stwh_article数据层接口。
        ///// </summary>
        //public static Istwh_articleDAL Createstwh_article()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_articleDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_articleDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_artype数据层接口。
        ///// </summary>
        //public static Istwh_artypeDAL Createstwh_artype()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_artypeDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_artypeDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_Banner数据层接口。
        ///// </summary>
        //public static Istwh_BannerDAL Createstwh_Banner()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_BannerDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_BannerDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_bidding数据层接口。
        ///// </summary>
        //public static Istwh_biddingDAL Createstwh_bidding()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_biddingDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_biddingDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_blogroll数据层接口。
        ///// </summary>
        //public static Istwh_blogrollDAL Createstwh_blogroll()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_blogrollDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_blogrollDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_buyaddress数据层接口。
        ///// </summary>
        //public static Istwh_buyaddressDAL Createstwh_buyaddress()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_buyaddressDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_buyaddressDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_buyuser数据层接口。
        ///// </summary>
        //public static Istwh_buyuserDAL Createstwh_buyuser()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_buyuserDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_buyuserDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_cjresult数据层接口。
        ///// </summary>
        //public static Istwh_cjresultDAL Createstwh_cjresult()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_cjresultDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_cjresultDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_cjtel数据层接口。
        ///// </summary>
        //public static Istwh_cjtelDAL Createstwh_cjtel()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_cjtelDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_cjtelDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_config数据层接口。
        ///// </summary>
        //public static Istwh_configDAL Createstwh_config()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_configDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_configDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_department数据层接口。
        ///// </summary>
        //public static Istwh_departmentDAL Createstwh_department()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_departmentDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_departmentDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_files数据层接口。
        ///// </summary>
        //public static Istwh_filesDAL Createstwh_files()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_filesDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_filesDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_filetype数据层接口。
        ///// </summary>
        //public static Istwh_filetypeDAL Createstwh_filetype()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_filetypeDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_filetypeDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_function数据层接口。
        ///// </summary>
        //public static Istwh_functionDAL Createstwh_function()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_functionDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_functionDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_leaveMSG数据层接口。
        ///// </summary>
        //public static Istwh_leaveMSGDAL Createstwh_leaveMSG()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_leaveMSGDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_leaveMSGDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_loginlog数据层接口。
        ///// </summary>
        //public static Istwh_loginlogDAL Createstwh_loginlog()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_loginlogDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_loginlogDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_menu_role数据层接口。
        ///// </summary>
        //public static Istwh_menu_roleDAL Createstwh_menu_role()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_menu_roleDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_menu_roleDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_menu_role_function数据层接口。
        ///// </summary>
        //public static Istwh_menu_role_functionDAL Createstwh_menu_role_function()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_menu_role_functionDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_menu_role_functionDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_menuinfo数据层接口。
        ///// </summary>
        //public static Istwh_menuinfoDAL Createstwh_menuinfo()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_menuinfoDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_menuinfoDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_notice数据层接口。
        ///// </summary>
        //public static Istwh_noticeDAL Createstwh_notice()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_noticeDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_noticeDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_noticetype数据层接口。
        ///// </summary>
        //public static Istwh_noticetypeDAL Createstwh_noticetype()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_noticetypeDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_noticetypeDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_order数据层接口。
        ///// </summary>
        //public static Istwh_orderDAL Createstwh_order()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_orderDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_orderDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_orderdetails数据层接口。
        ///// </summary>
        //public static Istwh_orderdetailsDAL Createstwh_orderdetails()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_orderdetailsDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_orderdetailsDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_product数据层接口。
        ///// </summary>
        //public static Istwh_productDAL Createstwh_product()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_productDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_productDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_productbrand数据层接口。
        ///// </summary>
        //public static Istwh_productbrandDAL Createstwh_productbrand()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_productbrandDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_productbrandDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_producttype数据层接口。
        ///// </summary>
        //public static Istwh_producttypeDAL Createstwh_producttype()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_producttypeDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_producttypeDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_producttype_p数据层接口。
        ///// </summary>
        //public static Istwh_producttype_pDAL Createstwh_producttype_p()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_producttype_pDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_producttype_pDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_producttype_pv数据层接口。
        ///// </summary>
        //public static Istwh_producttype_pvDAL Createstwh_producttype_pv()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_producttype_pvDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_producttype_pvDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_questions数据层接口。
        ///// </summary>
        //public static Istwh_questionsDAL Createstwh_questions()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_questionsDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_questionsDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_questionslist数据层接口。
        ///// </summary>
        //public static Istwh_questionslistDAL Createstwh_questionslist()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_questionslistDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_questionslistDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_questionslog数据层接口。
        ///// </summary>
        //public static Istwh_questionslogDAL Createstwh_questionslog()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_questionslogDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_questionslogDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_qutype数据层接口。
        ///// </summary>
        //public static Istwh_qutypeDAL Createstwh_qutype()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_qutypeDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_qutypeDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_recruit数据层接口。
        ///// </summary>
        //public static Istwh_recruitDAL Createstwh_recruit()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_recruitDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_recruitDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_Resources数据层接口。
        ///// </summary>
        //public static Istwh_ResourcesDAL Createstwh_Resources()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_ResourcesDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_ResourcesDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_roleinfo数据层接口。
        ///// </summary>
        //public static Istwh_roleinfoDAL Createstwh_roleinfo()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_roleinfoDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_roleinfoDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_shopcart数据层接口。
        ///// </summary>
        //public static Istwh_shopcartDAL Createstwh_shopcart()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_shopcartDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_shopcartDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_staff数据层接口。
        ///// </summary>
        //public static Istwh_staffDAL Createstwh_staff()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_staffDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_staffDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_static数据层接口。
        ///// </summary>
        //public static Istwh_staticDAL Createstwh_static()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_staticDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_staticDAL)objType;
        //}

        ///// <summary>
        ///// 创建stwh_userinfo数据层接口。
        ///// </summary>
        //public static Istwh_userinfoDAL Createstwh_userinfo()
        //{
        //    string ClassNamespace = AssemblyPath + ".stwh_userinfoDAL";
        //    object objType = CreateObject(AssemblyPath, ClassNamespace);
        //    return (Istwh_userinfoDAL)objType;
        //}
        #endregion

        #region 通过反射创建接口
        /// <summary>
        /// 创建指定名称接口类
        /// </summary>
        /// <param name="className">数据访问名称</param>
        /// <returns></returns>
        public static object CreateIDAL(string className)
        {
            string ClassNamespace = AssemblyPath + "." + className;
            object objType = CreateObject(AssemblyPath, ClassNamespace);
            return objType;
        }
        #endregion
    }
}

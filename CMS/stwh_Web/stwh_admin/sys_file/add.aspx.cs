﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_file
{
    public partial class add : Common.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
				SetMenuSpanText(this.menuSpan, "sys_file", "文章管理");
                List<stwh_filetype> AllListData = new stwh_filetypeBLL().GetModelList("");
                StringBuilder listSave = new StringBuilder();
                foreach (stwh_filetype item in AllListData)
                {
                    listSave.Append("<li><a data-pid=\"" + item.stwh_ftid + "\">" + item.stwh_ftname + "</a></li>");
                }
                this.selectShowList.InnerHtml = listSave.ToString();
            }
            catch (Exception)
            {

            }
        }
    }
}
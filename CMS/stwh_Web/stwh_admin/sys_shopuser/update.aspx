﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="update.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_shopuser.update" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <link href="/plugin/jQuery-colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">会员管理</span></li>
        <li class="active">修改数据</li>
    </ol>
    <div id="scrollPanel">
        <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx" method="post" role="form">
            <div class="container-fluid">
                <div class="row">
                    <input type="hidden" value="<%=UpdateModel.stwh_buid %>" name="stwh_buid" id="stwh_baid" />
                    <input type="hidden" value="<%=UpdateModel.stwh_buaddtime %>" name="stwh_buaddtime" id="stwh_buaddtime" />
                    <input type="hidden" value="<%=UpdateModel.stwh_buopenid %>" name="stwh_buopenid" id="stwh_buopenid" />
                    <input type="hidden" value="<%=UpdateModel.stwh_buqqid %>" name="stwh_buqqid" id="stwh_buqqid" />
                    <input type="hidden" value="<%=UpdateModel.stwh_buwbo %>" name="stwh_buwbo" id="stwh_buwbo" />
                    <input type="hidden" value="<%=UpdateModel.stwh_buimg %>" name="stwh_buimg" id="stwh_buimg" />
                    <div class="col-sm-1 st-text-right">
                        会员状态：
                    </div>
                    <div class="col-sm-11 form-group">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-primary btn-sm <%=UpdateModel.stwh_bustatus==0?"active":"" %>">
                                <input type="radio" name="stwh_bustatus" id="stwh_bustatus0" value="0" <%=UpdateModel.stwh_bustatus == 0?"checked=\"checked\"":"" %>/>
                                启用
                            </label>
                            <label class="btn btn-primary btn-sm <%=UpdateModel.stwh_bustatus==1?"active":"" %>">
                                <input type="radio" name="stwh_bustatus" id="stwh_bustatus1" value="1" <%=UpdateModel.stwh_bustatus == 1?"checked=\"checked\"":"" %>/>
                                禁用
                            </label>
                        </div>
                        <span class="text-danger">默认启用</span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        会员昵称：
                    </div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_bunicheng" name="stwh_bunicheng" value="<%=UpdateModel.stwh_bunicheng %>" class="st-input-text-300 form-control" placeholder="请输入会员昵称" />
                        <span class="text-danger"></span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        会员姓名：
                    </div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_buname" name="stwh_buname" value="<%=UpdateModel.stwh_buname %>" class="st-input-text-300 form-control" style="margin-right: 15px;" placeholder="请输入会员姓名" />
                        <span class="text-danger"></span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        会员性别：
                    </div>
                    <div class="col-sm-11 form-group">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-primary btn-sm <%=UpdateModel.stwh_busex==0?"active":"" %>">
                                <input type="radio" name="stwh_busex" id="stwh_busex0" value="0" <%=UpdateModel.stwh_busex == 0?"checked=\"checked\"":"" %>/>
                                先生
                            </label>
                            <label class="btn btn-primary btn-sm <%=UpdateModel.stwh_busex==1?"active":"" %>">
                                <input type="radio" name="stwh_busex" id="stwh_busex1" value="1" <%=UpdateModel.stwh_busex == 1?"checked=\"checked\"":"" %>/>
                                女生
                            </label>
                        </div>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        会员邮箱：
                    </div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_buemail" name="stwh_buemail" value="<%=UpdateModel.stwh_buemail %>" class="st-input-text-300 form-control" placeholder="请输入邮箱" />
                        <span class="text-danger"></span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        会员手机：
                    </div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_bumobile" name="stwh_bumobile" value="<%=UpdateModel.stwh_bumobile %>" maxlength="11" onkeydown="return checkNumber(event);" class="st-input-text-300 form-control" placeholder="请输入会员手机" />
                        <span class="text-danger">手机不能重复，否则无法添加数据，必填项</span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        登录密码：
                    </div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_bupwd" name="stwh_bupwd" value="<%=UpdateModel.stwh_bupwd %>" class="st-input-text-300 form-control" placeholder="请输入登录密码" />
                        <span class="text-danger">默认666666，密码长度6-12位，必填项</span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        生日：
                    </div>
                    <div class="col-sm-11 form-group">
                        <div class=" input-group st-input-text-300">
                            <input type="text" name="stwh_bushengri" id="stwh_bushengri" placeholder="请输入生日" class="form-control" value="<%=UpdateModel.stwh_bushengri.ToString("yyyy-MM-dd HH:mm:ss") %>">
                            <span id="calendar" class=" input-group-addon" style="cursor: pointer;"><span class="fa fa-calendar"></span></span>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-edit"></i>修改</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script type="text/javascript">
        $(function () {
            $("#calendar").click(function () {
                $('#stwh_bushengri').click();
            });
            $('#stwh_bushengri').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                autoApply: true,
                autoUpdateInput: false
            }).on({
                "apply.daterangepicker": function (ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD'));
                }
            });
            $("#addMenu").click(function () {
                var stwh_bumobile = $("#stwh_bumobile").val();
                if (!stwh_bumobile) {
                    $.bs.alert("请选择手机号码！", "info");
                    return false;
                }
                var stwh_bupwd = $("#stwh_bupwd").val();
                if (!stwh_bupwd) {
                    $.bs.alert("请选择会员登录密码！", "info");
                    return false;
                }
                else if (stwh_bupwd.length < 6 || stwh_bupwd.length > 12) {
                    $.bs.alert("会员登录密码格式错误！", "info");
                    return false;
                }

                $.post("/Handler/stwh_admin/sys_shopuser/update.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "-1");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>
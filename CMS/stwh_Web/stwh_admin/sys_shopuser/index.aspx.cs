﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_shopuser
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_shopuser", "会员管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);
                stwh_userinfo model = Session["htuser"] as stwh_userinfo;
                string ifstr = "1= 1";
                int ss = 0, cc = 0;
                List<stwh_buyuser> ListData = new stwh_buyuserBLL().GetListByPage<stwh_buyuser>("stwh_buid", "desc", ifstr, 15, 0, ref ss, ref cc, 0);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                string strDatas = "";
                foreach (stwh_buyuser item in ListData)
                {
                    strDatas += "<tr data-id=\"" + item.stwh_buid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_buid + "\" value=\"" + item.stwh_buid + "\" /></td><td>" + item.stwh_buid + "</td><td>" + item.stwh_bunicheng + "---[" + (item.stwh_bustatus == 1 ? "<span style=\"color:red;\">禁用</span>" : "<span style=\"color:green;\">启用</span>") + "]</td><td>" + item.stwh_buname + "</td><td>" + item.stwh_buemail + "</td><td>" + item.stwh_bumobile + "</td><td>" + item.stwh_buaddtime.ToString("yyyy-MM-dd HH:mm:ss") + "</td></tr>";
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"7\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);
            }
        }
    }
}
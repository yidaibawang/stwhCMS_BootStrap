﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_QuestionList
{
    public partial class update : Common.PageBase
    {
        public stwh_questionslist UpdateModel = new stwh_questionslist();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string mid = Request.QueryString["id"];
                if (PageValidate.IsNumber(mid))
                {
                    SetMenuSpanText(this.menuSpan, "sys_QuestionList", "试题选项管理");
                    UpdateModel = new stwh_questionslistBLL().GetModel(int.Parse(mid));
                    List<stwh_questions> AllListData = new stwh_questionsBLL().GetModelList("1 = 1 order by stwh_quorder desc,stwh_quaddtime desc");
                    StringBuilder listSave = new StringBuilder();
                    foreach (stwh_questions item in AllListData)
                    {
                        listSave.Append("<li><a data-pid=\"" + item.stwh_quid + "\">" + item.stwh_qutitle + "</a></li>");
                    }
                    this.selectShowList.InnerHtml = listSave.ToString();
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
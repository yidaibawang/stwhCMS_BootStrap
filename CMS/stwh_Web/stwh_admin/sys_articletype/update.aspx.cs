﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_articletype
{
    public partial class update : Common.PageBase
    {
        public stwh_artype UpdateModel = new stwh_artype();

        /// <summary>
        /// 获取文章栏目（并判断一级栏目下是否有子栏目）
        /// </summary>
        /// <param name="datasource">数据源</param>
        /// <param name="listSave">储存</param>
        /// <param name="pid">父id</param>
        /// <param name="dj">栏目等级</param>
        private void ArticleList(List<stwh_artype> datasource, StringBuilder listSave, int pid, int dj)
        {
            if (listSave.Length == 0)
            {
                listSave.Append("<li><a data-pid=\"0\">(1).一级栏目</a></li>");
            }
            //筛选出一级栏目
            List<stwh_artype> listadd = new List<stwh_artype>();
            foreach (stwh_artype item in datasource)
            {
                if (item.stwh_artparentid == pid) listadd.Add(item);
            }
            if (listadd.Count != 0)
            {
                //循环递归判断当前栏目下是否有子栏目
                foreach (stwh_artype item in listadd)
                {
                    listSave.Append("<li><a data-pid=\"" + item.stwh_artid + "\">");
                    for (int i = 0; i < dj; i++) listSave.Append("&nbsp;&nbsp;");
                    listSave.Append("(" + (dj + 1) + ")." + item.stwh_artname + "</a></li>");
                    ArticleList(datasource, listSave, item.stwh_artid, dj + 1);
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string mid = Request.QueryString["id"];
                if (PageValidate.IsNumber(mid))
                {
					SetMenuSpanText(this.menuSpan, "sys_articletype", "文章管理");
                    UpdateModel = new stwh_artypeBLL().GetModel(int.Parse(mid));
                    List<stwh_artype> AllListData = new stwh_artypeBLL().GetModelList("");
                    StringBuilder sb = new StringBuilder();
                    ArticleList(AllListData, sb, 0, 0);
                    if (UpdateModel.stwh_artparentid == 0) this.selectShowList.Visible = false;
                    else this.selectShowList.InnerHtml = sb.ToString();

                    if (UpdateModel.stwh_artparentid == 0) this.selectShowText.InnerText = "一级栏目";
                    else this.selectShowText.InnerText = AllListData.Where(a => a.stwh_artid == UpdateModel.stwh_artparentid).ToList<stwh_artype>()[0].stwh_artname;
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
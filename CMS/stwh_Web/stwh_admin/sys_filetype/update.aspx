﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="update.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_filetype.update" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">文章栏目管理</span></li>
        <li class="active">修改数据</li>
    </ol>
    <div id="scrollPanel">
    <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx"
    method="post" role="form">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-1 st-text-right">
                缩略图：</div>
            <div class="col-sm-11 form-group">
                <input type="hidden" name="stwh_ftid" id="stwh_ftid" value="<%=UpdateModel.stwh_ftid %>" />
                <input id="stwh_ftimg" name="stwh_ftimg" type="text" class="st-input-text-700 form-control" value="<%=UpdateModel.stwh_ftimg %>" placeholder="请输入上传缩略图" style="float: left;" />
                <div style="float: left;width: 100px;">
                    <div id="upwebico">
                    </div>
                </div>
                <div class="clearfix"></div>
                <div id="progressFile" class="progress progress-striped active" style=" display:none;">
                    <div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">
                    </div>
                </div>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                序号：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_ftorder" name="stwh_ftorder" onkeydown="return checkNumber(event);" value="<%=UpdateModel.stwh_ftorder %>" class="st-input-text-300 form-control" />
                <span class="text-danger">*越大越考前</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                分类名称：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_ftname" name="stwh_ftname" value="<%=UpdateModel.stwh_ftname %>"  class="st-input-text-700 form-control" placeholder="请输入文件分类名称" />
                
                <span class="text-danger">*文件分类名称内容长度不能超过100个字符</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                分类描述：</div>
            <div class="col-sm-11 form-group">
                <textarea id="stwh_ftdescription" name="stwh_ftdescription" class="st-input-text-700 form-control" placeholder="请输入文件分类描述" ><%=UpdateModel.stwh_ftdescription%></textarea>
                
                <span class="text-danger">非必填，内容长度不能超过100字符</span>
            </div>
        </div>
    </div>
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-edit"></i> 修改</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script src="/Plugin/swfobject.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $("#addMenu").click(function () {
                var stwh_ftimg = $("#stwh_ftimg").val();
                if (stwh_ftimg.length > 200) {
                    $.bs.alert("图片路径长度超出服务器限制！", "info");
                    return false;
                }
                var stwh_ftname = $("#stwh_ftname").val();
                if (!stwh_ftname) {
                    $.bs.alert("请输入栏目名称！", "info");
                    return false;
                }
                else if (!IsHanZF(1, 100, stwh_ftname)) {
                    $.bs.alert("栏目名称长度超出服务器限制！", "info");
                    return false;
                }
                var stwh_ftdescription = $("#stwh_ftdescription").val();
                if (stwh_ftdescription) {
                    if (!IsHanZF(1, 100, stwh_ftdescription)) {
                        $.bs.alert("Banner图片描述长度超出服务器限制！", "info");
                        return false;
                    }
                }
                $.post("/Handler/stwh_admin/sys_filetype/update.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "-1");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <script type="text/javascript">
        var auth = "<% = Request.Cookies[FormsAuthentication.FormsCookieName]==null ? string.Empty : Request.Cookies[FormsAuthentication.FormsCookieName].Value %>";
        var ASPSESSID = "<%= Session.SessionID %>";
        var flashvarsVoice = {
            IsSelect: "false", //运行多选文件进行上传
            Url: "/Handler/uploadHandler.ashx",
            FileExtension: "*.jpg;*.JPG;*.gif;*.GIF;*.png;*.PNG",
            FileType: "image",
            MaxSize: "<%=WebSite.Webthumbnail %>" //500KB 字节为单位
        };
        var params = {
            wmode: "transparent",
            play: true,
            loop: true,
            menu: true,
            devicefont: false,
            scale: "showall",
            quality: "high",
            bgcolor: "#ffffff",
            allowScriptAccess: "sameDomain",
            salign: ""
        };
        swfobject.embedSWF("/Plugin/upload.swf", "upwebico", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsVoice, { btnid: "upwebico", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);

        //设置进度
        function setProgress(info, name, IsSelect, btnid) {
            $("#progressFile").show().children().first().css("width", info + "%");
        }

        //上传完成时调用的方法（该方法被flash所调用）
        //filename:文件安上传成功后返回的文件名，包含扩展名,altString提示信息,msg服务器返回的成功消息
        function EndUpload(filename, altString, msg, btnid) {
            if (msg == "NoFile") {
                $.bs.alert("请选择文件！", "info");
            }
            else if (msg == "NoType") {
                $.bs.alert("请选择指定类型文件！", "info");
            }
            else if (msg == "NoSize") {
                $.bs.alert("文件大小超出限制，请联系管理员！", "info");
            }
            else if (msg == "login") {
                $.bs.alert("请先登录！", "info");
            }
            else if (msg == "Exception") {
                $.bs.alert("请稍后操作！", "info");
            }
            else {
                if ((/image/gi).test(msg)) {
                    if (btnid == "upwebico") {
                        $("#stwh_ftimg").val(msg);
                    }
                }
                else {
                    $.bs.alert(msg, "info");
                }
            }
        }

        //上传失败时调用的方法（该方法被flash所调用）
        function ErrorUpload(msg) {
            $.bs.alert(msg, "info");
        }
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>
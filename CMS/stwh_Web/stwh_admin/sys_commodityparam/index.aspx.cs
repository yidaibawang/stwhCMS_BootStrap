﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_commodityparam
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_commodityparam", "商品参数管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);
                int ss = 0, cc = 0;
                List<stwh_producttype_p> ListData = new stwh_producttype_pBLL().GetListByPage<stwh_producttype_p>("stwh_ptpid", "desc", "1=1", 15, 0, ref ss, ref cc, 1);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                string strDatas = "";
                foreach (stwh_producttype_p item in ListData)
                {
                    strDatas += "<tr data-id=\"" + item.stwh_ptpid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_ptpid + "\" value=\"" + item.stwh_ptpid + "\" /></td><td>" + item.stwh_ptpid + "</td><td>[" + item.stwh_ptname + "] " + item.stwh_ptptitle + "</td><td>" + item.stwh_ptpname + "</td><td>" + item.stwh_ptptype + "</td><td><input type=\"text\" min=\"0\" value=\"" + item.stwh_ptporder + "\" onkeydown=\"return checkNumber(event);\" style=\"width: 50px;\"/></td></tr>";
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"6\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);

                List<stwh_producttype> AllListData = new stwh_producttypeBLL().GetModelList("");
                StringBuilder sb = new StringBuilder();
                sb.Append("<li><a data-pid=\"0\">请选择</a></li>");
                ArticleList(AllListData, sb, 0, 0);
                this.selectShowList.InnerHtml = sb.ToString();
            }
        }

        /// <summary>
        /// 获取文章栏目（并判断一级栏目下是否有子栏目）
        /// </summary>
        /// <param name="datasource">数据源</param>
        /// <param name="listSave">储存</param>
        /// <param name="pid">父id</param>
        /// <param name="dj">栏目等级</param>
        private void ArticleList(List<stwh_producttype> datasource, StringBuilder listSave, int pid, int dj)
        {
            //筛选出一级栏目
            List<stwh_producttype> listadd = new List<stwh_producttype>();
            foreach (stwh_producttype item in datasource)
            {
                if (item.stwh_ptparentid == pid) listadd.Add(item);
            }
            if (listadd.Count != 0)
            {
                //循环递归判断当前栏目下是否有子栏目
                foreach (stwh_producttype item in listadd)
                {
                    listSave.Append("<li><a data-pid=\"" + item.stwh_ptid + "\">");
                    for (int i = 0; i < dj; i++) listSave.Append("&nbsp;&nbsp;");
                    listSave.Append("(" + (dj + 1) + ")." + item.stwh_ptname + "</a></li>");
                    ArticleList(datasource, listSave, item.stwh_ptid, dj + 1);
                }
            }
        }
    }
}
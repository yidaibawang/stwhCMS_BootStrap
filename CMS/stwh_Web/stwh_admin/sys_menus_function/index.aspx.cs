﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//导入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_menus_function
{
    public partial class index : Common.PageBase
    {
        /// <summary>
        /// 查询出指定深度的菜单
        /// </summary>
        /// <param name="MenuData">菜单数据</param>
        /// <param name="saveMenu">保存指定深度的菜单对象</param>
        /// <param name="Menu">递归查询出来的对象</param>
        /// <param name="menuparentID">父菜单id</param>
        private void ChkMenu(List<stwh_menuinfo> MenuData, List<stwh_menuinfo> saveMenu, stwh_menuinfo Menu, int menuparentID)
        {
            List<stwh_menuinfo> listdata = MenuData.Where(a => a.stwh_menuparentID == menuparentID).ToList<stwh_menuinfo>();
            if (listdata.Count == 0) saveMenu.Add(Menu);
            foreach (stwh_menuinfo item in listdata)
            {
                ChkMenu(MenuData, saveMenu, item, item.stwh_menuid);
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_menus_function", "菜单功能管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);
                List<stwh_menuinfo> childMenu = new List<stwh_menuinfo>();
                ChkMenu(GetMenuList(), childMenu, null, 0);
                List<stwh_function> listfunction = GetFunctionList();
                //序列化功能列表
                this.hidFunctionData.Value = JsonConvert.SerializeObject(listfunction);
                #region 生成功能列表数据
                string functionData = "";
                for (int i = 0; i < childMenu.Count; i++)
                {
                    functionData += "<h4><i class=\"" + childMenu[i].stwh_menuICO_url + "\"></i> " + childMenu[i].stwh_menuname + "</h4><p class=\"lead\"><table class=\"table table-striped table-bordered table-hover table-condensed\"><thead><tr><th style=\"width: 50px;\"><input type=\"checkbox\"class=\"AllChkBox\" id=\"parentFunction" + childMenu[i].stwh_menuid + "\" data-mid=\"" + childMenu[i].stwh_menuid + "\"/></th><th style=\"width: 100px;\">图标</th><th style=\"width: 150px;\">名称</th><th style=\"width: 150px;\">ID名称</th><th style=\"width: 200px;\">URL</th><th style=\"width: 250px;\">备注</th><th style=\"width: 200px;\">序号</th><th>状态</th></tr></thead><tbody>";
                    for (int j = 0; j < listfunction.Count; j++)
                    {
                        if (childMenu[i].stwh_menuid == listfunction[j].stwh_menuid)
                        {
                            functionData += "<tr data-mid=\"" + listfunction[j].stwh_fid + "\"><td><input type=\"checkbox\" name=\"chkc\" class=\"chkc" + childMenu[i].stwh_menuid + "\" value=\"" + listfunction[j].stwh_fid + "\" data-mid=\"" + childMenu[i].stwh_menuid + "\" /></td><td><span class=\"" + listfunction[j].stwh_fico + "\"></span></td><td>" + listfunction[j].stwh_fname + "</td><td>" + listfunction[j].stwh_idname + "</td><td>" + listfunction[j].stwh_furl + "</td><td>" + listfunction[j].stwh_fremark + "</td><td><input type=\"text\" min=\"0\" value=\"" + listfunction[j].stwh_forder + "\" onkeydown=\"return checkNumber(event);\" style=\"width:50px;\"/></td><td>" + (listfunction[j].stwh_fishow == 0 ? "<div class=\"btn-group\"data-toggle=\"buttons\"><label class=\"btn btn-primary btn-sm active\"><input type=\"radio\"name=\"radp" + listfunction[j].stwh_fid + "\"value=\"0\" checked=\"checked\"/>显示</label><label class=\"btn btn-primary btn-sm\"><input type=\"radio\"name=\"radp" + listfunction[j].stwh_fid + "\"value=\"1\"/>隐藏</label></div>" : "<div class=\"btn-group\"data-toggle=\"buttons\"><label class=\"btn btn-primary btn-sm\"><input type=\"radio\"name=\"radp" + listfunction[j].stwh_fid + "\"value=\"0\"/>显示</label><label class=\"btn btn-primary btn-sm active\"><input type=\"radio\"name=\"radp" + listfunction[j].stwh_fid + "\"value=\"1\"/>隐藏</label></div>") + "</td></tr>";
                        }
                    }
                    functionData += "</tbody></table></p>";
                }
                this.functionContent.InnerHtml = functionData;
                #endregion
            }
        }
    }
}
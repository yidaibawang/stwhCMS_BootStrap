﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_menus_function
{
    public partial class update : Common.PageBase
    {
        public stwh_function UpdateModel = new stwh_function();
        public string parentMenuName = "请选择";

        /// <summary>
        /// 生成菜单
        /// </summary>
        /// <param name="menusData">菜单数据</param>
        /// <param name="SaveList">存放菜单集合</param>
        /// <param name="code">父菜单id</param>
        /// <param name="dj">等级</param>
        private void CreateMenus(List<stwh_menuinfo> menusData, StringBuilder SaveList, int code, int dj)
        {
            //查询指定父编号的菜单，并按照序号进行升序排序
            List<stwh_menuinfo> chkmenulist = menusData.Where(a => a.stwh_menuparentID == code).OrderByDescending(a => a.stwh_menuorder).ToList<stwh_menuinfo>();
            if (chkmenulist.Count != 0)
            {
                foreach (stwh_menuinfo item in chkmenulist)
                {
                    string nullstring = "├";
                    for (int i = 0; i < dj; i++) nullstring += "──";
                    SaveList.Append("<li><a data-pid=\"" + item.stwh_menuid + "\">" + nullstring + "&nbsp;<span class=\"glyphicon " + item.stwh_menuICO_url + "\"></span> <span class=\"menuName\">" + item.stwh_menuname + "</span></a></li>");
                    CreateMenus(menusData, SaveList, item.stwh_menuid, dj + 1);
                }
            }
            else dj = dj - 1;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SetMenuSpanText(this.menuSpan, "sys_menus_function", "菜单功能管理");
                #region 获取菜单
                List<stwh_menuinfo> allmenu = GetMenuList();
                //获取父菜单数据
                List<stwh_menuinfo> menuparentlist = allmenu.Where(a => a.stwh_menuparentID != 0).OrderBy(a => a.stwh_menuorder).ToList<stwh_menuinfo>();
                StringBuilder sb = new StringBuilder();
                sb.Append("<li><a data-pid=\"-1\">├&nbsp;<span class=\"glyphicon glyphicon-th-list\"></span> <span class=\"menuName\">请选择</span></a></li>");
                CreateMenus(allmenu, sb, 0, 0);
                this.selectShowListMenu.InnerHtml = sb.ToString();
                #endregion
                string mid = Request.QueryString["id"];
                if (PageValidate.IsNumber(mid))
                {
                    List<stwh_function> modellist = GetFunctionList().Where(a => a.stwh_fid == int.Parse(mid)).ToList<stwh_function>();
                    if (modellist.Count == 0)
                    {
                        modellist = new stwh_functionBLL().GetModelList("");
                        DataCache.SetCache("cacheFunction", modellist);
                        UpdateModel = modellist.Where(a => a.stwh_fid == int.Parse(mid)).ToList<stwh_function>()[0];
                    }
                    else
                    {
                        UpdateModel = modellist[0];
                    }
                    parentMenuName = menuparentlist.Where(a => a.stwh_menuid == UpdateModel.stwh_menuid).ToList<stwh_menuinfo>()[0].stwh_menuname;
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
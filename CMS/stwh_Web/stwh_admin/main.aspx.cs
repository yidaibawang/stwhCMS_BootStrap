﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin
{
    public partial class main : Common.PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.litkey.InnerText = stwh_Web.stwh_admin.Common.WebSite.LoadWebSite().WebKey;
                this.litkeytime.InnerText = stwh_Web.stwh_admin.Common.WebSite.LoadWebSite().WebKeyTime;
                this.litkeytimeday.InnerText = (DateTime.Parse(stwh_Web.stwh_admin.Common.WebSite.LoadWebSite().WebKeyTime) - DateTime.Now).Days + "";
                #region 调用远程服务器系统公告内容
                //StwhServiceSoapClient clientservice = new StwhServiceSoapClient();
                //string result = clientservice.GetNotice(stwh_Web.stwh_admin.Common.WebSite.LoadWebSite().WebKey);
                //if (!string.IsNullOrEmpty(result))
                //{
                //    stwh_result resultobj = new stwh_result();
                //    resultobj = JsonConvert.DeserializeObject<stwh_result>(result);
                //    if (resultobj.msgcode == (0+""))
                //    {
                //        this.hidAllData.Value = resultobj.msg.ToString();
                //        List<stwh_notice> resultlist = JsonConvert.DeserializeObject<List<stwh_notice>>(resultobj.msg.ToString());
                //        string strDatas = "";
                //        foreach (stwh_notice item in resultlist)
                //        {
                //            strDatas += "<li class=\"list-group-item\"><a class=\"text-primary\" data-id=\"" + item.stwh_noid + "\"><span class=\"glyphicon glyphicon-comment\"></span> " + item.stwh_notitle + "----" + item.stwh_noaddtime.ToString("yyyy-MM-dd") + "</a></li>";
                //        }
                //        this.NoticeList.InnerHtml = strDatas;
                //    }
                //}
                #endregion
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息：" + ex.StackTrace);
            }
        }
    }
}
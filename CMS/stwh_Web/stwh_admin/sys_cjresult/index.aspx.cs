﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_cjresult
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_cjresult", "抽奖结果管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);
                int ss = 0, cc = 0;
                List<stwh_cjresult> ListData = new stwh_cjresultBLL().GetListByPage<stwh_cjresult>("stwh_cjrid", "desc", "1=1", 15, 0, ref ss, ref cc, 0);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                string strDatas = "";
                foreach (stwh_cjresult item in ListData)
                {
                    strDatas += "<tr data-id=\"" + item.stwh_cjrid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_cjrid + "\" value=\"" + item.stwh_cjrid + "\" /></td><td>" + item.stwh_cjrid + "</td><td>" + item.stwh_cjrmobile + "</td><td>" + item.stwh_cjraddtime.ToString("yyyy-MM-dd hh:mm:ss") + "</td></tr>";
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"4\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
                this.hidAllData.Value = JsonConvert.SerializeObject(ListData);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_cjuser
{
    public partial class update : Common.PageBase
    {
        public stwh_cjtel UpdateModel = new stwh_cjtel();

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                SetMenuSpanText(this.menuSpan, "sys_cjuser", "人员管理");
                string mid = Request.QueryString["id"];
                if (PageValidate.IsNumber(mid)) UpdateModel = new stwh_cjtelBLL().GetModel(int.Parse(mid));
            }
            catch (Exception)
            {

            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using stwh_Common.DEncrypt;
using System.Xml;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin
{
    public partial class index : Common.PageBase
    {
        /// <summary>
        /// 生成菜单(第一个父菜单下的子菜单)
        /// </summary>
        /// <param name="menusData">菜单数据</param>
        /// <param name="SaveList">存放菜单集合</param>
        /// <param name="code">父菜单id</param>
        private void CreateMenus(List<stwh_menuinfo> menusData, List<string> SaveList, int code)
        {
            //查询指定父编号的菜单，并按照序号进行升序排序
            foreach (stwh_menuinfo mm in menusData.Where(a => a.stwh_menuparentID == code).OrderByDescending(a => a.stwh_menuorder).ToList<stwh_menuinfo>())
            {
                SaveList.Add("<div class=\"acchead\" data-toggle=\"collapse\" data-target=\"#MenuPanel" + mm.stwh_menuid + "\" ><span class=\"" + mm.stwh_menuICO_url + "\"></span>" + mm.stwh_menuname + "<span class=\"glyphicon glyphicon-menu-down pull-right\"></span></div>");
                SaveList.Add("<div id=\"MenuPanel" + mm.stwh_menuid + "\" class=\"collapse accmenu in\">");
                foreach (stwh_menuinfo cc in menusData.Where(a => a.stwh_menuparentID == mm.stwh_menuid).OrderByDescending(a => a.stwh_menuorder).ToList<stwh_menuinfo>())
                {
                    SaveList.Add("<a class=\"childrenm\" href=\"/stwh_admin/" + cc.stwh_menuURL + "?mid=" + cc.stwh_menuid + "&wxuid=" + Request.QueryString["wxuid"] + "\">&nbsp;&nbsp;&nbsp;<span class=\"" + cc.stwh_menuICO_url + "\"></span>" + cc.stwh_menuname + " </a>");
                }
                SaveList.Add("</div>");
            }
        }

        /// <summary>
        /// 将id集合转换为字符串（以英文逗号隔开）
        /// </summary>
        /// <param name="listMenuIds">当前角色拥有的菜单集合</param>
        /// <returns></returns>
        private string CreateIds(List<stwh_menuinfo> listIds)
        {
            string jbmids = "";
            foreach (stwh_menuinfo item in listIds)
            {
                jbmids += (item.stwh_menuid + ",");
            }
            return jbmids != "" ? jbmids.Substring(0, jbmids.Length - 1) : "";
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                stwh_userinfo model = Session["htuser"] as stwh_userinfo;
                this.imgHead.ImageUrl = model.stwh_uiportrait;
                this.labName.Text = model.stwh_uiname;
                this.labTime.Text = model.stwh_uictime.ToString("yyyy-MM-dd");

                List<stwh_menuinfo> MenuList = new List<stwh_menuinfo>();
                //是超级管理员，获取全部菜单,包含属性隐藏的
                if (model.stwh_uiid == 1) MenuList = this.GetMenuList();
                else MenuList = new stwh_menuinfoBLL().GetModelList(model.stwh_rid);
                this.hidRoleMenuData.Value = CreateIds(MenuList);

                List<stwh_menuinfo> onemenulist = MenuList.Where(a => a.stwh_menuparentID == 0).OrderByDescending(a => a.stwh_menuorder).ToList<stwh_menuinfo>();
                List<string> saveList = new List<string>();
                if (onemenulist.Count > 0) CreateMenus(MenuList, saveList, onemenulist[0].stwh_menuid);
                string menustr = "";
                foreach (string item in saveList)
                {
                    menustr += item;
                }
                this.accordion.InnerHtml = menustr;
                //获取所有菜单数据并序列化json字符串
                this.hidAllMenuData.Value = JsonConvert.SerializeObject(this.GetMenuList());
            }
        }

        protected void exitOk_Click(object sender, EventArgs e)
        {
            Session.Remove("htuser");
            Session.Remove("Identify");
            Response.Redirect("/stwh_admin/invalid.htm");
        }
    }
}
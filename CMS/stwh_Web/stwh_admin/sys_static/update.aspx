﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="update.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_static.update" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">静态内容管理</span></li>
        <li class="active">修改数据</li>
    </ol>
    <div id="scrollPanel">
    <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx"
    method="post" role="form">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-1 st-text-right">
                标题：</div>
            <div class="col-sm-11 form-group">
                <input type="hidden" value="<%=UpdateModel.stwh_stid %>" name="stwh_stid" id="stwh_stid" />
            <input type="hidden" value="<%=UpdateModel.stwh_staddtime %>" name="stwh_staddtime" id="stwh_staddtime" />
                <input type="text" id="stwh_sttitle" name="stwh_sttitle"  value="<%=UpdateModel.stwh_sttitle%>" class="st-input-text-700 form-control" />
                <span class="text-danger">*必填项（静态标题修改后，请更新模板中的相应参数，以免造成数据无法显示！）</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                简介内容：</div>
            <div class="col-sm-11 form-group">
                <textarea id="stwh_stremark" name="stwh_stremark" class="st-input-text-700 form-control"><%=UpdateModel.stwh_stremark%></textarea>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                静态内容：</div>
            <div class="col-sm-11 form-group">
                <div id="stwh_stcontentPanel" style=" display:none;"><%=UpdateModel.stwh_stcontent%></div>
                <textarea id="stwh_stcontent" name="stwh_stcontent"  style="width:700px; height:300px;"></textarea>
            </div>
        </div>
    </div>
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-edit"></i> 修改</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <cms:stwhuescript ID="stwhuescript" runat="server" />
    <script type="text/javascript">
        //初始化富文本编辑器
        var qjOptions = {
            // 服务器统一请求接口路径
            serverUrl: "/handler/ueditor/controller.ashx",
            wordCount: true, //是否开启字数统计
            maximumWords: 5000, //允许的最大字符数
            elementPathEnabled: true, //是否启用元素路径，默认是显示
            autoHeightEnabled: false, //是否自动长高,默认true
            zIndex: 900, //编辑器层级的基数,默认是900
            emotionLocalization: true, //是否开启表情本地化，默认关闭。若要开启请确保emotion文件夹下包含官网提供的images表情文件夹
            autoFloatEnabled: false, //当设置为ture时，工具栏会跟随屏幕滚动，并且始终位于编辑区域的最上方
            pasteplain: false, //是否默认为纯文本粘贴。false为不使用纯文本粘贴，true为使用纯文本粘贴
            enableAutoSave: true, //启用自动保存
            saveInterval: 3000, //自动保存间隔时间
            allowDivTransToP: false,
            toolbars: [
                        ['fullscreen', 'source', 'cleardoc', 'undo', 'redo', '|',
                        'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', '|',
                         'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', '|',
                        'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
                        'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                        'directionalityltr', 'directionalityrtl', 'indent', '|',
                        'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                        'touppercase', 'tolowercase', 'simpleupload', 'insertimage', '|',
                        'link', 'unlink', '|',
                        'horizontal', 'date', 'time', 'spechars', '|',
                        'inserttable', 'deletetable', 'preview']
                    ]//工具栏
        };
        var qjUEObject = UE.getEditor('stwh_stcontent', qjOptions);

        setTimeout(function () {
            //qjUEObject.execCommand('drafts'); //载入本地数据（若存在）
            qjUEObject.setContent($("#stwh_stcontentPanel").html(), false);
            $("#stwh_sttitle").focus();
            $(window).scrollTop(0);
        }, 500);
    </script>
    <script type="text/javascript">
        $(function () {
            $("#addMenu").click(function () {
                var stwh_sttitle = $("#stwh_sttitle").val();
                if (!stwh_sttitle) {
                    $.bs.alert("请输入静态标题！", "info");
                    return false;
                }
                var stwh_stremark = $("#stwh_stremark").val();
                if (stwh_stremark) {
                    if (stwh_stremark.length > 100) {
                        $.bs.alert("简介内容长度超出服务器限制！", "info");
                        return false;
                    }
                }
                $.post("/Handler/stwh_admin/sys_static/update.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        qjUEObject.setContent("");
                        //清除本地数据
                        qjUEObject.execCommand("clearlocaldata");
                        $.bs.alert(data.msg, "success","-1");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>
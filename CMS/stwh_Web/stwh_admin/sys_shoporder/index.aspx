﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_shoporder.index" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li class="active"><span id="menuSpan" runat="server">订单管理</span></li>
    </ol>
    <div class="m-top-10 p-bottom-15 p-left-15 scrollTop">
        <form class="form-inline">
        <asp:Literal ID="litBtnList" runat="server"></asp:Literal>
        <div id="searchDiv" class="collapse">
            <div class="form-group">
                <input type="hidden" id="stwh_orstatus" name="stwh_orstatus" value="-1" />
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <span id="stwh_orstatusText">请选择订单状态</span> <span class="caret"></span>
                </button>
                <ul id="stwh_orstatusList" class="dropdown-menu" role="menu" runat="server" style="max-height: 300px;
                    overflow: auto; left: auto; top: auto;">
                    <li><a data-pid="-1">请选择订单状态</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="0">待支付</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="1">已支付</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="2">已取消</a></li>
                </ul>
            </div>
            <div class="form-group">
                <input type="hidden" id="stwh_orpaystyle" name="stwh_orpaystyle" value="-1" />
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <span id="stwh_orpaystyleText">请选择支付方式</span> <span class="caret"></span>
                </button>
                <ul id="stwh_orpaystyleList" class="dropdown-menu" role="menu" runat="server" style="max-height: 300px;
                    overflow: auto; left: auto; top: auto;">
                    <li><a data-pid="-1">请选择支付方式</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="0">未知(订单生成)</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="1">微信支付</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="2">支付宝支付</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="3">银行卡支付</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="4">到付</a></li>
                </ul>
            </div>
            <div class="form-group">
                &nbsp;&nbsp;<input type="text" class="form-control" name="stwh_bumobile" id="stwh_bumobile" placeholder="请输入会员手机号码" />
            </div>
            <div class="form-group">
                &nbsp;&nbsp;<input type="text" class="form-control" name="stwh_orddid" id="stwh_orddid" placeholder="请输入订单单号" />
            </div>
            <div class="form-group">
                &nbsp;&nbsp;
                <div class=" input-group">
                    <input type="text" name="stwh_ortime" id="stwh_ortime" placeholder="请选择订单时间段"  class="form-control" value="">
                    <span id="calendar" class=" input-group-addon"  style=" cursor:pointer;"><span class="fa fa-calendar"></span></span>
                </div>
            </div>
            <div class="form-group">
                &nbsp;&nbsp;<a href="#" class="btn btn-default" id="btnsearchOk"><span class="glyphicon glyphicon-search"></span></a>
            </div>
        </div>
        </form>
    </div>
    <div id="scrollPanel">
    <form id="form1" runat="server" class="form-inline">
    <div class="container-fluid">
        <table class="table table-striped table-bordered table-hover table-condensed">
            <thead>
                <tr>
                    <th style="width: 50px;">
                        <input type="checkbox" id="allParent" />
                    </th>
                    <th style=" width:50px;">
                        编号
                    </th>
                    <th style=" width:100px;">
                        会员手机
                    </th>
                    <th style=" width:150px;">
                        订单编号
                    </th>
                    <th style=" width:80px;">
                        订单状态
                    </th>
                    <th style=" width:100px;">
                        订单支付方式
                    </th>
                    <th style=" width:200px;">
                        订单时间
                    </th>
                    <th>
                        详细地址
                    </th>
                    <th style=" width:200px;">
                        备注
                    </th>
                    <th style=" width:100px;">
                        操作
                    </th>
                </tr>
            </thead>
            <tbody id="ChildDatas" runat="server">
            </tbody>
        </table>
    </div>
    <asp:HiddenField ID="hidListId" runat="server" />
    <asp:HiddenField ID="hidAllData" runat="server" />
    <asp:HiddenField ID="hidTotalSum" runat="server" />
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
        <div class="form-group">
            <label>
                每页显示：</label>
            <select id="basic" class="selectpicker show-tick" data-width="55px"
                data-style="btn-sm btn-default">
                <option>15</option>
                <option>25</option>
                <option>35</option>
                <option>45</option>
                <option>55</option>
            </select>
        </div>
        <div class="form-group">
            <ul id="pageUl" style="margin: 0px auto;">
            </ul>
        </div>
        <div class="form-group">
            共 <span id="SumCountSpan"><asp:Literal ID="littotalSum" runat="server"></asp:Literal></span> 条数据
        </div>
        </form>
    </div>
    <div class="modal fade" id="deleteMessageModal" tabindex="-1" role="dialog" aria-labelledby="h2"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="h2">
                        <i class="fa fa-exclamation-circle"></i> 系统提示
                    </h4>
                </div>
                <div class="modal-body">
                    <h4>
                        <span class="glyphicon glyphicon-info-sign"></span>你确定要删除吗（删除后不可恢复）？</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        取消
                    </button>
                    <button type="button" class="btn btn-default" id="btnDeleteOk">
                        确定
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <div class="modal fade" id="productModal" tabindex="-1" role="dialog" aria-labelledby="h1"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="h1">
                    </h4>
                </div>
                <div class="modal-body">
                    <table class="table table-striped table-bordered table-hover table-condensed">
                        <thead>
                            <tr>
                                <th>
                                    商品名称
                                </th>
                                <th style=" width:100px;text-align:center;">
                                    商品单价
                                </th>
                                <th style=" width:100px;text-align:center;">
                                    购买数量
                                </th>
                            </tr>
                        </thead>
                        <tbody id="productTbody">
                        </tbody>
                    </table>
                    <div style="margin:10px 0px 0px 0px; font-size:18px;">订单总价：￥<span id="OrderSumPrice" style="font-weight:700;"></span></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        确定
                    </button>
                </div>
            </div>
        </div>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script type="text/javascript">
        var AllData = JSON.parse($("#hidAllData").val());
        var listid = [];
        var pageCount = 15;//每页显示多少条数据
        var ifelse = "";
        //加载数据
        function LoadData(pCount,pIndex,ifstr)
        {
            $.post("/handler/stwh_admin/sys_shoporder/loaddata.ashx",{
                pageCount:pCount,
                pageIndex:pIndex,
                whereStr:ifstr
            },function(data){
                if (data.msgcode==-1) {
                    $.bs.alert(data.msg, "warning", "");
                }
                else
                {
                    $("#hidListId").val("");
                    AllData = data.msg;
                    $("#ChildDatas tr").remove();
                    $("#SumCountSpan").text(data.sumcount);
                    if (data.msg.length==0) {
                        $("#ChildDatas").append("<tr><td colspan=\"10\" align=\"center\">暂无数据</td></tr>");
                    }
                    else
                    {
                        $.each(data.msg, function (index, item) {
                            var zffs = "微信",orderstatus="<span style=\"color:red;\">待支付</span>";
                            if (item.stwh_orpaystyle == 0) zffs = "无";
                            else if (item.stwh_orpaystyle == 1) zffs = "微信";
                            else if (item.stwh_orpaystyle == 2) zffs = "支付宝";
                            else if (item.stwh_orpaystyle == 3) zffs = "银行卡";
                            else zffs = "到付";
                            if (item.stwh_orstatus == 1) orderstatus = "<span style=\"color:green;\">已支付</span>";
                            else if (item.stwh_orstatus == 2) orderstatus = "已取消";
                            else orderstatus = "<span style=\"color:red;\">待支付</span>";
                            $("#ChildDatas").append("<tr data-id=\"" + item.stwh_orid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_orid + "\" value=\"" + item.stwh_orid + "\" /></td><td>" + item.stwh_orid + "</td><td>" + item.stwh_bumobile + "</td><td>" + item.stwh_orddid + "</td><td>" + orderstatus + "</td><td>" + zffs + "</td><td>" + item.stwh_ortime.replace('T',' ') + "</td><td>" + item.stwh_oruser + " " + item.stwh_ortel+" " + item.stwh_oraddress + "</td><td>" + item.stwh_orremark + "</td><td><div class=\"btn btn-default btnshop\" data-id=\"" + item.stwh_orddid + "\">查看商品</div></td></tr>");
                        });
                        UpdateScroll();
                    }
                }
            },"json");
        }
        //重新设置分页
        function SetPaginator(pageCount,totalsum)
        {
            if (totalsum==0) totalsum = 1;
            $("#pageUl").bootstrapPaginator("setOptions",{
                currentPage: 1, //当前页面
                totalPages: totalsum / pageCount + ((totalsum % pageCount) == 0 ? 0 : 1), //总页数
                numberOfPages: 5, //页码数
                useBootstrapTooltip: true,
                onPageChanged: function (event, oldPage, newPage) {
                    if (oldPage != newPage) LoadData(pageCount,parseInt(newPage) - 1,ifelse);
                }
            });
        }
        $(function () {
            $("#stwh_orstatusList a").click(function () {
                $("#stwh_orstatusText").text($(this).text());
                $("#stwh_orstatus").val($(this).attr("data-pid"));
            });
            $("#stwh_orpaystyleList a").click(function () {
                $("#stwh_orpaystyleText").text($(this).text());
                $("#stwh_orpaystyle").val($(this).attr("data-pid"));
            });
            $("#calendar").click(function () {
                $('#stwh_ortime').click();
            });
            $('#stwh_ortime').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    format: 'YYYY-MM-DD',
                    separator: ',',
                    applyLabel: '确定',
                    cancelLabel: '清除'
                }
            }).on({
                "cancel.daterangepicker": function (e) {
                    $(this).val("");
                },
                "apply.daterangepicker": function (ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD') + ',' + picker.endDate.format('YYYY-MM-DD'));
                }
            });
            $("#pageUl").bootstrapPaginator({
                currentPage: 1, //当前页面
                totalPages: <%=totalPages %>, //总页数
                numberOfPages: 5, //页码数
                useBootstrapTooltip: true,
                onPageChanged: function (event, oldPage, newPage) {
                    if (oldPage != newPage) LoadData(pageCount,parseInt(newPage) - 1,ifelse);
                }
            });
            $("#allParent").click(function () {
                if ($(this).is(":checked")) {
                    $("#ChildDatas :checkbox").prop("checked", $(this).is(":checked"));
                    $("#ChildDatas :checkbox:checked").each(function (i, item) {
                        if (listid.IndexOfArray($(item).val()) == -1) listid.AddArray($(item).val());
                    });
                    $("#hidListId").val(listid.toStringArray());
                }
                else {
                    $("#ChildDatas :checkbox").prop("checked", $(this).is(":checked"));
                    $("#ChildDatas :checkbox:not(:checked)").each(function (i, item) {
                        listid.removeIndexArray(listid.IndexOfArray($(item).val()));
                    });
                    $("#hidListId").val(listid.toStringArray());
                }
            });
            $("#ChildDatas").on("click",":checkbox",function(){
                if ($(this).is(":checked")) {
                    if ($("#ChildDatas :checkbox:checked").length == $("#ChildDatas :checkbox").length) $("#allParent").prop("checked", true);
                    if (listid.IndexOfArray($(this).val()) == -1) listid.AddArray($(this).val());
                    $("#hidListId").val(listid.toStringArray());
                }
                else {
                    if ($("#ChildDatas :checkbox:not(:checked)").length == $("#ChildDatas :checkbox").length) $("#allParent").prop("checked", false);
                    listid.removeIndexArray(listid.IndexOfArray($(this).val()));
                    $("#hidListId").val(listid.toStringArray());
                }
            });
            $("#ChildDatas").on("click",".btnshop",function () {
                var jb_id = $(this).attr("data-id");
                $("#h1").text("订单编号："+jb_id);
                $.bs.loading(true);
                $.post("/handler/stwh_admin/sys_shoporder/orderdetails.ashx",{
                    orderid:jb_id
                },function(data){
                    $.bs.loading(false);
                    if (data.msgcode==-1) {
                        $.bs.alert(data.msg, "warning", "");
                    }
                    else
                    {
                        $("#productTbody tr").remove();
                        if (data.msg.length==0) {
                            $("#productTbody").append("<tr><td colspan=\"3\" align=\"center\">暂无数据</td></tr>");
                        }
                        else
                        {
                            var jb_sumprice = 0;
                            $.each(data.msg, function (index, item) {
                                jb_sumprice = parseFloat(item.stwh_orsprice) * parseFloat(item.stwh_orscount) + jb_sumprice;
                                $("#productTbody").append("<tr><td>" + item.stwh_orspname + "</td><td>" + item.stwh_orsprice + "</td><td>" + item.stwh_orscount + "</td></tr>");
                            });
                            $("#OrderSumPrice").text(jb_sumprice);
                        }
                        $("#productModal").modal('show');
                    }
                },"json");
                return false;
            });
            $("#btnDelete").click(function () {
                $("#deleteMessageModal").modal('show');
            });
            $("#btnDeleteOk").click(function () {
                var ids = $("#hidListId").val();
                if (!ids) {
                    $.bs.alert("请选择数据！", "info");
                    return false;
                }
                else if (!(/^[\d,]*$/).test(ids)) {
                    $.bs.alert("非法数据格式，请刷新页面重试！", "danger");
                    return false;
                }
                $.post(
                "/handler/stwh_admin/sys_shoporder/delete.ashx",
                {
                    ids: ids
                }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
            });

            $("#btnUpdate").click(function () {
                if ($("#ChildDatas :checkbox:checked").length == 0) {
                    $.bs.alert("请选择数据！", "warning", "");
                    return false;
                }
                $("#ChildDatas :checkbox:checked").each(function (i, item) {
                    if (i == 0) window.location.href = "update.aspx?id=" + $(item).val();
                });
                return false;
            });
            $("#btnSearch").click(function(){
                $('#searchDiv').collapse("toggle");
                return false;
            });
            $("#btnsearchOk").click(function(){
                var stwh_orstatus = $("#stwh_orstatus").val();
                var stwh_orpaystyle = $("#stwh_orpaystyle").val();
                var stwh_bumobile = $("#stwh_bumobile").val();
                if (stwh_bumobile != "" && !IsMobileTel(stwh_bumobile)) {
                    $.bs.alert("手机号码格式错误！", "info");
                    return;
                }
                var stwh_orddid = $("#stwh_orddid").val();
                var stwh_ortime = $("#stwh_ortime").val();

                if (stwh_bumobile=="" && stwh_orddid == "" && stwh_ortime == "" && stwh_orpaystyle == "-1" && stwh_orstatus == "-1") {
                    $.bs.alert("至少输入一个查询值！", "info");
                    return;
                }
                ifelse = '[{"name":"stwh_bumobile","value":"'+stwh_bumobile+'"},{"name":"stwh_orddid","value":"'+stwh_orddid+'"},{"name":"stwh_ortime","value":"'+stwh_ortime+'"},{"name":"stwh_orstatus","value":"'+stwh_orstatus+'"},{"name":"stwh_orpaystyle","value":"'+stwh_orpaystyle+'"}]';
                $.bs.loading(true);
                $.post("/handler/stwh_admin/sys_shoporder/loaddata.ashx",{
                    pageCount:pageCount,
                    pageIndex:0,
                    whereStr:ifelse
                },function(data){
                    $.bs.loading(false);
                    $('#searchDiv').collapse("toggle");
                    if (data.msgcode==-1) {
                        $.bs.alert(data.msg, "warning", "");
                    }
                    else
                    {
                        $("#hidListId").val("");
                        AllData = data.msg;
                        $("#ChildDatas tr").remove();
                        $("#SumCountSpan").text(data.sumcount);
                        $("#hidTotalSum").val(data.sumcount);
                        SetPaginator(pageCount,parseInt(data.sumcount));
                        if (data.msg.length==0) {
                            $("#ChildDatas").append("<tr><td colspan=\"10\" align=\"center\">暂无数据</td></tr>");
                        }
                        else
                        {
                            $.each(data.msg, function (index, item) {
                                var zffs = "微信",orderstatus="<span style=\"color:red;\">待支付</span>";
                                if (item.stwh_orpaystyle == 0) zffs = "无";
                                else if (item.stwh_orpaystyle == 1) zffs = "微信";
                                else if (item.stwh_orpaystyle == 2) zffs = "支付宝";
                                else if (item.stwh_orpaystyle == 3) zffs = "银行卡";
                                else zffs = "到付";
                                if (item.stwh_orstatus == 1) orderstatus = "<span style=\"color:green;\">已支付</span>";
                                else if (item.stwh_orstatus == 2) orderstatus = "已取消";
                                else orderstatus = "<span style=\"color:red;\">待支付</span>";
                                $("#ChildDatas").append("<tr data-id=\"" + item.stwh_orid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_orid + "\" value=\"" + item.stwh_orid + "\" /></td><td>" + item.stwh_orid + "</td><td>" + item.stwh_bumobile + "</td><td>" + item.stwh_orddid + "</td><td>" + orderstatus + "</td><td>" + zffs + "</td><td>" + item.stwh_ortime.replace('T',' ') + "</td><td>" + item.stwh_oruser + " " + item.stwh_ortel+" " + item.stwh_oraddress + "</td><td>" + item.stwh_orremark + "</td><td><div class=\"btn btn-default btnshop\" data-id=\"" + item.stwh_orddid + "\">查看商品</div></td></tr>");
                            });
                            UpdateScroll();
                        }
                    }
                },"json");
                return false;
            });
            $("#basic").change(function(){
                pageCount = $(this).val();
                LoadData(pageCount,0,ifelse);
                var totalsum = parseInt($("#hidTotalSum").val());
                SetPaginator(pageCount,totalsum);
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
    </body>
</html>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_Common;
using System.Xml;
using System.Reflection;
using stwh_BLL;

namespace stwh_Web.stwh_admin.Common
{
    public class WebSite
    {
        //private static string websitepath = HttpContext.Current.Request.MapPath("/config/website.config");
        private static string noturlpath = HttpContext.Current.Request.MapPath("/config/NotURL.config");

        /// <summary>
        /// 将stwh_config转换为stwh_website
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static stwh_website ConfigToWebSite(stwh_config model)
        {
            stwh_website cmodel = new stwh_website();
            cmodel.WebKey = model.stwh_cfkey;
            cmodel.WebKeyTime = model.stwh_cfkeytime;
            cmodel.Emailname = model.stwh_cfname;
            cmodel.Emailpwd = model.stwh_cfpwd;
            cmodel.Emailport = model.stwh_cfport;
            cmodel.Emailsend = model.stwh_cfsendname;
            cmodel.Emailsmtp = model.stwh_cfsmtp;
            cmodel.Emailuser = model.stwh_cfuser;
            cmodel.Seodescription = model.stwh_cfdescription;
            cmodel.Seodescription_m = model.stwh_cfdescription_m;
            cmodel.Seodescription_s = model.stwh_cfdescription_s;
            cmodel.Seokey = model.stwh_cfkeywords;
            cmodel.Seokey_m = model.stwh_cfkeywords_m;
            cmodel.Seokey_s = model.stwh_cfkeywords_s;
            cmodel.Webcopyright = model.stwh_cfcopyright;
            cmodel.Webcopyright_m = model.stwh_cfcopyright_m;
            cmodel.Webcopyright_s = model.stwh_cfcopyright_s;
            cmodel.Webfilesize = model.stwh_cfuploadsize;
            cmodel.Webfiletype = model.stwh_cfuploadtype;
            cmodel.Webico = model.stwh_cfico;
            cmodel.Webico_m = model.stwh_cfico_m;
            cmodel.Webico_s = model.stwh_cfico_m;
            cmodel.Weblogo = model.stwh_cflogo;
            cmodel.Weblogo_m = model.stwh_cflogo_m;
            cmodel.Weblogo_s = model.stwh_cflogo_s;
            cmodel.Webmaintain = model.stwh_cfweihu;
            cmodel.Webmaintain_m = model.stwh_cfweihu_m;
            cmodel.Webmaintain_s = model.stwh_cfweihu_s;
            cmodel.Webname = model.stwh_cfwebname;
            cmodel.Webname_m = model.stwh_cfwebname_m;
            cmodel.Webname_s = model.stwh_cfwebname_s;
            cmodel.Websytk = model.stwh_cfsytk;
            cmodel.Webtemplate = model.stwh_cfwebtemplate;
            cmodel.Webtemplate_m = model.stwh_cfmwebtemplate;
            cmodel.Webtemplate_s = model.stwh_cfshoptemplate;
            cmodel.Webthumbnail = model.stwh_cfthumbnail * 1024;
            cmodel.Webversion = model.stwh_cfversion;
            cmodel.Webystk = model.stwh_cfystk;
            cmodel.Webid = model.stwh_cfid;
            return cmodel;
        }

        /// <summary>
        /// 将stwh_website转换为stwh_config
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static stwh_config WebSiteToConfig(stwh_website cmodel)
        {
            stwh_config model = new stwh_config();
            model.stwh_cfkey = cmodel.WebKey;
            model.stwh_cfkeytime = cmodel.WebKeyTime;
            model.stwh_cfname = cmodel.Emailname;
            model.stwh_cfpwd = cmodel.Emailpwd;
            model.stwh_cfport = cmodel.Emailport;
            model.stwh_cfsendname = cmodel.Emailsend;
            model.stwh_cfsmtp = cmodel.Emailsmtp;
            model.stwh_cfuser = cmodel.Emailuser;
            model.stwh_cfdescription = cmodel.Seodescription;
            model.stwh_cfdescription_m = cmodel.Seodescription_m;
            model.stwh_cfdescription_s = cmodel.Seodescription_s;
            model.stwh_cfkeywords = cmodel.Seokey;
            model.stwh_cfkeywords_m = cmodel.Seokey_m;
            model.stwh_cfkeywords_s = cmodel.Seokey_s;
            model.stwh_cfcopyright = cmodel.Webcopyright;
            model.stwh_cfcopyright_m = cmodel.Webcopyright_m;
            model.stwh_cfcopyright_s = cmodel.Webcopyright_s;
            model.stwh_cfuploadsize = cmodel.Webfilesize;
            model.stwh_cfuploadtype = cmodel.Webfiletype;
            model.stwh_cfico = cmodel.Webico;
            model.stwh_cfico_m = cmodel.Webico_m;
            model.stwh_cfico_s = cmodel.Webico_s;
            model.stwh_cflogo = cmodel.Weblogo;
            model.stwh_cflogo_m = cmodel.Weblogo_m;
            model.stwh_cflogo_s = cmodel.Weblogo_s;
            model.stwh_cfweihu = cmodel.Webmaintain;
            model.stwh_cfweihu_m = cmodel.Webmaintain_m;
            model.stwh_cfweihu_s = cmodel.Webmaintain_s;
            model.stwh_cfwebname = cmodel.Webname;
            model.stwh_cfwebname_m = cmodel.Webname_m;
            model.stwh_cfwebname_s = cmodel.Webname_s;
            model.stwh_cfsytk = cmodel.Websytk;
            model.stwh_cfwebtemplate = cmodel.Webtemplate;
            model.stwh_cfmwebtemplate = cmodel.Webtemplate_m;
            model.stwh_cfshoptemplate = cmodel.Webtemplate_s;
            model.stwh_cfthumbnail = cmodel.Webthumbnail / 1024;
            model.stwh_cfversion = cmodel.Webversion;
            model.stwh_cfystk = cmodel.Webystk;
            model.stwh_cfid = cmodel.Webid;
            return model;
        }

        #region 读取网站配置文件(已注释)
        ///// <summary>
        ///// 读取网站配置文件
        ///// </summary>
        ///// <returns></returns>
        //private stwh_website ReadWebXML()
        //{
        //    stwh_website model = new stwh_website();
        //    try
        //    {
        //        XmlDocument xmldoc = new XmlDocument();
        //        xmldoc.Load(websitepath);
        //        XmlNodeList xmllist = xmldoc.GetElementsByTagName("Websites");
        //        XmlNode childxml = xmllist[0].ChildNodes[0];

        //        #region 原始赋值
        //        //XmlNode emailname = childxml.SelectSingleNode("//emailname");
        //        //if (emailname != null) model.Emailname = emailname.InnerText;
        //        //XmlNode emailpwd = childxml.SelectSingleNode("//emailpwd");
        //        //if (emailpwd != null) model.Emailpwd = emailpwd.InnerText;
        //        //XmlNode emailsend = childxml.SelectSingleNode("//emailsend");
        //        //if (emailsend != null) model.Emailsend = emailsend.InnerText;
        //        //XmlNode emailsmtp = childxml.SelectSingleNode("//emailsmtp");
        //        //if (emailsmtp != null) model.Emailsmtp = emailsmtp.InnerText;
        //        //XmlNode emailuser = childxml.SelectSingleNode("//emailuser");
        //        //if (emailuser != null) model.Emailuser = emailuser.InnerText;
        //        //XmlNode seodescription = childxml.SelectSingleNode("//seodescription");
        //        //if (seodescription != null) model.Seodescription = seodescription.InnerText;
        //        //XmlNode seokey = childxml.SelectSingleNode("//seokey");
        //        //if (seokey != null) model.Seokey = seokey.InnerText;
        //        //XmlNode webbtnbackground = childxml.SelectSingleNode("//webbtnbackground");
        //        //if (webbtnbackground != null) model.Webbtnbackground = webbtnbackground.InnerText;
        //        //XmlNode webfiletype = childxml.SelectSingleNode("//webfiletype");
        //        //if (webfiletype != null) model.Webfiletype = webfiletype.InnerText;
        //        //XmlNode webuploaddire = childxml.SelectSingleNode("//webuploaddire");
        //        //if (webuploaddire != null) model.WebUploadDire = webuploaddire.InnerText;
        //        //XmlNode webico = childxml.SelectSingleNode("//webico");
        //        //if (webico != null) model.Webico = webico.InnerText;
        //        //XmlNode webcopyright = childxml.SelectSingleNode("//webcopyright");
        //        //if (webcopyright != null) model.Webcopyright = webcopyright.InnerText;
        //        //XmlNode webbtncolor = childxml.SelectSingleNode("//webbtncolor");
        //        //if (webbtncolor != null) model.Webbtncolor = webbtncolor.InnerText;
        //        //XmlNode webname = childxml.SelectSingleNode("//webname");
        //        //if (webname != null) model.Webname = webname.InnerText;
        //        //XmlNode weblogo = childxml.SelectSingleNode("//weblogo");
        //        //if (weblogo != null) model.Weblogo = weblogo.InnerText;
        //        //XmlNode webtemplate = childxml.SelectSingleNode("//webtemplate");
        //        //if (webtemplate != null) model.Webtemplate = webtemplate.InnerText;
        //        //XmlNode websytk = childxml.SelectSingleNode("//websytk");
        //        //if (websytk != null) model.Websytk = websytk.InnerText;
        //        //XmlNode webystk = childxml.SelectSingleNode("//webystk");
        //        //if (webystk != null) model.Webystk = webystk.InnerText;
        //        //XmlNode webzcname = childxml.SelectSingleNode("//webzcname");
        //        //if (webzcname != null) model.Webzcname = webzcname.InnerText;
        //        //XmlNode webkey = childxml.SelectSingleNode("//webkey");
        //        //if (webkey != null) model.WebKey = webkey.InnerText;
        //        //XmlNode webkeytime = childxml.SelectSingleNode("//webkeytime");
        //        //if (webkeytime != null) model.WebKeyTime = webkeytime.InnerText;
        //        //XmlNode webversion = childxml.SelectSingleNode("//webversion");
        //        //if (webversion != null) model.Webversion = webversion.InnerText;

        //        //XmlNode emailport = childxml.SelectSingleNode("//emailport");
        //        //if (emailport != null) model.Emailport = int.Parse(emailport.InnerText);
        //        //XmlNode webbtnborder = childxml.SelectSingleNode("//webbtnborder");
        //        //if (webbtnborder != null) model.Webbtnborder = int.Parse(webbtnborder.InnerText);
        //        //XmlNode webbtnbottom = childxml.SelectSingleNode("//webbtnbottom");
        //        //if (webbtnbottom != null) model.Webbtnbottom = int.Parse(webbtnbottom.InnerText);
        //        //XmlNode webbtnradius = childxml.SelectSingleNode("//webbtnradius");
        //        //if (webbtnradius != null) model.Webbtnradius = int.Parse(webbtnradius.InnerText);
        //        //XmlNode webbtnright = childxml.SelectSingleNode("//webbtnright");
        //        //if (webbtnright != null) model.Webbtnright = int.Parse(webbtnright.InnerText);
        //        //XmlNode webbtnsizeH = childxml.SelectSingleNode("//webbtnsizeH");
        //        //if (webbtnsizeH != null) model.WebbtnsizeH = int.Parse(webbtnsizeH.InnerText);
        //        //XmlNode webbtnsizeW = childxml.SelectSingleNode("//webbtnsizeW");
        //        //if (webbtnsizeW != null) model.WebbtnsizeW = int.Parse(webbtnsizeW.InnerText);
        //        //XmlNode webcheck = childxml.SelectSingleNode("//webcheck");
        //        //if (webcheck != null) model.Webcheck = int.Parse(webcheck.InnerText);
        //        //XmlNode weblogs = childxml.SelectSingleNode("//weblogs");
        //        //if (weblogs != null) model.Weblogs = int.Parse(weblogs.InnerText);
        //        //XmlNode webfilesize = childxml.SelectSingleNode("//webfilesize");
        //        //if (webfilesize != null) model.Webfilesize = int.Parse(webfilesize.InnerText);
        //        //XmlNode webthumbnail = childxml.SelectSingleNode("//webthumbnail");
        //        //if (webthumbnail != null) model.Webthumbnail = int.Parse(webthumbnail.InnerText);
        //        //XmlNode weblanguage = childxml.SelectSingleNode("//weblanguage");
        //        //if (weblanguage != null) model.Weblanguage = int.Parse(weblanguage.InnerText);
        //        //XmlNode webmaintain = childxml.SelectSingleNode("//webmaintain");
        //        //if (webmaintain != null) model.Webmaintain = int.Parse(webmaintain.InnerText);
        //        //XmlNode webmaintain_m = childxml.SelectSingleNode("//webmaintain_m");
        //        //if (webmaintain_m != null) model.Webmaintain_m = int.Parse(webmaintain_m.InnerText);
        //        //XmlNode webtemplate_m = childxml.SelectSingleNode("//webtemplate_m");
        //        //if (webtemplate_m != null) model.Webmaintain_m = int.Parse(webtemplate_m.InnerText);
        //        #endregion
        //        #region 反射赋值
        //        PropertyInfo[] propertys = model.GetType().GetProperties();
        //        foreach (PropertyInfo pi in propertys)
        //        {
        //            XmlNode xmlnode = childxml.SelectSingleNode("//"+pi.Name.ToLower());
        //            if (xmlnode!=null)
        //            {
        //                if (pi.PropertyType == typeof(int)) pi.SetValue(model, int.Parse(xmlnode.InnerText), null);
        //                else if (pi.PropertyType == typeof(string)) pi.SetValue(model, xmlnode.InnerText, null);
        //            }
        //        }
        //        #endregion

        //        UpdateWebSite(model);
        //        DataCache.AddMCache("website", model);
        //    }
        //    catch (Exception)
        //    {
        //    }
        //    return model;
        //}
        #endregion

        /// <summary>
        /// 加载网站配置文件
        /// </summary>
        /// <returns></returns>
        public static stwh_website LoadWebSite()
        {
            stwh_website model = new stwh_website();
            object obj = DataCache.GetMCache("website");
            if (obj == null)
            {
                stwh_config configmodel = new stwh_configBLL().GetModel();
                if (configmodel != null)
                {
                    model = ConfigToWebSite(configmodel);
                    DataCache.AddMCache("website", model);
                }
            }
            else
            {
                model = obj as stwh_website;
            }
            return model;
        }

        /// <summary>
        /// 加载网站配置文件
        /// </summary>
        /// <param name="isload">是否强制加载配置文件，不从缓存中读取（true不从缓存读取，false如果存在从缓存中读取）</param>
        /// <returns></returns>
        public static stwh_website LoadWebSite(bool isload)
        {
            if (isload)
            {
                stwh_website model = new stwh_website();
                stwh_config configmodel = new stwh_configBLL().GetModel();
                if (configmodel != null) model = ConfigToWebSite(configmodel);
                return model;
            }
            else
            {
                return LoadWebSite();
            }
        }

        /// <summary>
        /// 加载不需要验证的功能页面或handler路径集合
        /// </summary>
        /// <returns></returns>
        public static List<string> LoadChkURL()
        {
            object obj = DataCache.GetMCache("noturl");
            if (obj == null)
            {
                try
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.Load(noturlpath);
                    XmlNodeList xmllist = xmldoc.GetElementsByTagName("Urls");
                    XmlNodeList childxmllist = xmllist[0].ChildNodes[0].ChildNodes;
                    List<string> listurl = new List<string>();
                    for (int i = 0; i < childxmllist.Count; i++)
                    {
                        //过滤注释
                        if (childxmllist[i].NodeType != XmlNodeType.Comment) listurl.Add(childxmllist[i].InnerText);
                    }
                    DataCache.AddMCache("noturl", listurl);
                    return listurl;
                }
                catch (Exception)
                {
                    return null;
                }
            }
            else
                return obj as List<string>;
        }

        /// <summary>
        /// 加载不需要验证的功能页面或handler路径集合
        /// </summary>
        /// <param name="isload">是否强制加载黑名单页面，true 强制，false 从缓存取出</param>
        /// <returns></returns>
        public static List<string> LoadChkURL(bool isload)
        {
            if (isload)
            {
                try
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.Load(noturlpath);
                    XmlNodeList xmllist = xmldoc.GetElementsByTagName("Urls");
                    XmlNodeList childxmllist = xmllist[0].ChildNodes[0].ChildNodes;
                    List<string> listurl = new List<string>();
                    for (int i = 0; i < childxmllist.Count; i++)
                    {
                        //过滤注释
                        if (childxmllist[i].NodeType != XmlNodeType.Comment) listurl.Add(childxmllist[i].InnerText);
                    }
                    DataCache.AddMCache("noturl", listurl);
                    return listurl;
                }
                catch (Exception)
                {
                    return null;
                }
            }
            else
                return LoadChkURL();
        }

        /// <summary>
        /// 更新网站配置信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static bool UpdateWebSite(stwh_config model)
        {
            bool result = false;
            try
            {
                #region 创建配置文件
                //XmlDocument xmldoc = new XmlDocument();
                ////加入XML的声明段落,<?xml version="1.0" encoding="gb2312"?>
                //XmlDeclaration xmldecl = xmldoc.CreateXmlDeclaration("1.0", "utf-8", null);
                //xmldoc.AppendChild(xmldecl);
                ////加入一个根节点
                //XmlElement xmlele = xmldoc.CreateElement("Websites");
                //xmldoc.AppendChild(xmlele);
                ////查询根节点
                //XmlNode root = xmldoc.SelectSingleNode("Websites");
                ////在根节点加入另外一个子节点
                //XmlElement xmlelechild = xmldoc.CreateElement("Website");
                //#region 原始创建xml
                //////设置<Website>的节点
                ////XmlElement webversion = xmldoc.CreateElement("webversion");
                ////webversion.InnerText = model.Webversion;
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webversion);

                //////设置<Website>的节点
                ////XmlElement emailname = xmldoc.CreateElement("emailname");
                ////emailname.InnerText = model.Emailname;
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(emailname);

                ////XmlElement emailport = xmldoc.CreateElement("emailport");
                ////emailport.InnerText = model.Emailport + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(emailport);

                ////XmlElement emailpwd = xmldoc.CreateElement("emailpwd");
                ////emailpwd.InnerText = model.Emailpwd + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(emailpwd);

                ////XmlElement emailsend = xmldoc.CreateElement("emailsend");
                ////emailsend.InnerText = model.Emailsend + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(emailsend);

                ////XmlElement emailsmtp = xmldoc.CreateElement("emailsmtp");
                ////emailsmtp.InnerText = model.Emailsmtp + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(emailsmtp);

                ////XmlElement emailuser = xmldoc.CreateElement("emailuser");
                ////emailuser.InnerText = model.Emailuser + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(emailuser);

                ////XmlElement seodescription = xmldoc.CreateElement("seodescription");
                ////seodescription.InnerText = model.Seodescription + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(seodescription);

                ////XmlElement seokey = xmldoc.CreateElement("seokey");
                ////seokey.InnerText = model.Seokey + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(seokey);

                ////XmlElement webbtnbackground = xmldoc.CreateElement("webbtnbackground");
                ////webbtnbackground.InnerText = model.Webbtnbackground + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webbtnbackground);

                ////XmlElement webbtnborder = xmldoc.CreateElement("webbtnborder");
                ////webbtnborder.InnerText = model.Webbtnborder + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webbtnborder);

                ////XmlElement webbtnbottom = xmldoc.CreateElement("webbtnbottom");
                ////webbtnbottom.InnerText = model.Webbtnbottom + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webbtnbottom);

                ////XmlElement webbtncolor = xmldoc.CreateElement("webbtncolor");
                ////webbtncolor.InnerText = model.Webbtncolor + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webbtncolor);

                ////XmlElement webbtnradius = xmldoc.CreateElement("webbtnradius");
                ////webbtnradius.InnerText = model.Webbtnradius + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webbtnradius);

                ////XmlElement webbtnright = xmldoc.CreateElement("webbtnright");
                ////webbtnright.InnerText = model.Webbtnright + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webbtnright);

                ////XmlElement webbtnsizeH = xmldoc.CreateElement("webbtnsizeH");
                ////webbtnsizeH.InnerText = model.WebbtnsizeH + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webbtnsizeH);

                ////XmlElement webbtnsizeW = xmldoc.CreateElement("webbtnsizeW");
                ////webbtnsizeW.InnerText = model.WebbtnsizeW + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webbtnsizeW);

                ////XmlElement webcheck = xmldoc.CreateElement("webcheck");
                ////webcheck.InnerText = model.Webcheck + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webcheck);

                ////XmlElement weblogs = xmldoc.CreateElement("weblogs");
                ////weblogs.InnerText = model.Weblogs + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(weblogs);

                ////XmlElement webcopyright = xmldoc.CreateElement("webcopyright");
                ////webcopyright.InnerText = model.Webcopyright + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webcopyright);

                ////XmlElement webfilesize = xmldoc.CreateElement("webfilesize");
                ////webfilesize.InnerText = model.Webfilesize + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webfilesize);

                ////XmlElement webthumbnail = xmldoc.CreateElement("webthumbnail");
                ////webthumbnail.InnerText = model.Webthumbnail + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webthumbnail);

                ////XmlElement webfiletype = xmldoc.CreateElement("webfiletype");
                ////webfiletype.InnerText = model.Webfiletype + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webfiletype);

                ////XmlElement webuploaddire = xmldoc.CreateElement("webuploaddire");
                ////webuploaddire.InnerText = model.WebUploadDire + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webuploaddire);

                ////XmlElement webico = xmldoc.CreateElement("webico");
                ////webico.InnerText = model.Webico + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webico);

                ////XmlElement weblanguage = xmldoc.CreateElement("weblanguage");
                ////weblanguage.InnerText = model.Weblanguage + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(weblanguage);

                ////XmlElement webmaintain = xmldoc.CreateElement("webmaintain");
                ////webmaintain.InnerText = model.Webmaintain + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webmaintain);

                ////XmlElement webmaintain_m = xmldoc.CreateElement("webmaintain_m");
                ////webmaintain_m.InnerText = model.Webmaintain_m + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webmaintain_m);

                ////XmlElement webname = xmldoc.CreateElement("webname");
                ////webname.InnerText = model.Webname + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webname);

                ////XmlElement weblogo = xmldoc.CreateElement("weblogo");
                ////weblogo.InnerText = model.Weblogo + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(weblogo);

                ////XmlElement webtemplate = xmldoc.CreateElement("webtemplate");
                ////webtemplate.InnerText = model.Webtemplate + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webtemplate);

                ////XmlElement webtemplate_m = xmldoc.CreateElement("webtemplate_m");
                ////webtemplate_m.InnerText = model.Webtemplate_m + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webtemplate_m);

                ////XmlElement websytk = xmldoc.CreateElement("websytk");
                ////websytk.InnerText = model.Websytk + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(websytk);

                ////XmlElement webystk = xmldoc.CreateElement("webystk");
                ////webystk.InnerText = model.Webystk + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webystk);

                ////XmlElement webzcname = xmldoc.CreateElement("webzcname");
                ////webzcname.InnerText = model.Webzcname + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webzcname);

                ////XmlElement webkey = xmldoc.CreateElement("webkey");
                ////webkey.InnerText = model.WebKey + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webkey);

                ////XmlElement webkeytime = xmldoc.CreateElement("webkeytime");
                ////webkeytime.InnerText = model.WebKeyTime + "";
                //////添加到<Website>节点中 
                ////xmlelechild.AppendChild(webkeytime);
                //#endregion
                //#region 反射创建xml
                //PropertyInfo[] propertys = model.GetType().GetProperties();
                //foreach (PropertyInfo pi in propertys)
                //{
                //    XmlElement xmlelechildlist = xmldoc.CreateElement(pi.Name.ToLower());
                //    xmlelechildlist.InnerText = pi.GetValue(model, null).ToString();
                //    xmlelechild.AppendChild(xmlelechildlist);
                //}
                //#endregion
                //root.AppendChild(xmlelechild);                
                //xmldoc.Save(websitepath);
                //DataCache.AddMCache("website", model);
                //result = true;
                #endregion

                new stwh_configBLL().Add(model);
                model = new stwh_configBLL().GetModel();
                DataCache.AddMCache("website", ConfigToWebSite(model));
                result = true;
            }
            catch (Exception)
            {
            }
            return result;
        }
    }
}
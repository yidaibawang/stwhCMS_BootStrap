﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;

namespace stwh_Web.stwh_admin.Common
{
    /// <summary>
    /// 自定义页面基类
    /// </summary>
    public class PageBase : System.Web.UI.Page
    {
        private string _menuString;
        /// <summary>
        /// 菜单数据
        /// </summary>
        public string MenuString
        {
            get { return _menuString; }
            set { _menuString = value; }
        }

        private stwh_website _webSite = new stwh_website();

        /// <summary>
        /// 网站配置信息
        /// </summary>
        public stwh_website WebSite
        {
            get { return _webSite; }
            set { _webSite = value; }
        }

        /// <summary>
        /// 获取所有菜单数据（包含隐藏的菜单数据）
        /// </summary>
        /// <returns></returns>
        public List<stwh_menuinfo> GetMenuList()
        {
            List<stwh_menuinfo> allmenu = new List<stwh_menuinfo>();
            object cacheMenu = DataCache.GetMCache("cacheMenu");
            if (cacheMenu == null)
            {
                allmenu = new stwh_menuinfoBLL().GetModelList("").OrderByDescending(a => a.stwh_menuorder).ToList<stwh_menuinfo>();
                DataCache.AddMCache("cacheMenu", allmenu);
            }
            else allmenu = (cacheMenu as List<stwh_menuinfo>).OrderByDescending(a => a.stwh_menuorder).ToList<stwh_menuinfo>(); ;
            return allmenu;
        }

        /// <summary>
        /// 设置网页前台菜单名称
        /// </summary>
        /// <param name="control">控件</param>
        /// <param name="menuname">路径名称</param>
        /// <param name="defaulttxt">菜单默认名称</param>
        public void SetMenuSpanText(System.Web.UI.HtmlControls.HtmlGenericControl control, string menuname, string defaulttxt)
        {
            if (string.IsNullOrEmpty(menuname))
            {
                control.InnerText = defaulttxt;
                return;
            }
            List<stwh_menuinfo> menulist = GetMenuList().Where(aa=>aa.stwh_menuURL.ToLower().IndexOf(menuname.ToLower())!=-1).ToList();
            if (menulist.Count == 0)
                control.InnerText = defaulttxt;
            else
            {
                if (menulist.Count >1)
                {
                    string menustr = "";
                    foreach (stwh_menuinfo item in menulist)
                    {
                        if (item.stwh_menuURL != "/")
                        {
                            if (item.stwh_menuURL.Split('/')[0].ToLower() == menuname.ToLower())
                            {
                                menustr = item.stwh_menuname;
                                break;
                            }
                        }
                    }
                    control.InnerText = menustr == "" ? defaulttxt : menustr;
                }
                else
                    control.InnerText = menulist[0].stwh_menuname;
            }
        }

        /// <summary>
        /// 获取功能列表
        /// </summary>
        /// <returns></returns>
        public List<stwh_function> GetFunctionList()
        {
            List<stwh_function> List = new List<stwh_function>();
            object cacheMenu = DataCache.GetMCache("cacheFunction");
            if (cacheMenu == null)
            {
                //获取显示的功能的按钮
                List = new stwh_functionBLL().GetModelList("stwh_fishow = 0").OrderByDescending(a => a.stwh_forder).ToList<stwh_function>();
                DataCache.AddMCache("cacheFunction", List);
            }
            else List = (cacheMenu as List<stwh_function>).OrderByDescending(a => a.stwh_forder).ToList<stwh_function>();
            return List;
        }

        /// <summary>
        /// 根据角色获取指定菜单的功能列表
        /// </summary>
        /// <param name="stwh_menuid">菜单id</param>
        /// <returns></returns>
        public List<stwh_function> GetFunctionForRole(int stwh_menuid)
        {
            List<stwh_function> List = new List<stwh_function>();

            try
            {
                stwh_userinfo model = Session["htuser"] as stwh_userinfo;

                //超级管理员（鼻祖）,系统默认超级管理员
                if (model.stwh_uiid == 1)
                {
                    //获取当前菜单下的所有功能按钮
                    List = GetFunctionList().Where(a => a.stwh_menuid == stwh_menuid).OrderByDescending(a => a.stwh_forder).ToList<stwh_function>();
                }
                else//不是系统默认超级管理员，验证按钮功能权限
                {
                    //获取管理员赋予的功能按钮并且是显示的
                    List = new stwh_functionBLL().GetModelList("stwh_fishow = 0", 0).Where(a => a.stwh_menuid == stwh_menuid && a.stwh_rid == model.stwh_rid).OrderByDescending(a => a.stwh_forder).ToList<stwh_function>();
                }
            }
            catch (Exception)
            {

            }

            return List;
        }

        /// <summary>
        /// 生成功能按钮
        /// </summary>
        /// <param name="mid">菜单id</param>
        /// <returns></returns>
        public string GetFunctionForRole(string mid)
        {
            string btnlist = "";

            try
            {
                List<stwh_function> list = GetFunctionForRole(int.Parse(mid));
                foreach (stwh_function item in list)
                {
                    btnlist += "<a id=\"" + item.stwh_idname + "\" class=\"btn btn-default\" href=\"" + item.stwh_furl + "\" title=\"" + item.stwh_fremark + "\"><span class=\"" + item.stwh_fico + "\"></span> " + item.stwh_fname + "</a> ";
                }
            }
            catch (Exception)
            {

            }

            return btnlist;
        }

        /// <summary>
        /// 生成菜单
        /// </summary>
        /// <param name="menusData">菜单数据</param>
        /// <param name="SaveList">存放菜单集合</param>
        /// <param name="code">父菜单id</param>
        private void CreateMenus(List<stwh_menuinfo> menusData, List<string> SaveList, int code)
        {
            //查询指定父编号的菜单，并按照序号进行升序排序
            foreach (stwh_menuinfo mm in menusData.Where(a => a.stwh_menuparentID == code).OrderByDescending(a => a.stwh_menuorder).ToList<stwh_menuinfo>())
            {
                SaveList.Add("<div class=\"acchead\" data-toggle=\"collapse\" data-target=\"#MenuPanel" + mm.stwh_menuid + "\" ><span class=\"" + mm.stwh_menuICO_url + "\"></span>" + mm.stwh_menuname + "<span class=\"glyphicon glyphicon-menu-down pull-right\"></span></div>");
                SaveList.Add("<div id=\"MenuPanel" + mm.stwh_menuid + "\" class=\"collapse accmenu in\">");
                foreach (stwh_menuinfo cc in menusData.Where(a => a.stwh_menuparentID == mm.stwh_menuid).OrderByDescending(a => a.stwh_menuorder).ToList<stwh_menuinfo>())
                {
                    SaveList.Add("<a class=\"childrenm\" href=\"/stwh_admin/" + cc.stwh_menuURL + "?mid=" + cc.stwh_menuid + "\">&nbsp;&nbsp;&nbsp;<span class=\"" + cc.stwh_menuICO_url + "\"></span>" + cc.stwh_menuname + " </a>");
                }
                SaveList.Add("</div>");
            }
            foreach (string item in SaveList)
            {
                MenuString += item;
            }
        }

        /// <summary>
        /// 页面初始化运行
        /// </summary>
        /// <param name="e"></param>
        protected override void OnInit(EventArgs e)
        {
            try
            {
                base.OnInit(e);

                object user = Session["htuser"];
                stwh_userinfo model = user as stwh_userinfo;
                List<stwh_menuinfo> MenuList = new List<stwh_menuinfo>();
                //是超级管理员，获取全部菜单,包含属性隐藏的
                if (model.stwh_uiid == 1) MenuList = GetMenuList();
                else MenuList = new stwh_menuinfoBLL().GetModelList(model.stwh_rid);
                List<string> saveList = new List<string>();
                CreateMenus(MenuList, saveList, 0);

                WebSite = stwh_Web.stwh_admin.Common.WebSite.LoadWebSite();
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息：" + ex.StackTrace);
                Response.Redirect("/stwh_admin/invalid.htm");
            }
        }
    }
}
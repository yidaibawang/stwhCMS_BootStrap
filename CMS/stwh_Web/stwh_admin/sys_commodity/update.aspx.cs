﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_commodity
{
    public partial class update : Common.PageBase
    {
        public stwh_product UpdateModel = new stwh_product();
        public string typePV = "";

        /// <summary>
        /// 获取文章栏目（并判断一级栏目下是否有子栏目）
        /// </summary>
        /// <param name="datasource">数据源</param>
        /// <param name="listSave">储存</param>
        /// <param name="pid">父id</param>
        /// <param name="dj">栏目等级</param>
        private void ArticleList(List<stwh_producttype> datasource, StringBuilder listSave, int pid, int dj)
        {
            //筛选出一级栏目
            List<stwh_producttype> listadd = new List<stwh_producttype>();
            foreach (stwh_producttype item in datasource)
            {
                if (item.stwh_ptparentid == pid) listadd.Add(item);
            }
            if (listadd.Count != 0)
            {
                //循环递归判断当前栏目下是否有子栏目
                foreach (stwh_producttype item in listadd)
                {
                    listSave.Append("<li><a data-pid=\"" + item.stwh_ptid + "\">");
                    for (int i = 0; i < dj; i++) listSave.Append("&nbsp;&nbsp;");
                    listSave.Append("(" + (dj + 1) + ")." + item.stwh_ptname + "</a></li>");
                    ArticleList(datasource, listSave, item.stwh_ptid, dj + 1);
                }
            }
        }

        /// <summary>
        /// 商品分类参数赋值
        /// </summary>
        /// <param name="model"></param>
        /// <param name="sb"></param>
        /// <param name="pvlist"></param>
        /// <param name="js"></param>
        private void CheckInputType(stwh_producttype_p model, StringBuilder sb,List<stwh_producttype_pv> pvlist,StringBuilder js)
        {
            string value = "";
            List < stwh_producttype_pv> chkvalue = pvlist.Where(aa => aa.stwh_ptpid == model.stwh_ptpid).ToList<stwh_producttype_pv>();
            if (chkvalue.Count != 0) value = chkvalue[0].stwh_ptpvvalue;
            switch (model.stwh_ptptype.Trim().ToLower())
            {
                case "text":
                    sb.Append("<div class=\"col-sm-1 st-text-right\">" + model.stwh_ptptitle + "：</div><div class=\"col-sm-11 form-group\"><input type=\"text\" id=\"" + model.stwh_ptpname + "\" name=\"" + model.stwh_ptpname + "\" value=\"" + value + "\" class=\"st-input-text-700 form-control\" placeholder=\"请输入" + model.stwh_ptptitle + "\" /></div><div class=\"line10\"></div>");
                    break;
                case "textarea":
                    sb.Append("<div class=\"col-sm-1 st-text-right\">" + model.stwh_ptptitle + "：</div><div class=\"col-sm-11 form-group\"><textarea id=\"" + model.stwh_ptpname + "\" name=\"" + model.stwh_ptpname + "\" class=\"st-input-text-700 form-control\" placeholder=\"请输入" + model.stwh_ptptitle + "\" >" + value + "</textarea></div><div class=\"line10\"></div>");
                    break;
                case "select":
                    break;
                case "number":
                    sb.Append("<div class=\"col-sm-1 st-text-right\">" + model.stwh_ptptitle + "：</div><div class=\"col-sm-11 form-group\"><input type=\"text\" id=\"" + model.stwh_ptpname + "\" name=\"" + model.stwh_ptpname + "\" onkeydown=\"return checkNumber(event);\" value=\"" + value + "\" class=\"st-input-text-700 form-control\" placeholder=\"请输入" + model.stwh_ptptitle + "\" /></div><div class=\"line10\"></div>");
                    break;
                case "datetime":
                    sb.Append("<div class=\"col-sm-1 st-text-right\">" + model.stwh_ptptitle + "：</div><div class=\"col-sm-11 form-group\"><input type=\"text\" id=\"" + model.stwh_ptpname + "\" name=\"" + model.stwh_ptpname + "\" class=\"st-input-text-300 form-control\" placeholder=\"请输入" + model.stwh_ptptitle + "\" value=\"" + value + "\" /></div><div class=\"line10\"></div>");
                    js.Append("<script>$(function(){$('#" + model.stwh_ptpname + "').datepicker({format: 'yyyy-mm-dd'}).on(\"changeDate\", function (ev) {var dqdate = new Date();$(this).val($(this).val() + \" \" + dqdate.getHours() + \":\" + dqdate.getMinutes() + \":\" + dqdate.getSeconds());}).val(new Date().Format(\"yyyy-MM-dd hh:mm:ss\"));});</script>");
                    break;
                case "editor":
                    sb.Append("<div class=\"col-sm-1 st-text-right\">" + model.stwh_ptptitle + "：</div><div class=\"col-sm-11 form-group\"><textarea id=\"" + model.stwh_ptpname + "\" name=\"" + model.stwh_ptpname + "\" style=\"width: 700px; height: 300px;\"></textarea></div><div class=\"line10\"></div>");
                    js.Append("<script>var qjUEObject_" + model.stwh_ptpname + " = UE.getEditor('" + model.stwh_ptpname + "', qjOptions);qjUEObject_" + model.stwh_ptpname + ".setContent('" + value + "', false);</script>");
                    break;
                case "file":
                    sb.Append("<div class=\"col-sm-1 st-text-right\">" + model.stwh_ptptitle + "：</div><div class=\"col-sm-11 form-group\"><input id=\"" + model.stwh_ptpname + "\" name=\"" + model.stwh_ptpname + "\" type=\"text\" class=\"st-input-text-700 form-control\" value=\"" + value + "\" placeholder=\"请输入" + model.stwh_ptptitle + "\" style=\"float: left;\" /><div style=\"float: left;width: 100px;\"><div id=\"up" + model.stwh_ptpname + "\"></div></div><div class=\"clearfix\"></div></div><div class=\"line10\"></div>");
                    js.Append("<script>swfobject.embedSWF(\"/Plugin/upload.swf\", \"up" + model.stwh_ptpname + "\", \"95\", \"34\", \"11.0.0\", \"/Plugin/expressInstall.swf\", $.extend({}, flashvarsFile, { btnid: \"up" + model.stwh_ptpname + "\", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);function chkFileBtnId(btnid,msg){if(btnid==\"upwebico\"){$(\"#stwh_pimage\").val(msg)}else if(btnid==\"upwebico1\"){if($(\"#stwh_pimagelist\").val()){$(\"#stwh_pimagelist\").val($(\"#stwh_pimagelist\").val()+\",\"+msg)}else{$(\"#stwh_pimagelist\").val(msg)}$(\"#imglistPanel\").append('<img src=\"'+msg+'\" class=\"img-thumbnail\" alt=\"\" style=\"width:110px; height:110px; margin:0px 10px 10px 0px;\" />')}else if(btnid==\"up" + model.stwh_ptpname + "\"){$(\"#" + model.stwh_ptpname + "\").val(msg);}}</script>");
                    break;
                default:
                    break;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string mid = Request.QueryString["id"];
                if (PageValidate.IsNumber(mid))
                {
					SetMenuSpanText(this.menuSpan, "sys_commodity", "文章管理");
                    UpdateModel = new stwh_productBLL().GetModel(int.Parse(mid));
                    List<stwh_producttype> AllListData = new stwh_producttypeBLL().GetModelList("");
                    StringBuilder sb = new StringBuilder();
                    ArticleList(AllListData, sb, 0, 0);
                    this.selectShowList.InnerHtml = sb.ToString();

                    List<stwh_productbrand> AllListData1 = new stwh_productbrandBLL().GetModelList("");
                    StringBuilder sb1 = new StringBuilder();
                    foreach (stwh_productbrand item in AllListData1)
                    {
                        sb1.Append("<li><a data-pid=\"" + item.stwh_pbid + "\">" + item.stwh_pbname + "</a></li>");
                    }
                    this.selectShowBrand.InnerHtml = sb1.ToString();

                    #region 获取参数值
                    //获取当前商品分类的参数
                    List<stwh_producttype_p> typeparam = new stwh_producttype_pBLL().GetModelList("stwh_ptid = " + UpdateModel.stwh_ptid, 1);
                    //获取当前商品参数的数值
                    List<stwh_producttype_pv> pvlist = new stwh_producttype_pvBLL().GetModelList("stwh_pid = " + UpdateModel.stwh_pid);
                    StringBuilder sb2 = new StringBuilder();
                    StringBuilder js = new StringBuilder();
                    foreach (stwh_producttype_p item in typeparam)
                    {
                        CheckInputType(item, sb2,pvlist,js);
                    }
                    typePV = sb2.ToString();
                    this.LiteralJS.Text = js.ToString();
                    #endregion
                }
            }
            catch (Exception)
            {

            }
        }
    }
}
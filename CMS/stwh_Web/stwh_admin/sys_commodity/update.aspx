﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="update.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_commodity.update" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">文章管理</span></li>
        <li class="active">修改数据</li>
    </ol>
    <ul id="setTab" class="nav nav-tabs" style="margin-bottom:1px;">
        <li class="active" style="margin-left: 15px;"><a href="#setpanel0" ><span class="fa fa-wrench">
        </span>
            基本信息</a> </li>
        <li><a href="#setpanel1" ><span class="fa fa-link"></span>
            商品属性</a> </li>
        <li><a href="#setpanel2" ><span class="fa fa-link"></span>
            SEO选项</a> </li>
    </ul>
    <div id="scrollPanel">
    <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx" method="post" role="form">
    <div class="container-fluid">
        <div id="setTabContent" class="tab-content" style="padding-top: 15px;">
            <div class="tab-pane active" id="setpanel0">
                <div class="row">
                    <div class="col-sm-1 st-text-right">
                        商品分类：</div>
                    <div class="col-sm-11 form-group">
                        <input type="hidden" id="stwh_ptid" name="stwh_ptid" value="<%=UpdateModel.stwh_ptid%>" />
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            <span id="selectShowText">
                                <%=UpdateModel.stwh_ptname%></span> <span class="caret"></span>
                        </button>
                        <ul id="selectShowList" class="dropdown-menu" role="menu" runat="server" style="max-height: 300px;
                            overflow: auto; left: auto; top: auto;">
                            <li><a data-pid="0">父级导航</a></li>
                            <li><a data-pid="0">系统设置</a></li>
                            <li><a data-pid="0">系统管理</a></li>
                            <li class="divider"></li>
                            <li><a data-pid="0">内容管理</a></li>
                        </ul>
                    </div>
                    <div class="line10">
                    </div>
                    <div id="typeParam">
                        <%=typePV %>
                    </div>
                    <div class="col-sm-1 st-text-right">
                        商品品牌：</div>
                    <div class="col-sm-11 form-group">
                        <input type="hidden" id="stwh_pid" name="stwh_pid" value="<%=UpdateModel.stwh_pid %>" />
                        <input type="hidden" id="stwh_pbid" name="stwh_pbid" value="<%=UpdateModel.stwh_pbid %>" />
                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                            <span id="SpanBrand">
                                <%=UpdateModel.stwh_pbname%></span> <span class="caret"></span>
                        </button>
                        <ul id="selectShowBrand" class="dropdown-menu" role="menu" runat="server" style="max-height: 300px;
                            overflow: auto; left: auto; top: auto;">
                            <li><a data-pid="0">父级导航</a></li>
                            <li><a data-pid="0">系统设置</a></li>
                            <li><a data-pid="0">系统管理</a></li>
                            <li class="divider"></li>
                            <li><a data-pid="0">内容管理</a></li>
                        </ul>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        序号：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_porder" name="stwh_porder" onkeydown="return checkNumber(event);"
                            value="<%=UpdateModel.stwh_porder %>" class="st-input-text-300 form-control" />
                        <span class="text-danger">*越大越考前</span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        商品缩略图：</div>
                    <div class="col-sm-11 form-group">
                        <input id="stwh_pimage" name="stwh_pimage" type="text" value="<%=UpdateModel.stwh_pimage %>"
                            class="st-input-text-700 form-control" placeholder="请输入上传封面图" style="float: left;" />
                        <div style="float: left; width: 100px;">
                            <div id="upwebico">
                            </div>
                        </div>
                        <div class="clearfix">
                        </div>
                        <div id="progressFile" class="progress progress-striped active" style="display: none;">
                            <div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-1 st-text-right">
                        商品相关图片：</div>
                    <div class="col-sm-11 form-group">
                        <input id="stwh_pimagelist" name="stwh_pimagelist" type="hidden" value="<%=UpdateModel.stwh_pimagelist %>" />
                        <div id="upwebico1">
                        </div>
                        <div class="clearfix">
                        </div>
                        <div id="imglistPanel">
                            <%
                                foreach (string item in UpdateModel.stwh_pimagelist.Split(','))
                                {
                            %>
                            <img src="<%=item %>" class="img-thumbnail" alt="" />
                            <%
                                }
                            %>
                        </div>
                        <span class="text-danger">*请上传相关图片。点击图片即可删除图片</span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        商品简短名称：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_ptitlesimple" name="stwh_ptitlesimple" value="<%=UpdateModel.stwh_ptitlesimple%>"
                            class="st-input-text-700 form-control" placeholder="请输入文章简短标题" />
                        <span class="text-danger"></span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        商品完整名称：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_ptitle" name="stwh_ptitle" value="<%=UpdateModel.stwh_ptitle%>"
                            class="st-input-text-700 form-control" placeholder="请输入文章标题" />
                        <span class="text-danger">*栏目名称内容长度不能超过100个字符</span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        关键词：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_pbiaoqian" name="stwh_pbiaoqian" value="<%=UpdateModel.stwh_pbiaoqian%>"
                            class="st-input-text-700 form-control" placeholder="请输入关键词" />
                        <span class="text-danger">*每个关键词以英文逗号隔开，最多支持10个关键词</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-1 st-text-right">
                        生产国家：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_pcountry" name="stwh_pcountry" value="<%=UpdateModel.stwh_pcountry%>"
                            class="st-input-text-700 form-control" placeholder="请输入生产国家" />
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        生产厂商：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_pcompany" name="stwh_pcompany" value="<%=UpdateModel.stwh_pcompany%>"
                            class="st-input-text-700 form-control" placeholder="请输入生产厂商" />
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        商品价格：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_pprice" name="stwh_pprice" value="<%=UpdateModel.stwh_pprice%>"
                            onkeydown="return checkNumber(event);" class="st-input-text-700 form-control"
                            placeholder="请输入商品价格" />
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        生产日期：</div>
                    <div class="col-sm-11 form-group">
                        <div class=" input-group st-input-text-300">
                            <input type="text" name="stwh_pcreation" id="stwh_pcreation" placeholder="请选择生产日期"  class="form-control" value="<%=UpdateModel.stwh_pcreation.ToString("yyyy-MM-dd HH:mm:ss") %>">
                            <span id="calendar" class=" input-group-addon"  style=" cursor:pointer;"><span class="fa fa-calendar"></span></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-1 st-text-right">
                        商品简要描述：</div>
                    <div class="col-sm-11 form-group">
                        <textarea id="stwh_pdescription" name="stwh_pdescription" class="st-input-text-700 form-control"
                            placeholder="请输入商品简要描述"><%=UpdateModel.stwh_pdescription%></textarea>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        商品详细描述：</div>
                    <div class="col-sm-11 form-group">
                        <div id="stwh_pcontentPanel" style=" display:none;"><%=UpdateModel.stwh_pcontent%></div>
                        <textarea id="stwh_pcontent" name="stwh_pcontent" style="width: 700px; height: 300px;"></textarea>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="setpanel1">
                <div class="row" style="margin-top: 10px;">
                    <div class="col-sm-1 st-text-right">
                        商品属性：</div>
                    <div class="col-sm-11 form-group">
                        <label>
                            <input type="checkbox" name="stwh_piszhiding" value="1" <%=UpdateModel.stwh_piszhiding == 1?"checked=\"checked\"":"" %> />置顶</label>&nbsp;&nbsp;<label><input
                                type="checkbox" name="stwh_ptuijian" value="1" <%=UpdateModel.stwh_ptuijian == 1?"checked=\"checked\"":"" %> />推荐</label>&nbsp;&nbsp;<label><input
                                    type="checkbox" name="stwh_ptoutiao" value="1" <%=UpdateModel.stwh_ptoutiao == 1?"checked=\"checked\"":"" %> />头条</label>&nbsp;&nbsp;<label><input
                                        type="checkbox" name="stwh_pgundong" value="1" <%=UpdateModel.stwh_pgundong == 1?"checked=\"checked\"":"" %> />滚动</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-1 st-text-right">
                        添加日期：</div>
                    <div class="col-sm-11 form-group">
                        <div class=" input-group st-input-text-300">
                            <input type="text" name="stwh_paddtime" id="stwh_paddtime" placeholder="请输入时间"  class="form-control" value="<%=UpdateModel.stwh_paddtime.ToString("yyyy-MM-dd HH:mm:ss") %>">
                            <span id="calendar1" class=" input-group-addon"  style=" cursor:pointer;"><span class="fa fa-calendar"></span></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-1 st-text-right">
                        审核状态：</div>
                    <div class="col-sm-11 form-group">
                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-primary btn-sm <%=UpdateModel.stwh_pissh==0?"active":"" %>">
                                <input type="radio" name="stwh_pissh" id="stwh_pissh0" value="0" <%=UpdateModel.stwh_pissh == 0?"checked=\"checked\"":"" %> />
                                待审核
                            </label>
                            <label class="btn btn-primary btn-sm <%=UpdateModel.stwh_pissh==1?"active":"" %>">
                                <input type="radio" name="stwh_pissh" id="stwh_pissh1" value="1" <%=UpdateModel.stwh_pissh == 1?"checked=\"checked\"":"" %> />
                                已审核
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="setpanel2">
                <div class="row">
                    <div class="col-sm-1 st-text-right">
                        seo标题：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" class="st-input-text-300 form-control" name="stwh_pseotitle" id="stwh_pseotitle"
                            placeholder="请输入seo标题" value="<%=UpdateModel.stwh_pseotitle %>" />
                        <span class="text-danger">*留空则显示文章标题</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-1 st-text-right">
                        页面关键词：</div>
                    <div class="col-sm-11 form-group">
                        <input type="text" class="st-input-text-700 form-control" name="stwh_psetokeywords"
                            value="<%=UpdateModel.stwh_psetokeywords %>" id="stwh_psetokeywords" placeholder="请输入seo关键词" />
                        <span class="text-danger">*留空则显示文章关键词，格式以英文逗号隔开</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-1 st-text-right">
                        页面描述：</div>
                    <div class="col-sm-11 form-group">
                        <textarea class="st-input-text-700 form-control" name="stwh_psetodescription" id="stwh_psetodescription"
                            placeholder="请输入seo页面描述"><%=UpdateModel.stwh_psetodescription%></textarea>
                        <span class="text-danger">*留空则显示文章导读</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-edit"></i> 修改</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <cms:stwhuescript ID="stwhuescript" runat="server" />
    <script src="/Plugin/swfobject.js" type="text/javascript"></script>
    <script type="text/javascript">
        //初始化富文本编辑器
        var qjOptions = {
            // 服务器统一请求接口路径
            serverUrl: "/handler/ueditor/controller.ashx",
            wordCount: true, //是否开启字数统计
            maximumWords: 5000, //允许的最大字符数
            elementPathEnabled: true, //是否启用元素路径，默认是显示
            autoHeightEnabled: false, //是否自动长高,默认true
            zIndex: 900, //编辑器层级的基数,默认是900
            emotionLocalization: true, //是否开启表情本地化，默认关闭。若要开启请确保emotion文件夹下包含官网提供的images表情文件夹
            autoFloatEnabled: false, //当设置为ture时，工具栏会跟随屏幕滚动，并且始终位于编辑区域的最上方
            pasteplain: false, //是否默认为纯文本粘贴。false为不使用纯文本粘贴，true为使用纯文本粘贴
            enableAutoSave: true, //启用自动保存
            saveInterval: 3000, //自动保存间隔时间
            allowDivTransToP: false,
            toolbars: [
                        ['fullscreen', 'source', 'cleardoc', 'undo', 'redo', '|',
                        'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', '|',
                         'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', '|',
                        'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
                        'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                        'directionalityltr', 'directionalityrtl', 'indent', '|',
                        'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                        'touppercase', 'tolowercase', 'simpleupload', 'insertimage', '|',
                        'link', 'unlink', '|',
                        'horizontal', 'date', 'time', 'spechars', '|',
                        'inserttable', 'deletetable', 'preview']
                    ]//工具栏
        };
        var qjUEObject = UE.getEditor('stwh_pcontent', qjOptions);
        setTimeout(function () {
            //qjUEObject.execCommand('drafts'); //载入本地数据（若存在）
            qjUEObject.setContent($("#stwh_pcontentPanel").html(), false);
            $("#stwh_ptitle").focus();
            $(window).scrollTop(0);
        }, 1000);
    </script>
    <script type="text/javascript">
        var currentTabId = "#setpanel0";
        $(function () {
            $("#selectShowList a").click(function () {
                $("#selectShowText").text($(this).text().Trim().split('.')[1]);
                var ptid = $(this).attr("data-pid");
                $("#stwh_ptid").val(ptid);

                //获取商品分类参数信息
                $.post("/Handler/stwh_admin/sys_commodity/parameter.ashx", {
                    ptid: ptid
                }, function (data) {
                    $("#typeParam").html(data);
                }, "text");
            });
            $("#selectShowBrand a").click(function () {
                $("#SpanBrand").text($(this).text().Trim());
                $("#stwh_pbid").val($(this).attr("data-pid"));
            });
            $("#addMenu").click(function () {
                var stwh_ptid = $("#stwh_ptid").val();
                if (stwh_ptid == "0") {
                    $.bs.alert("请选择分类！", "info");
                    return false;
                }
                var stwh_pbid = $("#stwh_pbid").val();
                if (stwh_pbid == "0") {
                    $.bs.alert("请选择品牌！", "info");
                    return false;
                }
                var stwh_pimage = $("#stwh_pimage").val();
                var stwh_ptitle = $("#stwh_ptitle").val();
                if (!stwh_ptitle) {
                    $.bs.alert("请输入商品名称！", "info");
                    return false;
                }
                var stwh_pbiaoqian = $("#stwh_pbiaoqian").val();
                if (stwh_pbiaoqian) {
                    var stwh_pbiaoqians = stwh_pbiaoqian.split(',');
                    if (stwh_pbiaoqians.length > 10) {
                        $.bs.alert("关键词数量超过限制！", "info");
                        return false;
                    }
                }
                var stwh_atauthor = $("#stwh_atauthor").val();
                var stwh_atsource = $("#stwh_atsource").val();
                var stwh_pdescription = $("#stwh_pdescription").val();
                var stwh_pcontent = qjUEObject.getContent();
                if (!stwh_pcontent) {
                    $.bs.alert("请输入商品描述信息！", "info");
                    return false;
                }
                var stwh_paddtime = $("#stwh_paddtime").val();
                if (!CheckDateTime(stwh_paddtime)) {
                    $.bs.alert("商品添加时间格式错误！", "info");
                    return false;
                }
                var stwh_pseotitle = $("#stwh_pseotitle").val();
                if (stwh_pseotitle) {
                    if (!IsHanZF(1, 100, stwh_pseotitle)) {
                        $.bs.alert("seo标题长度超出服务器限制100字符！", "info");
                        return false;
                    }
                }
                var stwh_psetokeywords = $("#stwh_psetokeywords").val();
                if (stwh_psetokeywords) {
                    if (!IsHanZF(1, 100, stwh_psetokeywords)) {
                        $.bs.alert("页面关键词长度超出服务器限制100字符！", "info");
                        return false;
                    }
                }
                var stwh_psetodescription = $("#stwh_psetodescription").val();
                if (stwh_psetodescription) {
                    if (!IsHanZF(1, 200, stwh_psetodescription)) {
                        $.bs.alert("页面描述长度超出服务器限制200字符！", "info");
                        return false;
                    }
                }

                $.post("/Handler/stwh_admin/sys_commodity/update.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        qjUEObject.setContent("");
                        //清除本地数据
                        qjUEObject.execCommand("clearlocaldata");
                        $.bs.alert(data.msg, "success", "-1");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });

            $('#stwh_paddtime,#stwh_pcreation').datepicker({
                format: 'yyyy-mm-dd'
            }).on("changeDate", function (ev) {
                var dqdate = new Date();
                $(this).val($(this).val() + " " + dqdate.getHours() + ":" + dqdate.getMinutes() + ":" + dqdate.getSeconds());
            }).val(new Date().Format("yyyy-MM-dd hh:mm:ss"));
            $("#calendar").click(function () {
                $('#stwh_pcreation').click();
            });
            $("#calendar1").click(function () {
                $('#stwh_paddtime').click();
            });
            $('#stwh_pcreation,#stwh_paddtime').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                autoApply: true,
                autoUpdateInput: false,
                startDate: '<%=UpdateModel.stwh_pcreation.ToString("yyyy-MM-dd HH:mm:ss") %>',
                locale: {
                    format: 'YYYY-MM-DD HH:mm:ss'
                }
            }).on({
                "apply.daterangepicker": function (ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD') + ' ' + new Date().Format("hh:mm:ss"));
                }
            });
            $('#stwh_paddtime').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                autoApply: true,
                autoUpdateInput: false,
                startDate: '<%=UpdateModel.stwh_paddtime.ToString("yyyy-MM-dd HH:mm:ss") %>',
                locale: {
                    format: 'YYYY-MM-DD HH:mm:ss'
                }
            }).on({
                "apply.daterangepicker": function (ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD') + ' ' + new Date().Format("hh:mm:ss"));
                }
            });

            $("#imglistPanel").on("click", "img", function () {
                var path = $(this).attr("src");
                var imgliststr = $("#stwh_pimagelist").val();
                var imglist = imgliststr.split(',');
                var l1 = imglist.length;
                imglist.removeIndexArray(imglist.IndexOfArray(imgliststr));
                var l2 = imglist.length;
                $("#stwh_pimagelist").val(imglist.toStringArray());
                $(this).remove();
            });
            $("#setTab li").click(function () {
                if ($(this).children("a").attr("href") != currentTabId) {
                    $(currentTabId).removeClass("active");
                    $("#setTab li").removeClass("active");
                    $(this).addClass("active");
                    currentTabId = $(this).children("a").attr("href");
                    $(currentTabId).addClass("active");
                    UpdateScroll();
                }
            });
        });
    </script>
    <script type="text/javascript">
        var auth = "<% = Request.Cookies[FormsAuthentication.FormsCookieName]==null ? string.Empty : Request.Cookies[FormsAuthentication.FormsCookieName].Value %>";
        var ASPSESSID = "<%= Session.SessionID %>";
        var ttfile = "*." + ("<%=WebSite.Webfiletype %>").replace(/,/ig, ";*.");
        var flashvarsFile = {
            IsSelect: "false", //运行多选文件进行上传
            Url: "/Handler/uploadHandler.ashx",
            FileExtension: ttfile,
            FileType: "image",
            MaxSize: "<%=WebSite.Webfilesize*1024 %>" //500KB 字节为单位
        };
        var flashvarsVoice = {
            IsSelect: "false", //运行多选文件进行上传
            Url: "/Handler/uploadHandler.ashx",
            FileExtension: "*.jpg;*.JPG;*.gif;*.GIF;*.png;*.PNG",
            FileType: "image",
            MaxSize: "<%=WebSite.Webthumbnail %>" //500KB 字节为单位
        };
        var flashvarsVoice1 = {
            IsSelect: "true", //运行多选文件进行上传
            Url: "/Handler/uploadHandler.ashx",
            FileExtension: "*.jpg;*.JPG;*.gif;*.GIF;*.png;*.PNG",
            FileType: "image",
            MaxSize: "1048576" //1MB 字节为单位
        };
        var params = {
            wmode: "transparent",
            play: true,
            loop: true,
            menu: true,
            devicefont: false,
            scale: "showall",
            quality: "high",
            bgcolor: "#ffffff",
            allowScriptAccess: "sameDomain",
            salign: ""
        };
        swfobject.embedSWF("/Plugin/upload.swf", "upwebico", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsVoice, { btnid: "upwebico", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);
        swfobject.embedSWF("/Plugin/upload.swf", "upwebico1", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsVoice1, { btnid: "upwebico1", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);
        //设置进度
        function setProgress(info, name, IsSelect, btnid) {
            if (btnid == "upwebico") {
                $("#progressFile").show().children().first().css("width", info + "%");
            }
        }

        //上传完成时调用的方法（该方法被flash所调用）
        //filename:文件安上传成功后返回的文件名，包含扩展名,altString提示信息,msg服务器返回的成功消息
        function EndUpload(filename, altString, msg, btnid) {
            if (msg == "NoFile") {
                $.bs.alert("请选择文件！", "info");
            }
            else if (msg == "NoType") {
                $.bs.alert("请选择指定类型文件！", "info");
            }
            else if (msg == "NoSize") {
                $.bs.alert("文件大小超出限制，请联系管理员！", "info");
            }
            else if (msg == "login") {
                $.bs.alert("请先登录！", "info");
            }
            else if (msg == "Exception") {
                $.bs.alert("请稍后操作！", "info");
            }
            else {
                if ((/image/gi).test(msg)) {
                    chkFileBtnId(btnid, msg);
                }
                else {
                    $.bs.alert(msg, "info");
                }
            }
        }

        //上传失败时调用的方法（该方法被flash所调用）
        function ErrorUpload(msg) {
            $.bs.alert(msg, "info");
        }

        function chkFileBtnId(btnid, msg) {
            if (btnid == "upwebico") {
                $("#stwh_pimage").val(msg);
            }
            else if (btnid == "upwebico1") {
                if ($("#stwh_pimagelist").val()) {
                    $("#stwh_pimagelist").val($("#stwh_pimagelist").val() + "," + msg);
                }
                else {
                    $("#stwh_pimagelist").val(msg);
                }
                $("#imglistPanel").append('<img src="' + msg + '" class="img-thumbnail" alt="" style="width:110px; height:110px; margin:0px 10px 10px 0px;" />');
            }
        }
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>

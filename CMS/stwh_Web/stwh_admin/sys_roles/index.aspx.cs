﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_roles
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_roles", "角色管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);
                //获取未删除角色信息
                int ss = 0, cc = 0;
                List<stwh_roleinfo> ListData = new stwh_roleinfoBLL().GetListByPage<stwh_roleinfo>("stwh_rid", "desc", "stwh_rdelstate = 0", 15, 0, ref ss, ref cc, 0);
                totalPages = cc / 15 + ((cc % 15) == 0 ? 0 : 1);
                if (totalPages == 0) totalPages = 1;
                this.hidTotalSum.Value = cc + "";
                this.littotalSum.Text = cc + "";
                string strDatas = "";
                foreach (stwh_roleinfo item in ListData)
                {
                    strDatas += "<tr data-id=\"" + item.stwh_rid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_rid + "\" value=\"" + item.stwh_rid + "\" /></td><td>" + item.stwh_rid + "</td><td>" + item.stwh_rname + "</td><td>" + item.stwh_rdescription + "</td><td>" + item.stwh_rctime.ToString("yyyy-MM-dd HH:mm:ss") + "</td><td>" + (item.stwh_rid == 1 ? "" : (item.stwh_rstate == 0 ? "<div class=\"btn-group\"data-toggle=\"buttons\"><label class=\"btn btn-primary btn-sm active\"><input type=\"radio\"name=\"radp" + item.stwh_rid + "\"value=\"0\" checked=\"checked\"/>启用</label><label class=\"btn btn-primary btn-sm\"><input type=\"radio\"name=\"radp" + item.stwh_rid + "\"value=\"1\"/>禁用</label></div>" : "<div class=\"btn-group\"data-toggle=\"buttons\"><label class=\"btn btn-primary btn-sm\"><input type=\"radio\"name=\"radp" + item.stwh_rid + "\"value=\"0\"/>启用</label><label class=\"btn btn-primary btn-sm active\"><input type=\"radio\"name=\"radp" + item.stwh_rid + "\"value=\"1\"/>禁用</label></div>")) + "</td></tr>";
                }
                if (string.IsNullOrEmpty(strDatas)) this.ChildDatas.InnerHtml = "<tr><td colspan=\"6\" align=\"center\">暂无数据</td></tr>";
                else this.ChildDatas.InnerHtml = strDatas;
                this.hidAllRoleData.Value = JsonConvert.SerializeObject(ListData);
            }
        }
    }
}
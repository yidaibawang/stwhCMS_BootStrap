﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using System.Text;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;

namespace stwh_Web.stwh_admin.sys_roles
{
    public partial class setting : Common.PageBase
    {
        public string mids = "";
        public string fids = "";
        private List<int> listMenuIds = new List<int>();//储存菜单id
        private List<int> listFunctionIds = new List<int>();//储存菜单功能id

        /// <summary>
        /// 将id存入集合中（不添加重复值）
        /// </summary>
        /// <param name="flag">标记（0表示是菜单id集合，1表示是菜单功能id）</param>
        /// <param name="mid">待存入的id</param>
        private void AddId(int flag, int mid)
        {
            switch (flag)
            {
                case 0:
                    if (listMenuIds.Count == 0) listMenuIds.Add(mid);
                    else
                    {
                        List<int> chklist = listMenuIds.Where(a => a == mid).ToList<int>();
                        if (chklist.Count == 0) listMenuIds.Add(mid);
                    }
                    break;
                default:
                    if (listFunctionIds.Count == 0)
                    {
                        listFunctionIds.Add(mid);
                    }
                    else
                    {
                        List<int> chklist = listFunctionIds.Where(a => a == mid).ToList<int>();
                        if (chklist.Count == 0) listFunctionIds.Add(mid);
                    }
                    break;
            }
        }

        /// <summary>
        /// 将id集合转换为字符串（以英文逗号隔开）
        /// </summary>
        /// <param name="listMenuIds">存id的集合</param>
        /// <returns></returns>
        private string CreateIds(List<int> listIds)
        {
            string jbmids = "";
            foreach (int item in listIds)
            {
                jbmids += (item + ",");
            }
            return jbmids != "" ? jbmids.Substring(0, jbmids.Length - 1) : "";
        }

        /// <summary>
        /// 生成角色权限菜单数据
        /// </summary>
        /// <param name="menusData">菜单数据</param>
        /// <param name="functionData">菜单功能数据</param>
        /// <param name="menuRoleList">角色拥有的菜单数据</param>
        /// <param name="menuRoleFunctionList">角色拥有的菜单功能数据</param>
        /// <param name="SaveList">储存对象</param>
        /// <param name="code">父菜单id</param>
        private void CreateMenus(List<stwh_menuinfo> menusData, List<stwh_function> functionData, List<stwh_menu_role> menuRoleList, List<stwh_menu_role_function> menuRoleFunctionList, StringBuilder SaveList, int code, int dj)
        {
            //查询指定父编号的菜单，并按照序号进行升序排序
            List<stwh_menuinfo> chkmenulist = menusData.Where(a => a.stwh_menuparentID == code).OrderByDescending(a => a.stwh_menuorder).ToList<stwh_menuinfo>();
            if (chkmenulist.Count != 0)
            {
                foreach (stwh_menuinfo item in chkmenulist)
                {
                    string nullstring = "├";
                    string menuName = item.stwh_menuname;
                    string bgstyle = "style=\"background-color:#eaeaea;\"";
                    if (dj == 0) menuName = "<span style=\"font-size:18px; font-weight:bold;\">" + item.stwh_menuname + "</span>";
                    else if (dj == 1)
                    {
                        menuName = "<span style=\"font-size:15px; font-weight:bold;\">" + item.stwh_menuname + "</span>";
                        bgstyle = "style=\"background-color:#f3f3f3;\"";
                    }
                    else bgstyle = "style=\"background-color:#fff;\"";
                    string btn = "";
                    if (code == 0) btn = "<button type=\"button\" class=\"btn btn-primary btn-xs zhedie\" data-id=\"" + item.stwh_menuid + "\" data-flag=\"0\">折叠</button>";
                    for (int i = 0; i < dj; i++) nullstring += "──";
                    //查询和角色菜单id是否相等，相等的选中复选框
                    List<stwh_menu_role> mrllist = menuRoleList.Where(a => a.stwh_menuid == item.stwh_menuid).ToList<stwh_menu_role>();
                    if (mrllist.Count == 0) SaveList.Append("<tr " + bgstyle + " class=\"menuline" + code + "\" data-id=\"" + item.stwh_menuid + "\"><td><input type=\"checkbox\" name=\"menu\" class=\"pmenuid menuid" + item.stwh_menuid + "\" value=\"" + item.stwh_menuid + "\" data-id=\"" + item.stwh_menuid + "\" data-pid = \"" + item.stwh_menuparentID + "\" /> " + nullstring + menuName + "&nbsp;&nbsp;" + btn + "</td><td>");
                    else
                    {
                        AddId(0, item.stwh_menuid);
                        AddId(0, item.stwh_menuparentID);
                        SaveList.Append("<tr " + bgstyle + " class=\"menuline" + code + "\" data-id=\"" + item.stwh_menuid + "\"><td><input type=\"checkbox\" name=\"menu\" class=\"pmenuid menuid" + item.stwh_menuid + "\" value=\"" + item.stwh_menuid + "\" data-id=\"" + item.stwh_menuid + "\" data-pid = \"" + item.stwh_menuparentID + "\" checked=\"checked\" /> " + nullstring + menuName + "&nbsp;&nbsp;" + btn + "</td><td>");
                    }
                    //查询当前菜单下的功能按钮
                    foreach (stwh_function func in functionData)
                    {
                        if (item.stwh_menuid == func.stwh_menuid)
                        {
                            //查询和角色权限的功能id是否相等，相等的选中复选框
                            List<stwh_menu_role_function> mrflist = menuRoleFunctionList.Where(a => a.stwh_fid == func.stwh_fid).ToList<stwh_menu_role_function>();
                            if (mrflist.Count == 0) SaveList.Append("<input type=\"checkbox\" name=\"function\" id=\"f" + func.stwh_fid + "\" class=\"function\" value=\"" + func.stwh_fid + "\" data-id=\"" + func.stwh_menuid + "\" data-pid = \"" + item.stwh_menuparentID + "\"/><lable for=\"f" + func.stwh_fid + "\">" + func.stwh_fname + "</lable>&nbsp;&nbsp;");
                            else
                            {
                                AddId(1, func.stwh_fid);
                                SaveList.Append("<input type=\"checkbox\" name=\"function\" class=\"function\" id\"f" + func.stwh_fid + "\" value=\"" + func.stwh_fid + "\" data-id=\"" + func.stwh_menuid + "\" data-pid = \"" + item.stwh_menuparentID + "\" checked=\"checked\" /> <lable for=\"f" + func.stwh_fid + "\">" + func.stwh_fname + "</lable>&nbsp;&nbsp;");
                            }
                        }
                    }
                    SaveList.Append("</td></tr>");
                    CreateMenus(menusData, functionData, menuRoleList, menuRoleFunctionList, SaveList, item.stwh_menuid, dj + 1);
                }
            }
            else dj = dj - 1;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            SetMenuSpanText(this.menuSpan, "sys_roles", "角色管理");
            List<stwh_menu_role> menuRoleList = new List<stwh_menu_role>();
            List<stwh_menu_role_function> menuRoleFunctionList = new List<stwh_menu_role_function>();

            string mid = Request.QueryString["id"];
            if (PageValidate.IsNumber(mid))
            {
                stwh_roleinfo model = new stwh_roleinfoBLL().GetModel(int.Parse(mid));
                this.hidRoleId.Value = model.stwh_rid + "";
                this.labRoleName.Text = model.stwh_rname;

                //获取当前角色的菜单
                menuRoleList = new stwh_menu_roleBLL().GetModelList("stwh_rid = " + model.stwh_rid);
                //获取当前角色的功能
                menuRoleFunctionList = new stwh_menu_role_functionBLL().GetModelList("stwh_rid = " + model.stwh_rid);
            }
            List<stwh_menuinfo> childMenu = GetMenuList();
            List<stwh_function> listfunction = GetFunctionList();

            this.hidAllData.Value = JsonConvert.SerializeObject(childMenu);

            StringBuilder sb = new StringBuilder();
            CreateMenus(GetMenuList(),GetFunctionList(),menuRoleList,menuRoleFunctionList, sb, 0, 0);

            if (sb == null || sb.Length == 0) this.ChildDatas.InnerHtml = "<tr><td colspan=\"2\" align=\"center\">暂无数据</td></tr>";
            else this.ChildDatas.InnerHtml = sb.ToString();

            mids = CreateIds(listMenuIds);
            fids = CreateIds(listFunctionIds);

            this.hidMenuIds.Value = mids;
            this.hidFunctionIds.Value = fids;
        }
    }
}
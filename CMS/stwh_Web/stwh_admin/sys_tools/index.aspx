﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_tools.index" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <link href="/plugin/jQuery-colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading">
        <i class="fa fa-spinner fa-spin fa-2x"></i>
    </div>
    <ol class="breadcrumb location scrollTop" style="margin-bottom: 1px;">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li class="active"><span id="menuSpan" runat="server">常用工具</span></li>
    </ol>
    <div id="scrollPanel">
        <form id="form1" runat="server">
        <div class="container-fluid">
            <h3>
                工具列表</h3>
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="thumbnail">
                        <a href="http://tool.chinaz.com/" target="_blank" class="text-muted">
                            <img src="../css/tools/1.png" /></a>
                    </div>
                    <div class="caption">
                        <h4>
                            站长工具</h4>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="thumbnail">
                        <a href="http://seo.chinaz.com/" target="_blank" class="text-muted">
                            <img src="../css/tools/1.png" /></a>
                    </div>
                    <div class="caption">
                        <h4>
                            SEO综合查询</h4>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="thumbnail">
                        <a href="http://alexa.chinaz.com/" target="_blank" class="text-muted">
                            <img src="../css/tools/1.png" /></a>
                    </div>
                    <div class="caption">
                        <h4>
                            网站alexa排名</h4>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="thumbnail">
                        <a href="http://mytool.chinaz.com/baidusort.aspx" target="_blank" class="text-muted">
                            <img src="../css/tools/1.png" /></a>
                    </div>
                    <div class="caption">
                        <h4>
                            百度权重查询</h4>
                    </div>
                </div>
            </div>
            <div style="height:50px;"></div>
        </div>
        </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
        <div class="form-group">
            <label>
                更多工具，整理中....</label>
        </div>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>

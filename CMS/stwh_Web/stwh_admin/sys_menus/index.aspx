﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_menus.index" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li class="active"><span id="menuSpan" runat="server">菜单管理</span></li>
    </ol>
    <div class="m-top-10 p-bottom-15 p-left-15 scrollTop">
        <form class="form-inline">
            <asp:Literal ID="litBtnList" runat="server"></asp:Literal>
        </form>
    </div>
    <div id="scrollPanel">
    <form id="form1" runat="server">
    <div class="container-fluid">
        <asp:HiddenField ID="hidMenuIds" runat="server" />
        <asp:HiddenField ID="hidAllMenuData" runat="server" />
        <table class="table table-striped table-bordered table-hover table-condensed">
            <thead>
                <tr>
                    <th style="width: 50px;">
                        <input type="checkbox" id="allParent" />
                    </th>
                    <th style="width: 100px;">
                        图标
                    </th>
                    <th style="width: 180px;">
                        名称（中）
                    </th>
                    <th style="width: 250px;">
                        名称（英）
                    </th>
                    <th style="width: 250px;">
                        URL
                    </th>
                    <th>
                        描述
                    </th>
                    <th style="width: 150px;">
                        状态
                    </th>
                    <th style="width: 50px;">
                        序号
                    </th>
                </tr>
            </thead>
            <tbody id="MenuParent" runat="server">
            </tbody>
        </table>
    </div>
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
        <div class="form-group">
            <label>
                每页显示：</label>
            <select id="basic" class="selectpicker show-tick" disabled="disabled" data-width="55px"
                data-style="btn-sm btn-default">
                <option>15</option>
                <option>25</option>
                <option>35</option>
                <option>45</option>
                <option>55</option>
            </select>
        </div>
        <div class="form-group">
            <ul id="pageUl" style="margin: 0px auto;">
            </ul>
        </div>
        <div class="form-group">
            共 <span id="SumCountSpan"><asp:Literal ID="littotalSum" runat="server"></asp:Literal></span> 条数据
        </div>
        </form>
    </div>
    <div class="modal fade" id="deleteMenuModal" tabindex="-1" role="dialog" aria-labelledby="updatePwdTitle"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="updatePwdTitle">
                        <i class="fa fa-exclamation-circle"></i> 系统提示
                    </h4>
                </div>
                <div class="modal-body">
                    <h4>
                        <span class="glyphicon glyphicon-info-sign"></span>你确定要删除菜单吗（删除后不可恢复）？</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        取消
                    </button>
                    <button type="button" class="btn btn-default" id="btnDeleteMenuOk">
                        确定
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script type="text/javascript">
        var AllMenuData = JSON.parse($("#hidAllMenuData").val());
        var mid = [];
        $(function () {
            $("#pageUl").bootstrapPaginator({
                currentPage: 1, //当前页面
                totalPages: <%=totalPages %>, //总页数
                numberOfPages: 5, //页码数
                useBootstrapTooltip: true,
                onPageChanged: function (event, oldPage, newPage) {
                    
                }
            });
            //全选、反选
            $("#allParent").click(function () {
                if ($(this).is(":checked")) {
                    $("#MenuParent :checkbox").prop("checked", $(this).is(":checked"));
                    $("#MenuParent :checkbox:checked").each(function (i, item) {
                        if (mid.IndexOfArray($(item).val()) == -1) mid.AddArray($(item).val());
                    });
                    $("#hidMenuIds").val(mid.toStringArray());
                }
                else {
                    $("#MenuParent :checkbox").prop("checked", $(this).is(":checked"));
                    $("#MenuParent :checkbox:not(:checked)").each(function (i, item) {
                        mid.removeIndexArray(mid.IndexOfArray($(item).val()));
                    });
                    $("#hidMenuIds").val(mid.toStringArray());
                }
            });
            $("#MenuParent :checkbox").click(function () {
                if ($(this).is(":checked")) {
                    if ($("#MenuParent :checkbox:checked").length == $("#MenuParent :checkbox").length) $("#allParent").prop("checked", true);
                    if (mid.IndexOfArray($(this).val()) == -1) mid.AddArray($(this).val());
                    $("#hidMenuIds").val(mid.toStringArray());
                }
                else {
                    if ($("#MenuParent :checkbox:not(:checked)").length == $("#MenuParent :checkbox").length) $("#allParent").prop("checked", false);
                    mid.removeIndexArray(mid.IndexOfArray($(this).val()));
                    $("#hidMenuIds").val(mid.toStringArray());
                }
            });
            $("#btnDelete").click(function () {
                $("#deleteMenuModal").modal('show');
            });
            $("#btnDeleteMenuOk").click(function () {
                var mids = $("#hidMenuIds").val();
                if (!mids) {
                    $.bs.alert("请选择数据！", "info");
                    return false;
                }
                else if (!(/^[\d,]*$/).test(mids)) {
                    $.bs.alert("非法数据格式，请刷新页面重试！", "danger");
                    return false;
                }
                $.post(
                "/handler/stwh_admin/sys_menus/delete.ashx",
                {
                    mids: mids
                }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
            });
            $("#btnUpdate").click(function () {
                $("#MenuParent :checkbox:checked").each(function (i, item) {
                    if (i == 0) window.location.href = "update.aspx?id=" + $(item).val();
                });
                $("#MenuChild :checkbox:checked").each(function (i, item) {
                    if (i == 0) window.location.href = "update.aspx?id=" + $(item).val();
                });
                return false;
            });
            $("#MenuParent").on("click", ".btn-group label", function (e) {
                var jb_status = $(this).children(":radio").val();
                var jb_mid = $(this).parent().parent().parent().attr("data-mid");

                $.each(AllMenuData, function (index, item) {
                    if (item.stwh_menuid == parseInt(jb_mid)) {
                        item.stwh_menustatus = parseInt(jb_status);
                        return false;
                    }
                });
            });
            $("#MenuParent").on("keyup", "input[type='text']", function () {
                var jb_order = $(this).val();
                var jb_mid = $(this).parent().parent().attr("data-mid");

                $.each(AllMenuData, function (index, item) {
                    if (item.stwh_menuid == parseInt(jb_mid)) {
                        item.stwh_menuorder = parseInt(jb_order);
                        return false;
                    }
                });
            }).on("change", "input[type='text']", function () {
                var jb_order = $(this).val();
                var jb_mid = $(this).parent().parent().attr("data-mid");

                $.each(AllMenuData, function (index, item) {
                    if (item.stwh_menuid == parseInt(jb_mid)) {
                        item.stwh_menuorder = parseInt(jb_order);
                        return false;
                    }
                });
            });
            $("#btnSave").click(function () {
                $.post(
                "/handler/stwh_admin/sys_menus/save.ashx",
                { data: JSON.stringify(AllMenuData) },
                function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>

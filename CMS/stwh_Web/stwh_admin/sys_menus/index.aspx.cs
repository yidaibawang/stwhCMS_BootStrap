﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;
using System.Text;

namespace stwh_Web.stwh_admin.sys_menus
{
    public partial class index : Common.PageBase
    {
        public int totalPages = 0;

        /// <summary>
        /// 生成菜单
        /// </summary>
        /// <param name="menusData">菜单数据</param>
        /// <param name="SaveList">存放菜单集合</param>
        /// <param name="code">父菜单id</param>
        /// <param name="dj">等级</param>
        private void CreateMenus(List<stwh_menuinfo> menusData, StringBuilder SaveList, int code, int dj)
        {
            //查询指定父编号的菜单，并按照序号进行升序排序
            List<stwh_menuinfo> chkmenulist = menusData.Where(a => a.stwh_menuparentID == code).OrderByDescending(a => a.stwh_menuorder).ToList<stwh_menuinfo>();
            if (chkmenulist.Count != 0)
            {
                foreach (stwh_menuinfo item in chkmenulist)
                {
                    string nullstring = "├";
                    for (int i = 0; i < dj; i++) nullstring += "──";
                    SaveList.Append("<tr data-mid=\"" + item.stwh_menuid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chkp" + item.stwh_menuid + "\" value=\"" + item.stwh_menuid + "\" /></td><td><span class=\"glyphicon  " + item.stwh_menuICO_url + "\"></span></td><td>" + nullstring + "&nbsp;" + item.stwh_menuname + "</td><td>" + item.stwh_menunameUS + "</td><td>" + item.stwh_menuURL + "</td><td>" + item.stwh_menudescription + "</td><td>" + (item.stwh_menustatus == 0 ? "<div class=\"btn-group\"data-toggle=\"buttons\"><label class=\"btn btn-primary btn-sm active\"><input type=\"radio\"name=\"radp" + item.stwh_menuid + "\"value=\"0\" checked=\"checked\"/>显示</label><label class=\"btn btn-primary btn-sm\"><input type=\"radio\"name=\"radp" + item.stwh_menuid + "\"value=\"1\"/>隐藏</label></div>" : "<div class=\"btn-group\"data-toggle=\"buttons\"><label class=\"btn btn-primary btn-sm\"><input type=\"radio\"name=\"radp" + item.stwh_menuid + "\"value=\"0\"/>显示</label><label class=\"btn btn-primary btn-sm active\"><input type=\"radio\"name=\"radp" + item.stwh_menuid + "\"value=\"1\"/>隐藏</label></div>") + "</td><td><input type=\"text\" min=\"0\" value=\"" + item.stwh_menuorder + "\" onkeydown=\"return checkNumber(event);\" style=\"width: 50px;\"/></td></tr>");
                    CreateMenus(menusData, SaveList, item.stwh_menuid, dj + 1);
                }
            }
            else dj = dj - 1;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string mid = Request.QueryString["mid"];
            if (string.IsNullOrEmpty(mid) || !PageValidate.IsNumber(mid)) Response.Redirect("/stwh_admin/invalid.htm");
            else
            {
                SetMenuSpanText(this.menuSpan, "sys_menus", "菜单管理");
                //设置功能按钮
                this.litBtnList.Text = GetFunctionForRole(mid);
                #region 获取菜单
                List<stwh_menuinfo> allmenu = GetMenuList();
                //totalPages = allmenu.Count / 15 + ((allmenu.Count % 15) == 0 ? 0 : 1);
                //if (totalPages == 0) totalPages = 1;
                this.littotalSum.Text = allmenu.Count + "";
                //获取所有菜单数据并序列化json字符串
                this.hidAllMenuData.Value = JsonConvert.SerializeObject(allmenu);
                StringBuilder sb = new StringBuilder();
                CreateMenus(allmenu, sb, 0,0);

                if (sb == null || sb.Length == 0) this.MenuParent.InnerHtml = "<tr><td colspan=\"8\" align=\"center\">暂无数据</td></tr>";
                else this.MenuParent.InnerHtml = sb.ToString();
                #endregion
            }
        }
    }
}
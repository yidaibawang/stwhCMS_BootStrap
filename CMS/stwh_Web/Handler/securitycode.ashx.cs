﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Text;
using System.IO;
/*
 * 在ashx文件中，若要对Session进行成功的读写，
 * 应该在使用Session的class后增加接口IRequiresSessionState
 * （注意：需要引入：using System.Web.SessionState;），
 * 否则context.Session["xxx"]读出的总是null 
 * 
 */
using System.Web.SessionState;
using stwh_Common;

namespace stwh_Web.Handler
{
    /// <summary>
    /// securitycode 的摘要说明
    /// </summary>
    public class securitycode : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            int w = 160, //宽度
                h = 35; //高度
            string code = "";
            Random rand = new Random();
            //定义随机字符串
            string letter =stwh_Web.stwh_admin.Common.WebSite.LoadWebSite().Webchecktext;
            for (int i = 0; i < 4; i++)
            {
                code += letter.Substring(rand.Next(0, letter.Length - 1), 1);
            }
            Bitmap bmp = ImageHepler.srBitmap(w, h, code);
            context.Session["Identify"] = code.ToString();
            //输出图像
            MemoryStream ms = new MemoryStream();
            bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            //设置响应类型
            context.Response.ContentType = "image/png";
            //将流写入http输出流中
            context.Response.BinaryWrite(ms.ToArray());
            context.Response.Flush();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
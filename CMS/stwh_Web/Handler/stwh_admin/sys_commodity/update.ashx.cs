﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;
using System.Web.SessionState;

namespace stwh_Web.Handler.stwh_admin.sys_commodity
{
    /// <summary>
    /// update 的摘要说明
    /// </summary>
    public class update : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            try
            {
                stwh_userinfo loginModel = BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    string data = context.Request["data"];
                    if (string.IsNullOrEmpty(data))
                    {
                        BaseHandler.SendResponseMsgs(context, "-1", "请上传数据！", 0);
                        return;
                    }
                    List<stwh_FormModel> dataList = JsonConvert.DeserializeObject<List<stwh_FormModel>>(data);
                    if (dataList.Count == 0) BaseHandler.SendResponseMsgs(context, "-1", "没有任何数据！", 0);
                    else
                    {
                        stwh_product model = FormModel.ToModel<stwh_product>(dataList);
                        if (new stwh_productBLL().Update(model))
                        {
                            #region 添加商品分类参数值
                            try
                            {
                                //获取刚刚添加成功商品的id
                                int stwh_pid = model.stwh_pid;
                                new stwh_producttype_pvBLL().Delete(stwh_pid, 1);
                                //获取商品分类参数
                                List<stwh_producttype_p> typeparam = new stwh_producttype_pBLL().GetModelList("stwh_ptid = " + model.stwh_ptid, 1);
                                List<stwh_producttype_pv> pvlist = new List<stwh_producttype_pv>();
                                foreach (stwh_producttype_p item in typeparam)
                                {
                                    stwh_producttype_pv modelpv = new stwh_producttype_pv();
                                    modelpv.stwh_pid = stwh_pid;
                                    modelpv.stwh_ptpid = item.stwh_ptpid;
                                    string vv = "";
                                    foreach (stwh_FormModel fmitem in dataList)
                                    {
                                        if (fmitem.Name.ToLower() == item.stwh_ptpname.ToLower()) vv = fmitem.Value;
                                    }
                                    modelpv.stwh_ptpvvalue = vv;
                                    pvlist.Add(modelpv);
                                }
                                foreach (stwh_producttype_pv item in pvlist)
                                {
                                    new stwh_producttype_pvBLL().Add(item);
                                }
                            }
                            catch (Exception)
                            {
                            }
                            #endregion
                            stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 修改商品成功！");
                            BaseHandler.SendResponseMsgs(context, "0", "操作成功！", 0);
                        }
                        else
                        {
                            stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 修改商品失败！");
                            BaseHandler.SendResponseMsgs(context, "-1", "修改失败，请仔细检查数据！", 0);
                        }
                    }
                }
                else BaseHandler.SendResponseMsgs(context, "-1", "数据传输方法错误，请采用post方式！", 0);
                
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息："+ex.StackTrace); BaseHandler.SendResponseMsgs(context, "-1", "服务器异常，请稍后再试！", 0);
                
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
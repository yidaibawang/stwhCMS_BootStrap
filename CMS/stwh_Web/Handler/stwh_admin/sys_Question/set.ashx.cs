﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;
using System.Web.SessionState;

namespace stwh_Web.Handler.stwh_admin.sys_Question
{
    /// <summary>
    /// set 的摘要说明
    /// </summary>
    public class set : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            try
            {
                stwh_userinfo loginModel = BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    string strandomcount = context.Request["strandomcount"];
                    if (string.IsNullOrEmpty(strandomcount))
                    {
                        BaseHandler.SendResponseMsgs(context, "-1", "请上传数据！", 0);
                        return;
                    }
                    if (!PageValidate.IsNumber(strandomcount))
                    {
                        BaseHandler.SendResponseMsgs(context, "-1", "参数格式错误，请刷新页面重试！", 0);
                        return;
                    }
                    INIFile inifile = new INIFile(System.Web.HttpRuntime.AppDomainAppPath + "config/questioncog.ini");
                    inifile.IniWriteValue("section", "key", strandomcount + "");

                    stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 设置随机生成试题数量成功！");
                    BaseHandler.SendResponseMsgs(context, "0", "操作成功！", 0);
                }
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息：" + ex.StackTrace);
                BaseHandler.SendResponseMsgs(context, "-1", "服务器异常，请稍后再试！", 0);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
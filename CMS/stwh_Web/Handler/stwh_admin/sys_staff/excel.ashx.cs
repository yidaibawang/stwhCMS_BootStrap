﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using stwh_Common.HttpProc;
using Newtonsoft.Json;
using System.Web.SessionState;
using System.Data;

namespace stwh_Web.Handler.stwh_admin.sys_staff
{
    /// <summary>
    /// excel 的摘要说明
    /// </summary>
    public class excel : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            try
            {
                stwh_userinfo loginModel = BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    string data = context.Request["data"];
                    if (string.IsNullOrEmpty(data))
                    {
                        BaseHandler.SendResponseMsgs(context, "-1", "请上传数据！", 0);
                        return;
                    }
                    List<stwh_FormModel> dataList = JsonConvert.DeserializeObject<List<stwh_FormModel>>(data);
                    if (dataList.Count == 0) BaseHandler.SendResponseMsgs(context, "-1", "没有任何数据！", 0);
                    else
                    {
                        //获取excel路径
                        string excelpath = "";
                        foreach (stwh_FormModel item in dataList)
                        {
                            if (item.Name.ToLower() == "excelpath")
                            {
                                excelpath = item.Value;
                                break;
                            }
                        }
                        //简单判断excel后缀格式
                        string[] excelsplit = excelpath.Split('.');
                        if (excelsplit.Length>1)
                        {
                            if (excelsplit[1].ToLower()=="xls" || excelsplit[1].ToLower()=="xlsx")
                            {
                                DataTable dt = AsposeHelper.InFileToData(WebClient.GetRootPath() + excelpath);
                                if (dt!=null)
                                {
                                    //获取部门信息
                                    List<stwh_department> departmentlist = new stwh_departmentBLL().GetModelList("");
                                    //导入数据失败数量
                                    int errorcount = 0;
                                    //获取列数
                                    int column = dt.Columns.Count;
                                    //获取总行数
                                    int row = dt.Rows.Count;
                                    if (column == 11)//格式一
                                    {
                                        for (int i = 0; i < row; i++)
                                        {
                                            try
                                            {
                                                #region 数据添加
                                                string stwh_snumber = dt.Rows[i][0].ToString();
                                                string stwh_sname = dt.Rows[i][1].ToString();
                                                string stwh_ssex = dt.Rows[i][2].ToString();
                                                string stwh_srztime = dt.Rows[i][3].ToString();
                                                string stwh_dtid = dt.Rows[i][4].ToString();
                                                string stwh_szw = dt.Rows[i][5].ToString();
                                                string stwh_stel = dt.Rows[i][6].ToString();
                                                string stwh_sbirthday = dt.Rows[i][7].ToString();
                                                string stwh_sxueli = dt.Rows[i][8].ToString();
                                                string stwh_ssfz = dt.Rows[i][9].ToString();
                                                string stwh_sremark = dt.Rows[i][10].ToString();

                                                //判断指纹号、姓名、部门、身份证是否为空
                                                if (string.IsNullOrEmpty(stwh_snumber) || string.IsNullOrEmpty(stwh_sname) || string.IsNullOrEmpty(stwh_dtid) || string.IsNullOrEmpty(stwh_ssfz))
                                                {
                                                    ++errorcount;
                                                    continue;
                                                }
                                                //查询部门是否存在
                                                List<stwh_department> searchDeapart = departmentlist.Where(a => a.stwh_dtname.Trim().ToLower() == stwh_dtid.Trim().ToLower()).ToList<stwh_department>();
                                                if (searchDeapart.Count == 0)
                                                {
                                                    ++errorcount;
                                                    continue;
                                                }
                                                stwh_staff model = new stwh_staff();
                                                model.stwh_snumber = int.Parse(stwh_snumber);
                                                model.stwh_sname = stwh_sname;
                                                model.stwh_ssex = int.Parse(stwh_ssex);
                                                if (!string.IsNullOrEmpty(stwh_srztime)) model.stwh_srztime = DateTime.Parse(stwh_srztime);
                                                model.stwh_dtid = searchDeapart[0].stwh_dtid;
                                                model.stwh_szw = stwh_szw;
                                                model.stwh_stel = stwh_stel;
                                                if (!string.IsNullOrEmpty(stwh_sbirthday)) model.stwh_sbirthday = DateTime.Parse(stwh_sbirthday);
                                                if (!string.IsNullOrEmpty(stwh_sxueli)) model.stwh_sxueli = stwh_sxueli;
                                                else model.stwh_sxueli = "小学";
                                                model.stwh_ssfz = stwh_ssfz;
                                                model.stwh_sremark = stwh_sremark;
                                                model.stwh_syqmoney = 4000;
                                                model.stwh_szzmoney = 4600;
                                                model.stwh_siszz = 0;
                                                model.stwh_sisht = 0;
                                                model.stwh_sislz = 0;
                                                model.stwh_saddtime = DateTime.Now;
                                                if (new stwh_staffBLL().Add(model) > 0) continue;
                                                else
                                                {
                                                    ++errorcount;
                                                    continue;
                                                }
                                                #endregion
                                            }
                                            catch (Exception)
                                            {
                                                ++errorcount;
                                                continue;
                                            }
                                        }
                                    }
                                    else if (column == 19)//格式二
                                    {
                                        for (int i = 0; i < row; i++)
                                        {
                                            try
                                            {
                                                #region 数据添加
                                                string stwh_snumber = dt.Rows[i][0].ToString();
                                                string stwh_sname = dt.Rows[i][1].ToString();
                                                string stwh_ssex = dt.Rows[i][2].ToString();
                                                string stwh_srztime = dt.Rows[i][3].ToString();
                                                string stwh_dtid = dt.Rows[i][4].ToString();
                                                string stwh_szw = dt.Rows[i][5].ToString();
                                                string stwh_stel = dt.Rows[i][6].ToString();
                                                string stwh_sbirthday = dt.Rows[i][7].ToString();
                                                string stwh_sxueli = dt.Rows[i][8].ToString();
                                                string stwh_ssfz = dt.Rows[i][9].ToString();
                                                string stwh_sremark = dt.Rows[i][10].ToString();
                                                string stwh_syqmoney = dt.Rows[i][11].ToString();
                                                string stwh_szzmoney = dt.Rows[i][12].ToString();
                                                string stwh_siszz = dt.Rows[i][13].ToString();
                                                string stwh_szztime = dt.Rows[i][14].ToString();
                                                string stwh_sisht = dt.Rows[i][15].ToString();
                                                string stwh_shttime = dt.Rows[i][16].ToString();
                                                string stwh_sislz = dt.Rows[i][17].ToString();
                                                string stwh_slztime = dt.Rows[i][18].ToString();

                                                //判断指纹号、姓名、部门、身份证是否为空
                                                if (string.IsNullOrEmpty(stwh_snumber) || string.IsNullOrEmpty(stwh_sname) || string.IsNullOrEmpty(stwh_dtid) || string.IsNullOrEmpty(stwh_ssfz))
                                                {
                                                    ++errorcount;
                                                    continue;
                                                }
                                                //查询部门是否存在
                                                List<stwh_department> searchDeapart = departmentlist.Where(a => a.stwh_dtname.Trim().ToLower() == stwh_dtid.Trim().ToLower()).ToList<stwh_department>();
                                                if (searchDeapart.Count == 0)
                                                {
                                                    ++errorcount;
                                                    continue;
                                                }
                                                stwh_staff model = new stwh_staff();
                                                model.stwh_snumber = int.Parse(stwh_snumber);
                                                model.stwh_sname = stwh_sname;
                                                model.stwh_ssex = int.Parse(stwh_ssex);
                                                if (!string.IsNullOrEmpty(stwh_srztime)) model.stwh_srztime = DateTime.Parse(stwh_srztime);
                                                model.stwh_dtid = searchDeapart[0].stwh_dtid;
                                                model.stwh_szw = stwh_szw;
                                                model.stwh_stel = stwh_stel;
                                                if (!string.IsNullOrEmpty(stwh_sbirthday)) model.stwh_sbirthday = DateTime.Parse(stwh_sbirthday);
                                                if (!string.IsNullOrEmpty(stwh_sxueli)) model.stwh_sxueli = stwh_sxueli;
                                                else model.stwh_sxueli = "小学";
                                                model.stwh_ssfz = stwh_ssfz;
                                                model.stwh_sremark = stwh_sremark;
                                                if (!string.IsNullOrEmpty(stwh_syqmoney)) model.stwh_syqmoney = int.Parse(stwh_syqmoney);
                                                else model.stwh_syqmoney = 4000;
                                                if (!string.IsNullOrEmpty(stwh_szzmoney)) model.stwh_szzmoney = int.Parse(stwh_szzmoney);
                                                else model.stwh_szzmoney = 4600;
                                                if (!string.IsNullOrEmpty(stwh_siszz)) model.stwh_siszz = int.Parse(stwh_siszz);
                                                else model.stwh_siszz = 0;
                                                if (!string.IsNullOrEmpty(stwh_szztime)) model.stwh_szztime = DateTime.Parse(stwh_szztime);
                                                if (!string.IsNullOrEmpty(stwh_sisht)) model.stwh_sisht = int.Parse(stwh_sisht);
                                                else model.stwh_sisht = 0;
                                                if (!string.IsNullOrEmpty(stwh_shttime)) model.stwh_shttime = DateTime.Parse(stwh_shttime);
                                                if (!string.IsNullOrEmpty(stwh_sislz)) model.stwh_sislz = int.Parse(stwh_sislz);
                                                else model.stwh_sislz = 0;
                                                if (!string.IsNullOrEmpty(stwh_slztime)) model.stwh_slztime = DateTime.Parse(stwh_slztime);
                                                model.stwh_saddtime = DateTime.Now;
                                                if (new stwh_staffBLL().Add(model) > 0) continue;
                                                else
                                                {
                                                    ++errorcount;
                                                    continue;
                                                }
                                                #endregion
                                            }
                                            catch (Exception)
                                            {
                                                ++errorcount;
                                                continue;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        BaseHandler.SendResponseMsgs(context, "-1", "上传excel文件数据格式错误，请参照模板进行设置！", 0);
                                        return;
                                    }
                                    stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 成功导入excel数据到数据库中！");
                                    BaseHandler.SendResponseMsgs(context, "0", "Excel文件有 " + row + "条数据，成功导入"+(row - errorcount)+" 条，失败 "+errorcount+" 条！", 0);
                                }
                                else
                                    BaseHandler.SendResponseMsgs(context, "-1", "上传excel文件数据格式错误，请参照模板进行设置！", 0);
                            }
                            else
                                BaseHandler.SendResponseMsgs(context, "-1", "上传excel文件为非法文件！", 0);
                        }
                        else
                            BaseHandler.SendResponseMsgs(context, "-1", "上传excel文件为非法文件！", 0);
                    }
                }
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息：" + ex.StackTrace);
                BaseHandler.SendResponseMsgs(context, "-1", "服务器异常，请稍后再试！", 0);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;
using System.Web.SessionState;

namespace stwh_Web.Handler.stwh_admin.sys_staff
{
    /// <summary>
    /// loadData 的摘要说明
    /// </summary>
    public class loadData : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            try
            {
                BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    string pageCount = context.Request["pageCount"];
                    string pageIndex = context.Request["pageIndex"];
                    string whereStr = context.Request["whereStr"];
                    if (PageValidate.IsNumber(pageCount) && PageValidate.IsNumber(pageIndex))
                    {
                        #region 分析查询条件
                        if (string.IsNullOrEmpty(whereStr)) whereStr = "1 = 1";
                        else
                        {
                            List<stwh_FormModel> dataList = JsonConvert.DeserializeObject<List<stwh_FormModel>>(whereStr);
                            if (dataList.Count != 0)
                            {
                                whereStr = "1 = 1 and ";
                                foreach (stwh_FormModel item in dataList)
                                {
                                    if (item.Name.ToLower().Trim() == "stwh_sname")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value)) whereStr += "stwh_sname like '%" + item.Value + "%' and ";
                                    }
                                    else if (item.Name.ToLower().Trim() == "stwh_stel")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value)) whereStr += "stwh_stel like '%" + item.Value + "%' and ";
                                    }
                                    else if (item.Name.ToLower().Trim() == "stwh_sislz")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value) && item.Value != "-1") whereStr += "stwh_sislz= " + item.Value + " and ";
                                    }
                                    else if (item.Name.ToLower().Trim() == "stwh_sisht")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value) && item.Value != "-1") whereStr += "stwh_sisht= " + item.Value + " and ";
                                    }
                                    else if (item.Name.ToLower().Trim() == "stwh_siszz")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value) && item.Value != "-1") whereStr += "stwh_siszz= " + item.Value + " and ";
                                    }
                                    else if (item.Name.ToLower().Trim() == "stwh_dtid")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value) && item.Value != "0") whereStr += "stwh_dtid= " + item.Value + " and ";
                                    }
                                    else if (item.Name.ToLower().Trim() == "month")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value) && item.Value != "0") whereStr += "month(stwh_sbirthday) =  " + item.Value + " and ";
                                    }
                                    else if (item.Name.ToLower().Trim() == "rztime")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value) && item.Value.Trim() != ",")
                                        {
                                            string[] times = item.Value.Split(',');
                                            whereStr += "stwh_srztime between '" + times[0] + "' and '"+times[1]+"' and ";
                                        }
                                    }
                                    else if (item.Name.ToLower().Trim() == "zztime")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value) && item.Value.Trim() != ",")
                                        {
                                            string[] times = item.Value.Split(',');
                                            whereStr += "stwh_szztime between '" + times[0] + "' and '" + times[1] + "' and ";
                                        }
                                    }
                                    else if (item.Name.ToLower().Trim() == "qdtime")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value) && item.Value.Trim() != ",")
                                        {
                                            string[] times = item.Value.Split(',');
                                            whereStr += "stwh_shttime between '" + times[0] + "' and '" + times[1] + "' and ";
                                        }
                                    }
                                    else if (item.Name.ToLower().Trim() == "lztime")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value) && item.Value.Trim() != ",")
                                        {
                                            string[] times = item.Value.Split(',');
                                            whereStr += "stwh_slztime between '" + times[0] + "' and '" + times[1] + "' and ";
                                        }
                                    }
                                    else if (item.Name.ToLower().Trim() == "stwh_saddtime")
                                    {
                                        if (!string.IsNullOrEmpty(item.Value)) whereStr += "CONVERT(varchar(300),stwh_saddtime,120) like '" + item.Value + "%' and ";
                                    }
                                }
                                whereStr += "1 = 1";
                            }
                            else whereStr = "1 = 1";
                        }
                        #endregion
                        int ss = 0, cc = 0;
                        List<stwh_staff> ListData = new stwh_staffBLL().GetListByPage<stwh_staff>("stwh_sid", "desc", whereStr, int.Parse(pageCount), int.Parse(pageIndex), ref ss, ref cc, 0);
                        BaseHandler.SendResponseMsgs(context, "0", ListData, cc);
                    }
                    else BaseHandler.SendResponseMsgs(context, "-1", "数据格式错误！", 0);
                }
                else BaseHandler.SendResponseMsgs(context, "-1", "数据传输方法错误，请采用post方式！", 0);
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息：" + ex.StackTrace);
                BaseHandler.SendResponseMsgs(context, "-1", "服务器异常，请稍后再试！", 0);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
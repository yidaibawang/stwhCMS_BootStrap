﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using stwh_Common.HttpProc;
using Newtonsoft.Json;
using System.Web.SessionState;
using System.Collections;
using System.Data;

namespace stwh_Web.Handler.stwh_admin.sys_staff
{
    /// <summary>
    /// download 的摘要说明
    /// </summary>
    public class download : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            try
            {
                stwh_userinfo loginModel = BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    string whereStr = context.Request["whereStr"];
                    #region 分析查询条件
                    if (string.IsNullOrEmpty(whereStr)) whereStr = "1 = 1";
                    else
                    {
                        List<stwh_FormModel> dataList = JsonConvert.DeserializeObject<List<stwh_FormModel>>(whereStr);
                        if (dataList.Count != 0)
                        {
                            whereStr = "1 = 1 and ";
                            foreach (stwh_FormModel item in dataList)
                            {
                                if (item.Name.ToLower().Trim() == "stwh_sname")
                                {
                                    if (!string.IsNullOrEmpty(item.Value)) whereStr += "stwh_sname like '%" + item.Value + "%' and ";
                                }
                                else if (item.Name.ToLower().Trim() == "stwh_stel")
                                {
                                    if (!string.IsNullOrEmpty(item.Value)) whereStr += "stwh_stel like '%" + item.Value + "%' and ";
                                }
                                else if (item.Name.ToLower().Trim() == "stwh_sislz")
                                {
                                    if (!string.IsNullOrEmpty(item.Value) && item.Value != "-1") whereStr += "stwh_sislz= " + item.Value + " and ";
                                }
                                else if (item.Name.ToLower().Trim() == "stwh_sisht")
                                {
                                    if (!string.IsNullOrEmpty(item.Value) && item.Value != "-1") whereStr += "stwh_sisht= " + item.Value + " and ";
                                }
                                else if (item.Name.ToLower().Trim() == "stwh_siszz")
                                {
                                    if (!string.IsNullOrEmpty(item.Value) && item.Value != "-1") whereStr += "stwh_siszz= " + item.Value + " and ";
                                }
                                else if (item.Name.ToLower().Trim() == "stwh_dtid")
                                {
                                    if (!string.IsNullOrEmpty(item.Value) && item.Value != "0") whereStr += "stwh_dtid= " + item.Value + " and ";
                                }
                                else if (item.Name.ToLower().Trim() == "month")
                                {
                                    if (!string.IsNullOrEmpty(item.Value) && item.Value != "0") whereStr += "month(stwh_sbirthday) =  " + item.Value + " and ";
                                }
                                else if (item.Name.ToLower().Trim() == "rztime")
                                {
                                    if (!string.IsNullOrEmpty(item.Value) && item.Value.Trim() != ",")
                                    {
                                        string[] times = item.Value.Split(',');
                                        whereStr += "stwh_srztime between '" + times[0] + "' and '" + times[1] + "' and ";
                                    }
                                }
                                else if (item.Name.ToLower().Trim() == "zztime")
                                {
                                    if (!string.IsNullOrEmpty(item.Value) && item.Value.Trim() != ",")
                                    {
                                        string[] times = item.Value.Split(',');
                                        whereStr += "stwh_szztime between '" + times[0] + "' and '" + times[1] + "' and ";
                                    }
                                }
                                else if (item.Name.ToLower().Trim() == "qdtime")
                                {
                                    if (!string.IsNullOrEmpty(item.Value) && item.Value.Trim() != ",")
                                    {
                                        string[] times = item.Value.Split(',');
                                        whereStr += "stwh_shttime between '" + times[0] + "' and '" + times[1] + "' and ";
                                    }
                                }
                                else if (item.Name.ToLower().Trim() == "lztime")
                                {
                                    if (!string.IsNullOrEmpty(item.Value) && item.Value.Trim() != ",")
                                    {
                                        string[] times = item.Value.Split(',');
                                        whereStr += "stwh_slztime between '" + times[0] + "' and '" + times[1] + "' and ";
                                    }
                                }
                                else if (item.Name.ToLower().Trim() == "stwh_saddtime")
                                {
                                    if (!string.IsNullOrEmpty(item.Value)) whereStr += "CONVERT(varchar(300),stwh_saddtime,120) like '" + item.Value + "%' and ";
                                }
                            }
                            whereStr += "1 = 1";
                        }
                        else whereStr = "1 = 1";
                    }
                    #endregion
                    string savepath = "/stwhup/" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xls";

                    //生成列的中文对应表
                    Dictionary<string, string> nameList = new Dictionary<string,string>();
                    nameList.Add("stwh_sid", "编号{split}10");
                    nameList.Add("stwh_snumber", "指纹号{split}15");
                    nameList.Add("stwh_sname", "姓名{split}15");
                    nameList.Add("stwh_ssex", "性别（0男性，1女性）{split}30");
                    nameList.Add("stwh_srztime", "入职时间{split}30");
                    nameList.Add("stwh_dtname", "所属部门{split}15");
                    nameList.Add("stwh_szw", "职务{split}15");
                    nameList.Add("stwh_stel", "联系电话{split}18");
                    nameList.Add("stwh_sbirthday", "出生年月{split}30");
                    nameList.Add("stwh_sxueli", "学历{split}15");
                    nameList.Add("stwh_ssfz", "身份证号码{split}35");
                    nameList.Add("stwh_sremark", "备注{split}15");
                    nameList.Add("stwh_syqmoney", "试用期薪水{split}18");
                    nameList.Add("stwh_szzmoney", "转正薪水{split}18");
                    nameList.Add("stwh_siszz", "是否转正（0未转正，1已转正）{split}40");
                    nameList.Add("stwh_szztime", "转正时间{split}30");
                    nameList.Add("stwh_sisht", "是否签订合同（0未签订，1已签订）{split}40");
                    nameList.Add("stwh_shttime", "签订时间{split}30");
                    nameList.Add("stwh_sislz", "是否离职（0未离职，1已离职）{split}40");
                    nameList.Add("stwh_slztime", "离职时间{split}30");
                    nameList.Add("stwh_saddtime", "添加时间{split}30");

                    DataTable dt = new stwh_staffBLL().GetList(whereStr).Tables[0];
                    AsposeHelper.OutFileToDisk(dt, "已成功导出" + dt.Rows.Count + "条数据", "员工数据", WebClient.GetRootPath() + savepath, nameList);

                    stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 导出员工数据到excel成功！");
                    BaseHandler.SendResponseMsgs(context, "0", savepath, 0);
                }
                else BaseHandler.SendResponseMsgs(context, "-1", "数据传输方法错误，请采用post方式！", 0);
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息：" + ex.StackTrace);
                BaseHandler.SendResponseMsgs(context, "-1", "服务器异常，请稍后再试！", 0);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//引入命名空间
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using Newtonsoft.Json;
using System.Web.SessionState;

namespace stwh_Web.Handler.stwh_admin.sys_messages
{
    /// <summary>
    /// deleteALL 的摘要说明
    /// </summary>
    public class deleteALL : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                stwh_userinfo loginModel = BaseHandler.IsCheckLogged(context);
                if (context.Request.HttpMethod.ToLower() == "post")
                {
                    if (new stwh_leaveMSGBLL().DeleteListALL())
                    {
                        stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 清空留言成功！");
                        BaseHandler.SendResponseMsgs(context, "0", "操作成功！", 0);
                        
                    }
                    else
                    {
                        stwh_admin.BaseHandler.AddLog("[" + loginModel.stwh_uiname + "] 清空留言失败！");
                        BaseHandler.SendResponseMsgs(context, "-1", "操作失败，请仔细检查数据！", 0);
                        
                    }
                }
                else
                {
                    BaseHandler.SendResponseMsgs(context, "-1", "数据传输方法错误，请采用post方式！", 0);
                    
                }
            }
            catch (Exception ex)
            {
                FileHelper.WriteError(ex.Message + "\r\n详细信息："+ex.StackTrace); 
                BaseHandler.SendResponseMsgs(context, "-1", "服务器异常，请稍后再试！", 0);
                
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="main.aspx.cs" Inherits="stwh_Web.stwh_admin.main" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div id="scrollPanel">
        <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="row" style="margin-top: 15px;">
                <div class="col-md-6 col-lg-7">
                    <div class="panel panel-default system-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                版本声明
                            </h3>
                        </div>
                        <div class="panel-body">
                            产品密匙：<span id="litkey" runat="server"></span><br />
                            有效期：<span id="litkeytime" class="key" runat="server"></span> 剩余<span id="litkeytimeday"
                                class="key" runat="server"></span>天
                            <button class="btn btn-warning btn-sm" id="moneyBtn">
                                立即续费</button><br />
                            企龙管理系统<%=WebSite.Webversion%>由上海舒同文化传播有限公司开发，任何个人或组织不得在未授权允许的情况下删除、修改、拷贝本软件及其它副本上一切关于版权的信息。
                        </div>
                    </div>
                    <div class="panel panel-default system-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                程序信息
                            </h3>
                        </div>
                        <div class="panel-body">
                            当前版本:企龙管理系统<%=WebSite.Webversion%><br />
                            产品开发:上海舒同文化传播有限公司<br />
                            地址: 上海市浦东新区浦东大道1097弄16号楼22-E<br />
                            电话: 021-5130 2685 / 021-5821 4991<br />
                            Email: looksea@shijiguanli.com<br />
                            官网: <a href="http://www.ofall.cn/" target="_blank">点击进入</a><br />
                        </div>
                    </div>
                    <div class="panel panel-default system-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                系统消息
                            </h3>
                        </div>
                        <div class="panel-body">
                            服务器系统及版本:<%=Request.ServerVariables["HTTP_USER_AGENT"]%><br />
                            服务器IIS版本:<%=Request.ServerVariables["SERVER_SOFTWARE"].ToString() %><br />
                            服务器名:<%=Request.ServerVariables["SERVER_NAME"].ToString()%><br />
                            服务器IP:<%=Request.ServerVariables["LOCAL_ADDR"].ToString()%><br />
                            HTTP端口:<%=Request.ServerVariables["SERVER_PORT"].ToString()%>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-5">
                    <div class="panel panel-default system-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                系统公告
                            </h3>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group" id="NoticeList" runat="server">
                            </ul>
                        </div>
                    </div>
                    <div class="panel panel-default system-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                最新天气
                            </h3>
                        </div>
                        <div class="panel-body">
                            <iframe width="100%" scrolling="no" height="120" frameborder="0" allowtransparency="true"
                                src="http://i.tianqi.com/index.php?c=code&id=19&color=%23&icon=5&temp=1&num=3">
                            </iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hidAllData" runat="server" />
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script type="text/javascript">
        var AllData = JSON.parse($("#hidAllData").val());
        $(function () {
            $("#NoticeList").on("click", "a", function (e) {
                var jb_mid = $(this).attr("data-id");
                $.each(AllData, function (index, item) {
                    if (item.stwh_noid == parseInt(jb_mid)) {
                        parent.parent.ShowNotice("系统公告", item.stwh_nocontent);
                        return false;
                    }
                });
            });
            $("#NoticeList").on("click", ".btn", function (e) {
                var jb_mid = $(this).attr("data-id");
                $.each(AllData, function (index, item) {
                    if (item.stwh_noid == parseInt(jb_mid)) {
                        parent.parent.ShowVoiceNotice(item.stwh_nocontent);
                        return false;
                    }
                });
            });
            $("#moneyBtn").click(function () {
                parent.parent.ShowNotice("系统提示", "请联系供应商进行续费！");
                return false;
            });
            setInterval(function () {
                $.post(
                "/handler/stwh_admin/sys_notices/service.ashx",
                {}, function (data) {
                    if (data.msgcode == "0") {
                        AllData = JSON.parse(data.msg);
                        var strDatas = "";
                        $.each(AllData, function (index, item) {
                            strDatas += "<li class=\"list-group-item\"><a class=\"text-primary\" data-id=\"" + item.stwh_noid + "\"><span class=\"glyphicon glyphicon-comment\"></span> " + item.stwh_notitle + "----" + item.stwh_noaddtime.split('T')[0] + "</a></li>";
                        });
                        $("#NoticeList").html(strDatas);
                    }
                }, "json");
            }, 60000);
        });
    </script>
    <script type="text/javascript">
        function SetFormHeight() {
            $("#scrollPanel").css("height", (parent.parent.GetMarinRightHeight()) + "px");
        }
        function UpdateScroll() {
            $("#scrollPanel").mCustomScrollbar("update");
        }
        $(function () {
            SetFormHeight();
        });
        (function ($) {
            $(window).load(function () {
                $("#scrollPanel").mCustomScrollbar({
                    scrollInertia: 100,
                    autoHideScrollbar: true
                });
            });
        })(jQuery);
    </script>
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
</body>
</html>

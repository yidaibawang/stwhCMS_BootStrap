﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_menus_function.index" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
    <style type="text/css">
        .lead
        {
            padding: 0px 15px;
            font-size: 15px;
        }
        .lead label
        {
            font-weight: normal;
            margin-right: 10px;
        }
    </style>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span>返回上一页</a></li>
        <li class="active"><span id="menuSpan" runat="server">菜单功能管理</span></li>
    </ol>
    <div class="m-top-10 p-bottom-15 p-left-15 scrollTop">
        <form class="form-inline">
            <asp:Literal ID="litBtnList" runat="server"></asp:Literal>
        </form>
    </div>
    <div id="scrollPanel">
    <form id="form1" runat="server">
    <div class="container-fluid">
        <div id="functionContent" runat="server" style="padding-left: 15px;">
        </div>
    </div>
    <div class="modal fade" id="deleteMenuModal" tabindex="-1" role="dialog" aria-labelledby="updatePwdTitle"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="updatePwdTitle">
                        <i class="fa fa-exclamation-circle"></i> 系统提示
                    </h4>
                </div>
                <div class="modal-body">
                    <h4>
                        <span class="glyphicon glyphicon-info-sign"></span>你确定要删除菜单功能吗（删除后不可恢复）？</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        取消
                    </button>
                    <button type="button" class="btn btn-default" id="btnDeleteOk">
                        确定
                    </button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidFunctionIds" runat="server" />
    <asp:HiddenField ID="hidFunctionData" runat="server" />
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
        <div class="form-group">
            <label>
                每页显示：</label>
            <select id="basic" class="selectpicker show-tick" disabled="disabled" data-width="55px"
                data-style="btn-sm btn-default">
                <option>15</option>
                <option>25</option>
                <option>35</option>
                <option>45</option>
                <option>55</option>
            </select>
        </div>
        <div class="form-group">
            <ul id="pageUl" style="margin: 0px auto;">
            </ul>
        </div>
        <div class="form-group">
            共 <span id="SumCountSpan"><asp:Literal ID="littotalSum" runat="server"></asp:Literal></span> 条数据
        </div>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script type="text/javascript">
        var AllFunctionData = JSON.parse($("#hidFunctionData").val());
        var fid = [];
        $(function () {
            $("#pageUl").bootstrapPaginator({
                currentPage: 1, //当前页面
                totalPages: 1, //总页数
                numberOfPages: 5, //页码数
                useBootstrapTooltip: true,
                onPageChanged: function (event, oldPage, newPage) {
                    if (oldPage != newPage) {
                        
                    }
                }
            });
            $("#functionContent").on("click", ".btn-group label", function (e) {
                var jb_status = $(this).children(":radio").val();
                var jb_mid = $(this).parent().parent().parent().attr("data-mid");
                $.each(AllFunctionData, function (index, item) {
                    if (item.stwh_fid == parseInt(jb_mid)) {
                        item.stwh_fishow = parseInt(jb_status);
                        return false;
                    }
                });
            });
            $(".AllChkBox").click(function () {
                var id = $(this).attr("data-mid");
                if ($(this).is(":checked")) {
                    $("tbody :checkbox").prop("checked", $(this).is(":checked"));
                    $("tbody :checkbox:checked").each(function (i, item) {
                        if (fid.IndexOfArray($(item).val()) == -1) fid.AddArray($(item).val());
                    });
                    $("#hidFunctionIds").val(fid.toStringArray());
                }
                else {
                    $("tbody :checkbox").prop("checked", $(this).is(":checked"));
                    $("tbody :checkbox:not(:checked)").each(function (i, item) {
                        fid.removeIndexArray(fid.IndexOfArray($(item).val()));
                    });
                    $("#hidFunctionIds").val(fid.toStringArray());
                }
            });
            $("tbody :checkbox").click(function () {
                if ($(this).is(":checked")) {
                    if ($("tbody :checkbox:checked").length == $("tbody :checkbox").length) $("#parentFunction" + $(this).attr("data-mid")).prop("checked", true);
                    if (fid.IndexOfArray($(this).val()) == -1) fid.AddArray($(this).val());
                    $("#hidFunctionIds").val(fid.toStringArray());
                }
                else {
                    if ($("tbody :checkbox:not(:checked)").length == $("tbody :checkbox").length) $("#parentFunction" + $(this).attr("data-mid")).prop("checked", false);
                    fid.removeIndexArray(fid.IndexOfArray($(this).val()));
                    $("#hidFunctionIds").val(fid.toStringArray());
                }
            });
            $("input[type='text']").keyup(function (event) {
                var jb_order = $(this).val();
                var jb_mid = $(this).parent().parent().attr("data-mid");

                $.each(AllFunctionData, function (index, item) {
                    if (item.stwh_fid == parseInt(jb_mid)) {
                        item.stwh_forder = parseInt(jb_order);
                        return false;
                    }
                });
            }).change(function () {
                var jb_order = $(this).val();
                var jb_mid = $(this).parent().parent().attr("data-mid");

                $.each(AllFunctionData, function (index, item) {
                    if (item.stwh_fid == parseInt(jb_mid)) {
                        item.stwh_forder = parseInt(jb_order);
                        return false;
                    }
                });
            });
            $("#btnDelete").click(function () {
                $("#deleteMenuModal").modal('show');
            });
            $("#btnDeleteOk").click(function () {
                var mids = $("#hidFunctionIds").val();
                if (!mids) {
                    $.bs.alert("请选择数据！", "info");
                    return false;
                }
                else if (!(/^[\d,]*$/).test(mids)) {
                    $.bs.alert("非法数据格式，请刷新页面重试！", "danger");
                    return false;
                }
                $.post(
                "/handler/stwh_admin/sys_menus_function/delete.ashx",
                {
                    mids: mids
                }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
            });
            $("#btnUpdate").click(function () {
                if ($("tbody :checkbox:checked").length == 0) {
                    $.bs.alert("请选择数据！", "warning", "");
                    return false;
                }
                $("tbody :checkbox:checked").each(function (i, item) {
                    if (i == 0) window.location.href = "update.aspx?id=" + $(item).val();
                });
                return false;
            });
            $("#btnSave").click(function () {
                $.post(
                "/handler/stwh_admin/sys_menus_function/save.ashx",
                { data: JSON.stringify(AllFunctionData) },
                function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>

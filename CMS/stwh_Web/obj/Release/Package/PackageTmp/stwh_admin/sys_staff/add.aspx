﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_staff.add" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading">
        <i class="fa fa-spinner fa-spin fa-2x"></i>
    </div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">员工管理</span></li>
        <li class="active">添加数据</li>
    </ol>
    <div id="scrollPanel">
        <form id="form1" runat="server" class="form-horizontal" role="form">
        <div class="container-fluid">
            <h3>基本信息：</h3>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="selectShowList" class="col-sm-2 control-label">
                            <input type="hidden" id="stwh_dtid" name="stwh_dtid" value="0" />归属部门：</label>
                        <div class="col-sm-10">
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <span id="selectShowText">请选择归属部门</span> <span class="caret"></span>
                            </button>
                            <ul id="selectShowList" class="dropdown-menu" role="menu" runat="server" style="max-height: 300px;
                                overflow: auto; left: auto; top: auto;">
                                <li><a data-pid="0">父级导航</a></li>
                                <li><a data-pid="0">系统设置</a></li>
                                <li><a data-pid="0">系统管理</a></li>
                                <li class="divider"></li>
                                <li><a data-pid="0">内容管理</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="stwh_sname" class="col-sm-2 control-label">
                            员工姓名：</label>
                        <div class="col-sm-10">
                            <input type="text" id="stwh_sname" name="stwh_sname" value="" class="st-input-text-300 form-control"
                                placeholder="请输入员工姓名" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="stwh_snumber" class="col-sm-2 control-label">
                            添指纹号：</label>
                        <div class="col-sm-10">
                            <input type="text" id="stwh_snumber" name="stwh_snumber" value="" class="st-input-text-300 form-control" placeholder="请输入指纹号" />
                            <span class="text-danger">只能为数字</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            员工性别：</label>
                        <div class="col-sm-10">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-primary btn-sm active">
                                    <input type="radio" name="stwh_ssex" id="stwh_ssex0" value="0" checked="checked" />
                                    男
                                </label>
                                <label class="btn btn-primary btn-sm">
                                    <input type="radio" name="stwh_ssex" id="stwh_ssex1" value="1" />
                                    女
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="stwh_snumber" class="col-sm-2 control-label">
                            员工职务：</label>
                        <div class="col-sm-10">
                            <input type="text" id="stwh_szw" name="stwh_szw" value="" class="st-input-text-300 form-control"
                                placeholder="请输入职务" />
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            联系电话：</label>
                        <div class="col-sm-10">
                            <input type="text" id="stwh_stel" name="stwh_stel" value="" class="st-input-text-300 form-control" maxlength="11" placeholder="请输入联系电话" />
                            <span class="text-danger"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="stwh_snumber" class="col-sm-2 control-label">
                            身份证号：</label>
                        <div class="col-sm-10">
                            <input type="text" id="stwh_ssfz" name="stwh_ssfz" value="" class="st-input-text-300 form-control" maxlength="18" placeholder="请输入身份证号码" /> 
                            <span class="text-danger">末尾为字母的，请输入大写字母（不能重复添加同一张身份证号码）</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="stwh_snumber" class="col-sm-2 control-label">
                            员工生日：</label>
                        <div class="col-sm-10">
                            <div class=" input-group st-input-text-300">
                                <input type="text" name="stwh_sbirthday" id="stwh_sbirthday" placeholder="请输入员工生日"  class="form-control" value="">
                                <span id="calendar" class=" input-group-addon"  style=" cursor:pointer;"><span class="fa fa-calendar"></span></span>
                            </div>
                            <span class="text-danger">自动识别身份证号码生日</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            员工学历：</label>
                        <div class="col-sm-10">
                            <input type="hidden" id="stwh_sxueli" name="stwh_sxueli" value="小学" />
                            <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                                <span id="xuelispan">小学</span> <span class="caret"></span>
                            </button>
                            <ul id="xuelilist" class="dropdown-menu" role="menu" style="max-height: 300px;
                                overflow: auto; left: auto; top: auto;">
                                <li><a data-pid="小学">小学</a></li>
                                <li><a data-pid="初中">初中</a></li>
                                <li><a data-pid="高中">高中</a></li>
                                <li><a data-pid="大专">大专</a></li>
                                <li><a data-pid="本科">本科</a></li>
                                <li><a data-pid="研究生">研究生</a></li>
                                <li><a data-pid="博士">博士</a></li>
                                <li><a data-pid="硕士">硕士</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            试用薪资：</label>
                        <div class="col-sm-10">
                            <input type="text" id="stwh_syqmoney" name="stwh_syqmoney" value="4000" class="st-input-text-300 form-control" placeholder="请输入试用薪资" />
                            <span class="text-danger">只能为数字</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="stwh_snumber" class="col-sm-2 control-label">
                            转正薪资：</label>
                        <div class="col-sm-10">
                            <input type="text" id="stwh_szzmoney" name="stwh_szzmoney" value="4600" class="st-input-text-300 form-control"  placeholder="请输入转正薪资" />
                            <span class="text-danger">只能为数字</span>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6"> 
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            入职时间：</label>
                        <div class="col-sm-10">
                            <div class=" input-group st-input-text-300">
                                <input type="text" name="stwh_srztime" id="stwh_srztime" placeholder="请输入入职时间"  class="form-control" value="">
                                <span id="calendar1" class=" input-group-addon"  style=" cursor:pointer;"><span class="fa fa-calendar"></span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h3>其他信息：</h3>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="stwh_snumber" class="col-sm-2 control-label">
                            是否转正：</label>
                        <div class="col-sm-10">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-primary btn-sm active">
                                    <input type="radio" name="stwh_siszz" id="stwh_siszz0" value="0" checked="checked" />
                                    未转正
                                </label>
                                <label class="btn btn-primary btn-sm">
                                    <input type="radio" name="stwh_siszz" id="stwh_siszz1" value="1" />
                                    已转正
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            转正时间：</label>
                        <div class="col-sm-10">
                            <div class=" input-group st-input-text-300">
                                <input type="text" name="stwh_szztime" id="stwh_szztime" placeholder="请输入转正时间"  class="form-control" value="">
                                <span id="calendar2" class=" input-group-addon"  style=" cursor:pointer;"><span class="fa fa-calendar"></span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="stwh_snumber" class="col-sm-2 control-label">
                            合同状态：</label>
                        <div class="col-sm-10">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-primary btn-sm active">
                                    <input type="radio" name="stwh_sisht" id="stwh_sisht0" value="0" checked="checked" />
                                    未签订
                                </label>
                                <label class="btn btn-primary btn-sm">
                                    <input type="radio" name="stwh_sisht" id="stwh_sisht1" value="1" />
                                    已签订
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            签订时间：</label>
                        <div class="col-sm-10">
                            <div class=" input-group st-input-text-300">
                                <input type="text" name="stwh_shttime" id="stwh_shttime" placeholder="请输入签订时间"  class="form-control" value="">
                                <span id="calendar3" class=" input-group-addon"  style=" cursor:pointer;"><span class="fa fa-calendar"></span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="stwh_snumber" class="col-sm-2 control-label">
                            是否离职：</label>
                        <div class="col-sm-10">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-primary btn-sm active">
                                    <input type="radio" name="stwh_sislz" id="stwh_sislz0" value="0" checked="checked" />
                                    未离职
                                </label>
                                <label class="btn btn-primary btn-sm">
                                    <input type="radio" name="stwh_sislz" id="stwh_sislz1" value="1" />
                                    已离职
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            离职时间：</label>
                        <div class="col-sm-10">
                            <div class=" input-group st-input-text-300">
                                <input type="text" name="stwh_slztime" id="stwh_slztime" placeholder="请输入离职时间"  class="form-control" value="">
                                <span id="calendar4" class=" input-group-addon"  style=" cursor:pointer;"><span class="fa fa-calendar"></span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="stwh_snumber" class="col-sm-2 control-label">
                            备注信息：</label>
                        <div class="col-sm-10">
                            <textarea id="stwh_sremark" name="stwh_sremark" class="st-input-text-300 form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">
                            添加时间：</label>
                        <div class="col-sm-10">
                            <div class=" input-group st-input-text-300">
                                <input type="text" name="stwh_saddtime" id="stwh_saddtime" placeholder="请选择时间"  class="form-control" value="">
                                <span id="calendar5" class=" input-group-addon"  style=" cursor:pointer;"><span class="fa fa-calendar"></span></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div style="height:180px;"></div>
        </div>
        </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
        <button id="addMenu" class="btn btn-default">
            <i class="fa fa-plus"></i>添加</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script type="text/javascript">
        var currentTabId = "#setpanel0";
        $(function () {
            $("#selectShowList a").click(function () {
                $("#selectShowText").text($(this).text().Trim().split('.')[1]);
                $("#stwh_dtid").val($(this).attr("data-pid"));
            });
            $("#xuelilist a").click(function () {
                $("#xuelispan").text($(this).text());
                $("#stwh_sxueli").val($(this).attr("data-pid"));
            });
            $("#stwh_ssfz").bind("keyup", function () {
                var sfz = $(this).val();
                if (sfz.length > 14) {
                    sfz = sfz.substr(6, 8);
                    sfz = sfz.substr(0, 4) + "-" + sfz.substr(4, 2) + "-" + sfz.substr(6, 2);
                    if (!isNaN(new Date(Date.parse(sfz)))) {
                        $("#stwh_sbirthday").val(sfz);
                    }
                }
            });
            $("#addMenu").click(function () {
                var stwh_dtid = $("#stwh_dtid").val();
                if (stwh_dtid == "0") {
                    $.bs.alert("请选择归属部门！", "info");
                    return false;
                }
                var stwh_sname = $("#stwh_sname").val();
                if (!stwh_sname) {
                    $.bs.alert("请输入员工姓名！", "info");
                    return false;
                }
                var stwh_snumber = $("#stwh_snumber").val();
                if (!stwh_snumber) {
                    $.bs.alert("请输入指纹号！", "info");
                    return false;
                }
                else if (!IsNumber(stwh_snumber)) {
                    $.bs.alert("指纹号只能为数字！", "info");
                    return false;
                }
                var stwh_stel = $("#stwh_stel").val();
                if (stwh_stel) {
                    if (!IsMobileTel(stwh_stel)) {
                        $.bs.alert("联系电话格式错误！", "info");
                        return false;
                    }
                }
                var stwh_ssfz = $("#stwh_ssfz").val();
                if (!stwh_ssfz) {
                    $.bs.alert("请输入身份证号码！", "info");
                    return false;
                }
                else if (!IsCard(stwh_ssfz)) {
                    $.bs.alert("身份证号码格式错误！", "info");
                    return false;
                }
                var stwh_syqmoney = $("#stwh_syqmoney").val();
                if (stwh_syqmoney) {
                    if (!IsNumber(stwh_syqmoney)) {
                        $.bs.alert("试用薪资只能为数字！", "info");
                        return false;
                    }
                }
                var stwh_szzmoney = $("#stwh_szzmoney").val();
                if (stwh_szzmoney) {
                    if (!IsNumber(stwh_szzmoney)) {
                        $.bs.alert("转正薪水只能为数字！", "info");
                        return false;
                    }
                }
                var stwh_szztime = $("#stwh_szztime").val();
                if (stwh_szztime) {
                    if (!CheckDateTime(stwh_szztime)) {
                        $.bs.alert("转正时间格式错误！", "info");
                        return false;
                    }
                }
                var stwh_shttime = $("#stwh_shttime").val();
                if (stwh_shttime) {
                    if (!CheckDateTime(stwh_shttime)) {
                        $.bs.alert("合同签订时间格式错误！", "info");
                        return false;
                    }
                }
                var stwh_slztime = $("#stwh_slztime").val();
                if (stwh_slztime) {
                    if (!CheckDateTime(stwh_slztime)) {
                        $.bs.alert("离职时间格式错误！", "info");
                        return false;
                    }
                }
                var stwh_sremark = $("#stwh_sremark").val();
                if (stwh_sremark) {
                    if (!IsHanZF(1, 200, stwh_sremark)) {
                        $.bs.alert("备注信息长度超出服务器限制200字符！", "info");
                        return false;
                    }
                }
                var stwh_saddtime = $("#stwh_saddtime").val();
                if (!CheckDateTime(stwh_saddtime)) {
                    $.bs.alert("添加时间格式错误！", "info");
                    return false;
                }

                $.post("/Handler/stwh_admin/sys_staff/add.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
            
            $("#calendar").click(function () {
                $('#stwh_sbirthday').click();
            });
            $("#calendar1").click(function () {
                $('#stwh_srztime').click();
            });
            $("#calendar2").click(function () {
                $('#stwh_szztime').click();
            });
            $("#calendar3").click(function () {
                $('#stwh_shttime').click();
            });
            $("#calendar4").click(function () {
                $('#stwh_slztime').click();
            });
            $("#calendar5").click(function () {
                $('#stwh_saddtime').click();
            });
            $('#stwh_sbirthday').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                autoApply: true,
                autoUpdateInput: false
            }).on({
                "apply.daterangepicker": function (ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD'));
                }
            });
            $('#stwh_szztime,#stwh_shttime,#stwh_slztime,#stwh_srztime,#stwh_saddtime').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                autoApply: true,
                autoUpdateInput: false
            }).on({
                "apply.daterangepicker": function (ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD') + " " + new Date().Format("hh:mm:ss"));
                }
            });

            $("#stwh_srztime,#stwh_saddtime").val(new Date().Format("yyyy-MM-dd hh:mm:ss"));
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_roles.index" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li class="active"><span id="menuSpan" runat="server">角色管理</span></li>
    </ol>
    <div class="m-top-10 p-bottom-15 p-left-15 scrollTop">
        <form class="form-inline">
            <asp:Literal ID="litBtnList" runat="server"></asp:Literal>
        </form>
    </div>
    <div id="scrollPanel">
    <form id="form1" runat="server">
    <div class="container-fluid">
        <table class="table table-striped table-bordered table-hover table-condensed">
            <thead>
                <tr>
                    <th style="width: 50px;">
                        <input type="checkbox" id="allParent" />
                    </th>
                    <th style=" width:50px;">
                        编号
                    </th>
                    <th style="width: 150px;">
                        角色名称
                    </th>
                    <th style="width: 300px;">
                        角色描述
                    </th>
                    <th style="width: 200px;">
                        创建时间
                    </th>
                    <th style="width: 150px;">
                        状态
                    </th>
                </tr>
            </thead>
            <tbody id="ChildDatas" runat="server">
            </tbody>
        </table>
    </div>
    <asp:HiddenField ID="hidRoleIds" runat="server" />
    <asp:HiddenField ID="hidAllRoleData" runat="server" />
    <asp:HiddenField ID="hidTotalSum" runat="server" />
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
        <div class="form-group">
            <label>
                每页显示：</label>
            <select id="basic" class="selectpicker show-tick" data-width="55px"
                data-style="btn-sm btn-default">
                <option>15</option>
                <option>25</option>
                <option>35</option>
                <option>45</option>
                <option>55</option>
            </select>
        </div>
        <div class="form-group">
            <ul id="pageUl" style="margin: 0px auto;">
            </ul>
        </div>
        <div class="form-group">
            共 <asp:Literal ID="littotalSum" runat="server"></asp:Literal> 条数据
        </div>
        </form>
    </div>
    <div class="modal fade" id="deleteMenuModal" tabindex="-1" role="dialog" aria-labelledby="updatePwdTitle"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="updatePwdTitle">
                        <i class="fa fa-exclamation-circle"></i> 系统提示
                    </h4>
                </div>
                <div class="modal-body">
                    <h4>
                        <span class="glyphicon glyphicon-info-sign"></span>你确定要删除角色吗（删除后不可恢复）？</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        取消
                    </button>
                    <button type="button" class="btn btn-default" id="btnDeleteOk">
                        确定
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script type="text/javascript">
        var AllRoleData = JSON.parse($("#hidAllRoleData").val());
        var rid = [];
        var pageCount = 15;//每页显示多少条数据
        //加载数据
        function LoadData(pCount,pIndex)
        {
            $.post("/handler/stwh_admin/sys_roles/loaddata.ashx",{
                pageCount:pCount,
                pageIndex:pIndex
            },function(data){
                if (data.msgcode==-1) {
                    $.bs.alert(data.msg, "warning", "");
                }
                else
                {
                    $("#hidRoleIds").val("");
                    AllRoleData = data.msg;
                    $("#ChildDatas tr").remove();
                    if (data.msg.length==0) {
                        $("#ChildDatas").append("<tr><td colspan=\"6\" align=\"center\">暂无数据</td></tr>");
                    }
                    else
                    {
                        $.each(data.msg, function (index, item) {
                            $("#ChildDatas").append("<tr data-id=\"" + item.stwh_rid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_rid + "\" value=\"" + item.stwh_rid + "\" /></td><td>" + item.stwh_rid + "</td><td>" + item.stwh_rname + "</td><td>" + item.stwh_rdescription + "</td><td>" + item.stwh_rctime.replace('T',' ') + "</td><td>" + (item.stwh_rid==1? "" : (item.stwh_rstate == 0 ? "<div class=\"btn-group\"data-toggle=\"buttons\"><label class=\"btn btn-primary btn-sm active\"><input type=\"radio\"name=\"radp" + item.stwh_rid + "\"value=\"0\" checked=\"checked\"/>启用</label><label class=\"btn btn-primary btn-sm\"><input type=\"radio\"name=\"radp" + item.stwh_rid + "\"value=\"1\"/>禁用</label></div>" : "<div class=\"btn-group\"data-toggle=\"buttons\"><label class=\"btn btn-primary btn-sm\"><input type=\"radio\"name=\"radp" + item.stwh_rid + "\"value=\"0\"/>启用</label><label class=\"btn btn-primary btn-sm active\"><input type=\"radio\"name=\"radp" + item.stwh_rid + "\"value=\"1\"/>禁用</label></div>")) + "</td></tr>");
                        });
                        UpdateScroll();
                    }
                }
            },"json");
        }
        $(function () {
            $("#pageUl").bootstrapPaginator({
                currentPage: 1, //当前页面
                totalPages: <%=totalPages %>, //总页数
                numberOfPages: 5, //页码数
                useBootstrapTooltip: true,
                onPageChanged: function (event, oldPage, newPage) {
                    if (oldPage != newPage) {
                        LoadData(pageCount,parseInt(newPage) - 1);
                    }
                }
            });
            $("#allParent").click(function () {
                if ($(this).is(":checked")) {
                    $("#ChildDatas :checkbox").prop("checked", $(this).is(":checked"));
                    $("#ChildDatas :checkbox:checked").each(function (i, item) {
                        if (rid.IndexOfArray($(item).val()) == -1) rid.AddArray($(item).val());
                    });
                    $("#hidRoleIds").val(rid.toStringArray());
                }
                else {
                    $("#ChildDatas :checkbox").prop("checked", $(this).is(":checked"));
                    $("#ChildDatas :checkbox:not(:checked)").each(function (i, item) {
                        rid.removeIndexArray(rid.IndexOfArray($(item).val()));
                    });
                    $("#hidRoleIds").val(rid.toStringArray());
                }
            });
            $("#ChildDatas").on("click",":checkbox",function(){
              if ($(this).is(":checked")) {
                    if ($("#ChildDatas :checkbox:checked").length == $("#ChildDatas :checkbox").length) $("#allParent").prop("checked", true);
                    if (rid.IndexOfArray($(this).val()) == -1) rid.AddArray($(this).val());
                    $("#hidRoleIds").val(rid.toStringArray());
                }
                else {
                    if ($("#ChildDatas :checkbox:not(:checked)").length == $("#ChildDatas :checkbox").length) $("#allParent").prop("checked", false);
                    rid.removeIndexArray(rid.IndexOfArray($(this).val()));
                    $("#hidRoleIds").val(rid.toStringArray());
                }  
            });
            $("#btnDelete").click(function () {
                $("#deleteMenuModal").modal('show');
            });
            $("#btnDeleteOk").click(function () {
                var mids = $("#hidRoleIds").val();
                if (!mids) {
                    $.bs.alert("请选择数据！", "info");
                    return false;
                }
                else if (!(/^[\d,]*$/).test(mids)) {
                    $.bs.alert("非法数据格式，请刷新页面重试！", "danger");
                    return false;
                }
                $.post(
                "/handler/stwh_admin/sys_roles/delete.ashx",
                {
                    mids: mids
                }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
            });
            $("#ChildDatas").on("click",".btn-group label",function () {
                var jb_status = $(this).children(":radio").val();
                var jb_mid = $(this).parent().parent().parent().attr("data-id");
                $.each(AllRoleData, function (index, item) {
                    if (item.stwh_rid == parseInt(jb_mid)) {
                        item.stwh_rstate = parseInt(jb_status);
                        return false;
                    }
                });
            });
            $("#btnUpdate").click(function () {
                if ($("tbody :checkbox:checked").length == 0) {
                    $.bs.alert("请选择数据！", "warning", "");
                    return false;
                }
                $("tbody :checkbox:checked").each(function (i, item) {
                    if (i == 0) window.location.href = "update.aspx?id=" + $(item).val();
                });
                return false;
            });
            $("#btnSetting").click(function(){
                if ($("tbody :checkbox:checked").length == 0) {
                    $.bs.alert("请选择数据！", "warning", "");
                    return false;
                }
                $("tbody :checkbox:checked").each(function (i, item) {
                    if (i == 0) window.location.href = "setting.aspx?id=" + $(item).val();
                });
                return false;
            });
            $("#btnSave").click(function () {
                $.post(
                "/handler/stwh_admin/sys_roles/save.ashx",
                { data: JSON.stringify(AllRoleData) },
                function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
            });
            $("#basic").change(function(){
                pageCount = $(this).val();
                LoadData(pageCount,0);
                var totalsum = parseInt($("#hidTotalSum").val());
                $("#pageUl").bootstrapPaginator("setOptions",{
                    currentPage: 1, //当前页面
                    totalPages: totalsum / pageCount + ((totalsum % pageCount) == 0 ? 0 : 1), //总页数
                    numberOfPages: 5, //页码数
                    useBootstrapTooltip: true,
                    onPageChanged: function (event, oldPage, newPage) {
                        if (oldPage != newPage) {
                            LoadData(pageCount,parseInt(newPage) - 1);
                        }
                    }
                });
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_QuestionList.add" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <link href="/plugin/jQuery-colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading">
        <i class="fa fa-spinner fa-spin fa-2x"></i>
    </div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">试题选项管理</span></li>
        <li class="active">添加数据</li>
    </ol>
    <div id="scrollPanel">
        <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx"
        method="post" role="form">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-1 st-text-right">
                    归属试题：</div>
                <div class="col-sm-11 form-group">
                    <input type="hidden" id="stwh_quid" name="stwh_quid" value="0" />
                    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        <span id="selectShowText">请选择试题</span> <span class="caret"></span>
                    </button>
                    <ul id="selectShowList" class="dropdown-menu" role="menu" runat="server" style="max-height: 300px;
                        overflow: auto; left: auto; top: auto;">
                        <li><a data-pid="0">父级导航</a></li>
                        <li><a data-pid="0">系统设置</a></li>
                        <li><a data-pid="0">系统管理</a></li>
                        <li class="divider"></li>
                        <li><a data-pid="0">内容管理</a></li>
                    </ul>
                </div>
                <div class="line10">
                </div>
                <div class="col-sm-1 st-text-right">
                    序号：</div>
                <div class="col-sm-11 form-group">
                    <input type="text" id="stwh_qulorder" name="stwh_qulorder" onkeydown="return checkNumber(event);"
                        value="1" style="margin-right: 15px;" class="st-input-text-300 form-control pull-left" />
                    <span class="text-danger">*越大越考前</span>
                </div>
                <div class="line10">
                </div>
                <div class="col-sm-1 st-text-right">
                    试题选项描述：</div>
                <div class="col-sm-11 form-group">
                    <textarea id="stwh_qulmiaoshu" name="stwh_qulmiaoshu" style="width: 700px; height: 300px;"></textarea>
                </div>
            </div>
        </div>
        </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
        <button id="addMenu" class="btn btn-default">
            <i class="fa fa-plus"></i> 添加</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <cms:stwhuescript ID="stwhuescript" runat="server" />
    <script type="text/javascript">
        //初始化富文本编辑器
        var qjOptions = {
            // 服务器统一请求接口路径
            serverUrl: "/handler/ueditor/controller.ashx",
            wordCount: true, //是否开启字数统计
            maximumWords: 5000, //允许的最大字符数
            elementPathEnabled: true, //是否启用元素路径，默认是显示
            autoHeightEnabled: false, //是否自动长高,默认true
            zIndex: 900, //编辑器层级的基数,默认是900
            emotionLocalization: true, //是否开启表情本地化，默认关闭。若要开启请确保emotion文件夹下包含官网提供的images表情文件夹
            autoFloatEnabled: false, //当设置为ture时，工具栏会跟随屏幕滚动，并且始终位于编辑区域的最上方
            pasteplain: false, //是否默认为纯文本粘贴。false为不使用纯文本粘贴，true为使用纯文本粘贴
            enableAutoSave: true, //启用自动保存
            saveInterval: 3000, //自动保存间隔时间
            allowDivTransToP: false,
            toolbars: [
                        ['fullscreen', 'source', 'cleardoc', 'undo', 'redo', '|',
                        'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', '|',
                         'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', '|',
                        'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
                        'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                        'directionalityltr', 'directionalityrtl', 'indent', '|',
                        'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                        'touppercase', 'tolowercase', 'simpleupload', 'insertimage', '|',
                        'link', 'unlink', '|',
                        'horizontal', 'date', 'time', 'spechars', '|',
                        'inserttable', 'deletetable', 'preview']
                    ]//工具栏
        };
        var qjUEObject = UE.getEditor('stwh_qulmiaoshu', qjOptions);

        setTimeout(function () {
            qjUEObject.execCommand('drafts'); //载入本地数据（若存在）
            $(window).scrollTop(0);
        }, 500);
    </script>
    <script type="text/javascript">
        var currentTabId = "#setpanel0";
        $(function () {
            $("#selectShowList a").click(function () {
                $("#selectShowText").text($(this).text().Trim());
                $("#stwh_quid").val($(this).attr("data-pid"));
            });
            $("#addMenu").click(function () {
                var stwh_quid = $("#stwh_quid").val();
                if (stwh_quid == "0") {
                    $.bs.alert("请选择试题！", "info");
                    return false;
                }
                var stwh_qulmiaoshu = qjUEObject.getContent();
                if (!stwh_qulmiaoshu) {
                    $.bs.alert("请输入试题选项描述内容！", "info");
                    return false;
                }

                $.post("/Handler/stwh_admin/sys_QuestionList/add.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        qjUEObject.setContent("");
                        //清除本地数据
                        qjUEObject.execCommand("clearlocaldata");
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="stwh_Web.stwh_admin.index" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
</head>
<body class="main_body">
	<div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <form id="Form1" runat="server">
    <div class="container-fluid">
        <cms:stwhbtnarbar ID="stwhbtnarbar" runat="server" />
        <div class="row">
            <div id="mainLeft" class="col-sm-3 col-md-2 col-lg-2 sidebar">
                <div class="headdiv">
                    <div class="pull-left">
                        <asp:Image ID="imgHead" runat="server" CssClass="img-circle" />
                    </div>
                    <div class="pull-left">
                        <p>
                            姓名：<span><asp:Label ID="labName" runat="server" Text="Label"></asp:Label></span></p>
                        <p>
                            时间：<asp:Label ID="labTime" runat="server" Text="Label"></asp:Label></p>
                    </div>
                    <div class="pull-right">
                        <div class="btn-group-vertical btn-group-xs">
                            <div class="btn btn-primary btn-sm" id="btnZD" title="折叠菜单"><i class="fa fa-angle-up"></i></div>
                            <div class="btn btn-primary btn-sm" id="btnZK" title="展开菜单"><i class="fa fa-angle-down"></i></div>
                        </div>
                    </div>
                </div>
                <div class="panel-group" id="accordion" runat="server">
                </div>
            </div>
            <div id="mainRight" class="col-sm-9 col-md-10 col-lg-10 main">
                <iframe id="mainframe" name="mainframe" src="/stwh_admin/main.aspx" border="0" frameborder="0"
                    framespacing="0" width="100%" height="100%" marginheight="0" marginwidth="0"
                    scrolling="no" vspale="0"></iframe>
            </div>
        </div>
    </div>
    <div class="mainBtnPanel" data-id="0">
        <div class="btn btn-primary"><span class="glyphicon glyphicon-menu-left"></span></div>
    </div>
    <div class="modal fade" id="myExit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">
                        <i class="fa fa-exclamation-circle"></i> 系统提示
                    </h4>
                </div>
                <div class="modal-body">
                    <span class="glyphicon glyphicon-info-sign"></span> 你确定要退出系统吗？
                </div>
                <div id="exitForm" class="modal-footer" runat="server">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        取消
                    </button>
                    <asp:Button ID="exitOk" runat="server" Text="确定" BorderStyle="None" CssClass="btn btn-primary"
                        OnClick="exitOk_Click" />
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <div class="modal fade" id="updatePwd" tabindex="-1" role="dialog" aria-labelledby="updatePwdTitle"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="updatePwdTitle">
                        <i class="fa fa-lock"></i> 修改密码
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="pwddiv1">
                        <p>
                        <span class="pwdspan">新 密 码：</span><asp:TextBox ID="txtNewPwd" runat="server" TextMode="Password" CssClass="pwdinput"></asp:TextBox></p>
                    <p>
                        <span class="pwdspan">确认密码：</span><asp:TextBox ID="txtNewPwdOk" runat="server" TextMode="Password" CssClass="pwdinput"></asp:TextBox></p>
                    </div>
                    <div class="pwddiv2">
                        建议新密码不要与原密码一样
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        取消
                    </button>
                    <asp:Button ID="updatePwdOk" runat="server" Text="确定" BorderStyle="None" CssClass="btn btn-primary"
                        OnClientClick="return updatePwdOk_Click();" />
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <div class="modal fade" id="NoticeWindow" tabindex="-1" role="dialog" aria-labelledby="NoticeTitle"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="NoticeTitle">
                        <i class="fa fa-exclamation-circle"></i> <span>系统公告</span>
                    </h4>
                </div>
                <div class="modal-body" id="NoticeContent">
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        确定
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <asp:HiddenField ID="hidAllMenuData" runat="server" />
    <asp:HiddenField ID="hidRoleMenuData" runat="server" />
    </form>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <cms:stwhbtnarbarjs ID="stwhbtnarbarjs" runat="server" />
    <script type="text/javascript">
        var AllMenuData = JSON.parse($("#hidAllMenuData").val());
        var mobj = [];
        var rmidlist = $("#hidRoleMenuData").val().split(',');
        $(function () {
            $(".pmenu").click(function () {
                if (!$(this).hasClass("pmenu_cur")) {
                    var pmid = $(this).attr("data-id");
                    var byMenuStr = "";
                    $.each(AllMenuData, function (index, item) {
                        if (item.stwh_menuparentID == pmid && (rmidlist.IndexOfArray(item.stwh_menuid) != -1)) mobj.push(item);
                    });
                    $.each(mobj, function (index, mm) {
                        byMenuStr += "<div class=\"acchead\" data-toggle=\"collapse\" data-target=\"#MenuPanel" + mm.stwh_menuid + "\" ><span class=\"" + mm.stwh_menuICO_url + "\"></span>" + mm.stwh_menuname + "<span class=\"glyphicon glyphicon-menu-down pull-right\"></span></div>";
                        byMenuStr += "<div id=\"MenuPanel" + mm.stwh_menuid + "\" class=\"collapse accmenu in\">";
                        $.each(AllMenuData, function (index, cc) {
                            if (cc.stwh_menuparentID == mm.stwh_menuid && (rmidlist.IndexOfArray(cc.stwh_menuid) != -1)) {
                                byMenuStr += "<a class=\"childrenm\" href=\"/stwh_admin/" + cc.stwh_menuURL + "?mid=" + cc.stwh_menuid + "&wxuid=" + $("#hidwxuid").val() + "\">&nbsp;&nbsp;&nbsp;<span class=\"" + cc.stwh_menuICO_url + "\"></span>" + cc.stwh_menuname + " </a>";
                            }
                        });
                        byMenuStr += "</div>";
                    });
                    $("#accordion").html(byMenuStr);
                    mobj = [];
                    $(".pmenu").removeClass("pmenu_cur");
                    $(this).addClass("pmenu_cur");
                    $("#mainLeft").mCustomScrollbar("update");
                }
            });
        });
    </script>
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>

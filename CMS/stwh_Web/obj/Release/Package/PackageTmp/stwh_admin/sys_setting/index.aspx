﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_setting.index" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <link href="/plugin/jQuery-colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li class="active"><span id="menuSpan" runat="server">基本信息设置</span></li>
    </ol>
    <ul id="setTab" class="nav nav-tabs" style="margin-bottom:1px;">
        <li class="active" style="margin-left: 15px;"><a href="#setpanel0" >
            <span class="fa fa-wrench"></span> 基本信息设置</a> </li>
        <li><a href="#setpanel2" ><span class="fa fa-globe"></span> SEO设置</a>
        </li>
        <li><a href="#setpanel3" ><span class="fa fa-envelope"></span> 邮箱配置</a>
        </li>
        <li><a href="#setpanel4" ><span class="fa fa-list-alt"></span> 使用条款</a>
        </li>
        <li><a href="#setpanel5" ><span class="fa fa-list-alt"></span> 隐私条款</a>
        </li>
        <li><a href="#setpanel6" ><span class="fa fa-fire"></span> 网站模板</a>
        </li>
        <li><a href="#setpanel7" ><span class="fa fa-fire"></span> 手机网站模板</a>
        </li>
        <li><a href="#setpanel8" ><span class="fa fa-fire"></span> 商城网站模板</a>
        </li>
    </ul>
    <div id="scrollPanel">
        <form id="form1" runat="server">
        <div class="container-fluid">
            <div id="setTabContent" class="tab-content">
                <div class="tab-pane active" id="setpanel0">
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            网站维护（pc）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <input type="hidden" name="stwh_cfid" id="stwh_cfid" value="" runat="server" />
                            <input type="hidden" name="stwh_cfkey" id="stwh_cfkey" value="" runat="server" />
                            <input type="hidden" name="stwh_cfkeytime" id="stwh_cfkeytime" value="" runat="server" />
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-primary btn-sm <%=wmm1 %>">
                                    <input type="radio" name="stwh_cfweihu" id="stwh_cfweihu0" value="0" runat="server" />
                                    关闭
                                </label>
                                <label class="btn btn-primary btn-sm <%=wmm2 %>">
                                    <input type="radio" name="stwh_cfweihu" id="stwh_cfweihu1" value="1" runat="server" />
                                    开启
                                </label>
                            </div>
                            <span class="text-danger">* 是否开启网站维护，默认：关闭，开启网站维护后，pc端网站无法显示内容</span>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            网站维护（移动）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-primary btn-sm <%=wmm_m1 %>">
                                    <input type="radio" name="stwh_cfweihu_m" id="stwh_cfweihu_m0" value="0" runat="server" />
                                    关闭
                                </label>
                                <label class="btn btn-primary btn-sm <%=wmm_m2 %>">
                                    <input type="radio" name="stwh_cfweihu_m" id="stwh_cfweihu_m1" value="1" runat="server" />
                                    开启
                                </label>
                            </div>
                            <span class="text-danger">* 是否开启网站维护，默认：关闭，开启网站维护后，移动端网站无法显示内容</span>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            网站维护（商城）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <div class="btn-group" data-toggle="buttons">
                                <label class="btn btn-primary btn-sm <%=wmm_s1 %>">
                                    <input type="radio" name="stwh_cfweihu_s" id="stwh_cfweihu_s0" value="0" runat="server" />
                                    关闭
                                </label>
                                <label class="btn btn-primary btn-sm <%=wmm_s2 %>">
                                    <input type="radio" name="stwh_cfweihu_s" id="stwh_cfweihu_s1" value="1" runat="server" />
                                    开启
                                </label>
                            </div>
                            <span class="text-danger">* 是否开启网站维护，默认：关闭，开启网站维护后，移动端网站无法显示内容</span>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            网站图标（PC）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <input id="stwh_cfico" name="stwh_cfico" type="text" runat="server" class="form-control"
                                placeholder="请上传网站图标" style="width: 50%; float: left;" /><div style="float: left;
                                    width: 100px;">
                                    <div id="upstwh_cfico">
                                    </div>
                                </div>
                            <div class="clearfix">
                            </div>
                            <span class="text-danger">* 支持网络图标</span>
                            <div class="clearfix">
                            </div>
                            <div id="progressFile" class="progress progress-striped active" style="display: none;">
                                <div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            网站图标（移动）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <input id="stwh_cfico_m" name="stwh_cfico_m" type="text" runat="server" class="form-control"
                                placeholder="请上传网站图标" style="width: 50%; float: left;" /><div style="float: left;
                                    width: 100px;">
                                    <div id="upstwh_cfico_m">
                                    </div>
                                </div>
                            <div class="clearfix">
                            </div>
                            <span class="text-danger">* 支持网络图标</span>
                            <div class="clearfix">
                            </div>
                            <div id="progressFile1" class="progress progress-striped active" style="display: none;">
                                <div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            网站图标（商城）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <input id="stwh_cfico_s" name="stwh_cfico_s" type="text" runat="server" class="form-control"
                                placeholder="请上传网站图标" style="width: 50%; float: left;" /><div style="float: left;
                                    width: 100px;">
                                    <div id="upstwh_cfico_s">
                                    </div>
                                </div>
                            <div class="clearfix">
                            </div>
                            <span class="text-danger">* 支持网络图标</span>
                            <div class="clearfix">
                            </div>
                            <div id="progressFile2" class="progress progress-striped active" style="display: none;">
                                <div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            网站Logo（PC）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <input id="stwh_cflogo" name="stwh_cflogo" type="text" runat="server" class="form-control"
                                placeholder="请上传网站logo" style="width: 50%; float: left;" /><div style="float: left;
                                    width: 100px;">
                                    <div id="upstwh_cflogo">
                                    </div>
                                </div>
                            <div class="clearfix">
                            </div>
                            <span class="text-danger">* 支持网络图片</span>
                            <div class="clearfix">
                            </div>
                            <div id="stwh_cflogoprogress" class="progress progress-striped active" style="display: none;">
                                <div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            网站Logo（移动）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <input id="stwh_cflogo_m" name="stwh_cflogo_m" type="text" runat="server" class="form-control"
                                placeholder="请上传移动网站logo" style="width: 50%; float: left;" /><div style="float: left;
                                    width: 100px;">
                                    <div id="upstwh_cflogo_m">
                                    </div>
                                </div>
                            <div class="clearfix">
                            </div>
                            <span class="text-danger">* 支持网络图片</span>
                            <div class="clearfix">
                            </div>
                            <div id="stwh_cflogo_mprogress" class="progress progress-striped active" style="display: none;">
                                <div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            网站Logo（商城）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <input id="stwh_cflogo_s" name="stwh_cflogo_s" type="text" runat="server" class="form-control"
                                placeholder="请上传商城网站logo" style="width: 50%; float: left;" /><div style="float: left;
                                    width: 100px;">
                                    <div id="upstwh_cflogo_s">
                                    </div>
                                </div>
                            <div class="clearfix">
                            </div>
                            <span class="text-danger">* 支持网络图片</span>
                            <div class="clearfix">
                            </div>
                            <div id="stwh_cflogo_sprogress" class="progress progress-striped active" style="display: none;">
                                <div class="progress-bar progress-bar-primary" role="progressbar" style="width: 0%;">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            网站名称（PC）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <input id="stwh_cfwebname" name="stwh_cfwebname" type="text" runat="server" class="form-control"
                                placeholder="请输入网站名称" value="上海舒同文化传播有限公司" style="width: 50%;" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            网站名称（移动）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <input id="stwh_cfwebname_m" name="stwh_cfwebname_m" type="text" runat="server" class="form-control"
                                placeholder="请输入移动网站名称" value="上海舒同文化传播有限公司" style="width: 50%;" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            网站名称（商城）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <input id="stwh_cfwebname_s" name="stwh_cfwebname_s" type="text" runat="server" class="form-control"
                                placeholder="请输入商城网站名称" value="上海舒同文化传播有限公司" style="width: 50%;" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            上传大小：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <input id="stwh_cfuploadsize" name="stwh_cfuploadsize" type="text" runat="server" class="form-control"
                                placeholder="请输入上传文件大小" min="1" max="30720" onkeydown="return checkNumber(event);"
                                value="30720" style="width: 50%;" />
                            <span class="text-danger">* 单位KB,最大30720KB（30MB），最小1KB</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            缩略图上传大小：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <input id="stwh_cfthumbnail" name="stwh_cfthumbnail" type="text" runat="server" class="form-control"
                                placeholder="请输入上传文件大小" onkeydown="return checkNumber(event);"
                                value="300" style="width: 50%;" />
                            <span class="text-danger">* 单位KB,默认300KB。（提示：1GB=1024MB、1MB=1024KB、1KB=1024字节、1字节=8位。缩略图上传大小受上传大小总的限制）</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            上传类型：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <input id="stwh_cfuploadtype" name="stwh_cfuploadtype" type="text" runat="server" class="form-control"
                                placeholder="请输入上传网站图标" value="png,gif,jpg,bmp,doc,mp3,flv,pdf,ico" style="width: 50%;" />
                            <span class="text-danger">* 多个扩展名用","格开,","为英文状态下符号</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            网站版权（PC）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <textarea id="stwh_cfcopyright" name="stwh_cfcopyright" runat="server" class="form-control"
                                placeholder="请输入网站版权信息" style="width: 50%;"></textarea>
                            <span class="text-danger">* 支持html格式内容</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            网站版权（移动）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <textarea id="stwh_cfcopyright_m" name="stwh_cfcopyright_m" runat="server" class="form-control"
                                placeholder="请输入移动网站版权信息" style="width: 50%;"></textarea>
                            <span class="text-danger">* 支持html格式内容</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            网站版权（商城）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <textarea id="stwh_cfcopyright_s" name="stwh_cfcopyright_s" runat="server" class="form-control"
                                placeholder="请输入商城网站版权信息" style="width: 50%;"></textarea>
                            <span class="text-danger">* 支持html格式内容</span>
                        </div>
                    </div>
                </div>
                <div class="tab-pane " id="setpanel2">
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            关键词（PC）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <textarea id="stwh_cfkeywords" name="stwh_cfkeywords" class="form-control" runat="server" placeholder="请输入关键词"
                                style="width: 50%;"></textarea>
                            <span class="text-danger">* 多个关键词以英文逗号分割，建议关键词控制在20个以内</span>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            关键词（移动）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <textarea id="stwh_cfkeywords_m" name="stwh_cfkeywords_m" class="form-control" runat="server" placeholder="请输入关键词"
                                style="width: 50%;"></textarea>
                            <span class="text-danger">* 多个关键词以英文逗号分割，建议关键词控制在20个以内</span>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            关键词（商城）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <textarea id="stwh_cfkeywords_s" name="stwh_cfkeywords_s" class="form-control" runat="server" placeholder="请输入关键词"
                                style="width: 50%;"></textarea>
                            <span class="text-danger">* 多个关键词以英文逗号分割，建议关键词控制在20个以内</span>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            网页描述（PC）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <textarea id="stwh_cfdescription" name="stwh_cfdescription" runat="server" class="form-control"
                                placeholder="请输入PC网页描述" style="width: 50%; height: 150px;"></textarea>
                            <span class="text-danger">* 建议网页描述内容控制在200字内</span>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            网页描述（移动）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <textarea id="stwh_cfdescription_m" name="stwh_cfdescription_m" runat="server" class="form-control"
                                placeholder="请输入移动网页描述" style="width: 50%; height: 150px;"></textarea>
                            <span class="text-danger">* 建议网页描述内容控制在200字内</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            网页描述（商城）：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <textarea id="stwh_cfdescription_s" name="stwh_cfdescription_s" runat="server" class="form-control"
                                placeholder="请输入商城网页描述" style="width: 50%; height: 150px;"></textarea>
                            <span class="text-danger">* 建议网页描述内容控制在200字内</span>
                        </div>
                    </div>
                </div>
                <div class="tab-pane " id="setpanel3">
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            地址：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <input id="stwh_cfsmtp" name="stwh_cfsmtp" type="text" runat="server" class="form-control"
                                placeholder="请输入SMTP服务器地址" value="smtp.live.com" style="width: 50%;" />
                            <span class="text-danger">* SMTP服务器地址。常见：smtp.live.com/smtp.qq.com等</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            端口：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <input id="stwh_cfport" name="stwh_cfport" type="text" runat="server" min="1" value="25"
                                class="form-control" onkeydown="return checkNumber(event);" placeholder="请输入端口"
                                style="width: 50%;" />
                            <span class="text-danger">* 只能输入数字</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            发件人：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <input id="stwh_cfsendname" name="stwh_cfsendname" type="text" runat="server" class="form-control"
                                placeholder="请输入发件人邮箱" value="" style="width: 50%;" />
                            <span class="text-danger">* 格式：xxxx@xxx.com。例如：ofall@163.com</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            显示名称：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <input id="stwh_cfname" name="stwh_cfname" type="text" runat="server" class="form-control"
                                placeholder="请输入显示名称" value="上海舒同文化传播有限公司" style="width: 50%;" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            用户名：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <input id="stwh_cfuser" name="stwh_cfuser" type="text" runat="server" class="form-control"
                                placeholder="请输入用户名" value="" style="width: 50%;" />
                            <span class="text-danger">* 格式：xxxx@xxx.com。例如：ofall@163.com</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            密码：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <asp:TextBox ID="stwh_cfpwd" TextMode="Password" CssClass="form-control" runat="server"
                                Width="50%"></asp:TextBox>
                            <span class="text-danger">* 必填</span>
                        </div>
                    </div>
                </div>
                <div class="tab-pane " id="setpanel4">
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            使用条款：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <div id="stwh_cfsytkPanel" style=" display:none;"><%=configmodel.stwh_cfsytk %></div>
                            <textarea id="stwh_cfsytk" name="stwh_cfsytk" style="width: 700px; height: 300px;"></textarea>
                        </div>
                    </div>
                </div>
                <div class="tab-pane " id="setpanel5">
                    <div class="row" style="margin-top: 10px;">
                        <div class="col-xs-3 col-sm-3 col-md-2 col-lg-1 st-text-right">
                            隐私条款：
                        </div>
                        <div class="col-xs-9 col-sm-9 col-md-10 col-lg-11">
                            <div id="stwh_cfystkPanel" style=" display:none;"><%=configmodel.stwh_cfystk%></div>
                            <textarea id="stwh_cfystk" name="stwh_cfystk" style="width: 700px; height: 300px;"></textarea>
                        </div>
                    </div>
                </div>
                <div class="tab-pane " id="setpanel6">
                    <input type="hidden" id="stwh_cfwebtemplate" name="stwh_cfwebtemplate" value="" runat="server" />
                    <div id="templateDiv" class="row" style="margin-top: 10px;" runat="server">
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <a href="http://show.ofall.cn/">
                                <div class="thumbnail">
                                    <img src="/stwh_admin/css/default/more-template.png" alt="浏览更多" />
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane " id="setpanel7">
                    <input type="hidden" id="stwh_cfmwebtemplate" name="stwh_cfmwebtemplate" value="" runat="server" />
                    <div id="templateDiv_m" class="row" style="margin-top: 10px;" runat="server">
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <a href="http://show.ofall.cn/">
                                <div class="thumbnail">
                                    <img src="/stwh_admin/css/default/more-template.png" alt="浏览更多" />
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane " id="setpanel8">
                    <input type="hidden" id="stwh_cfshoptemplate" name="stwh_cfshoptemplate" value="" runat="server" />
                    <div id="templateDiv_s" class="row" style="margin-top: 10px;" runat="server">
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-md-3">
                            <a href="http://show.ofall.cn/">
                                <div class="thumbnail">
                                    <img src="/stwh_admin/css/default/more-template.png" alt="浏览更多" />
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
        <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <cms:stwhuescript ID="stwhuescript" runat="server" />
    <script src="/Plugin/swfobject.js" type="text/javascript"></script>
    <script src="/Plugin/jQuery-colorpicker/js/colorpicker.js" type="text/javascript"></script>
    <script type="text/javascript">
        var currentTabId = "#setpanel0";
        $(function () {
            $("#btnSave").click(function () {
                var stwh_cfwebname = $("#stwh_cfwebname").val();
                if (!stwh_cfwebname) {
                    $.bs.alert("请设置网站名称！", "info");
                    return false;
                }
                var stwh_cfuploadsize = $("#stwh_cfuploadsize").val();
                if (!stwh_cfuploadsize) {
                    $.bs.alert("请设置上传文件大小！", "info");
                    return false;
                }
                if (parseInt(stwh_cfuploadsize) < 1 || parseInt(stwh_cfuploadsize) > 30720) {
                    $.bs.alert("上传大小只能取1-30720之间的值！", "info");
                    return false;
                }
                var stwh_cfthumbnail = $("#stwh_cfthumbnail").val();
                if (!stwh_cfthumbnail) {
                    $.bs.alert("请设置上传文件大小！", "info");
                    return false;
                }
                if (parseInt(stwh_cfthumbnail) < 1 || parseInt(stwh_cfthumbnail) > 30720) {
                    $.bs.alert("缩略图大小只能取1-30720之间的值！", "info");
                    return false;
                }
                var stwh_cfuploadtype = $("#stwh_cfuploadtype").val();
                if (!stwh_cfuploadtype) {
                    $.bs.alert("请设置上传文件类型！", "info");
                    return false;
                }
                if (qjUEObject1.getContentLength(true) > 50000) {
                    qjUEObject1.focus();
                    $.bs.alert("使用条款内容超出限制5000字！", "info");
                    return false;
                }
                if (qjUEObject2.getContentLength(true) > 50000) {
                    qjUEObject2.focus();
                    $.bs.alert("隐私条款内容超出限制5000字！", "info");
                    return false;
                }
                $.post("/handler/stwh_admin/sys_setting/save.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
            });
            $("#templateDiv .thumbnail").hover(function () {
                $(this).addClass("thumbnailhover");
            }, function () {
                $(this).removeClass("thumbnailhover");
            }).click(function () {
                $("#templateDiv .thumbnail").removeClass("activethumbnail");
                $(this).addClass("activethumbnail");
                $("#stwh_cfwebtemplate").val($(this).attr("data-template"));
            });
            $("#templateDiv_m .thumbnail").hover(function () {
                $(this).addClass("thumbnailhover");
            }, function () {
                $(this).removeClass("thumbnailhover");
            }).click(function () {
                $("#templateDiv_m .thumbnail").removeClass("activethumbnail");
                $(this).addClass("activethumbnail");
                $("#stwh_cfmwebtemplate").val($(this).attr("data-template"));
            });
            $("#templateDiv_s .thumbnail").hover(function () {
                $(this).addClass("thumbnailhover");
            }, function () {
                $(this).removeClass("thumbnailhover");
            }).click(function () {
                $("#templateDiv_s .thumbnail").removeClass("activethumbnail");
                $(this).addClass("activethumbnail");
                $("#stwh_cfshoptemplate").val($(this).attr("data-template"));
            });
            $("#setTab li").click(function () {
                if ($(this).children("a").attr("href") != currentTabId) {
                    $(currentTabId).removeClass("active");
                    $("#setTab li").removeClass("active");
                    $(this).addClass("active");
                    currentTabId = $(this).children("a").attr("href");
                    $(currentTabId).addClass("active");
                    UpdateScroll();
                }
            });
        });
    </script>
    <script type="text/javascript">
        //初始化富文本编辑器
        var qjOptions = {
            // 服务器统一请求接口路径
            serverUrl: "/handler/ueditor/controller.ashx",
            wordCount: true, //是否开启字数统计
            maximumWords: 50000, //允许的最大字符数
            elementPathEnabled: true, //是否启用元素路径，默认是显示
            autoHeightEnabled: false, //是否自动长高,默认true
            zIndex: 900, //编辑器层级的基数,默认是900
            emotionLocalization: true, //是否开启表情本地化，默认关闭。若要开启请确保emotion文件夹下包含官网提供的images表情文件夹
            autoFloatEnabled: false, //当设置为ture时，工具栏会跟随屏幕滚动，并且始终位于编辑区域的最上方
            pasteplain: false, //是否默认为纯文本粘贴。false为不使用纯文本粘贴，true为使用纯文本粘贴
            enableAutoSave: true, //启用自动保存
            saveInterval: 3000, //自动保存间隔时间
            allowDivTransToP: false,
            toolbars: [
                        ['fullscreen', 'source', 'cleardoc', 'undo', 'redo', '|',
                        'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', '|',
                         'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', '|',
                        'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
                        'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                        'directionalityltr', 'directionalityrtl', 'indent', '|',
                        'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                        'touppercase', 'tolowercase', 'simpleupload', 'insertimage', '|',
                        'link', 'unlink', '|',
                        'horizontal', 'date', 'time', 'spechars', '|',
                        'inserttable', 'deletetable', 'preview']
            ]//工具栏
        };
        var qjUEObject1 = UE.getEditor('stwh_cfsytk', qjOptions);
        var qjUEObject2 = UE.getEditor('stwh_cfystk', qjOptions);

        setTimeout(function () {
            qjUEObject1.setContent($("#stwh_cfsytkPanel").html(), false);
            qjUEObject2.setContent($("#stwh_cfystkPanel").html(), false);
        }, 1000);
    </script>
    <script type="text/javascript">
        var auth = "<% = Request.Cookies[FormsAuthentication.FormsCookieName]==null ? string.Empty : Request.Cookies[FormsAuthentication.FormsCookieName].Value %>";
        var ASPSESSID = "<%= Session.SessionID %>";
        var flashvarsVoice = {
            IsSelect: "false", //运行多选文件进行上传
            Url: "/Handler/uploadHandler.ashx",
            FileExtension: "*.ico",
            FileType: "image",
            MaxSize: "512000" //500KB 字节为单位
        };
        var flashvarsLogo = {
            IsSelect: "false", //运行多选文件进行上传
            Url: "/Handler/uploadHandler.ashx",
            FileExtension: "*.jpg;*.JPG;*.gif;*.GIF;*.png;*.PNG",
            FileType: "image",
            MaxSize: "<%=configmodel.stwh_cfthumbnail*1024 %>" //500KB 字节为单位
        };
        var params = {
            wmode: "transparent",
            play: true,
            loop: true,
            menu: true,
            devicefont: false,
            scale: "showall",
            quality: "high",
            bgcolor: "#ffffff",
            allowScriptAccess: "sameDomain",
            salign: ""
        };
        swfobject.embedSWF("/Plugin/upload.swf", "upstwh_cfico", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsVoice, { btnid: "upstwh_cfico", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);
        swfobject.embedSWF("/Plugin/upload.swf", "upstwh_cfico_m", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsVoice, { btnid: "upstwh_cfico_m", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);
        swfobject.embedSWF("/Plugin/upload.swf", "upstwh_cfico_s", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsVoice, { btnid: "upstwh_cfico_s", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);
        swfobject.embedSWF("/Plugin/upload.swf", "upstwh_cflogo", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsLogo, { btnid: "upstwh_cflogo", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);
        swfobject.embedSWF("/Plugin/upload.swf", "upstwh_cflogo_m", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsLogo, { btnid: "upstwh_cflogo_m", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);
        swfobject.embedSWF("/Plugin/upload.swf", "upstwh_cflogo_s", "95", "34", "11.0.0", "/Plugin/expressInstall.swf", $.extend({}, flashvarsLogo, { btnid: "upstwh_cflogo_s", 'ASPSESSID': ASPSESSID, 'AUTHID': auth }), params);

        //设置进度
        function setProgress(info, name, IsSelect, btnid) {
            if (btnid == "upstwh_cfico") {
                $("#progressFile").show().children().first().css("width", info + "%");
            }
            else if (btnid == "upstwh_cfico_m") {
                $("#progressFile1").show().children().first().css("width", info + "%");
            }
            else if (btnid == "upstwh_cfico_s") {
                $("#progressFile2").show().children().first().css("width", info + "%");
            }
            else if (btnid == "upstwh_cflogo") {
                $("#stwh_cflogoprogress").show().children().first().css("width", info + "%");
            }
            else if (btnid == "upstwh_cflogo_m") {
                $("#stwh_cflogo_mprogress").show().children().first().css("width", info + "%");
            }
            else if (btnid == "upstwh_cflogo_s") {
                $("#stwh_cflogo_sprogress").show().children().first().css("width", info + "%");
            }
        }

        //上传完成时调用的方法（该方法被flash所调用）
        //filename:文件安上传成功后返回的文件名，包含扩展名,altString提示信息,msg服务器返回的成功消息
        function EndUpload(filename, altString, msg, btnid) {
            if (msg == "NoFile") {
                $.bs.alert("请选择文件！", "info");
            }
            else if (msg == "NoType") {
                $.bs.alert("请选择指定类型文件！", "info");
            }
            else if (msg == "NoSize") {
                $.bs.alert("文件大小超出限制，请联系管理员！", "info");
            }
            else if (msg == "login") {
                $.bs.alert("请先登录！", "info");
            }
            else if (msg == "Exception") {
                $.bs.alert("请稍后操作！", "info");
            }
            else {
                if ((/image/gi).test(msg)) {
                    if (btnid == "upstwh_cfico") {
                        $("#stwh_cfico").val(msg);
                    }
                    else if (btnid == "upstwh_cfico_m") {
                        $("#stwh_cfico_m").val(msg);
                    }
                    else if (btnid == "upstwh_cfico_s") {
                        $("#stwh_cfico_s").val(msg);
                    }
                    else if (btnid == "upstwh_cflogo") {
                        $("#stwh_cflogo").val(msg);
                    }
                    else if (btnid == "upstwh_cflogo_m") {
                        $("#stwh_cflogo_m").val(msg);
                    }
                    else if (btnid == "upstwh_cflogo_s") {
                        $("#stwh_cflogo_s").val(msg);
                    }
                }
                else {
                    $.bs.alert(msg, "info");
                }
            }
        }

        //上传失败时调用的方法（该方法被flash所调用）
        function ErrorUpload(msg) {
            $.bs.alert(msg, "info");
        }
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>
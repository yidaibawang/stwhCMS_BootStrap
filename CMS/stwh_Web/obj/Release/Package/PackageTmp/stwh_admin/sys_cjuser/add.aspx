﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_cjuser.add" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">人员管理</span></li>
        <li class="active">添加数据</li>
    </ol>
    <div id="scrollPanel">
    <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx"
    method="post" role="form">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-1 st-text-right">
                抽奖人姓名：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_cjname" name="stwh_cjname" value=""  class="st-input-text-300 form-control" required="required" />
                
                <span class="text-danger">*请输入抽奖人姓名</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                手机号码：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_cjmobile" name="stwh_cjmobile" value=""  class="st-input-text-300 form-control" required="required" />
                
                <span class="text-danger">*请输入抽奖人联系电话，不能重复</span>
            </div>
        </div>
    </div>
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-plus"></i> 添加</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script type="text/javascript">
        $(function () {
            $("#addMenu").click(function () {
                var stwh_cjname = $("#stwh_cjname").val();
                if (!stwh_cjname) {
                    $.bs.alert("请输入参与抽奖人员姓名！", "info");
                    return false;
                }
                var stwh_cjmobile = $("#stwh_cjmobile").val();
                if (!stwh_cjmobile) {
                    $.bs.alert("请输入手机号码！", "info");
                    return false;
                }
                $.bs.loading(true);
                $.post("/Handler/stwh_admin/sys_cjuser/add.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    $.bs.loading(false);
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>
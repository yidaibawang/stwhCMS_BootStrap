﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="update.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_menus.update" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">菜单管理</span></li>
        <li class="active">修改数据</li>
    </ol>
    <div id="scrollPanel">
    <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx"
    method="post" role="form">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-1 st-text-right">
                上级菜单：</div>
            <div class="col-sm-11 ">
                <input type="hidden" id="stwh_menuid" name="stwh_menuid" value="<%=UpdateModel.stwh_menuid %>" />
                <input type="hidden" id="stwh_menuparentID" name="stwh_menuparentID" value="<%=UpdateModel.stwh_menuparentID %>" />
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    <span id="selectShowMenuText">
                        <%=parentMenuName%></span> <span class="caret"></span>
                </button>
                <ul id="selectShowListMenu" class="dropdown-menu" role="menu" runat="server" style="max-height:300px; overflow:auto; left:auto; top:auto;">
                    <li><a data-pid="0">父级导航</a></li>
                    <li><a data-pid="1">系统设置</a></li>
                    <li><a data-pid="2">系统管理</a></li>
                    <li class="divider"></li>
                    <li><a data-pid="3">内容管理</a></li>
                </ul>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                菜单序号：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_menuorder" name="stwh_menuorder" onkeydown="return checkNumber(event);"
                    value="<%=UpdateModel.stwh_menuorder %>" min="0" class="st-input-text-300 form-control" />
                <span class="text-danger">*越大越考前</span></div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                是否隐藏：</div>
            <div class="col-sm-11">
                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-primary btn-sm <%=UpdateModel.stwh_menustatus==0?"active":"" %>">
                        <input type="radio" name="stwh_menustatus" id="option1" value="0" <%=UpdateModel.stwh_menustatus==0?"checked=\"checked\"":"" %> />
                        显示
                    </label>
                    <label class="btn btn-primary btn-sm <%=UpdateModel.stwh_menustatus==1?"active":"" %>">
                        <input type="radio" name="stwh_menustatus" id="option2" value="1" <%=UpdateModel.stwh_menustatus==1?"checked=\"checked\"":"" %> />
                        隐藏
                    </label>
                </div>
                <span class="text-danger">*隐藏后不显示在界面导航中</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                菜单中文名称：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_menuname" name="stwh_menuname" value="<%=UpdateModel.stwh_menuname %>"
                    maxlength="100" class="st-input-text-300 form-control" required="required" />
                <span class="text-danger">*中文名称，100字符内，可由汉字、数字、大小写字母、中英文括号、下划线（_）、减号（-）组成。</span></div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                菜单英文名称：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_menunameUS" name="stwh_menunameUS" value="<%=UpdateModel.stwh_menunameUS %>"
                    maxlength="100" class="st-input-text-300 form-control" />
                <span class="text-danger">非必填，100字符内，可由汉字、数字、大小写字母、中英文括号、下划线（_）、减号（-）组成。</span>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                菜单图标：</div>
            <div class="col-sm-11">
                <input type="hidden" id="stwh_menuICO_url" name="stwh_menuICO_url" value="<%=UpdateModel.stwh_menuICO_url %>"
                    class="st-input-text-300" />
                <div id="tubiaoDiv" class="bs-glyphicons">
                    <ul>
                        <li><span class="fa fa-angellist" data-ico="fa fa-angellist"></span></li>
                        <li><span class="fa fa-area-chart" data-ico="fa fa-area-chart"></span></li>
                        <li><span class="fa fa-at" data-ico="fa fa-at"></span></li>
                        <li><span class="fa fa-bell-slash" data-ico="fa fa-bell-slash"></span></li>
                        <li><span class="fa fa-bell-slash-o" data-ico="fa fa-bell-slash-o"></span></li>
                        <li><span class="fa fa-bicycle" data-ico="fa fa-bicycle"></span></li>
                        <li><span class="fa fa-binoculars" data-ico="fa fa-binoculars"></span></li>
                        <li><span class="fa fa-birthday-cake" data-ico="fa fa-birthday-cake"></span></li>
                        <li><span class="fa fa-bus" data-ico="fa fa-bus"></span></li>
                        <li><span class="fa fa-calculator" data-ico="fa fa-calculator"></span></li>
                        <li><span class="fa fa-cc" data-ico="fa fa-cc"></span></li>
                        <li><span class="fa fa-cc-amex" data-ico="fa fa-cc-amex"></span></li>
                        <li><span class="fa fa-cc-discover" data-ico="fa fa-cc-discover"></span></li>
                        <li><span class="fa fa-cc-mastercard" data-ico="fa fa-cc-mastercard"></span></li>
                        <li><span class="fa fa-cc-paypal" data-ico="fa fa-cc-paypal"></span></li>
                        <li><span class="fa fa-cc-stripe" data-ico="fa fa-cc-stripe"></span></li>
                        <li><span class="fa fa-cc-visa" data-ico="fa fa-cc-visa"></span></li>
                        <li><span class="fa fa-copyright" data-ico="fa fa-copyright"></span></li>
                        <li><span class="fa fa-eyedropper" data-ico="fa fa-eyedropper"></span></li>
                        <li><span class="fa fa-futbol-o" data-ico="fa fa-futbol-o"></span></li>
                        <li><span class="fa fa-google-wallet" data-ico="fa fa-google-wallet"></span></li>
                        <li><span class="fa fa-ils" data-ico="fa fa-ils"></span></li>
                        <li><span class="fa fa-ioxhost" data-ico="fa fa-ioxhost"></span></li>
                        <li><span class="fa fa-lastfm" data-ico="fa fa-lastfm"></span></li>
                        <li><span class="fa fa-lastfm-square" data-ico="fa fa-lastfm-square"></span></li>
                        <li><span class="fa fa-line-chart" data-ico="fa fa-line-chart"></span></li>
                        <li><span class="fa fa-meanpath" data-ico="fa fa-meanpath"></span></li>
                        <li><span class="fa fa-newspaper-o" data-ico="fa fa-newspaper-o"></span></li>
                        <li><span class="fa fa-paint-brush" data-ico="fa fa-paint-brush"></span></li>
                        <li><span class="fa fa-paypal" data-ico="fa fa-paypal"></span></li>
                        <li><span class="fa fa-pie-chart" data-ico="fa fa-pie-chart"></span></li>
                        <li><span class="fa fa-plug" data-ico="fa fa-plug"></span></li>
                        <li><span class="fa fa-shekel" data-ico="fa fa-shekel"></span></li>
                        <li><span class="fa fa-sheqel" data-ico="fa fa-sheqel"></span></li>
                        <li><span class="fa fa-slideshare" data-ico="fa fa-slideshare"></span></li>
                        <li><span class="fa fa-soccer-ball-o" data-ico="fa fa-soccer-ball-o"></span></li>
                        <li><span class="fa fa-toggle-off" data-ico="fa fa-toggle-off"></span></li>
                        <li><span class="fa fa-toggle-on" data-ico="fa fa-toggle-on"></span></li>
                        <li><span class="fa fa-trash" data-ico="fa fa-trash"></span></li>
                        <li><span class="fa fa-tty" data-ico="fa fa-tty"></span></li>
                        <li><span class="fa fa-twitch" data-ico="fa fa-twitch"></span></li>
                        <li><span class="fa fa-wifi" data-ico="fa fa-wifi"></span></li>
                        <li><span class="fa fa-yelp" data-ico="fa fa-yelp"></span></li>
                        <li><span class="fa fa-adjust" data-ico="fa fa-adjust"></span></li>
                        <li><span class="fa fa-anchor" data-ico="fa fa-anchor"></span></li>
                        <li><span class="fa fa-archive" data-ico="fa fa-archive"></span></li>
                        <li><span class="fa fa-arrows" data-ico="fa fa-arrows"></span></li>
                        <li><span class="fa fa-arrows-h" data-ico="fa fa-arrows-h"></span></li>
                        <li><span class="fa fa-arrows-v" data-ico="fa fa-arrows-v"></span></li>
                        <li><span class="fa fa-asterisk" data-ico="fa fa-asterisk"></span></li>
                        <li><span class="fa fa-automobile" data-ico="fa fa-automobile"></span></li>
                        <li><span class="fa fa-ban" data-ico="fa fa-ban"></span></li>
                        <li><span class="fa fa-bank" data-ico="fa fa-bank"></span></li>
                        <li><span class="fa fa-bar-chart" data-ico="fa fa-bar-chart"></span></li>
                        <li><span class="fa fa-bar-chart-o" data-ico="fa fa-bar-chart-o"></span></li>
                        <li><span class="fa fa-barcode" data-ico="fa fa-barcode"></span></li>
                        <li><span class="fa fa-bars" data-ico="fa fa-bars"></span></li>
                        <li><span class="fa fa-beer" data-ico="fa fa-beer"></span></li>
                        <li><span class="fa fa-bell" data-ico="fa fa-bell"></span></li>
                        <li><span class="fa fa-bell-o" data-ico="fa fa-bell-o"></span></li>
                        <li><span class="fa fa-bolt" data-ico="fa fa-bolt"></span></li>
                        <li><span class="fa fa-bomb" data-ico="fa fa-bomb"></span></li>
                        <li><span class="fa fa-book" data-ico="fa fa-book"></span></li>
                        <li><span class="fa fa-bookmark" data-ico="fa fa-bookmark"></span></li>
                        <li><span class="fa fa-bookmark-o" data-ico="fa fa-bookmark-o"></span></li>
                        <li><span class="fa fa-briefcase" data-ico="fa fa-briefcase"></span></li>
                        <li><span class="fa fa-bug" data-ico="fa fa-bug"></span></li>
                        <li><span class="fa fa-building" data-ico="fa fa-building"></span></li>
                        <li><span class="fa fa-building-o" data-ico="fa fa-building-o"></span></li>
                        <li><span class="fa fa-bullhorn" data-ico="fa fa-bullhorn"></span></li>
                        <li><span class="fa fa-bullseye" data-ico="fa fa-bullseye"></span></li>
                        <li><span class="fa fa-cab" data-ico="fa fa-cab"></span></li>
                        <li><span class="fa fa-calendar" data-ico="fa fa-calendar"></span></li>
                        <li><span class="fa fa-calendar-o" data-ico="fa fa-calendar-o"></span></li>
                        <li><span class="fa fa-camera" data-ico="fa fa-camera"></span></li>
                        <li><span class="fa fa-camera-retro" data-ico="fa fa-camera-retro"></span></li>
                        <li><span class="fa fa-car" data-ico="fa fa-car"></span></li>
                        <li><span class="fa fa-caret-square-o-down" data-ico="fa fa-caret-square-o-down"></span>
                        </li>
                        <li><span class="fa fa-caret-square-o-left" data-ico="fa fa-caret-square-o-left"></span>
                        </li>
                        <li><span class="fa fa-caret-square-o-right" data-ico="fa fa-caret-square-o-right"></span>
                        </li>
                        <li><span class="fa fa-caret-square-o-up" data-ico="fa fa-caret-square-o-up"></span>
                        </li>
                        <li><span class="fa fa-certificate" data-ico="fa fa-certificate"></span></li>
                        <li><span class="fa fa-check" data-ico="fa fa-check"></span></li>
                        <li><span class="fa fa-check-circle" data-ico="fa fa-check-circle"></span></li>
                        <li><span class="fa fa-check-circle-o" data-ico="fa fa-check-circle-o"></span></li>
                        <li><span class="fa fa-check-square" data-ico="fa fa-check-square"></span></li>
                        <li><span class="fa fa-check-square-o" data-ico="fa fa-check-square-o"></span></li>
                        <li><span class="fa fa-child" data-ico="fa fa-child"></span></li>
                        <li><span class="fa fa-circle" data-ico="fa fa-circle"></span></li>
                        <li><span class="fa fa-circle-o" data-ico="fa fa-circle-o"></span></li>
                        <li><span class="fa fa-circle-o-notch" data-ico="fa fa-circle-o-notch"></span></li>
                        <li><span class="fa fa-circle-thin" data-ico="fa fa-circle-thin"></span></li>
                        <li><span class="fa fa-clock-o" data-ico="fa fa-clock-o"></span></li>
                        <li><span class="fa fa-close" data-ico="fa fa-close"></span></li>
                        <li><span class="fa fa-cloud" data-ico="fa fa-cloud"></span></li>
                        <li><span class="fa fa-cloud-download" data-ico="fa fa-cloud-download"></span></li>
                        <li><span class="fa fa-cloud-upload" data-ico="fa fa-cloud-upload"></span></li>
                        <li><span class="fa fa-code" data-ico="fa fa-code"></span></li>
                        <li><span class="fa fa-code-fork" data-ico="fa fa-code-fork"></span></li>
                        <li><span class="fa fa-coffee" data-ico="fa fa-coffee"></span></li>
                        <li><span class="fa fa-cog" data-ico="fa fa-cog"></span></li>
                        <li><span class="fa fa-cogs" data-ico="fa fa-cogs"></span></li>
                        <li><span class="fa fa-comment" data-ico="fa fa-comment"></span></li>
                        <li><span class="fa fa-comment-o" data-ico="fa fa-comment-o"></span></li>
                        <li><span class="fa fa-comments" data-ico="fa fa-comments"></span></li>
                        <li><span class="fa fa-comments-o" data-ico="fa fa-comments-o"></span></li>
                        <li><span class="fa fa-compass" data-ico="fa fa-compass"></span></li>
                        <li><span class="fa fa-credit-card" data-ico="fa fa-credit-card"></span></li>
                        <li><span class="fa fa-crop" data-ico="fa fa-crop"></span></li>
                        <li><span class="fa fa-crosshairs" data-ico="fa fa-crosshairs"></span></li>
                        <li><span class="fa fa-cube" data-ico="fa fa-cube"></span></li>
                        <li><span class="fa fa-cubes" data-ico="fa fa-cubes"></span></li>
                        <li><span class="fa fa-cutlery" data-ico="fa fa-cutlery"></span></li>
                        <li><span class="fa fa-dashboard" data-ico="fa fa-dashboard"></span></li>
                        <li><span class="fa fa-database" data-ico="fa fa-database"></span></li>
                        <li><span class="fa fa-desktop" data-ico="fa fa-desktop"></span></li>
                        <li><span class="fa fa-dot-circle-o" data-ico="fa fa-dot-circle-o"></span></li>
                        <li><span class="fa fa-download" data-ico="fa fa-download"></span></li>
                        <li><span class="fa fa-edit" data-ico="fa fa-edit"></span></li>
                        <li><span class="fa fa-ellipsis-h" data-ico="fa fa-ellipsis-h"></span></li>
                        <li><span class="fa fa-ellipsis-v" data-ico="fa fa-ellipsis-v"></span></li>
                        <li><span class="fa fa-envelope" data-ico="fa fa-envelope"></span></li>
                        <li><span class="fa fa-envelope-o" data-ico="fa fa-envelope-o"></span></li>
                        <li><span class="fa fa-envelope-square" data-ico="fa fa-envelope-square"></span>
                        </li>
                        <li><span class="fa fa-eraser" data-ico="fa fa-eraser"></span></li>
                        <li><span class="fa fa-exchange" data-ico="fa fa-exchange"></span></li>
                        <li><span class="fa fa-exclamation" data-ico="fa fa-exclamation"></span></li>
                        <li><span class="fa fa-exclamation-circle" data-ico="fa fa-exclamation-circle"></span>
                        </li>
                        <li><span class="fa fa-exclamation-triangle" data-ico="fa fa-exclamation-triangle"></span>
                        </li>
                        <li><span class="fa fa-external-link" data-ico="fa fa-external-link"></span></li>
                        <li><span class="fa fa-external-link-square" data-ico="fa fa-external-link-square"></span>
                        </li>
                        <li><span class="fa fa-eye" data-ico="fa fa-eye"></span></li>
                        <li><span class="fa fa-eye-slash" data-ico="fa fa-eye-slash"></span></li>
                        <li><span class="fa fa-fax" data-ico="fa fa-fax"></span></li>
                        <li><span class="fa fa-female" data-ico="fa fa-female"></span></li>
                        <li><span class="fa fa-fighter-jet" data-ico="fa fa-fighter-jet"></span></li>
                        <li><span class="fa fa-file-archive-o" data-ico="fa fa-file-archive-o"></span></li>
                        <li><span class="fa fa-file-audio-o" data-ico="fa fa-file-audio-o"></span></li>
                        <li><span class="fa fa-file-code-o" data-ico="fa fa-file-code-o"></span></li>
                        <li><span class="fa fa-file-excel-o" data-ico="fa fa-file-excel-o"></span></li>
                        <li><span class="fa fa-file-image-o" data-ico="fa fa-file-image-o"></span></li>
                        <li><span class="fa fa-file-movie-o" data-ico="fa fa-file-movie-o"></span></li>
                        <li><span class="fa fa-file-pdf-o" data-ico="fa fa-file-pdf-o"></span></li>
                        <li><span class="fa fa-file-photo-o" data-ico="fa fa-file-photo-o"></span></li>
                        <li><span class="fa fa-file-picture-o" data-ico="fa fa-file-picture-o"></span></li>
                        <li><span class="fa fa-file-powerpoint-o" data-ico="fa fa-file-powerpoint-o"></span>
                        </li>
                        <li><span class="fa fa-file-sound-o" data-ico="fa fa-file-sound-o"></span></li>
                        <li><span class="fa fa-file-video-o" data-ico="fa fa-file-video-o"></span></li>
                        <li><span class="fa fa-file-word-o" data-ico="fa fa-file-word-o"></span></li>
                        <li><span class="fa fa-file-zip-o" data-ico="fa fa-file-zip-o"></span></li>
                        <li><span class="fa fa-film" data-ico="fa fa-film"></span></li>
                        <li><span class="fa fa-filter" data-ico="fa fa-filter"></span></li>
                        <li><span class="fa fa-fire" data-ico="fa fa-fire"></span></li>
                        <li><span class="fa fa-fire-extinguisher" data-ico="fa fa-fire-extinguisher"></span>
                        </li>
                        <li><span class="fa fa-flag" data-ico="fa fa-flag"></span></li>
                        <li><span class="fa fa-flag-checkered" data-ico="fa fa-flag-checkered"></span></li>
                        <li><span class="fa fa-flag-o" data-ico="fa fa-flag-o"></span></li>
                        <li><span class="fa fa-flash" data-ico="fa fa-flash"></span></li>
                        <li><span class="fa fa-flask" data-ico="fa fa-flask"></span></li>
                        <li><span class="fa fa-folder" data-ico="fa fa-folder"></span></li>
                        <li><span class="fa fa-folder-o" data-ico="fa fa-folder-o"></span></li>
                        <li><span class="fa fa-folder-open" data-ico="fa fa-folder-open"></span></li>
                        <li><span class="fa fa-folder-open-o" data-ico="fa fa-folder-open-o"></span></li>
                        <li><span class="fa fa-frown-o" data-ico="fa fa-frown-o"></span></li>
                        <li><span class="fa fa-gamepad" data-ico="fa fa-gamepad"></span></li>
                        <li><span class="fa fa-gavel" data-ico="fa fa-gavel"></span></li>
                        <li><span class="fa fa-gear" data-ico="fa fa-gear"></span></li>
                        <li><span class="fa fa-gears" data-ico="fa fa-gears"></span></li>
                        <li><span class="fa fa-gift" data-ico="fa fa-gift"></span></li>
                        <li><span class="fa fa-glass" data-ico="fa fa-glass"></span></li>
                        <li><span class="fa fa-globe" data-ico="fa fa-globe"></span></li>
                        <li><span class="fa fa-graduation-cap" data-ico="fa fa-graduation-cap"></span></li>
                        <li><span class="fa fa-group" data-ico="fa fa-group"></span></li>
                        <li><span class="fa fa-hdd-o" data-ico="fa fa-hdd-o"></span></li>
                        <li><span class="fa fa-headphones" data-ico="fa fa-headphones"></span></li>
                        <li><span class="fa fa-heart" data-ico="fa fa-heart"></span></li>
                        <li><span class="fa fa-heart-o" data-ico="fa fa-heart-o"></span></li>
                        <li><span class="fa fa-history" data-ico="fa fa-history"></span></li>
                        <li><span class="fa fa-home" data-ico="fa fa-home"></span></li>
                        <li><span class="fa fa-image" data-ico="fa fa-image"></span></li>
                        <li><span class="fa fa-inbox" data-ico="fa fa-inbox"></span></li>
                        <li><span class="fa fa-info" data-ico="fa fa-info"></span></li>
                        <li><span class="fa fa-info-circle" data-ico="fa fa-info-circle"></span></li>
                        <li><span class="fa fa-institution" data-ico="fa fa-institution"></span></li>
                        <li><span class="fa fa-key" data-ico="fa fa-key"></span></li>
                        <li><span class="fa fa-keyboard-o" data-ico="fa fa-keyboard-o"></span></li>
                        <li><span class="fa fa-language" data-ico="fa fa-language"></span></li>
                        <li><span class="fa fa-laptop" data-ico="fa fa-laptop"></span></li>
                        <li><span class="fa fa-leaf" data-ico="fa fa-leaf"></span></li>
                        <li><span class="fa fa-legal" data-ico="fa fa-legal"></span></li>
                        <li><span class="fa fa-lemon-o" data-ico="fa fa-lemon-o"></span></li>
                        <li><span class="fa fa-level-down" data-ico="fa fa-level-down"></span></li>
                        <li><span class="fa fa-level-up" data-ico="fa fa-level-up"></span></li>
                        <li><span class="fa fa-life-bouy" data-ico="fa fa-life-bouy"></span></li>
                        <li><span class="fa fa-life-buoy" data-ico="fa fa-life-buoy"></span></li>
                        <li><span class="fa fa-life-ring" data-ico="fa fa-life-ring"></span></li>
                        <li><span class="fa fa-life-saver" data-ico="fa fa-life-saver"></span></li>
                        <li><span class="fa fa-lightbulb-o" data-ico="fa fa-lightbulb-o"></span></li>
                        <li><span class="fa fa-location-arrow" data-ico="fa fa-location-arrow"></span></li>
                        <li><span class="fa fa-lock" data-ico="fa fa-lock"></span></li>
                        <li><span class="fa fa-magic" data-ico="fa fa-magic"></span></li>
                        <li><span class="fa fa-magnet" data-ico="fa fa-magnet"></span></li>
                        <li><span class="fa fa-mail-forward" data-ico="fa fa-mail-forward"></span></li>
                        <li><span class="fa fa-mail-reply" data-ico="fa fa-mail-reply"></span></li>
                        <li><span class="fa fa-mail-reply-all" data-ico="fa fa-mail-reply-all"></span></li>
                        <li><span class="fa fa-male" data-ico="fa fa-male"></span></li>
                        <li><span class="fa fa-map-marker" data-ico="fa fa-map-marker"></span></li>
                        <li><span class="fa fa-meh-o" data-ico="fa fa-meh-o"></span></li>
                        <li><span class="fa fa-microphone" data-ico="fa fa-microphone"></span></li>
                        <li><span class="fa fa-microphone-slash" data-ico="fa fa-microphone-slash"></span>
                        </li>
                        <li><span class="fa fa-minus" data-ico="fa fa-minus"></span></li>
                        <li><span class="fa fa-minus-circle" data-ico="fa fa-minus-circle"></span></li>
                        <li><span class="fa fa-minus-square" data-ico="fa fa-minus-square"></span></li>
                        <li><span class="fa fa-minus-square-o" data-ico="fa fa-minus-square-o"></span></li>
                        <li><span class="fa fa-mobile" data-ico="fa fa-mobile"></span></li>
                        <li><span class="fa fa-mobile-phone" data-ico="fa fa-mobile-phone"></span></li>
                        <li><span class="fa fa-money" data-ico="fa fa-money"></span></li>
                        <li><span class="fa fa-moon-o" data-ico="fa fa-moon-o"></span></li>
                        <li><span class="fa fa-mortar-board" data-ico="fa fa-mortar-board"></span></li>
                        <li><span class="fa fa-music" data-ico="fa fa-music"></span></li>
                        <li><span class="fa fa-navicon" data-ico="fa fa-navicon"></span></li>
                        <li><span class="fa fa-paper-plane" data-ico="fa fa-paper-plane"></span></li>
                        <li><span class="fa fa-paper-plane-o" data-ico="fa fa-paper-plane-o"></span></li>
                        <li><span class="fa fa-paw" data-ico="fa fa-paw"></span></li>
                        <li><span class="fa fa-pencil" data-ico="fa fa-pencil"></span></li>
                        <li><span class="fa fa-pencil-square" data-ico="fa fa-pencil-square"></span></li>
                        <li><span class="fa fa-pencil-square-o" data-ico="fa fa-pencil-square-o"></span>
                        </li>
                        <li><span class="fa fa-phone" data-ico="fa fa-phone"></span></li>
                        <li><span class="fa fa-phone-square" data-ico="fa fa-phone-square"></span></li>
                        <li><span class="fa fa-photo" data-ico="fa fa-photo"></span></li>
                        <li><span class="fa fa-picture-o" data-ico="fa fa-picture-o"></span></li>
                        <li><span class="fa fa-plane" data-ico="fa fa-plane"></span></li>
                        <li><span class="fa fa-plus" data-ico="fa fa-plus"></span></li>
                        <li><span class="fa fa-plus-circle" data-ico="fa fa-plus-circle"></span></li>
                        <li><span class="fa fa-plus-square" data-ico="fa fa-plus-square"></span></li>
                        <li><span class="fa fa-plus-square-o" data-ico="fa fa-plus-square-o"></span></li>
                        <li><span class="fa fa-power-off" data-ico="fa fa-power-off"></span></li>
                        <li><span class="fa fa-print" data-ico="fa fa-print"></span></li>
                        <li><span class="fa fa-puzzle-piece" data-ico="fa fa-puzzle-piece"></span></li>
                        <li><span class="fa fa-qrcode" data-ico="fa fa-qrcode"></span></li>
                        <li><span class="fa fa-question" data-ico="fa fa-question"></span></li>
                        <li><span class="fa fa-question-circle" data-ico="fa fa-question-circle"></span>
                        </li>
                        <li><span class="fa fa-quote-left" data-ico="fa fa-quote-left"></span></li>
                        <li><span class="fa fa-quote-right" data-ico="fa fa-quote-right"></span></li>
                        <li><span class="fa fa-random" data-ico="fa fa-random"></span></li>
                        <li><span class="fa fa-recycle" data-ico="fa fa-recycle"></span></li>
                        <li><span class="fa fa-refresh" data-ico="fa fa-refresh"></span></li>
                        <li><span class="fa fa-remove" data-ico="fa fa-remove"></span></li>
                        <li><span class="fa fa-reorder" data-ico="fa fa-reorder"></span></li>
                        <li><span class="fa fa-reply" data-ico="fa fa-reply"></span></li>
                        <li><span class="fa fa-reply-all" data-ico="fa fa-reply-all"></span></li>
                        <li><span class="fa fa-retweet" data-ico="fa fa-retweet"></span></li>
                        <li><span class="fa fa-road" data-ico="fa fa-road"></span></li>
                        <li><span class="fa fa-rocket" data-ico="fa fa-rocket"></span></li>
                        <li><span class="fa fa-rss" data-ico="fa fa-rss"></span></li>
                        <li><span class="fa fa-rss-square" data-ico="fa fa-rss-square"></span></li>
                        <li><span class="fa fa-search" data-ico="fa fa-search"></span></li>
                        <li><span class="fa fa-search-minus" data-ico="fa fa-search-minus"></span></li>
                        <li><span class="fa fa-search-plus" data-ico="fa fa-search-plus"></span></li>
                        <li><span class="fa fa-send" data-ico="fa fa-send"></span></li>
                        <li><span class="fa fa-send-o" data-ico="fa fa-send-o"></span></li>
                        <li><span class="fa fa-share" data-ico="fa fa-share"></span></li>
                        <li><span class="fa fa-share-alt" data-ico="fa fa-share-alt"></span></li>
                        <li><span class="fa fa-share-alt-square" data-ico="fa fa-share-alt-square"></span>
                        </li>
                        <li><span class="fa fa-share-square" data-ico="fa fa-share-square"></span></li>
                        <li><span class="fa fa-share-square-o" data-ico="fa fa-share-square-o"></span></li>
                        <li><span class="fa fa-shield" data-ico="fa fa-shield"></span></li>
                        <li><span class="fa fa-shopping-cart" data-ico="fa fa-shopping-cart"></span></li>
                        <li><span class="fa fa-sign-in" data-ico="fa fa-sign-in"></span></li>
                        <li><span class="fa fa-sign-out" data-ico="fa fa-sign-out"></span></li>
                        <li><span class="fa fa-signal" data-ico="fa fa-signal"></span></li>
                        <li><span class="fa fa-sitemap" data-ico="fa fa-sitemap"></span></li>
                        <li><span class="fa fa-sliders" data-ico="fa fa-sliders"></span></li>
                        <li><span class="fa fa-smile-o" data-ico="fa fa-smile-o"></span></li>
                        <li><span class="fa fa-sort" data-ico="fa fa-sort"></span></li>
                        <li><span class="fa fa-sort-alpha-asc" data-ico="fa fa-sort-alpha-asc"></span></li>
                        <li><span class="fa fa-sort-alpha-desc" data-ico="fa fa-sort-alpha-desc"></span>
                        </li>
                        <li><span class="fa fa-sort-amount-asc" data-ico="fa fa-sort-amount-asc"></span>
                        </li>
                        <li><span class="fa fa-sort-amount-desc" data-ico="fa fa-sort-amount-desc"></span>
                        </li>
                        <li><span class="fa fa-sort-asc" data-ico="fa fa-sort-asc"></span></li>
                        <li><span class="fa fa-sort-desc" data-ico="fa fa-sort-desc"></span></li>
                        <li><span class="fa fa-sort-down" data-ico="fa fa-sort-down"></span></li>
                        <li><span class="fa fa-sort-numeric-asc" data-ico="fa fa-sort-numeric-asc"></span>
                        </li>
                        <li><span class="fa fa-sort-numeric-desc" data-ico="fa fa-sort-numeric-desc"></span>
                        </li>
                        <li><span class="fa fa-sort-up" data-ico="fa fa-sort-up"></span></li>
                        <li><span class="fa fa-space-shuttle" data-ico="fa fa-space-shuttle"></span></li>
                        <li><span class="fa fa-spinner" data-ico="fa fa-spinner"></span></li>
                        <li><span class="fa fa-spoon" data-ico="fa fa-spoon"></span></li>
                        <li><span class="fa fa-square" data-ico="fa fa-square"></span></li>
                        <li><span class="fa fa-square-o" data-ico="fa fa-square-o"></span></li>
                        <li><span class="fa fa-star" data-ico="fa fa-star"></span></li>
                        <li><span class="fa fa-star-half" data-ico="fa fa-star-half"></span></li>
                        <li><span class="fa fa-star-half-empty" data-ico="fa fa-star-half-empty"></span>
                        </li>
                        <li><span class="fa fa-star-half-full" data-ico="fa fa-star-half-full"></span></li>
                        <li><span class="fa fa-star-half-o" data-ico="fa fa-star-half-o"></span></li>
                        <li><span class="fa fa-star-o" data-ico="fa fa-star-o"></span></li>
                        <li><span class="fa fa-suitcase" data-ico="fa fa-suitcase"></span></li>
                        <li><span class="fa fa-sun-o" data-ico="fa fa-sun-o"></span></li>
                        <li><span class="fa fa-support" data-ico="fa fa-support"></span></li>
                        <li><span class="fa fa-tablet" data-ico="fa fa-tablet"></span></li>
                        <li><span class="fa fa-tachometer" data-ico="fa fa-tachometer"></span></li>
                        <li><span class="fa fa-tag" data-ico="fa fa-tag"></span></li>
                        <li><span class="fa fa-tags" data-ico="fa fa-tags"></span></li>
                        <li><span class="fa fa-tasks" data-ico="fa fa-tasks"></span></li>
                        <li><span class="fa fa-taxi" data-ico="fa fa-taxi"></span></li>
                        <li><span class="fa fa-terminal" data-ico="fa fa-terminal"></span></li>
                        <li><span class="fa fa-thumb-tack" data-ico="fa fa-thumb-tack"></span></li>
                        <li><span class="fa fa-thumbs-down" data-ico="fa fa-thumbs-down"></span></li>
                        <li><span class="fa fa-thumbs-o-down" data-ico="fa fa-thumbs-o-down"></span></li>
                        <li><span class="fa fa-thumbs-o-up" data-ico="fa fa-thumbs-o-up"></span></li>
                        <li><span class="fa fa-thumbs-up" data-ico="fa fa-thumbs-up"></span></li>
                        <li><span class="fa fa-ticket" data-ico="fa fa-ticket"></span></li>
                        <li><span class="fa fa-times" data-ico="fa fa-times"></span></li>
                        <li><span class="fa fa-times-circle" data-ico="fa fa-times-circle"></span></li>
                        <li><span class="fa fa-times-circle-o" data-ico="fa fa-times-circle-o"></span></li>
                        <li><span class="fa fa-tint" data-ico="fa fa-tint"></span></li>
                        <li><span class="fa fa-toggle-down" data-ico="fa fa-toggle-down"></span></li>
                        <li><span class="fa fa-toggle-left" data-ico="fa fa-toggle-left"></span></li>
                        <li><span class="fa fa-toggle-right" data-ico="fa fa-toggle-right"></span></li>
                        <li><span class="fa fa-toggle-up" data-ico="fa fa-toggle-up"></span></li>
                        <li><span class="fa fa-trash-o" data-ico="fa fa-trash-o"></span></li>
                        <li><span class="fa fa-tree" data-ico="fa fa-tree"></span></li>
                        <li><span class="fa fa-trophy" data-ico="fa fa-trophy"></span></li>
                        <li><span class="fa fa-truck" data-ico="fa fa-truck"></span></li>
                        <li><span class="fa fa-umbrella" data-ico="fa fa-umbrella"></span></li>
                        <li><span class="fa fa-university" data-ico="fa fa-university"></span></li>
                        <li><span class="fa fa-unlock" data-ico="fa fa-unlock"></span></li>
                        <li><span class="fa fa-unlock-alt" data-ico="fa fa-unlock-alt"></span></li>
                        <li><span class="fa fa-unsorted" data-ico="fa fa-unsorted"></span></li>
                        <li><span class="fa fa-upload" data-ico="fa fa-upload"></span></li>
                        <li><span class="fa fa-user" data-ico="fa fa-user"></span></li>
                        <li><span class="fa fa-users" data-ico="fa fa-users"></span></li>
                        <li><span class="fa fa-video-camera" data-ico="fa fa-video-camera"></span></li>
                        <li><span class="fa fa-volume-down" data-ico="fa fa-volume-down"></span></li>
                        <li><span class="fa fa-volume-off" data-ico="fa fa-volume-off"></span></li>
                        <li><span class="fa fa-volume-up" data-ico="fa fa-volume-up"></span></li>
                        <li><span class="fa fa-warning" data-ico="fa fa-warning"></span></li>
                        <li><span class="fa fa-wheelchair" data-ico="fa fa-wheelchair"></span></li>
                        <li><span class="fa fa-wrench" data-ico="fa fa-wrench"></span></li>
                        <li><span class="fa fa-file" data-ico="fa fa-file"></span></li>
                        <li><span class="fa fa-file-o" data-ico="fa fa-file-o"></span></li>
                        <li><span class="fa fa-file-text" data-ico="fa fa-file-text"></span></li>
                        <li><span class="fa fa-file-text-o" data-ico="fa fa-file-text-o"></span></li>
                        <li><span class="fa fa-bitcoin" data-ico="fa fa-bitcoin"></span></li>
                        <li><span class="fa fa-btc" data-ico="fa fa-btc"></span></li>
                        <li><span class="fa fa-cny" data-ico="fa fa-cny"></span></li>
                        <li><span class="fa fa-dollar" data-ico="fa fa-dollar"></span></li>
                        <li><span class="fa fa-eur" data-ico="fa fa-eur"></span></li>
                        <li><span class="fa fa-euro" data-ico="fa fa-euro"></span></li>
                        <li><span class="fa fa-gbp" data-ico="fa fa-gbp"></span></li>
                        <li><span class="fa fa-inr" data-ico="fa fa-inr"></span></li>
                        <li><span class="fa fa-jpy" data-ico="fa fa-jpy"></span></li>
                        <li><span class="fa fa-krw" data-ico="fa fa-krw"></span></li>
                        <li><span class="fa fa-rmb" data-ico="fa fa-rmb"></span></li>
                        <li><span class="fa fa-rouble" data-ico="fa fa-rouble"></span></li>
                        <li><span class="fa fa-rub" data-ico="fa fa-rub"></span></li>
                        <li><span class="fa fa-ruble" data-ico="fa fa-ruble"></span></li>
                        <li><span class="fa fa-rupee" data-ico="fa fa-rupee"></span></li>
                        <li><span class="fa fa-try" data-ico="fa fa-try"></span></li>
                        <li><span class="fa fa-turkish-lira" data-ico="fa fa-turkish-lira"></span></li>
                        <li><span class="fa fa-usd" data-ico="fa fa-usd"></span></li>
                        <li><span class="fa fa-won" data-ico="fa fa-won"></span></li>
                        <li><span class="fa fa-yen" data-ico="fa fa-yen"></span></li>
                        <li><span class="fa fa-align-center" data-ico="fa fa-align-center"></span></li>
                        <li><span class="fa fa-align-justify" data-ico="fa fa-align-justify"></span></li>
                        <li><span class="fa fa-align-left" data-ico="fa fa-align-left"></span></li>
                        <li><span class="fa fa-align-right" data-ico="fa fa-align-right"></span></li>
                        <li><span class="fa fa-bold" data-ico="fa fa-bold"></span></li>
                        <li><span class="fa fa-chain" data-ico="fa fa-chain"></span></li>
                        <li><span class="fa fa-chain-broken" data-ico="fa fa-chain-broken"></span></li>
                        <li><span class="fa fa-clipboard" data-ico="fa fa-clipboard"></span></li>
                        <li><span class="fa fa-columns" data-ico="fa fa-columns"></span></li>
                        <li><span class="fa fa-copy" data-ico="fa fa-copy"></span></li>
                        <li><span class="fa fa-cut" data-ico="fa fa-cut"></span></li>
                        <li><span class="fa fa-dedent" data-ico="fa fa-dedent"></span></li>
                        <li><span class="fa fa-files-o" data-ico="fa fa-files-o"></span></li>
                        <li><span class="fa fa-floppy-o" data-ico="fa fa-floppy-o"></span></li>
                        <li><span class="fa fa-font" data-ico="fa fa-font"></span></li>
                        <li><span class="fa fa-header" data-ico="fa fa-header"></span></li>
                        <li><span class="fa fa-indent" data-ico="fa fa-indent"></span></li>
                        <li><span class="fa fa-italic" data-ico="fa fa-italic"></span></li>
                        <li><span class="fa fa-link" data-ico="fa fa-link"></span></li>
                        <li><span class="fa fa-list" data-ico="fa fa-list"></span></li>
                        <li><span class="fa fa-list-alt" data-ico="fa fa-list-alt"></span></li>
                        <li><span class="fa fa-list-ol" data-ico="fa fa-list-ol"></span></li>
                        <li><span class="fa fa-list-ul" data-ico="fa fa-list-ul"></span></li>
                        <li><span class="fa fa-outdent" data-ico="fa fa-outdent"></span></li>
                        <li><span class="fa fa-paperclip" data-ico="fa fa-paperclip"></span></li>
                        <li><span class="fa fa-paragraph" data-ico="fa fa-paragraph"></span></li>
                        <li><span class="fa fa-paste" data-ico="fa fa-paste"></span></li>
                        <li><span class="fa fa-repeat" data-ico="fa fa-repeat"></span></li>
                        <li><span class="fa fa-rotate-left" data-ico="fa fa-rotate-left"></span></li>
                        <li><span class="fa fa-rotate-right" data-ico="fa fa-rotate-right"></span></li>
                        <li><span class="fa fa-save" data-ico="fa fa-save"></span></li>
                        <li><span class="fa fa-scissors" data-ico="fa fa-scissors"></span></li>
                        <li><span class="fa fa-strikethrough" data-ico="fa fa-strikethrough"></span></li>
                        <li><span class="fa fa-subscript" data-ico="fa fa-subscript"></span></li>
                        <li><span class="fa fa-superscript" data-ico="fa fa-superscript"></span></li>
                        <li><span class="fa fa-table" data-ico="fa fa-table"></span></li>
                        <li><span class="fa fa-text-height" data-ico="fa fa-text-height"></span></li>
                        <li><span class="fa fa-text-width" data-ico="fa fa-text-width"></span></li>
                        <li><span class="fa fa-th" data-ico="fa fa-th"></span></li>
                        <li><span class="fa fa-th-large" data-ico="fa fa-th-large"></span></li>
                        <li><span class="fa fa-th-list" data-ico="fa fa-th-list"></span></li>
                        <li><span class="fa fa-underline" data-ico="fa fa-underline"></span></li>
                        <li><span class="fa fa-undo" data-ico="fa fa-undo"></span></li>
                        <li><span class="fa fa-unlink" data-ico="fa fa-unlink"></span></li>
                        <li><span class="fa fa-angle-double-down" data-ico="fa fa-angle-double-down"></span>
                        </li>
                        <li><span class="fa fa-angle-double-left" data-ico="fa fa-angle-double-left"></span>
                        </li>
                        <li><span class="fa fa-angle-double-right" data-ico="fa fa-angle-double-right"></span>
                        </li>
                        <li><span class="fa fa-angle-double-up" data-ico="fa fa-angle-double-up"></span>
                        </li>
                        <li><span class="fa fa-angle-down" data-ico="fa fa-angle-down"></span></li>
                        <li><span class="fa fa-angle-left" data-ico="fa fa-angle-left"></span></li>
                        <li><span class="fa fa-angle-right" data-ico="fa fa-angle-right"></span></li>
                        <li><span class="fa fa-angle-up" data-ico="fa fa-angle-up"></span></li>
                        <li><span class="fa fa-arrow-circle-down" data-ico="fa fa-arrow-circle-down"></span>
                        </li>
                        <li><span class="fa fa-arrow-circle-left" data-ico="fa fa-arrow-circle-left"></span>
                        </li>
                        <li><span class="fa fa-arrow-circle-o-down" data-ico="fa fa-arrow-circle-o-down"></span>
                        </li>
                        <li><span class="fa fa-arrow-circle-o-left" data-ico="fa fa-arrow-circle-o-left"></span>
                        </li>
                        <li><span class="fa fa-arrow-circle-o-right" data-ico="fa fa-arrow-circle-o-right"></span>
                        </li>
                        <li><span class="fa fa-arrow-circle-o-up" data-ico="fa fa-arrow-circle-o-up"></span>
                        </li>
                        <li><span class="fa fa-arrow-circle-right" data-ico="fa fa-arrow-circle-right"></span>
                        </li>
                        <li><span class="fa fa-arrow-circle-up" data-ico="fa fa-arrow-circle-up"></span>
                        </li>
                        <li><span class="fa fa-arrow-down" data-ico="fa fa-arrow-down"></span></li>
                        <li><span class="fa fa-arrow-left" data-ico="fa fa-arrow-left"></span></li>
                        <li><span class="fa fa-arrow-right" data-ico="fa fa-arrow-right"></span></li>
                        <li><span class="fa fa-arrow-up" data-ico="fa fa-arrow-up"></span></li>
                        <li><span class="fa fa-arrows-alt" data-ico="fa fa-arrows-alt"></span></li>
                        <li><span class="fa fa-caret-down" data-ico="fa fa-caret-down"></span></li>
                        <li><span class="fa fa-caret-left" data-ico="fa fa-caret-left"></span></li>
                        <li><span class="fa fa-caret-right" data-ico="fa fa-caret-right"></span></li>
                        <li><span class="fa fa-caret-up" data-ico="fa fa-caret-up"></span></li>
                        <li><span class="fa fa-chevron-circle-down" data-ico="fa fa-chevron-circle-down"></span>
                        </li>
                        <li><span class="fa fa-chevron-circle-left" data-ico="fa fa-chevron-circle-left"></span>
                        </li>
                        <li><span class="fa fa-chevron-circle-right" data-ico="fa fa-chevron-circle-right"></span>
                        </li>
                        <li><span class="fa fa-chevron-circle-up" data-ico="fa fa-chevron-circle-up"></span>
                        </li>
                        <li><span class="fa fa-chevron-down" data-ico="fa fa-chevron-down"></span></li>
                        <li><span class="fa fa-chevron-left" data-ico="fa fa-chevron-left"></span></li>
                        <li><span class="fa fa-chevron-right" data-ico="fa fa-chevron-right"></span></li>
                        <li><span class="fa fa-chevron-up" data-ico="fa fa-chevron-up"></span></li>
                        <li><span class="fa fa-hand-o-down" data-ico="fa fa-hand-o-down"></span></li>
                        <li><span class="fa fa-hand-o-left" data-ico="fa fa-hand-o-left"></span></li>
                        <li><span class="fa fa-hand-o-right" data-ico="fa fa-hand-o-right"></span></li>
                        <li><span class="fa fa-hand-o-up" data-ico="fa fa-hand-o-up"></span></li>
                        <li><span class="fa fa-long-arrow-down" data-ico="fa fa-long-arrow-down"></span>
                        </li>
                        <li><span class="fa fa-long-arrow-left" data-ico="fa fa-long-arrow-left"></span>
                        </li>
                        <li><span class="fa fa-long-arrow-right" data-ico="fa fa-long-arrow-right"></span>
                        </li>
                        <li><span class="fa fa-long-arrow-up" data-ico="fa fa-long-arrow-up"></span></li>
                        <li><span class="fa fa-backward" data-ico="fa fa-backward"></span></li>
                        <li><span class="fa fa-compress" data-ico="fa fa-compress"></span></li>
                        <li><span class="fa fa-eject" data-ico="fa fa-eject"></span></li>
                        <li><span class="fa fa-expand" data-ico="fa fa-expand"></span></li>
                        <li><span class="fa fa-fast-backward" data-ico="fa fa-fast-backward"></span></li>
                        <li><span class="fa fa-fast-forward" data-ico="fa fa-fast-forward"></span></li>
                        <li><span class="fa fa-forward" data-ico="fa fa-forward"></span></li>
                        <li><span class="fa fa-pause" data-ico="fa fa-pause"></span></li>
                        <li><span class="fa fa-play" data-ico="fa fa-play"></span></li>
                        <li><span class="fa fa-play-circle" data-ico="fa fa-play-circle"></span></li>
                        <li><span class="fa fa-play-circle-o" data-ico="fa fa-play-circle-o"></span></li>
                        <li><span class="fa fa-step-backward" data-ico="fa fa-step-backward"></span></li>
                        <li><span class="fa fa-step-forward" data-ico="fa fa-step-forward"></span></li>
                        <li><span class="fa fa-stop" data-ico="fa fa-stop"></span></li>
                        <li><span class="fa fa-youtube-play" data-ico="fa fa-youtube-play"></span></li>
                        <li><span class="fa fa-adn" data-ico="fa fa-adn"></span></li>
                        <li><span class="fa fa-android" data-ico="fa fa-android"></span></li>
                        <li><span class="fa fa-apple" data-ico="fa fa-apple"></span></li>
                        <li><span class="fa fa-behance" data-ico="fa fa-behance"></span></li>
                        <li><span class="fa fa-behance-square" data-ico="fa fa-behance-square"></span></li>
                        <li><span class="fa fa-bitbucket" data-ico="fa fa-bitbucket"></span></li>
                        <li><span class="fa fa-bitbucket-square" data-ico="fa fa-bitbucket-square"></span>
                        </li>
                        <li><span class="fa fa-codepen" data-ico="fa fa-codepen"></span></li>
                        <li><span class="fa fa-css3" data-ico="fa fa-css3"></span></li>
                        <li><span class="fa fa-delicious" data-ico="fa fa-delicious"></span></li>
                        <li><span class="fa fa-deviantart" data-ico="fa fa-deviantart"></span></li>
                        <li><span class="fa fa-digg" data-ico="fa fa-digg"></span></li>
                        <li><span class="fa fa-dribbble" data-ico="fa fa-dribbble"></span></li>
                        <li><span class="fa fa-dropbox" data-ico="fa fa-dropbox"></span></li>
                        <li><span class="fa fa-drupal" data-ico="fa fa-drupal"></span></li>
                        <li><span class="fa fa-empire" data-ico="fa fa-empire"></span></li>
                        <li><span class="fa fa-facebook" data-ico="fa fa-facebook"></span></li>
                        <li><span class="fa fa-facebook-square" data-ico="fa fa-facebook-square"></span>
                        </li>
                        <li><span class="fa fa-flickr" data-ico="fa fa-flickr"></span></li>
                        <li><span class="fa fa-foursquare" data-ico="fa fa-foursquare"></span></li>
                        <li><span class="fa fa-ge" data-ico="fa fa-ge"></span></li>
                        <li><span class="fa fa-git" data-ico="fa fa-git"></span></li>
                        <li><span class="fa fa-git-square" data-ico="fa fa-git-square"></span></li>
                        <li><span class="fa fa-github" data-ico="fa fa-github"></span></li>
                        <li><span class="fa fa-github-alt" data-ico="fa fa-github-alt"></span></li>
                        <li><span class="fa fa-github-square" data-ico="fa fa-github-square"></span></li>
                        <li><span class="fa fa-gittip" data-ico="fa fa-gittip"></span></li>
                        <li><span class="fa fa-google" data-ico="fa fa-google"></span></li>
                        <li><span class="fa fa-google-plus" data-ico="fa fa-google-plus"></span></li>
                        <li><span class="fa fa-google-plus-square" data-ico="fa fa-google-plus-square"></span>
                        </li>
                        <li><span class="fa fa-hacker-news" data-ico="fa fa-hacker-news"></span></li>
                        <li><span class="fa fa-html5" data-ico="fa fa-html5"></span></li>
                        <li><span class="fa fa-instagram" data-ico="fa fa-instagram"></span></li>
                        <li><span class="fa fa-joomla" data-ico="fa fa-joomla"></span></li>
                        <li><span class="fa fa-jsfiddle" data-ico="fa fa-jsfiddle"></span></li>
                        <li><span class="fa fa-linkedin" data-ico="fa fa-linkedin"></span></li>
                        <li><span class="fa fa-linkedin-square" data-ico="fa fa-linkedin-square"></span>
                        </li>
                        <li><span class="fa fa-linux" data-ico="fa fa-linux"></span></li>
                        <li><span class="fa fa-maxcdn" data-ico="fa fa-maxcdn"></span></li>
                        <li><span class="fa fa-openid" data-ico="fa fa-openid"></span></li>
                        <li><span class="fa fa-pagelines" data-ico="fa fa-pagelines"></span></li>
                        <li><span class="fa fa-pied-piper" data-ico="fa fa-pied-piper"></span></li>
                        <li><span class="fa fa-pied-piper-alt" data-ico="fa fa-pied-piper-alt"></span></li>
                        <li><span class="fa fa-pinterest" data-ico="fa fa-pinterest"></span></li>
                        <li><span class="fa fa-pinterest-square" data-ico="fa fa-pinterest-square"></span>
                        </li>
                        <li><span class="fa fa-qq" data-ico="fa fa-qq"></span></li>
                        <li><span class="fa fa-ra" data-ico="fa fa-ra"></span></li>
                        <li><span class="fa fa-rebel" data-ico="fa fa-rebel"></span></li>
                        <li><span class="fa fa-reddit" data-ico="fa fa-reddit"></span></li>
                        <li><span class="fa fa-reddit-square" data-ico="fa fa-reddit-square"></span></li>
                        <li><span class="fa fa-renren" data-ico="fa fa-renren"></span></li>
                        <li><span class="fa fa-skype" data-ico="fa fa-skype"></span></li>
                        <li><span class="fa fa-slack" data-ico="fa fa-slack"></span></li>
                        <li><span class="fa fa-soundcloud" data-ico="fa fa-soundcloud"></span></li>
                        <li><span class="fa fa-spotify" data-ico="fa fa-spotify"></span></li>
                        <li><span class="fa fa-stack-exchange" data-ico="fa fa-stack-exchange"></span></li>
                        <li><span class="fa fa-stack-overflow" data-ico="fa fa-stack-overflow"></span></li>
                        <li><span class="fa fa-steam" data-ico="fa fa-steam"></span></li>
                        <li><span class="fa fa-steam-square" data-ico="fa fa-steam-square"></span></li>
                        <li><span class="fa fa-stumbleupon" data-ico="fa fa-stumbleupon"></span></li>
                        <li><span class="fa fa-stumbleupon-circle" data-ico="fa fa-stumbleupon-circle"></span>
                        </li>
                        <li><span class="fa fa-tencent-weibo" data-ico="fa fa-tencent-weibo"></span></li>
                        <li><span class="fa fa-trello" data-ico="fa fa-trello"></span></li>
                        <li><span class="fa fa-tumblr" data-ico="fa fa-tumblr"></span></li>
                        <li><span class="fa fa-tumblr-square" data-ico="fa fa-tumblr-square"></span></li>
                        <li><span class="fa fa-twitter" data-ico="fa fa-twitter"></span></li>
                        <li><span class="fa fa-twitter-square" data-ico="fa fa-twitter-square"></span></li>
                        <li><span class="fa fa-vimeo-square" data-ico="fa fa-vimeo-square"></span></li>
                        <li><span class="fa fa-vine" data-ico="fa fa-vine"></span></li>
                        <li><span class="fa fa-vk" data-ico="fa fa-vk"></span></li>
                        <li><span class="fa fa-wechat" data-ico="fa fa-wechat"></span></li>
                        <li><span class="fa fa-weibo" data-ico="fa fa-weibo"></span></li>
                        <li><span class="fa fa-weixin" data-ico="fa fa-weixin"></span></li>
                        <li><span class="fa fa-windows" data-ico="fa fa-windows"></span></li>
                        <li><span class="fa fa-wordpress" data-ico="fa fa-wordpress"></span></li>
                        <li><span class="fa fa-xing" data-ico="fa fa-xing"></span></li>
                        <li><span class="fa fa-xing-square" data-ico="fa fa-xing-square"></span></li>
                        <li><span class="fa fa-yahoo" data-ico="fa fa-yahoo"></span></li>
                        <li><span class="fa fa-youtube" data-ico="fa fa-youtube"></span></li>
                        <li><span class="fa fa-youtube-square" data-ico="fa fa-youtube-square"></span></li>
                        <li><span class="fa fa-ambulance" data-ico="fa fa-ambulance"></span></li>
                        <li><span class="fa fa-h-square" data-ico="fa fa-h-square"></span></li>
                        <li><span class="fa fa-hospital-o" data-ico="fa fa-hospital-o"></span></li>
                        <li><span class="fa fa-medkit" data-ico="fa fa-medkit"></span></li>
                        <li><span class="fa fa-stethoscope" data-ico="fa fa-stethoscope"></span></li>
                        <li><span class="fa fa-user-md" data-ico="fa fa-user-md"></span></li>
                    </ul>
                </div>
            </div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                菜单路径：</div>
            <div class="col-sm-11 form-group">
                <input type="text" id="stwh_menuURL" name="stwh_menuURL" value="<%=UpdateModel.stwh_menuURL %>"
                    class="st-input-text-300 form-control" required="required" />
                <span class="text-danger">*请求路径（非技术人员请勿更改，否则会影响程序功能运行）</span></div>
            <div class="line10">
            </div>
            <div class="col-sm-1 st-text-right">
                备注说明：</div>
            <div class="col-sm-11 form-group">
                <textarea id="stwh_menudescription" name="stwh_menudescription" class="st-input-text-300 form-control"><%=UpdateModel.stwh_menudescription%></textarea>
            </div>
        </div>
    </div>
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-edit"></i> 修改</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script type="text/javascript">
        $(function () {
            $("#tubiaoDiv li span[data-ico=\"" + $("#stwh_menuICO_url").val() + "\"]").parent().addClass("hoverClass");
            $("#selectShowListMenu a").click(function () {
                $("#selectShowMenuText").text($(this).children(".menuName").text());
                $("#stwh_menuparentID").val($(this).attr("data-pid"));
            });
            $("#tubiaoDiv li").click(function () {
                $("#tubiaoDiv li").removeClass("hoverClass");
                $(this).addClass("hoverClass");
                $("#stwh_menuICO_url").val($(this).children("span").attr("data-ico"));
            });
            $("#addMenu").click(function () {
                var stwh_menuorder = $("#stwh_menuorder").val();
                if (!stwh_menuorder) {
                    $.bs.alert("请输入菜单序号！", "info");
                    return false;
                }
                else if (!IsNumber(stwh_menuorder)) {
                    $.bs.alert("菜单序号格式错误！", "danger");
                    return false;
                }
                var stwh_menuname = $("#stwh_menuname").val();
                if (!stwh_menuname) {
                    $.bs.alert("请输入菜单中文名称！", "info");
                    return false;
                }
                else if (!IsHanZF(1, 15, stwh_menuname)) {
                    $.bs.alert("菜单中文名称格式错误！", "danger");
                    return false;
                }
                var stwh_menunameUS = $("#stwh_menunameUS").val();
                if (stwh_menunameUS) {
                    if (!IsHanZF(1, 50, stwh_menunameUS)) {
                        $.bs.alert("菜单英文名称格式错误！", "danger");
                        return false;
                    }
                }
                var stwh_menuICO_url = $("#stwh_menuICO_url").val();
                if (!stwh_menuICO_url) {
                    $.bs.alert("请选择菜单图标！", "info");
                    return false;
                }
                var stwh_menuURL = $("#stwh_menuURL").val();
                if (!stwh_menuURL) {
                    $.bs.alert("请输入菜单路径！", "info");
                    return false;
                }
                $.post("/Handler/stwh_admin/sys_menus/update.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "-1");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>

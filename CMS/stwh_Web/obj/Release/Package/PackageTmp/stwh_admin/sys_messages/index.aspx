﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_messages.index" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li class="active"><span id="menuSpan" runat="server">留言管理</span></li>
    </ol>
    <div class="m-top-10 p-bottom-15 p-left-15 scrollTop">
        <form class="form-inline">
            <asp:Literal ID="litBtnList" runat="server"></asp:Literal>
            <div id="searchDiv" class="collapse">
                <div class="form-group">
                    <input type="text" class="form-control" name="stwh_lmname" id="stwh_lmname" placeholder="请输入姓名" />
                </div>
                <div class="form-group">
                    &nbsp;&nbsp;<input type="text" class="form-control" name="stwh_lmsj" id="stwh_lmsj" placeholder="请输入手机号码" />
                </div>
                <div class="form-group">
                    &nbsp;&nbsp;<input type="text" class="form-control" name="stwh_lmemail" id="stwh_lmemail" placeholder="请输入邮箱" />
                </div>
                <div class="form-group">
                    &nbsp;&nbsp;
                    <div class=" input-group">
                        <input type="text" name="stwh_lmaddtime" id="stwh_lmaddtime" placeholder="请输入时间"  class="form-control" value="">
                        <span id="calendar" class=" input-group-addon"  style=" cursor:pointer;"><span class="fa fa-calendar"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    &nbsp;&nbsp;<a href="#" class="btn btn-default" id="btnsearchOk"><span class="glyphicon glyphicon-search"></span></a>
                </div>
            </div>
        </form>
    </div>
    <div id="scrollPanel">
    <form id="form1" runat="server" class="form-inline">
    <div class="container-fluid">
        <table class="table table-striped table-bordered table-hover table-condensed">
            <thead>
                <tr>
                    <th style="width: 50px;">
                        <input type="checkbox" id="allParent" />
                    </th>
                    <th style=" width:50px;">
                        编号
                    </th>
                    <th style="width: 100px;">
                        昵称
                    </th>
                    <th style="width: 100px;">
                        姓名
                    </th>
                    <th style="width: 50px;">
                        年龄
                    </th>
                    <th style="width: 50px;">
                        性别
                    </th>
                    <th style="width: 150px;">
                        手机
                    </th>
                    <th style="width: 150px;">
                        email
                    </th>
                    <th style="width: 200px;">
                        主题
                    </th>
                    <th>
                        留言内容
                    </th>
                    <th style="width: 150px;">
                        留言时间
                    </th>
                    <th style="width: 150px;">
                        状态
                    </th>
                </tr>
            </thead>
            <tbody id="ChildDatas" runat="server">
            </tbody>
        </table>
    </div>
    <asp:HiddenField ID="hidListId" runat="server" />
    <asp:HiddenField ID="hidAllData" runat="server" />
    <asp:HiddenField ID="hidTotalSum" runat="server" />
    </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
        <div class="form-group">
            <label>
                每页显示：</label>
            <select id="basic" class="selectpicker show-tick" data-width="55px"
                data-style="btn-sm btn-default">
                <option>15</option>
                <option>25</option>
                <option>35</option>
                <option>45</option>
                <option>55</option>
            </select>
        </div>
        <div class="form-group">
            <ul id="pageUl" style="margin: 0px auto;">
            </ul>
        </div>
        <div class="form-group">
            共 <span id="SumCountSpan"><asp:Literal ID="littotalSum" runat="server"></asp:Literal></span> 条数据
        </div>
        </form>
    </div>
    <div class="modal fade" id="deleteMessageAllModal" tabindex="-1" role="dialog" aria-labelledby="h1"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="h2">
                        <i class="fa fa-exclamation-circle"></i> 系统提示
                    </h4>
                </div>
                <div class="modal-body">
                    <h4>
                        <span class="glyphicon glyphicon-info-sign"></span>你确定要清空留言吗（清空后不可恢复）？</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        取消
                    </button>
                    <button type="button" class="btn btn-default" id="btnDeleteAllOk">
                        确定
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <div class="modal fade" id="deleteMessageModal" tabindex="-1" role="dialog" aria-labelledby="h2"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="h2">
                        <i class="fa fa-exclamation-circle"></i> 系统提示
                    </h4>
                </div>
                <div class="modal-body">
                    <h4>
                        <span class="glyphicon glyphicon-info-sign"></span>你确定要删除吗（删除后不可恢复）？</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        取消
                    </button>
                    <button type="button" class="btn btn-default" id="btnDeleteOk">
                        确定
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script type="text/javascript">
        var AllData = JSON.parse($("#hidAllData").val());
        var listid = [];
        var pageCount = 15;//每页显示多少条数据
        var ifelse = "";
        //加载数据
        function LoadData(pCount,pIndex,ifstr)
        {
            $.post("/handler/stwh_admin/sys_messages/loaddata.ashx",{
                pageCount:pCount,
                pageIndex:pIndex,
                whereStr:ifstr
            },function(data){
                if (data.msgcode==-1) {
                    $.bs.alert(data.msg, "warning", "");
                }
                else
                {
                    $("#hidListId").val("");
                    AllData = data.msg;
                    $("#ChildDatas tr").remove();
                    $("#SumCountSpan").text(data.sumcount);
                    if (data.msg.length==0) {
                        $("#ChildDatas").append("<tr><td colspan=\"12\" align=\"center\">暂无数据</td></tr>");
                    }
                    else
                    {
                        $.each(data.msg, function (index, item) {
                            $("#ChildDatas").append("<tr data-id=\"" + item.stwh_lmid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_lmid + "\" value=\"" + item.stwh_lmid + "\" /></td><td>" + item.stwh_lmid + "</td><td>" + item.stwh_lmnc + "</td><td>" + item.stwh_lmname + "</td><td>" + item.stwh_lmage + "</td><td>" + (item.stwh_lmsex == 0 ? "男" : "女") + "</td><td>" + item.stwh_lmsj + "</td><td>" + item.stwh_lmemail + "</td><td>" + item.stwh_lmsubject + "</td><td>" + item.stwh_lmlynr + "</td><td>" + item.stwh_lmaddtime.replace('T',' ') + "</td><td>" + (item.stwh_lmissh == 0 ? "<div class=\"btn-group\"data-toggle=\"buttons\"><label class=\"btn btn-primary btn-sm active\"><input type=\"radio\"name=\"radp" + item.stwh_lmid + "\"value=\"0\" checked=\"checked\"/>取消</label><label class=\"btn btn-primary btn-sm\"><input type=\"radio\"name=\"radp" + item.stwh_lmid + "\"value=\"1\"/>审核</label></div>" : "<div class=\"btn-group\"data-toggle=\"buttons\"><label class=\"btn btn-primary btn-sm\"><input type=\"radio\"name=\"radp" + item.stwh_lmid + "\"value=\"0\"/>取消</label><label class=\"btn btn-primary btn-sm active\"><input type=\"radio\"name=\"radp" + item.stwh_lmid + "\"value=\"1\"/>审核</label></div>") + "</td></tr>");
                        });
                        UpdateScroll();
                    }
                }
            },"json");
        }
        //重新设置分页
        function SetPaginator(pageCount,totalsum)
        {
            if (totalsum==0) totalsum = 1;
            $("#pageUl").bootstrapPaginator("setOptions",{
                    currentPage: 1, //当前页面
                    totalPages: totalsum / pageCount + ((totalsum % pageCount) == 0 ? 0 : 1), //总页数
                    numberOfPages: 5, //页码数
                    useBootstrapTooltip: true,
                    onPageChanged: function (event, oldPage, newPage) {
                        if (oldPage != newPage) LoadData(pageCount,parseInt(newPage) - 1,ifelse);
                    }
                });
        }
        $(function () {
            $("#pageUl").bootstrapPaginator({
                currentPage: 1, //当前页面
                totalPages: <%=totalPages %>, //总页数
                numberOfPages: 5, //页码数
                useBootstrapTooltip: true,
                onPageChanged: function (event, oldPage, newPage) {
                    if (oldPage != newPage) LoadData(pageCount,parseInt(newPage) - 1,ifelse);
                }
            });
            $("#allParent").click(function () {
                if ($(this).is(":checked")) {
                    $("#ChildDatas :checkbox").prop("checked", $(this).is(":checked"));
                    $("#ChildDatas :checkbox:checked").each(function (i, item) {
                        if (listid.IndexOfArray($(item).val()) == -1) listid.AddArray($(item).val());
                    });
                    $("#hidListId").val(listid.toStringArray());
                }
                else {
                    $("#ChildDatas :checkbox").prop("checked", $(this).is(":checked"));
                    $("#ChildDatas :checkbox:not(:checked)").each(function (i, item) {
                        listid.removeIndexArray(listid.IndexOfArray($(item).val()));
                    });
                    $("#hidListId").val(listid.toStringArray());
                }
            });
            $("#ChildDatas").on("click",":checkbox",function(){
              if ($(this).is(":checked")) {
                    if ($("#ChildDatas :checkbox:checked").length == $("#ChildDatas :checkbox").length) $("#allParent").prop("checked", true);
                    if (listid.IndexOfArray($(this).val()) == -1) listid.AddArray($(this).val());
                    $("#hidListId").val(listid.toStringArray());
                }
                else {
                    if ($("#ChildDatas :checkbox:not(:checked)").length == $("#ChildDatas :checkbox").length) $("#allParent").prop("checked", false);
                    listid.removeIndexArray(listid.IndexOfArray($(this).val()));
                    $("#hidListId").val(listid.toStringArray());
                }
            });
            $("#ChildDatas").on("click", ".btn-group label", function (e) {
                var jb_status = $(this).children(":radio").val();
                var jb_mid = $(this).parent().parent().parent().attr("data-id");

                $.each(AllData, function (index, item) {
                    if (item.stwh_lmid == parseInt(jb_mid)) {
                        item.stwh_lmissh = parseInt(jb_status);
                        return false;
                    }
                });
            });
            $("#btnDelete").click(function () {
                $("#deleteMessageModal").modal('show');
            });
            $("#btnDeleteOk").click(function () {
                var ids = $("#hidListId").val();
                if (!ids) {
                    $.bs.alert("请选择数据！", "info");
                    return false;
                }
                else if (!(/^[\d,]*$/).test(ids)) {
                    $.bs.alert("非法数据格式，请刷新页面重试！", "danger");
                    return false;
                }
                $.post(
                "/handler/stwh_admin/sys_messages/delete.ashx",
                {
                    ids: ids
                }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
            });
            $("#btnDeleteAll").click(function () {
                $("#deleteMessageAllModal").modal('show');
            });
            $("#btnDeleteAllOk").click(function () { 
                $.post(
                "/handler/stwh_admin/sys_messages/deleteALL.ashx",
                {data:null}, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
            });
            $("#btnSave").click(function () {
                $.post(
                "/handler/stwh_admin/sys_messages/save.ashx",
                { data: JSON.stringify(AllData) },
                function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
            });
            $("#btnSearch").click(function(){
                $('#searchDiv').collapse("toggle");
                return false;
            });
            $("#btnsearchOk").click(function(){
                var stwh_lmname = $("#stwh_lmname").val();
                if (!IsHanZF(1,50,stwh_lmname) && stwh_lmname !="") {
                    $.bs.alert("操作人员格式错误！", "info");
                    return;
                }
                var stwh_lmsj = $("#stwh_lmsj").val();
                if (!IsTel(stwh_lmsj) && stwh_lmsj != "") {
                    if (!IsMobileTel(stwh_lmsj)) {
                        $.bs.alert("手机格式错误！", "info");
                        return;
                    }
                }
                var stwh_lmemail = $("#stwh_lmemail").val();
                if (!IsEmail(stwh_lmemail) && stwh_lmemail != "") {
                    $.bs.alert("邮箱格式错误！", "info");
                    return;
                }
                var stwh_lmaddtime = $("#stwh_lmaddtime").val();
                if (stwh_lmname=="" && stwh_lmsj == "" && stwh_lmemail == "" && stwh_lmaddtime == "") {
                    $.bs.alert("至少输入一个查询值！", "info");
                    return;
                }
                ifelse = '[{"name":"stwh_lmname","value":"'+stwh_lmname+'"},{"name":"stwh_lmsj","value":"'+stwh_lmsj+'"},{"name":"stwh_lmemail","value":"'+stwh_lmemail+'"},{"name":"stwh_lmaddtime","value":"'+stwh_lmaddtime+'"}]';
                $.bs.loading(true);
                $.post("/handler/stwh_admin/sys_messages/loaddata.ashx",{
                    pageCount:pageCount,
                    pageIndex:0,
                    whereStr:ifelse
                },function(data){
                    $.bs.loading(false);
                    $('#searchDiv').collapse("toggle");
                    if (data.msgcode==-1) {
                        $.bs.alert(data.msg, "warning", "");
                    }
                    else
                    {
                        $("#hidListId").val("");
                        AllData = data.msg;
                        $("#ChildDatas tr").remove();
                        $("#SumCountSpan").text(data.sumcount);
                        $("#hidTotalSum").val(data.sumcount);
                        SetPaginator(pageCount,parseInt(data.sumcount));
                        if (data.msg.length==0) {
                            $("#ChildDatas").append("<tr><td colspan=\"12\" align=\"center\">暂无数据</td></tr>");
                        }
                        else
                        {
                            $.each(data.msg, function (index, item) {
                                $("#ChildDatas").append("<tr data-id=\"" + item.stwh_lmid + "\"><td><input type=\"checkbox\" name=\"chkp\" id=\"chk" + item.stwh_lmid + "\" value=\"" + item.stwh_lmid + "\" /></td><td>" + item.stwh_lmid + "</td><td>" + item.stwh_lmnc + "</td><td>" + item.stwh_lmname + "</td><td>" + item.stwh_lmage + "</td><td>" + (item.stwh_lmsex == 0 ? "男" : "女") + "</td><td>" + item.stwh_lmsj + "</td><td>" + item.stwh_lmemail + "</td><td>" + item.stwh_lmsubject + "</td><td>" + item.stwh_lmlynr + "</td><td>" + item.stwh_lmaddtime.replace('T',' ') + "</td><td>" + (item.stwh_lmissh == 0 ? "<div class=\"btn-group\"data-toggle=\"buttons\"><label class=\"btn btn-primary btn-sm active\"><input type=\"radio\"name=\"radp" + item.stwh_lmid + "\"value=\"0\" checked=\"checked\"/>取消</label><label class=\"btn btn-primary btn-sm\"><input type=\"radio\"name=\"radp" + item.stwh_lmid + "\"value=\"1\"/>审核</label></div>" : "<div class=\"btn-group\"data-toggle=\"buttons\"><label class=\"btn btn-primary btn-sm\"><input type=\"radio\"name=\"radp" + item.stwh_lmid + "\"value=\"0\"/>取消</label><label class=\"btn btn-primary btn-sm active\"><input type=\"radio\"name=\"radp" + item.stwh_lmid + "\"value=\"1\"/>审核</label></div>") + "</td></tr>");
                            });
                            UpdateScroll();
                        }
                    }
                },"json");
                return false;
            });
            $("#calendar").click(function () {
                $('#stwh_lmaddtime').click();
            });
            $('#stwh_lmaddtime').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                autoApply: true,
                autoUpdateInput: false
            }).on({
                "apply.daterangepicker": function (ev, picker) {
                    $(this).val(picker.startDate.format('YYYY-MM-DD'));
                }
            });

            $("#basic").change(function(){
                pageCount = $(this).val();
                LoadData(pageCount,0,ifelse);
                var totalsum = parseInt($("#hidTotalSum").val());
                SetPaginator(pageCount,totalsum);
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>
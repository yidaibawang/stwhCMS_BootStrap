﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="add.aspx.cs" Inherits="stwh_Web.stwh_admin.sys_shopaddress.add" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <cms:stwhbtmeta ID="stwhbtmeta" runat="server" />
    <title>上海舒同文化传播有限公司-企龙管理系统 </title>
    <cms:stwhbtlink ID="stwhbtlink" runat="server" />
    <link href="/plugin/jQuery-colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css" />
    <%--<link href="/stwh_admin/css/animate.css" rel="stylesheet" type="text/css" />--%>
</head>
<body>
    <div class="loading" id="loading"><i class="fa fa-spinner fa-spin fa-2x"></i></div>
    <ol class="breadcrumb location scrollTop">
        <li><a href="javascript:history.go(-1);"><span class="fa fa-mail-reply"></span> 返回上一页</a></li>
        <li><span id="menuSpan" runat="server">会员地址管理</span></li>
        <li class="active">添加数据</li>
    </ol>
    <div id="scrollPanel">
        <form id="form1" runat="server" action="/Handler/stwh_admin/sys_menus_function/add.ashx" method="post" role="form">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-1 st-text-right">
                        会员id：
                    </div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_buid" name="stwh_buid" value="" class="st-input-text-300 form-control"  onkeydown="return checkNumber(event);" placeholder="请输入会员id" />
                        <span class="text-danger">“会员ID”可以通过查看会员管理获取，编号就是会员的ID</span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        收货人：
                    </div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_baname" name="stwh_baname" value="" class="st-input-text-300 form-control" placeholder="请输入收货人" />
                        <span class="text-danger"></span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        联系电话：
                    </div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_bamobile" name="stwh_bamobile" value="" class="st-input-text-300 form-control" style="margin-right: 15px;" placeholder="请输入收货人联系电话" />
                        <span class="text-danger"></span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        所在地区：
                    </div>
                    <div class="col-sm-11 form-group">
                        <div id="diquDiv">
  		                    <select class="prov form-control" name="city" style="width:100px; float:left; margin-right:15px;"></select> 
    	                    <select class="city form-control" name="xiang" style="width:150px;  float:left; margin-right:15px;" disabled="disabled"></select>
                            <select class="dist form-control" name="cun" style="width:200px;  float:left;" disabled="disabled"></select>
                        </div>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        详细地址：
                    </div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_baaddress" name="stwh_baaddress" value="" class="st-input-text-300 form-control" placeholder="请输入详细地址" />
                        <span class="text-danger"></span>
                    </div>
                    <div class="line10">
                    </div>
                    <div class="col-sm-1 st-text-right">
                        邮编：
                    </div>
                    <div class="col-sm-11 form-group">
                        <input type="text" id="stwh_bayoubian" name="stwh_bayoubian" value="0" maxlength="6" onkeydown="return checkNumber(event);" class="st-input-text-300 form-control" placeholder="请输入邮编" />
                        <span class="text-danger">默认0，代表6个0</span>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="container-fluid st-page">
        <form class="form-inline" role="form">
            <button id="addMenu" class="btn btn-default"><i class="fa fa-plus"></i>添加</button>
        </form>
    </div>
    <cms:stwhbtscript ID="stwhbtscript" runat="server" />
    <script src="/Plugin/cityselect/js/jquery.cityselect.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#diquDiv").citySelect({
                prov: "北京",
                nodata: "none"
            });
            $("#addMenu").click(function () {
                var stwh_buid = $("#stwh_buid").val();
                if (!stwh_buid) {
                    $.bs.alert("请输入会员ID！", "info");
                    return false;
                }
                var stwh_baname = $("#stwh_baname").val();
                if (!stwh_baname) {
                    $.bs.alert("请输入收货人姓名！", "info");
                    return false;
                }
                else if (!(/^[\u4e00-\u9fa5]{2,20}$/).test(stwh_baname)) {
                    $.bs.alert("收货人姓名格式错误！", "info");
                    return false;
                }
                var stwh_bamobile = $("#stwh_bamobile").val();
                if (!stwh_bamobile) {
                    $.bs.alert("请输入手机号码！", "info");
                    return false;
                }
                else if (!(/^[a-zA-Z0-9+-]{6,12}$/).test(stwh_bamobile)) {
                    $.bs.alert("手机号码格式错误！", "info");
                    return false;
                }
                var stwh_baaddress = $("#stwh_baaddress").val();
                if (!stwh_baaddress) {
                    $.bs.alert("请输入详细地址！", "info");
                    return false;
                }
                var stwh_bayoubian = $("#stwh_bayoubian").val();
                if (!stwh_bayoubian) {
                    $.bs.alert("请输入邮编！", "info");
                    return false;
                }

                $.post("/Handler/stwh_admin/sys_shopaddress/add.ashx", { data: JSON.stringify($("#form1").serializeArray()) }, function (data) {
                    if (data.msgcode == "0") {
                        $.bs.alert(data.msg, "success", "this");
                    }
                    else {
                        $.bs.alert(data.msg, "danger");
                    }
                }, "json");
                return false;
            });
        });
    </script>
    <cms:stwhbtchildpagejs ID="stwhbtchildpagejs" runat="server" />
    <asp:Literal ID="LiteralJS" runat="server"></asp:Literal>
    <cms:stwhbtfooterjs ID="stwhbtfooterjs" runat="server" />
</body>
</html>
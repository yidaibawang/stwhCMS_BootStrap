﻿$.fn.bootstrapPaginator.defaults.itemTexts = function (type, page, current) {
    switch (type) {
        case "first":
            return "首页";
        case "prev":
            return "上一页";
        case "next":
            return "下一页";
        case "last":
            return "尾页";
        case "page":
            return page;
    }
};
$.fn.bootstrapPaginator.defaults.tooltipTitles = function (type, page, current) {
    switch (type) {
        case "first":
            return "跳转到首页";
        case "prev":
            return "跳转到上一页";
        case "next":
            return "跳转到下一页";
        case "last":
            return "跳转到尾页";
        case "page":
            return (page === current) ? "当前页" : "跳转到第" + page + "页";
    }
};
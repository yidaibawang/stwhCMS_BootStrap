﻿var qj_storage = window.localStorage;
$(function () {
    if (qj_storage) {
        $.bs.options = {
            isHide: true,
            isSpeak: qj_storage.getItem("sysSound") == 0 ? true : false,
            hideTime: 1500
        };
    }
    $("#btnRefresh").click(function () {
        var dqurl = window.location.href.split('#')[0];
        window.location.href = dqurl;
        return false;
    });
    $(window).scroll(function () {
        if (parseInt($(this).scrollTop()) > 10) {
            if ($(".scrollTop").css("position") != "fixed") {
                $(".scrollTop").css({
                    "position": "fixed",
                    "width": "100%"
                });
                $($(".scrollTop")[1]).stop().animate({
                    "top": "40px"
                }, 500);
            }
        } else {
            $(".scrollTop").css({
                "position": "static"
            });
            $($(".scrollTop")[1]).css("top", "0px");
        }
    });
});
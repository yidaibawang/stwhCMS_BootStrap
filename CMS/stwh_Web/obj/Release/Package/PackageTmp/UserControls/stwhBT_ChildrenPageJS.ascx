﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="stwhBT_ChildrenPageJS.ascx.cs"
    Inherits="stwh_Web.UserControls.stwhBT_ChildrenPageJS" %>
<script type="text/javascript">
    function SetFormHeight() {
        $("#scrollPanel").css("height", (parent.parent.GetMarinRightHeight()-160) + "px");
    }
    function UpdateScroll() {
        $("#scrollPanel").mCustomScrollbar("update");
    }
    (function ($) {
        $(window).load(function () {
            SetFormHeight();
            $("#scrollPanel").mCustomScrollbar({
                scrollInertia: 80,
                autoHideScrollbar: true,
                advanced: {
                    autoScrollOnFocus: false
                }
            });
        });
    })(jQuery);
</script>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using stwh_Model;
using stwh_BLL;
using stwh_Common;
using stwh_Web.stwh_admin.Common;

namespace stwh_Web
{
    public partial class installkey : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void setKey_Click(object sender, EventArgs e)
        {
            string key = this.key.Value;
            if (string.IsNullOrEmpty(key))
            {
                this.Literal1.Text = "<script>alert('请输入系统key');</script>";
            }
            else
            {
                if (!PageValidate.IsCustom(@"[a-zA-Z0-9-]{20,}", key)) this.Literal1.Text = "<script>alert('系统key错误');</script>";
                else
                {
                    stwh_config conmodel = new stwh_config();
                    conmodel.stwh_cfkey = key;
                    if (WebSite.UpdateWebSite(conmodel)) this.Literal1.Text = "<script>alert('系统key设置成功！！');</script>";
                    else this.Literal1.Text = "<script>alert('系统key设置失败');</script>";
                }
            }
        }
    }
}
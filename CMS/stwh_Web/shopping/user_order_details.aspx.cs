﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using stwh_Web.stwh_admin.Common;
using stwh_Model;
using stwh_BLL;
using stwh_Common;

namespace stwh_Web.shopping
{
    /// <summary>
    /// 订单详细页面
    /// </summary>
    public partial class user_order_details : WebPageShopBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //判断是否登录
            stwh_buyuser model = CheckLogin();
            if (model == null) Response.Redirect("/shop/login.html?reUrl=/shop/user_order.html");
            else
            {
                string orderid = Request["orderid"];
                if (!PageValidate.IsCustom(@"^\d{16,}$", orderid)) throw new Exception("数据格式非法，请从网站正常提交！！");
                PageBaseVT pbvt = new PageBaseShoppingVT("user_order_details");
                pbvt.OutPutHtml();
            }
        }
    }
}
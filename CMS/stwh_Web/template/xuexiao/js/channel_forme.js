﻿

function channelSubmit() {

    //读取title参数，确定模式及显示

    //ecookies操作--添加----------------------------------
    var ecookies = MathRand(15);
    var ecookiesname = "2015ysmsgbook";
    if ($.cookie(ecookiesname) == null || $.cookie(ecookiesname) == "") {
        //写入cookie
        $.cookie(ecookiesname, ecookies, { expires: 1, domain: domain });
    } else {
        ecookies = $.cookie(ecookiesname);
    }
    //ecookies操作结束--添加----------------------------------

    //定义变量
    var Activityid = parseInt($("#channel_ActivityId").val()) ? parseInt($("#channel_ActivityId").val()) : 0;
    var app_id = 10001;     //Pc:10001        mobile: 10002        android: 10003        ios: 10004        other: 10005
    //------------------------
    var channel_company = $("#channel_company").val();
    var channel_user = $("#channel_user").val()
    var channel_job = $("#channel_job").val();
    var channel_contact = $("#channel_contact").val();
    var channel_corp = $("#channel_corp").val();
    var NContentChar = "|||";
    //var Ncontent = channel_company + NContentChar + channel_user + NContentChar + channel_job + NContentChar + channel_contact + NContentChar + channel_corp + NContentChar + eContactPhone;
    var Ncontent = channel_company + NContentChar + channel_user + NContentChar + channel_job + NContentChar + channel_contact + NContentChar + channel_corp + NContentChar + ip + NContentChar + sourceUrl;
    //alert(Ncontent);
    //------------------------
    var Remarks = '渠道加盟';


    //判断数据
    if (Activityid == 0) { layer.msg("活动ID不能为空", 1, -1); return false; }

    if (channel_company == "") { layer.msg("请输入公司名称", 1, -1); $("#channel_company").focus(); return false; }

    if (channel_user == "") { layer.msg("请输入联系人", 1, -1); $("#channel_user").focus(); return false; }

    if (channel_job == "") { layer.msg("请输入在职职位", 1, -1); $("#channel_job").focus(); return false; }

    if (channel_contact == "") { layer.msg("请输入联系方式", 1, -1); $("#channel_contact").focus(); return false; }

    if (channel_corp == "") { layer.msg("请输入合作意向", 1, -1); $("#channel_corp").focus(); return false; }

    //if (eContactPhone == "") { layer.msg("请输入申请人手机", 1, -1); $("#eContactPhone").focus(); return false; }

    //if ($("input[name=eCategory]:checked").size() <= 0) {
    //    layer.msg('请至少选择一项类别', 1, -1);		//弹出窗口，几秒后关闭
    //    return false;
    //}
    //if (eQQ == "" || !checkQQ(eQQ)) {
    //    layer.msg("请输入正确QQ号码", 1, -1); $("#eQQ").focus(); return false;
    //}
    //if (channel_corp == "" || !checkEmail(channel_corp)) {
    //    layer.msg("请输入正确电子邮件", 1, -1); $("#channel_corp").focus(); return false;
    //}
    //if (eContactPhone == "" || !checkMobile(eContactPhone)) {
    //    layer.msg("请输入正确手机号码", 1, -1); $("#eContactPhone").focus(); return false;
    //}


    //定义提交字段
    var myfields = "userid,username,Activityid,addtime,updatetime,ecookies,app_id,Ncontent,Remarks";


    //提交数据
    $("#submitMsg").attr("disabled", true);
    $.ajax({
        type: "post",
        url: "/Project/Activity/HandlerJson.aspx",
        data: { ctrl: "add", type: "Activity_history", fields: myfields, userid: "0", username: "-", Activityid: Activityid, addtime: "datetime", updatetime: "datetime", ecookies: ecookies, app_id: app_id, Ncontent: Ncontent, Remarks: Remarks },
        dataType: "json",
        success: function (data) {
            if (data.result == '1') {
                //清空input,清空textarea,清空选择
                var e = document.getElementsByTagName("input");
                for (var i = 0; i < e.length; i++) {
                    if (e[i].type == "text") {
                        e[i].value = '';
                    }
                }
                $("textarea").val('');
                //$("#demoWin").show();
                alert("提交成功，请等待回复！");

                //for mail      //105201007@qq.com
                var toMail = "105201007@qq.com";
                //test.yingsheng.cc:8080/Communication-email-send.htm?mail_subject=test&mail_body=test_body&to_mail=wish03@163.com
                $.ajax({
                    type: "post",
                    async: true,	//false时执行完再继续执行
                    url: "/Communication-email-send.htm",
                    data: { mail_subject: "【" + Remarks + "】" + (new Date()).toLocaleString(), to_mail: toMail, mail_body: Ncontent },
                    dataType: "json",
                    success: function (data) {
                        if (data.result == '1') {
                            //layer.msg(data.msg, 3, -1);
                            //alert(data.msg);
                        }
                        else {
                            //layer.msg(data.msg, 3, -1);
                            //alert(data.msg);
                            return false;
                        }
                    }, error: function () { //alert("系统繁忙，请稍候重试！"); return false; 
                    }
                });


            }
            else {
                layer.msg(data.msg, 1, -1);
                return false;
            }
            $("#submitMsg").attr("disabled", false);
        }, error: function () { $("#submitMsg").attr("disabled", false); alert("系统繁忙，请稍候重试！"); return false; }
    });
}






function teacherSubmit() {

    //读取title参数，确定模式及显示

    //ecookies操作--添加----------------------------------
    var ecookies = MathRand(15);
    var ecookiesname = "2015ysmsgbook";
    if ($.cookie(ecookiesname) == null || $.cookie(ecookiesname) == "") {
        //写入cookie
        $.cookie(ecookiesname, ecookies, { expires: 1, domain: domain });
    } else {
        ecookies = $.cookie(ecookiesname);
    }
    //ecookies操作结束--添加----------------------------------

    //定义变量
    var Activityid = parseInt($("#teacher_ActivityId").val()) ? parseInt($("#teacher_ActivityId").val()) : 0;
    var app_id = 10001;     //Pc:10001        mobile: 10002        android: 10003        ios: 10004        other: 10005
    //------------------------
    var teacher_user = $("#teacher_user").val();
    var teacher_mobile = $("#teacher_mobile").val()
    var teacher_field = $("#teacher_field").val();
    var teacher_experience = $("#teacher_experience").val();
    var NContentChar = "|||";
    //var Ncontent = teacher_user + NContentChar + teacher_mobile + NContentChar + teacher_experience + NContentChar + teacher_field + NContentChar + channel_corp + NContentChar + eContactPhone;
    var Ncontent = teacher_user + NContentChar + teacher_mobile + NContentChar + teacher_field + NContentChar + teacher_experience + NContentChar + ip + NContentChar + sourceUrl;
    //alert(Ncontent);
    //------------------------
    var Remarks = '讲师合作';


    //判断数据
    if (Activityid == 0) { layer.msg("活动ID不能为空", 1, -1); return false; }

    if (teacher_user == "") { layer.msg("请输入讲师名称", 1, -1); $("#teacher_user").focus(); return false; }

    if (teacher_mobile == "") { layer.msg("请输入手机号码", 1, -1); $("#teacher_mobile").focus(); return false; }

    if (teacher_mobile == "" || !checkMobile(teacher_mobile)) {
        layer.msg("请输入正确手机号码", 1, -1); $("#teacher_mobile").focus(); return false;
    }

    if (teacher_field == "") { layer.msg("请输入擅长课程领域", 1, -1); $("#teacher_field").focus(); return false; }

    if (teacher_experience == "") { layer.msg("请输入培训经历", 1, -1); $("#teacher_experience").focus(); return false; }

    //if (eContactPhone == "") { layer.msg("请输入申请人手机", 1, -1); $("#eContactPhone").focus(); return false; }

    //if ($("input[name=eCategory]:checked").size() <= 0) {
    //    layer.msg('请至少选择一项类别', 1, -1);		//弹出窗口，几秒后关闭
    //    return false;
    //}
    //if (eQQ == "" || !checkQQ(eQQ)) {
    //    layer.msg("请输入正确QQ号码", 1, -1); $("#eQQ").focus(); return false;
    //}
    //if (channel_corp == "" || !checkEmail(channel_corp)) {
    //    layer.msg("请输入正确电子邮件", 1, -1); $("#channel_corp").focus(); return false;
    //}
    //if (eContactPhone == "" || !checkMobile(eContactPhone)) {
    //    layer.msg("请输入正确手机号码", 1, -1); $("#eContactPhone").focus(); return false;
    //}


    //定义提交字段
    var myfields = "userid,username,Activityid,addtime,updatetime,ecookies,app_id,Ncontent,Remarks";


    //提交数据
    $("#submitMsg").attr("disabled", true);
    $.ajax({
        type: "post",
        url: "/Project/Activity/HandlerJson.aspx",
        data: { ctrl: "add", type: "Activity_history", fields: myfields, userid: "0", username: "-", Activityid: Activityid, addtime: "datetime", updatetime: "datetime", ecookies: ecookies, app_id: app_id, Ncontent: Ncontent, Remarks: Remarks },
        dataType: "json",
        success: function (data) {
            if (data.result == '1') {
                //清空INPUT值,清空radio选择
                var e = document.getElementsByTagName("input");
                for (var i = 0; i < e.length; i++) {
                    if (e[i].type == "text") {
                        e[i].value = '';
                    }
                }
                $("textarea").val('');
                //$("#demoWin").show();
                alert("提交成功，请等待回复！");

                //for mail      //105201007@qq.com
                var toMail = "105201007@qq.com";
                //test.yingsheng.cc:8080/Communication-email-send.htm?mail_subject=test&mail_body=test_body&to_mail=wish03@163.com
                $.ajax({
                    type: "post",
                    async: true,	//false时执行完再继续执行
                    url: "/Communication-email-send.htm",
                    data: { mail_subject: "【" + Remarks + "】" + (new Date()).toLocaleString(), to_mail: toMail, mail_body: Ncontent },
                    dataType: "json",
                    success: function (data) {
                        if (data.result == '1') {
                            //layer.msg(data.msg, 3, -1);
                            //alert(data.msg);
                        }
                        else {
                            //layer.msg(data.msg, 3, -1);
                            //alert(data.msg);
                            return false;
                        }
                    }, error: function () { //alert("系统繁忙，请稍候重试！"); return false; 
                    }
                });


            }
            else {
                layer.msg(data.msg, 1, -1);
                return false;
            }
            $("#submitMsg").attr("disabled", false);
        }, error: function () { $("#submitMsg").attr("disabled", false); alert("系统繁忙，请稍候重试！"); return false; }
    });
}




//验证手机号
function checkMobile(sMobile) {
    if (!(/^1[3|4|5|7|8|][0-9]\d{4,8}$/.test(sMobile))) {
        return false;
    } else {
        return true;
    }
}

//验证电子邮件
function checkEmail(str) {
    var reg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
    if (reg.test(str)) {
        return true;
    } else {
        return false;
    }
}

//验证QQ
function checkQQ(str) {
    if (str.search(/^[1-9]\d{4,12}$/) != -1) {
        return true;
    }
    else {
        return false;
    }
}


//验证数字
function checkNumber(str) {
    if (!isNan(str)) {
        return true;
    }
    else {
        return false;
    }
}


//js随机数
function MathRand(num) {
    var Str = "";
    for (var i = 0; i < num; i++) {
        Str += Math.floor(Math.random() * 10);
    }
    return Str;
}

//显示窗口
function showWin(parm) {
    $("#win").zjslayer("withClose");
}
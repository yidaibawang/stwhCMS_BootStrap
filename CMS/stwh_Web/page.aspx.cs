﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using stwh_Web.stwh_admin.Common;
using stwh_Common;

namespace stwh_Web
{
    public partial class page : WebPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string template = Request["template"];
            if (!string.IsNullOrEmpty(template)&&PageValidate.IsCustom("[a-zA-Z0-9_-]{1,}",template))
            {
                PageBaseVT pbvt = new PageBaseVT(template);
                pbvt.OutPutHtml();
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using stwh_Web.stwh_admin.Common;

namespace stwh_Web
{
    public partial class _404 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            PageBaseVT pbvt = new PageBaseVT("404");
            pbvt.OutPutHtml();
        }
    }
}
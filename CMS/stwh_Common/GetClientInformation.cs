﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//导入命名空间
using System.Text.RegularExpressions;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json.Linq;

namespace stwh_Common
{
    /// <summary>
    /// web客户端操作类
    /// </summary>
    public class GetClientInformation
    {
        [DllImport("Iphlpapi.dll")]
        private static extern int SendARP(Int32 dest, Int32 host, ref Int64 mac, ref Int32 length);
        [DllImport("Ws2_32.dll")]
        private static extern Int32 inet_addr(string ip);

        /// <summary>
        /// 根据ip获取客户端地理位置
        /// </summary>
        /// <param name="ip">待查询ip地址</param>
        /// <returns></returns>
        private static string GetPlace(string ip)
        {
            try
            {
                string result = "未知";
                if (!string.IsNullOrEmpty(ip))
                {
                    HttpProc.WebClient wc = new HttpProc.WebClient();
                    result = wc.GetHtml("http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=json&ip=" + ip).Trim();
                    if (result == "-2") result = "未知";
                    else
                    {
                        JObject jobj = JsonConvert.DeserializeObject(result) as JObject;
                        string ret = jobj.Root["ret"].ToString().Trim();
                        if (ret == "-1") result = "未知";
                        else if (ret == "1")
                        {
                            result = jobj.Root["country"].ToString().Trim() + " ";
                            result += jobj.Root["province"].ToString().Trim() + " ";
                            result += jobj.Root["city"].ToString().Trim() + " ";
                            result += jobj.Root["district"].ToString().Trim() + " ";
                        }
                    }
                }
                return result;
            }
            catch (Exception)
            {
                return "未知";
            }
        }

        /// <summary>
        /// 获取客户端ip地址
        /// </summary>
        /// <returns></returns>
        public static string GetClientIP()
        {
            string result = "";
            try
            {
                if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null) result = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (string.IsNullOrEmpty(result)) result = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                if (string.IsNullOrEmpty(result)) result = HttpContext.Current.Request.UserHostAddress;
            }
            catch (Exception)
            {
                result = "";
            }
            return result;
        }

        /// <summary>
        /// 根据ip获取客户端地理位置
        /// </summary>
        /// <returns></returns>
        public static string GetClientPlace()
        {
            return GetPlace(GetClientIP());
        }

        /// <summary>
        /// 根据ip获取客户端地理位置
        /// </summary>
        /// <param name="ip">待查询ip地址</param>
        /// <returns></returns>
        public static string GetClientPlace(string ip)
        {
            return GetPlace(ip);
        }

        /// <summary>
        /// 获取客户端浏览器
        /// </summary>
        /// <returns></returns>
        public static string GetClientBrowserVersions()
        {
            string browserVersions = string.Empty;
            HttpBrowserCapabilities hbc = HttpContext.Current.Request.Browser;
            string browserType = hbc.Browser.ToString();     //获取浏览器类型
            string browserVersion = hbc.Version.ToString();    //获取版本号
            browserVersions = browserType + browserVersion;
            return browserVersions;
        }

        /// <summary>
        /// 获取客户端操作系统
        /// </summary>
        /// <returns></returns>
        public static string GetClientSystemCheck()
        {
            string Agent = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];

            if (Agent.IndexOf("NT 4.0") > 0) return "Windows NT ";
            else if (Agent.IndexOf("NT 5.0") > 0) return "Windows 2000";
            else if (Agent.IndexOf("NT 5.1") > 0) return "Windows XP";
            else if (Agent.IndexOf("NT 5.2") > 0) return "Windows 2003";
            else if (Agent.IndexOf("NT 6.0") > 0) return "Windows Vista";
            else if (Agent.IndexOf("WindowsCE") > 0) return "Windows CE";
            else if (Agent.IndexOf("NT") > 0) return "Windows NT ";
            else if (Agent.IndexOf("9x") > 0) return "Windows ME";
            else if (Agent.IndexOf("98") > 0) return "Windows 98";
            else if (Agent.IndexOf("95") > 0) return "Windows 95";
            else if (Agent.IndexOf("Win32") > 0) return "Win32";
            else if (Agent.IndexOf("Linux") > 0) return "Linux";
            else if (Agent.IndexOf("SunOS") > 0) return "SunOS";
            else if (Agent.IndexOf("Mac") > 0) return "Mac";
            else if (Agent.IndexOf("Linux") > 0) return "Linux";
            else if (Agent.IndexOf("Windows") > 0) return "Windows";
            return "未知类型";
        }

        /// <summary>  
        /// 获取Mac地址信息(外网无法获取到mac，仅限于局域网使用)
        /// </summary>  
        /// <param name="IP">ip地址</param>  
        /// <returns></returns>  
        public static string GetClientMac(string IP)
        {
            Int32 ldest = inet_addr(IP);
            Int64 macinfo = new Int64();
            Int32 len = 6;
            int res = SendARP(ldest, 0, ref macinfo, ref len);
            string mac_src = macinfo.ToString("X");
            while (mac_src.Length < 12) mac_src = mac_src.Insert(0, "0");
            string mac_dest = "";
            for (int i = 0; i < 11; i++)
            {
                if (0 == (i % 2))
                {
                    if (i == 10) mac_dest = mac_dest.Insert(0, mac_src.Substring(i, 2));
                    else mac_dest = "-" + mac_dest.Insert(0, mac_src.Substring(i, 2));
                }
            }
            return mac_dest;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace stwh_Common
{
    public partial class FileHelper
    {
        /// <summary>
        /// 用于将错误信息输出到txt文件
        /// </summary>
        /// <param name="errorMessage">错误详细信息</param>
        public static void WriteError(string errorMessage)
        {
            try
            {
                //文件夹目录路径
                string direpath = System.Web.HttpContext.Current.Server.MapPath("~/Error/");
                string filename = DateTime.Today.ToString("yyyyMMdd") + ".txt";
                if (!Directory.Exists(direpath))
                {
                    Directory.CreateDirectory(direpath);
                    File.Create(direpath + filename).Close();
                }
                using (StreamWriter w = File.AppendText(direpath + filename))
                {
                    w.WriteLine("\r\nLog Entry : ");
                    w.WriteLine("{0}", DateTime.Now.ToString());
                    w.WriteLine(errorMessage);
                    w.WriteLine("________________________________________________________");
                    w.Flush();
                    w.Close();
                }
            }
            catch (Exception)
            {
                
            }
        }
    }
}

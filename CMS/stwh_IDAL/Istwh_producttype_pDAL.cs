﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using stwh_Model;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_producttype_p接口层
    /// </summary>
    public interface Istwh_producttype_pDAL : IBaseDAL
    {
        /// <summary>
        /// 获得数据列表
        /// </summary>
        DataSet GetList(string strWhere, int flag);

        /// <summary>
        /// 得到一个对象实体
        /// </summary>
        stwh_producttype_p GetModel(int stwh_ptpid, int flag);
    }
}

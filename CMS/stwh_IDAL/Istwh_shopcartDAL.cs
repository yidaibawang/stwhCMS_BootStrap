﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using stwh_Model;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_shopcart接口层
    /// </summary>
    public interface Istwh_shopcartDAL : IBaseDAL
    {
        /// <summary>
        /// 更新购物车
        /// </summary>
        /// <param name="model">数据对象</param>
        /// <param name="controller">操作类型（0添加产品数量，1修改产品数量）</param>
        /// <returns></returns>
        int Add(stwh_shopcart jbmodel, int controller);
    }
}

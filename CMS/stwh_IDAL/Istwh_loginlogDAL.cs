﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_loginlog接口层
    /// </summary>
    public interface Istwh_loginlogDAL : IBaseDAL
    {
        /// <summary>
        /// 删除全部数据
        /// </summary>
        bool DeleteListALL();
    }
}

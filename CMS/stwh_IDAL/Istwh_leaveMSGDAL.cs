﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_leaveMSG接口层
    /// </summary>
    public interface Istwh_leaveMSGDAL : IBaseDAL
    {
        /// <summary>
        /// 删除全部数据
        /// </summary>
        bool DeleteListALL();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace stwh_IDAL
{
    /// <summary>
    /// stwh_function接口层
    /// </summary>
    public interface Istwh_functionDAL : IBaseDAL
    {
        /// <summary>
        /// 获得数据列表
        /// </summary>
        DataSet GetList(string strWhere, int flag);
    }
}

﻿using System;
using System.Data;
using System.Collections.Generic;
using stwh_Common;
using stwh_Model;
using stwh_IDAL;
using stwh_DALFactory;

namespace stwh_BLL
{
	/// <summary>
	/// stwh_order
	/// </summary>
	public partial class stwh_orderBLL
	{
        private readonly Istwh_orderDAL dal = DataAccess.CreateIDAL("stwh_orderDAL") as Istwh_orderDAL;
		public stwh_orderBLL()
		{}
		#region  BasicMethod
        /// <summary>
        /// 分页获取用户消息
        /// </summary>
        /// <typeparam name="T">指代类型</typeparam>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public List<T> GetListByPage<T>(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount) where T : class
        {
            DataSet ds = dal.GetListByPage(FieldColumn, FieldOrder, If, pageSize, pageNumber, ref selectCount, ref d_peopleCount);
            return IListDataSet.DataSetToIList<T>(ds, 0) as List<T>;
        }

        /// <summary>
        /// 分页获取用户消息
        /// </summary>
        /// <typeparam name="T">指代类型</typeparam>
        /// <param name="FieldColumn">排序的列名</param>
        /// <param name="FieldOrder">降序排列还是升序排列</param>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示的条数</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="selectCount">查询的总记录条数</param>
        /// <param name="d_peopleCount">总记录条数（不带条件）</param>
        /// <returns></returns>
        public List<T> GetListByPage<T>(string FieldColumn, string FieldOrder, string If, int pageSize, int pageNumber, ref int selectCount, ref int d_peopleCount, int flag) where T : class
        {
            DataSet ds = dal.GetListByPage(FieldColumn, FieldOrder, If, pageSize, pageNumber, ref selectCount, ref d_peopleCount, flag);
            return IListDataSet.DataSetToIList<T>(ds, 0) as List<T>;
        }

        /// <summary>
        /// 分页获取会员订单信息和订单详细信息
        /// </summary>
        /// <param name="If">查询条件</param>
        /// <param name="pageSize">每页显示多少条</param>
        /// <param name="pageNumber">页码</param>
        /// <param name="orderdetailslist">订单详细数据（输出参数）</param>
        /// <returns></returns>
        public List<stwh_order> GetOrder(string If, int pageSize, int pageNumber,ref List<stwh_orderdetails> orderdetailslist)
        {
            DataSet ds = dal.GetOrder(If, pageSize, pageNumber);
            orderdetailslist = IListDataSet.DataSetToIList<stwh_orderdetails>(ds, 1) as List<stwh_orderdetails>;
            return DataTableToList(ds.Tables[0]);
        }

		/// <summary>
		/// 得到最大ID
		/// </summary>
		public int GetMaxId()
		{
			return dal.GetMaxId();
		}

		/// <summary>
		/// 是否存在该记录
		/// </summary>
		public bool Exists(int stwh_orid)
		{
			return dal.Exists(stwh_orid);
		}

        /// <summary>
        /// 生成订单（更新订单状态）
        /// </summary>
		public int  Add(stwh_Model.stwh_order model)
		{
			return dal.Add(model);
		}

        /// <summary>
        /// 获取会员最新订单商品总价
        /// </summary>
        /// <param name="stwh_buid">会员id</param>
        /// <returns></returns>
        public decimal GetOrderTotalPrice(int stwh_buid, ref string stwh_orddid)
        {
            return dal.GetOrderTotalPrice(stwh_buid,ref stwh_orddid);
        }

        /// <summary>
        /// 根据订单编号获取会员订单商品总价
        /// </summary>
        /// <param name="stwh_orddid">订单编号</param>
        /// <returns></returns>
        public decimal GetOrderTotalPrice(string stwh_orddid)
        {
            return dal.GetOrderTotalPrice(stwh_orddid);
        }

		/// <summary>
		/// 更新一条数据
		/// </summary>
		public bool Update(stwh_Model.stwh_order model)
		{
			return dal.Update(model);
		}

        /// <summary>
        /// 更新订单支付状态
        /// </summary>
        /// <param name="stwh_orddid">订单编号</param>
        /// <param name="stwh_orstatus">订单状态</param>
        /// <returns></returns>
        public bool Update(string stwh_orddid, int stwh_orstatus)
        {
            return dal.Update(stwh_orddid,stwh_orstatus);
        }

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool Delete(int stwh_orid)
		{
			return dal.Delete(stwh_orid);
		}

        /// <summary>
        /// 根据订单单号删除订单
        /// </summary>
        /// <param name="stwh_orddid">订单编号</param>
        /// <returns></returns>
        public bool Delete(string stwh_orddid)
        {
            return dal.Delete(stwh_orddid);
        }

		/// <summary>
		/// 删除一条数据
		/// </summary>
		public bool DeleteList(string stwh_oridlist )
		{
			return dal.DeleteList(stwh_oridlist );
		}

		/// <summary>
		/// 得到一个对象实体
		/// </summary>
		public stwh_Model.stwh_order GetModel(int stwh_orid)
		{
            return dal.GetModel(stwh_orid) as stwh_order;
		}

		/// <summary>
		/// 得到一个对象实体，从缓存中
		/// </summary>
		public stwh_Model.stwh_order GetModelByCache(int stwh_orid)
		{
			string CacheKey = "stwh_orderModel-" + stwh_orid;
			object objModel = stwh_Common.DataCache.GetMCache(CacheKey);
			if (objModel == null)
			{
				try
				{
					objModel = dal.GetModel(stwh_orid);
                    if (objModel != null) stwh_Common.DataCache.AddMCache(CacheKey, objModel);
				}
				catch{}
			}
			return (stwh_Model.stwh_order)objModel;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
		{
			return dal.GetList(strWhere);
		}

		/// <summary>
		/// 获得前几行数据
		/// </summary>
		public DataSet GetList(int Top,string strWhere,string filedOrder)
		{
			return dal.GetList(Top,strWhere,filedOrder);
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<stwh_Model.stwh_order> GetModelList(string strWhere)
		{
			DataSet ds = dal.GetList(strWhere);
			return DataTableToList(ds.Tables[0]);
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public List<stwh_Model.stwh_order> DataTableToList(DataTable dt)
		{
			List<stwh_Model.stwh_order> modelList = new List<stwh_Model.stwh_order>();
			int rowsCount = dt.Rows.Count;
			if (rowsCount > 0)
			{
				stwh_Model.stwh_order model;
				for (int n = 0; n < rowsCount; n++)
				{
                    model = dal.DataRowToModel(dt.Rows[n]) as stwh_order;
                    if (model != null) modelList.Add(model);
				}
			}
			return modelList;
		}

		/// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetAllList()
		{
			return GetList("");
		}

		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public int GetRecordCount(string strWhere)
		{
			return dal.GetRecordCount(strWhere);
		}
		/// <summary>
		/// 分页获取数据列表
		/// </summary>
		public DataSet GetListByPage(string strWhere, string orderby, int startIndex, int endIndex)
		{
			return dal.GetListByPage( strWhere,  orderby,  startIndex,  endIndex);
		}
		#endregion  BasicMethod
	}
}

